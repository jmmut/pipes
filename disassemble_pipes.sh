
if [ $# -ne 1 ]
then
	echo "this script takes 1 argument: the path to the compiled pipes program"
	exit 1
fi

tail -c +21 $1 > $1.bin
objdump -D --target=binary -M x86-64 -m i386 $1.bin > $1.s
echo "disassembled into $1.s"
