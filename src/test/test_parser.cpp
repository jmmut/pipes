#include <iostream>

#include "parser/Parser.h"
#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "expression/ExpressionString.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::test::Test;
using randomize::test::assert_equals;
using randomize::test::assert_throws;

#define ASSERT_EQUALS_OR_THROW_DEBUG(result, expected) \
    ASSERT_EQUALS_OR_THROW_SS_MSG(result, expected, result.debugEquals(expected));

void assert_equals_debug(const auto &actual, const auto &expected,
                         source_location location = source_location::current()) {
    assert_equals(actual, expected, actual.debugEquals(expected), location);
}

static auto TEST_IDENTIFIERS = Identifiers{
        {"f", ofConstant(3)},
        {"a", ofConstant(3)},
        {"b", ofConstant(3)},
        {"c", ofConstant(3)},
        {"d", ofConstant(3)}};

auto ofIntIdentifier(std::string name) {
    return ofIdentifier(name, IntType::make_ptr(""));
}

int main(int argc, char **argv) {
    randomize::log::log_level = randomize::log::parseLogLevel(argc == 2? argv[1] : "info");
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"basic parsing", {
            []() {
                auto result = tokenizeAndParse("3");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofConstant(3));
            },
            []() {
                auto result = tokenizeAndParse("f", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofIntIdentifier("f"));
            },
            []() {
                auto result = tokenizeAndParse("function a {a}");
                assert_equals_debug(result, ofLambda(-1, "a", 1, ofParameter("a", 0)));
            },
            []() {
                auto result = tokenizeAndParse("function a {b}", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "a", 1, ofIntIdentifier("b")));
            },
            []() {
                auto result = tokenizeAndParse("function {b}", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "", 1, ofIntIdentifier("b")));
            },
            []() {
                auto result = tokenizeAndParse("function() {b}", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "", 1, ofIntIdentifier("b")));
            },
            []() {
                auto result = tokenizeAndParse("a | b", TEST_IDENTIFIERS);
                auto expected = ofCall(ofIntIdentifier("a"), ofIntIdentifier("b"), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("3 - 2");
                auto expected = ofBinaryOperation(Token::MINUS, ofConstant(3), ofConstant(2), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("3 - - 2");
                auto expected = ofBinaryOperation(Token::MINUS, ofConstant(3), ofConstant(-2), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("- 3 - 2");
                auto expected = ofBinaryOperation(Token::MINUS, ofConstant(-3), ofConstant(2), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                ASSERT_THROWS(tokenizeAndParse("3 - - - 2"););
            },
            []() {
                ASSERT_THROWS(tokenizeAndParse("3 - - - - 2"););
            },
            []() {
                auto result = tokenizeAndParse("3 |* 2");
                auto expected = ofBinaryOperation(Token::MULTIPLY, ofConstant(3), ofConstant(2), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("3 |/ 2");
                auto expected = ofBinaryOperation(Token::DIVIDE, ofConstant(3), ofConstant(2), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("3 > 2");
                auto expected = ofBinaryOperation(Token::GREATER_THAN, ofConstant(3), ofConstant(2), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }
    }}.countFailed();

    failed += TestSuite{"combination of lambdas and calls", {
            []() {
                auto result = tokenizeAndParse("a | function b {c}", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofCall(ofIntIdentifier("a"),
                                                             ofLambda(-1, "b", 1,
                                                                       ofIntIdentifier("c")), {}));
            },
            []() {
                auto result = tokenizeAndParse("function a {b | c}", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "a", 1,
                                                               ofCall(ofIntIdentifier("b"),
                                                                       ofIntIdentifier("c"), {})));
            },
            []() {
                auto result = tokenizeAndParse("function a {b} | c", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofCall(ofLambda(-1, "a", 1,
                                                                       ofIntIdentifier("b")),
                                                             ofIntIdentifier("c"), {}));
            },
            []() {
                try {
                    auto result = tokenizeAndParse("function (a | b) {c}");
                    ASSERT_OR_THROW_MSG(false, "should have thrown an exception");
                } catch (std::exception &e) {
                    // ok
                }
            },
            []() {
                try {
                    auto result = tokenizeAndParse("function a | b {c}");
                    ASSERT_OR_THROW_MSG(false, "should have thrown an exception");
                } catch (std::exception &e) {
                    // ok
                }
            },
            []() {
                auto result = tokenizeAndParse("function a {b |function c {d}}", TEST_IDENTIFIERS);
                auto expected = ofLambda(-1, "a", 1, ofCall(ofIntIdentifier("b"),
                                                              ofLambda(-1, "c", 2,
                                                                        ofIntIdentifier("d")), {}));
                ASSERT_EQUALS_OR_THROW_SS_MSG(result, expected, result.debugEquals(expected));
            }
    }}.countFailed();

    failed += TestSuite{"several calls or lambdas", {
            []() {
                auto result = tokenizeAndParse("a | b | c", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofCall(ofCall(ofIntIdentifier("a"),
                                                                     ofIntIdentifier("b"), {}),
                                                             ofIntIdentifier("c"), {}));
            },
            []() {
                auto result = tokenizeAndParse("function a {function b {c}}", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "a", 1,
                                                               ofLambda(-1, "b", 2,
                                                                         ofIntIdentifier("c"))));
            }
    }}.countFailed();

    failed += TestSuite{"parenthesis", {
            []() {
                auto result = tokenizeAndParse("(3)");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofConstant(3));
            },
            []() {
                auto result = tokenizeAndParse("(f)", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofIntIdentifier("f"));
            },
            []() {
                auto result = tokenizeAndParse("(function a {a})");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "a", 1, ofParameter("a", 0)));
            },
            []() {
                auto result = tokenizeAndParse("(function a {b})", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "a", 1, ofIntIdentifier("b")));
            },
            []() {
                auto result = tokenizeAndParse("(function a {3})");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "a", 1, ofConstant(3)));
            },
            []() {
                auto result = tokenizeAndParse("(function() {3})");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, ofLambda(-1, "", 1, ofConstant(3)));
            },
            []() {
                auto result = tokenizeAndParse("(a | b)", TEST_IDENTIFIERS);
                auto expected = ofCall(ofIntIdentifier("a"), ofIntIdentifier("b"), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("(function x {x}) | f", TEST_IDENTIFIERS);
                auto expected = ofCall(ofLambda(-1, "x", 1, ofParameter("x", 0)),
                                        ofIntIdentifier("f"), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }
    }}.countFailed();


    failed += TestSuite{"lambda levels", {
            []() {
                auto result = tokenizeAndParse(
                        "function a {function b {a}} | function f {function a {function b {a|(b|f)}}}");  // KI = C K
//                auto result = parse("(a->b->a)|f->a->b->a|(b|f)");  // KI = C K
                auto K = ofLambda(-1, "a", 1, ofLambda(-1, "b", 2, ofParameter("a", 1)));
                auto CBody = ofCall(ofParameter("a", 1), ofCall(ofParameter("b", 0), ofParameter
                                     ("f", 2), {}), {});
                auto C = ofLambda(-1, "f", 1,
                                  ofLambda(-1, "a", 2, ofLambda(-1, "b", 3, std::move(CBody))));
                auto expected = ofCall(std::move(K), std::move(C), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("function a {a@0}");
                auto expected = tokenizeAndParse("function a {a}");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                try {
                    tokenizeAndParse("function a {a@0@3}");
                    ASSERT_OR_THROW_MSG(false, "should have thrown an exception");
                } catch (std::exception &e) {
                    ; // ok, exception thrown as expected
                }
            }
    }}.countFailed();

    failed += TestSuite{"name overwriting?", {
            []() {
                auto result = tokenizeAndParse("function p {function p {p@0} |function second {7 |p}}");
                auto expected = tokenizeAndParse("function p {function p {p@0} |function second {7 |p@1}}");
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }
    }}.countFailed();

    failed += TestSuite{"binary operations (semicolon, types, etc)", {
            []() {
                auto result = tokenizeAndParse("3;5");
                auto expected = ofBinaryOperation(Token::STATEMENT_SEPARATOR,
                                                  ofConstant(3), ofConstant(5), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }, []() {
                ASSERT_THROWS(tokenizeAndParse("3 :function x :int64"););
            }, []() {
                ASSERT_THROWS(tokenizeAndParse("(1,) :iterable()"););
            }, []() {
                ASSERT_THROWS(tokenizeAndParse("(3) :iterable(number :int64)"););
            }, []() {
                auto result = tokenizeAndParse("[4] ||function x {x}");
                auto expected = ofBinaryOperation(Token::LOOP_CALL,
                                                  ofArray({ofConstant(4)}, {}),
                                                  ofLambda(-1, "x", 1, ofParameter("x")),
                                                  {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }, []() {
                auto result = tokenizeAndParse("[4] =||function x {x}");
                auto expected = ofBinaryOperation(Token::MUTABLE_LOOP_CALL,
                                                  ofArray({ofConstant(4)}, {}),
                                                  ofLambda(-1, "x", 1, ofParameter("x")),
                                                  {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }, []() {
                ASSERT_THROWS(tokenizeAndParse("a =4"););
            }, []() {
                ASSERT_THROWS(tokenizeAndParse("a +3 =a"););
            }, []() {
                ASSERT_THROWS(tokenizeAndParse("a = a+3"););
            }, []() {
                auto result = tokenizeAndParse("4 =a");
                auto expected = ofBinaryOperation(Token::DEFINITION,
                                                  ofConstant(4),
                                                  ofVariable("a", UnknownType::make_ptr("")),
                                                  {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }, []() {
                auto result = tokenizeAndParse("4 =a; a");
                auto definition = ofBinaryOperation(Token::DEFINITION,
                                                    ofConstant(4),
                                                    ofVariable("a", UnknownType::make_ptr("")),
                                                    {});
                auto expected = ofBinaryOperation(Token::STATEMENT_SEPARATOR,
                                                  std::move(definition),
                                                  ofVariable("a", UnknownType::make_ptr("")),
                                                  {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }, []() {
//                auto result =
                ASSERT_THROWS_MSG(tokenizeAndParse("3 |function {1 =a} =a");,
                                  "variable shadowing is not supported yet");
//                auto body = ofBinaryOperation(Token::DEFINITION,
//                                              ofConstant(1),
//                                              ofVariable("a", UnknownType::make_ptr("")),
//                                              {});
//                auto func = ofLambda(-1, "", 1, std::move(body));
//                auto call = ofCall(ofConstant(3), std::move(func), {});
//                auto expected = ofBinaryOperation(Token::DEFINITION,
//                                              std::move(call),
//                                              ofVariable("a", UnknownType::make_ptr("")),
//                                              {});
//                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }
    }}.countFailed();


    failed += TestSuite{"branches and whiles", {Test{[](){
        auto result = tokenizeAndParse("while(1) {5}");
        auto expected = ofWhile(ofConstant(1), ofConstant(5), {});
        assert_equals(result, expected);
    }}}}.countFailed();


    failed += TestSuite{"chars", {
            []() {
                char letter = 'a';
                auto result = tokenizeAndParse(std::string{"'"} + letter + "'");
                auto expected = ofConstant(letter);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }
    }}.countFailed();

    failed += TestSuite{"call syntax sugar", {
            []() {
                auto result = tokenizeAndParse("a f c", TEST_IDENTIFIERS);
                auto expected = tokenizeAndParse("a |(c |f)", TEST_IDENTIFIERS);
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            },
            []() {
                auto result = tokenizeAndParse("f(a)", TEST_IDENTIFIERS);
                auto expected = ofCall(ofIntIdentifier("a"), ofIntIdentifier("f"), {});
                ASSERT_EQUALS_OR_THROW_DEBUG(result, expected);
            }
    }}.countFailed();

    failed += TestSuite{"arrays", {[]() {
        auto parsed = tokenizeAndParse("[]");
        auto expected = ofArray({}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("[1]");
        auto expected = ofArray({ofConstant(1)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("[1, 2]");
        auto expected = ofArray({ofConstant(1), ofConstant(2)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("[1,]");
        auto expected = ofArray({ofConstant(1)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("[1 + 3, 2]");
        auto expected = ofArray(
                Expressions{ofBinaryOperation(Token::PLUS, ofConstant(1), ofConstant(3), {}),
                            ofConstant(2)},
                {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("[function c {c}, 3]");
        auto expected = ofArray({ofLambda(-1, "c", 1, ofParameter("c", 0)), ofConstant(3)}, {});
        // this works because it's just parsing, not type checking. Type check rejects this.
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("\"abc\"");
        auto expected = ofArray({ofConstant('a'), ofConstant('b'), ofConstant('c')}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("[1] :array(n :int64)");
        auto expected = ofArray({ofConstant(1)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }}}.countFailed();


    failed += TestSuite{"tuples", {[]() {
        ASSERT_THROWS(tokenizeAndParse("()"););
    }, []() {
        auto parsed = tokenizeAndParse("(3)");
        auto expected = ofConstant(3);
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("(3,)");
        auto expected = ofTuple({ofConstant(3)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        ASSERT_THROWS(tokenizeAndParse("(,)"););
    }, []() {
        auto parsed = tokenizeAndParse("(3, 5)");
        auto expected = ofTuple({ofConstant(3), ofConstant(5)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("(function c {c}, 3)");
        auto expected = ofTuple({ofLambda(-1, "c", 1, ofParameter("c", 0)), ofConstant(3)}, {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto parsed = tokenizeAndParse("(2, 3) |function t { t }");
        auto expected = ofCall(ofTuple({ofConstant(2), ofConstant(3)}, {}),
                               ofLambda(-1, "t", 1, ofParameter("t", 0)), {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        ASSERT_THROWS(tokenizeAndParse("(2 :int64,) :iterable(f :function x)"););
    }}}.countFailed();

    failed += TestSuite{"structs", {Test{[]() {
        assert_throws([]() {tokenizeAndParse("struct ()");});
    }}, Test{[]() {
        assert_throws([]() {tokenizeAndParse("struct (,)");});
    }}, Test{[]() {
        auto parsed = tokenizeAndParse("struct(my_member :int64)");
        auto expected = ofStruct({ofField("my_member")});
        assert_equals_debug(parsed, expected);
    }}, Test{[]() {
        auto parsed = tokenizeAndParse("struct(my_member :int64,)");
        auto expected = ofStruct({ofField("my_member")});
        assert_equals_debug(parsed, expected);
    }}, Test{[]() {
        auto parsed = tokenizeAndParse("struct(x :int64, y :int64)");
        auto expected = ofStruct({ofField("x"), ofField("y")});
        assert_equals_debug(parsed, expected);
    }}, Test{[]() {
        auto parsed = tokenizeAndParse("struct(x :int64, y :int64,)");
        auto expected = ofStruct({ofField("x"), ofField("y")});
        randomize::test::assert_equals(parsed, expected);
    }}, Test{[]() {
        assert_throws([]() {tokenizeAndParse("struct (,a :int64)");});
    }}, Test{[]() {
        assert_throws([]() {tokenizeAndParse("struct (a :int64,,)");});
    }}, Test{[]() {
        auto identifiers = Identifiers{{"MyType", ofStruct({ofField("p", IntType::make_ptr(""))})}};
        auto parsed = tokenizeAndParse("(5,) :MyType", identifiers);
        auto expected = ofTuple({ofConstant(5)}, {});
        assert_equals_debug(parsed, expected);
    }}, Test{[]() {
        auto parsed = tokenizeAndParse("(5,) :struct(p :int64)");
        auto expected = ofTuple({ofConstant(5)}, {});
        assert_equals_debug(parsed, expected);
    }}}}.countFailed();

    failed += TestSuite{"struct access", {[]() {
        auto parsed = tokenizeAndParse("(5,) :struct(p :int64) |function s { s.p }");
        auto expected = ofCall(ofTuple({ofConstant(5)}, {}),
                               ofLambda(-1, "s", 1,
                                        ofBinaryOperation(
                                                Token::FIELD_ACCESS,
                                                ofParameter("s", 0),
                                                ofField("p"),
                                                {})), {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
//        ASSERT_THROWS(
            auto parsed = tokenizeAndParse("(5,) :struct(p :int64) .p");
//        );
        auto expected = ofBinaryOperation(Token::FIELD_ACCESS,
                                          ofTuple({ofConstant(5)}, {}),
                                          ofField("p"),
                                          {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto identifiers = Identifiers{{"my_var", tokenizeAndParse("(5,) :struct(p :int64)")}};
        auto parsed = tokenizeAndParse("my_var.p", identifiers);
        auto expected = ofBinaryOperation(Token::FIELD_ACCESS,
                                          ofIdentifier("my_var", UnknownType::make_ptr("")),
                                          ofField("p"),
                                          {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto identifiers = Identifiers{{"my_var", tokenizeAndParse("(5,) :struct(p :int64)")}};
//        ASSERT_THROWS(
            auto parsed = tokenizeAndParse("my_var .p", identifiers);
//        );
        auto expected = ofBinaryOperation(Token::FIELD_ACCESS,
                                          ofIdentifier("my_var", UnknownType::make_ptr("")),
                                          ofField("p"),
                                          {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto identifiers = Identifiers{{"my_var", tokenizeAndParse("(5,) :struct(p :int64)")}};
        ASSERT_THROWS(
            auto parsed = tokenizeAndParse("my_var. p", identifiers);
        );
//        auto expected = ofBinaryOperation(Token::FIELD_ACCESS,
//                                          ofIdentifier("my_var", UnknownType::make_ptr()),
//                                          ofIdentifier("p", UnknownType::make_ptr()),
//                                          {});
//        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto identifiers = Identifiers{{"my_var", tokenizeAndParse("(5,) :struct(f1 :int64)")}};
        auto parsed = tokenizeAndParse("my_var.f1.f2", identifiers);
        auto expected = ofBinaryOperation(
                Token::FIELD_ACCESS,
                ofBinaryOperation(
                        Token::FIELD_ACCESS,
                        ofIdentifier("my_var", UnknownType::make_ptr("")),
                        ofField("f1"),
                        {}),
                ofField("f2"),
                {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }, []() {
        auto identifiers = Identifiers{{"Coords", tokenizeAndParse("struct(x :int64)")}};
        auto parsed = tokenizeAndParse("(5,) :Coords |function c {c.x}", identifiers);
        auto expected = ofCall(
                ofTuple({ofConstant(5)}, {}),
                ofLambda(-1, "c", 1,
                         ofBinaryOperation(Token::FIELD_ACCESS,
                                           ofParameter("c", 0),
                                           ofField("x"),
                                           {})), {});
        ASSERT_EQUALS_OR_THROW_DEBUG(parsed, expected);
    }}}.countFailed();


    failed += TestSuite{"typeof", {Test{[]() {
        auto expression = tokenizeAndParse("function x :int64 -> result :typeof(x) {x}");
        auto expectedReturnType = TypeofType{ofIntIdentifier("x"), ""};
        assert_equals(expression.pipesType()->getReturnedType(), expectedReturnType);
    }}, Test{[]() {
        // find = function container :iterable(element :typeof(to_find)) -> to_find :int64
    }}}}.countFailed();


    return failed > 0;
}
