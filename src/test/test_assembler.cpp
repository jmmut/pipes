#include <iostream>

#include "assembler/Assembler.h"
#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "assembler/PipesAssembler.h"
#include "typing/Program.h"
#include "assembler/VariableScopeTracker.h"
#include "expression/ExpressionType.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::test::Test;
using randomize::test::assert_equals;

int main(int argc, char **argv) {
    randomize::log::log_level = randomize::log::parseLogLevel(argc == 2? argv[1] : "error");
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"basic assembler", {
            Test{[]() {
                Assembler assembler{1024};

                auto func = compile_return_42(assembler);

                assembler.printMachineCode();

                auto x = (*func)();
                assert_equals(x, 42);
            }},
            Test{[]() {
                Assembler assembler{1024};
                auto func = compile_fibonacci(assembler);
                assembler.printMachineCode();
                int32_t checkpoint = 7;
                auto x = (*func)(checkpoint);
                assert_equals(x, 13);
            }},
            Test{[]() {
                Assembler assembler{1024};
                assembler.emit_mov_imm64_to_reg(Register64::RAX, 17);
                assembler.emit_mov_imm64_to_reg(Register64::RDX, 0);
                assembler.emit_mov_imm64_to_reg(Register64::RDI, 5);
                assembler.emit_div_RDX_RAX_into_RAX_RDX(Register64::RDI);
                assembler.emit_return();
                auto func = assembler.getEntryPoint<int()>();
                auto x = (*func)();
                assert_equals(x, 3);
            }},
            Test{[]() {
                Assembler assembler{1024};
                auto &m = malloc;
                auto ptr = malloc(8);
                free(ptr);

                assembler.emit_mov_imm64_to_reg(Register64::RAX, reinterpret_cast<int64_t>(m));
                assembler.emit_mov_imm64_to_reg(Register64::RDI, 8);
                assembler.emit_call(Register64::RAX);

                assembler.emit_mov_imm64_to_reg(Register64::RDI, 35);
                assembler.emit_mov_reg_to_mem(Register64::RDI, 0, Register64::RAX);
                auto &f = free;
                assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RDI);
                assembler.emit_mov_imm64_to_reg(Register64::RAX, reinterpret_cast<int64_t>(f));
                assembler.emit_call(Register64::RAX);
                assembler.emit_mov_imm64_to_reg(Register64::RAX, 35);
                assembler.emit_return();
                auto func = assembler.getEntryPoint<int()>();
                auto x = (*func)();
                assert_equals(x, 35);
            }}
    }}.countFailed();

    failed += TestSuite{"basic pipes assembler", {
        Test{[]() {
            auto assembler = PipesAssembler("5");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 5);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 |decrement");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 4);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 |function a {a}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 5);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 5);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |function a {a}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 5);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |decrement");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 4);
        }}
    }}.countFailed();

    failed += TestSuite{"binary operators", {
        Test{[]() {
            auto assembler = PipesAssembler("3 + 5 + argument");
            auto func = assembler.getEntryPoint();
            auto result = func(1);
            assert_equals(result, 9);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument - 1 - argument");
            auto func = assembler.getEntryPoint();
            auto result = func(3);
            assert_equals(result, -1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |* 5");
            auto func = assembler.getEntryPoint();
            auto result = func(3);
            assert_equals(result, 15);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |/ 5");
            auto func = assembler.getEntryPoint();
            auto result = func(17);
            assert_equals(result, 3);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-13 |/ 4");
            auto func = assembler.getEntryPoint();
            auto result = func(17);
            assert_equals(result, -3);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-4 |/ 3");
            auto func = assembler.getEntryPoint();
            auto result = func(17);
            assert_equals(result, -1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 |%3");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 2);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-5 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-10 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 0);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-9 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-3 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 7);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-2 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 8);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("-1 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 9);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("0 |% 10");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 0);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("3;5");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 5);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 > 3");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("3 > 5");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 0);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("3 + 2 > 4");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("4 >= 4");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("4 <= 4");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 1);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("4 < 4");
            auto func = assembler.getEntryPoint();
            auto result = func(0);
            assert_equals(result, 0);
        }}
    }}.countFailed();

    failed += TestSuite{"nested code", {
        Test{[]() {
            auto assembler = PipesAssembler("argument |function a {a} |decrement");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 4);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 |function a {a} |decrement");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 4);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 |function a {a} |function a {a} |function a {a}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 5);
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |(function a {a} | function x {x})");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 5, "function called with a function");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |(decrement | function x {x})");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 4, "function called with a function");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("function a {a} | function f {argument |f}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            assert_equals(result, 5, "putting argument in any place");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 | function a {a |decrement}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 4, "injecting values");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("5 | function a {a |function x {x}}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 5, "injecting values");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("function p {p}|function f {"
                                            "  function p {p}|function g {"
                                            "    function p {p}|function h {"
                                            "        7 |f }}}");
            // f010: f09f, 0   call   f020: f0f8, 0
            // f030: f114, 0   call   f040: f16d, 1
            // f058: f189, 0   call   f068: f1e2, 1
            auto func = assembler.getEntryPoint();
            auto result = (*func)(2);
            assert_equals(result, 7, "intermediate useless lambdas with name collision");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("7 |function arg {2|function arg2 {5 | function x {x}"
                                            " |function x {arg}}}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 7, "maintain rsi during the whole function");
        }}
    }}.countFailed();

    failed += TestSuite{"closures", {
        Test{[]() {
            auto assembler = PipesAssembler("argument | function x {decrement | function f {x|f}}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(5);
            // dec 70be 0   2nd
            // 11  714e 1   3rd
            // 10  71b8 0   1st
            assert_equals(result, 4, "non-trivial function called with a function");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument | (5 | function a {function b {a}})");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 5, "returning a function with a closure");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |(decrement |function f {function a {a |f |f}})");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 6, "should capture the same parameter several times");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("decrement |(decrement |(4 |(9 |("
                                            "function a {function x {function b {a}}}"
                                            "|function f {function a9 {function a4 {"
                                            "    a9 |f |(a4 |f |pipes_programs/lambda_calc/false)}}}))))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 9, "should capture across several lambda levels");
        }}
    }}.countFailed();

    failed += TestSuite{"combinators", {
        Test{[]() {
            auto assembler = PipesAssembler("argument |(decrement |("
                                            "function g {function b {b|g}}"
                                            "))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 7, "capture both argument and function, and call them");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("decrement |(argument |("
                                            "function g {function b {g|b}}"
                                            "))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 7, "capture both argument and function, and call them");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |(decrement |(decrement |("
                                            "function f {function g {function b {b|g|f}}}"
                                            ")))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 6, "bluebird with parameters");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |(decrement |(function x {x} |(decrement |("
                                            "function f {function g {function a {function b {b|(a|g)|f}}}}"
                                            "))))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 6, "evaluated blackbird");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("argument |( decrement |( function x {x} |( decrement"
                                            "  |(function f {function g {function a {a|g|f}}}"
                                            "    |(function f {function g {function a {a|g|f}}}"
                                            "      |function f {function g {function a {a|g|f}}})))))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(8);
            assert_equals(result, 6, "blackbird");
        }}
    }}.countFailed();

    failed += TestSuite{"lambdas that need several closures", {
        Test{[]() {
            auto assembler = PipesAssembler("9 |( decrement |("
                                            "function f {function a {a |f |f}}"
                                            "|function g {function b {b |g |g |g}}"
                                            "))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 1, "9 decremented pow(2, 3) times");
        }}
    }}.countFailed();

    failed += TestSuite{"identifier", {
            Test{[]() {
                auto identifiers = Identifiers{{"inc",
                                                tokenizeAndParse("function x { x |increment }")}};
                auto text = "4 |inc";
                auto expression = tokenizeAndParse(text, identifiers);
                auto assembler = PipesAssembler{Program{CodeFile{text, "", ""},
                                                        expression, identifiers}};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 5);
            }},
            Test{[]() {
                auto identifiers = Identifiers{{"plus_3", tokenizeAndParse(
                        "(\n"
                        "    3\n"
                        "    |function k {\n"
                        "        function b {\n"
                        "            b + k\n"
                        "        }\n"
                        "    }\n"
                        ")")}};
                auto text = "2 |plus_3";
                try {
                    auto expression = tokenizeAndParse(text, identifiers);
                    auto assembler = PipesAssembler{Program{CodeFile{text, "", ""}, expression, identifiers}};
                    auto func = assembler.getEntryPoint();
                    func(0);
                    assert_equals(true, false, "should have thrown an exception");
                } catch (std::exception &e) {
                    bool isUnimplemented = contains_ignorecase(e.what(), "unimplemented");
                    assert_equals(isUnimplemented, true);
                }
            }},
            Test{[]() {
                auto identifiers = Identifiers{{"decrement_pow_2_3_times", tokenizeAndParse(
                        "function x {x |( decrement |("
                        "  function f {function a {a |f |f}}"
                        "  |function g {function b {b |g |g |g}}"
                        "))}")}};
                auto text = "argument |decrement_pow_2_3_times";
                auto expression = tokenizeAndParse(text, identifiers);
                auto assembler = PipesAssembler{Program{CodeFile{text, "", ""},
                                                        expression, identifiers}};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(9);
                assert_equals(result, 1);
            }},
            Test{[]() {
                auto assembler = PipesAssembler{"5 |new_array |deref"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(9);
                assert_equals(result, 5);
            }},
            Test{[]() {
                auto assembler = PipesAssembler{"90 |new_array ||function x :int64 { 'a' } |deref"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(9);
                assert_equals(result, 90);
            }},
            Test{[]() {
                // for some reason this fails but 'g = function x { x |f }' doesn't
                auto identifiers = Identifiers{{"f", "function x { x + 1 }"},
                                               {"g", "f"}};
                auto assembler = PipesAssembler{tokenizeAndParseAndCheckTypes("3 |g", identifiers)};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 4);
            }},
            Test{[]() {
                // for some reason this fails but 'g = function x { x |f }' doesn't
                auto identifiers = Identifiers{{"sum", "function x { function y {x + y}}"},
                                               {"g", "sum"},
                                               {"with_2", "function z { function a { a z 2}}"}};
                auto assembler = PipesAssembler{tokenizeAndParseAndCheckTypes("4 sum 5 with_2 sum",
                                                identifiers)};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 11);
            }},
            Test{[]() {
                auto assembler = PipesAssembler{
                    "4 |function x {x |print_current_function}"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 4);
                std::cout << std::endl;
            }},
            Test{[]() {
                auto assembler = PipesAssembler{
                    "4 |function x {x |function y {y |print_function_stack}}"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 4);
                std::cout << std::endl;
            }},
            Test{[]() {
                auto identifiers = Identifiers{
                    {"f", "function x {x |function y {y |print_function_stack}}", "f.pipes"}};
                auto assembler = PipesAssembler{tokenizeAndParseAndCheckTypes("4 |f", identifiers)};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 4);
                std::cout << std::endl;
            }},
            Test{[]() {
                auto assembler = PipesAssembler{
                    "4 |function x {x |function y {y |print_call_stack}}"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 4);
                std::cout << std::endl;
            }},
            Test{[]() {
                auto identifiers = Identifiers{
                    {"f", "function x {x |function y {y |print_call_stack}}", "f.pipes"}};
                auto assembler = PipesAssembler{tokenizeAndParseAndCheckTypes("4 |f", identifiers)};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(0);
                assert_equals(result, 4);
                std::cout << std::endl;
            }}
    }}.countFailed();

    failed += TestSuite{"church bools", {
        Test{[]() {
            auto assembler = PipesAssembler("3 |( 7 |pipes_programs/lambda_calc/true)");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 7, "pipes_programs/lambda_calc/true of (7, 3) is 7");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("3 |( 7 |pipes_programs/lambda_calc/false)");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 3, "pipes_programs/lambda_calc/false of (7, 3) is 3");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("3 |(7 |(pipes_programs/lambda_calc/false | function x {x}))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 3, "passing pipes_programs/lambda_calc/true to a function");
        }},
        Test{[]() {
            auto assembler = PipesAssembler("3 |(7 |(pipes_programs/lambda_calc/true | function x {x}))");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 7, "passing pipes_programs/lambda_calc/true to a function");
        }}
    }}.countFailed();

    // TODO: DISABLED
    //failed +=
            TestSuite{"church numerals", {
        Test{[]() {

            auto program = tokenizeAndParseAndCheckTypes(
                    CodeFile{"8 |( decrement |( 0 |lambda_calc/to_church))", "pipes_programs", ""});
            auto assembler = PipesAssembler(program);
            auto func = assembler.getEntryPoint();
            auto result = (*func)(1);
            assert_equals(result, 8, "8 decremented 0 times");
        }},
        Test{[]() {
            auto program = tokenizeAndParseAndCheckTypes(
                    CodeFile{"8 |( decrement |( 3 |lambda_calc/to_church))", "pipes_programs", ""});
            auto assembler = PipesAssembler(program);
            auto func = assembler.getEntryPoint();
            auto result = (*func)(1);
            assert_equals(result, 5, "8 decremented 3 times");
        }}
    }}.countFailed();
    failed += TestSuite{"church structs", {
        Test{[]() {
            auto assembler = PipesAssembler("function a {function b {function f{b |(a |f)}}}"
                                            "|function pair {4 |(7 |pair) |function p {pipes_programs/lambda_calc/true |p}}");
            auto func = assembler.getEntryPoint();
            auto result = (*func)(1);
            assert_equals(result, 7, "first of (7, 4) is 7");
        }}
    }}.countFailed();

    failed += TestSuite{"branch", {
//            Test{[]() {
//                auto assembler = PipesAssembler("1 |branch {5, 4}");
//                auto func = assembler.getEntryPoint();
//                auto result = (*func)(1);
//                assert_equals(result, 5);
//            }},
            Test{[]() {
                auto assembler = PipesAssembler("1 |branch {5} {4}");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 5);
            }},
//            Test{[]() {
//                auto assembler = PipesAssembler(
//                        "function x {branch x, 4 |function choose {1 |choose}} |function f {8 |f}");
//                auto func = assembler.getEntryPoint();
//                auto result = (*func)(1);
//                assert_equals(result, 8);
//            }},
            Test{[]() {
                auto assembler = PipesAssembler{" 0 |branch {3} {5 |increment}"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 6);
            }},
            Test{[]() {
                auto assembler = PipesAssembler{" 0 |branch {3} {5 +8}"};
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 13);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("1 |branch {3} {5} +8");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 11);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("1 |branch {3+argument} {5} +8");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 12);
            }}
    }}.countFailed();


    failed += TestSuite{"while", {Test{[]() {
        auto assembler = PipesAssembler("[0] |function counter {while (10 > (counter #1)) {counter "
                                        "=||increment} ;counter #1}");
        auto func = assembler.getEntryPoint();
        auto result = (*func)(1);
        assert_equals(result, 10);
    }}}}.countFailed();

    failed += TestSuite{"iterables", {
            Test{[]() {
                auto assembler = PipesAssembler("[1, 2]");
                auto func = assembler.getEntryPoint();
//                auto result =
                        (*func)(1);
//                assert_equals(result, 12);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("[1, 2, 3]\n"
                                                "||function x { x+1 }\n"
                                                "||function x { x+3 }\n"
                                                "+ 8 |deref");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 5);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("[1, 2, 3]\n"
                                                "||function x { x+1 }\n"
                                                "|function x { 3; x }\n"
                                                "||function x { x+10 }\n"
                                                "+8 |deref");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 12);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("5 |function x { [1, x+1, 2] } +16 |deref");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 6);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("((1,2), (3,4))"
                                                "|function x :iterable { x +16 |deref :iterable +8 |deref }");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 3);
            }},
            Test{[]() {
                auto code = "[5] |deref1";
                auto identifiers = Identifiers{{"deref1", "deref"}};
                auto program = tokenizeAndParseAndCheckTypes(buildCode(code), identifiers);
//                auto assembler = PipesAssembler(program);
//                auto func = assembler.getEntryPoint();
//                auto result = (*func)(1);
//                assert_equals(result, 1);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("[5] ||function x { (x, 4) } |deref");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 1);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("[5] |function a :array {"
                                                "  a ||function x { 4 }; a +8 |deref}");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 5);
            }},
            Test{[]() {
                auto assembler = PipesAssembler("[5] |function a :array {"
                                                "  a =||function x { 4 }; a +8 |deref}");
                auto func = assembler.getEntryPoint();
                auto result = (*func)(1);
                assert_equals(result, 4);
            }}
    }}.countFailed();

    failed += TestSuite{"structs", {Test{[]() {
        auto assembler = PipesAssembler("(5,) :struct(p :int64) |function s {s.p}");
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}, Test{[]() {
        auto assembler = PipesAssembler("(5,6) :struct(x :int64, y :int64) |function p {p.x}");
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}, Test{[]() {
        auto identifiers = Identifiers{{"MyStruct", tokenizeAndParse("struct(p :int64)")}};
        auto program = tokenizeAndParseAndCheckTypes(buildCode("(5,) :MyStruct |function s {s.p}"),
                                                     identifiers);
        auto assembler = PipesAssembler(program);
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}, Test{[]() {
        auto assembler = PipesAssembler(
                "(function(x) {x+1}, 9) :struct(doThing :function(x), x :int64)"
                "|function mc {5 |mc.doThing}");
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 6);
    }}, Test{[]() {
        auto program = tokenizeAndParseAndCheckTypes(CodeFile{
                "((4, 5) :demos/Coords, (7, 8) :demos/Coords) |function(coordsWrap) {"
                "   coordsWrap array/get 2 .x } -3",
                "pipes_programs/tests/", "virtual_file.pipes"
        });
        auto assembler = PipesAssembler(program);
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 4);
    }}, Test{[]() {
        randomize::test::assert_throws([]() {
            auto program = tokenizeAndParseAndCheckTypes(CodeFile{
                    "((4, 5) :demos/Coords, (7, 8) :demos/Coords)"
                    "|function(coordsWrap :iterable(element) -> result :typeof(element)) {"
                    "   coordsWrap +16 |deref .x } -3",
                    "pipes_programs/tests/", "virtual_file.pipes"
            });
            auto assembler = PipesAssembler(program);
            auto func = assembler.getEntryPoint();
            auto result = (*func)(0);
            assert_equals(result, 4);
        });
    }}, Test{[]() {
        auto program = tokenizeAndParseAndCheckTypes(CodeFile{
                "[5] array/get 1",
                "pipes_programs/tests/", "virtual_file.pipes"
        });
        auto assembler = PipesAssembler(program);
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}, Test{[]() {
        auto program = tokenizeAndParseAndCheckTypes(CodeFile{
                "[4,5,6] array/append 7 +16 |deref",
                "pipes_programs/tests/", "virtual_file.pipes"
        });
        auto assembler = PipesAssembler(program);
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}}}.countFailed();


    // The next tests are not proper tests but helped to bootstrap those syscalls

//    failed += TestSuite2{"syscalls", {{[]() {
//        auto assembler = PipesAssembler("'a' |print_char");
//        auto func = assembler.getEntryPoint();
//        auto result = (*func)(0);
//        assert_equals(result, 'a');
//    }}, Test{[]() {
//        auto assembler = PipesAssembler("0 |read_char");
//        auto func = assembler.getEntryPoint();
//        auto result = (*func)(0);
//        assert_equals(result, 'a');
//    }}}}.countFailed();

    failed += TestSuite{"variables", {Test{[]() {
        auto assembler = PipesAssembler("5 =a");
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}, Test{[]() {
        auto variablesInScope = std::vector<int>{};
        bool variableScopeEnded = false;

        {
            auto tracker = VariableScopeTracker{ExpressionType::BINARY_OPERATION,
                                                variablesInScope,
                                                [&]() {
                variableScopeEnded = true;
            }};
            assert_equals(variablesInScope.size(), 0u);
            assert_equals(variableScopeEnded, false);
        }
        assert_equals(variablesInScope.size(), 0u);
        assert_equals(variableScopeEnded, false);
    }}, Test{[]() {
        auto variablesInScope = std::vector<int>{};
        bool variableScopeEnded = false;

        {
            auto tracker = VariableScopeTracker{ExpressionType::LAMBDA,
                                                variablesInScope,
                                                [&]() {
                variableScopeEnded = true;
            }};
            assert_equals(variablesInScope.size(), 1u);
            assert_equals(variableScopeEnded, false);
        }
        assert_equals(variablesInScope.size(), 0u);
        assert_equals(variableScopeEnded, true);
    }}, Test{[]() {
        auto assembler = PipesAssembler("3 |function {5 =a}");
        auto func = assembler.getEntryPoint();
        auto result = (*func)(0);
        assert_equals(result, 5);
    }}}}.countFailed();


    return failed > 0;
}
