#include <iostream>

#include "assembler/Assembler.h"
#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "assembler/PipesAssembler.h"
#include "typing/Program.h"
#include "assembler/VariableScopeTracker.h"
#include "expression/ExpressionType.h"
#include "language_server/response.h"
#include "language_server/message_buffer.h"
#include "language_server/analyzer.h"
#include "parser/TypeParser.h"

using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::test::Test;
using randomize::test::assert_equals;
using randomize::test::assert_throws;

int main(int argc, char **argv) {
    randomize::log::log_level = randomize::log::parseLogLevel(argc == 2? argv[1] : "error");
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"basic header", {
            Test{[]() {
                auto actual = generate_message_with_header("asdf"s);
                auto expected = "Content-Length: 4\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\nasdf";
                assert_equals(actual, expected);
            }},
            Test{[]() {
                auto message = "Content-Length: 4\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\nasdf"s;
                MessageBuffer buffer;
                for (size_t i = 0; i < message.size() - 1; ++i) {
                    buffer.handle_char(message[i]);
                    assert_equals(buffer.is_message_complete(), false,
                                  "failed at char " + std::to_string(i));
                }
                buffer.handle_char(message.back());
                assert_equals(buffer.is_message_complete(), true);
            }},
            Test{[]() {
                auto header = "Content-Length: 4\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\n"s;
                auto size = extract_body_size_from_header(header);
                assert_equals(size, 4);
            }},
            Test{[]() {
                auto header = "Content-Length: 14\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\n"s;
                auto size = extract_body_size_from_header(header);
                assert_equals(size, 14);
            }},
            Test{[]() {
                assert_throws([]() {
                    auto header = "Content-Length: \r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\n"s;
                    extract_body_size_from_header(header);
                });
            }},
            Test{[]() {
                auto full_request = "Content-Length: 4\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\nasdf"s;
                MessageBuffer buffer;
                for (auto letter :full_request) {
                    buffer.handle_char(letter);
                }
                assert_equals(buffer.is_message_complete(), true);
                assert_equals(buffer.body(), "asdf"s);
            }},
            Test{[]() {
                auto full_request = "Content-Length: 4\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\nasdf"s;
                MessageBuffer buffer;
                size_t count = 2;
                size_t completed = 0;
                for (size_t i = 0; i < count; ++i) {
                    for (auto letter : full_request) {
                        buffer.handle_char(letter);
                        if (buffer.is_message_complete()) {
                            ++completed;
                        }
                        if (buffer.is_message_complete()) {
                            buffer.reset();
                        }
                    }
                }
                assert_equals(completed, count);
            }}
    }}.countFailed();

    failed += TestSuite{"initialize", {
            Test{[]() {
                auto input_method_initialize = R"({"jsonrpc":"2.0","id":"1","method":"initialize","params":{"processId":null,"rootUri":"file:///home/jmmut/Documents/conan/pipes/","capabilities":{"workspace":{"applyEdit":true,"workspaceEdit":{"documentChanges":true},"didChangeConfiguration":{},"didChangeWatchedFiles":{"dynamicRegistration":true},"symbol":{},"executeCommand":{},"workspaceFolders":false,"configuration":true},"textDocument":{"synchronization":{"willSave":true,"willSaveWaitUntil":true,"didSave":true},"completion":{"completionItem":{"snippetSupport":true}},"hover":{},"signatureHelp":{},"references":{},"documentHighlight":{},"formatting":{},"rangeFormatting":{},"onTypeFormatting":{},"definition":{},"codeAction":{},"rename":{"prepareSupport":true,"dynamicRegistration":false},"semanticHighlightingCapabilities":{"semanticHighlighting":false}}}}})";
                assert_equals(extract_method(input_method_initialize), "initialize");
                assert_equals(extract_str(input_method_initialize, "\"id\""), "1");
                auto parsed = parse(input_method_initialize);
                assert_equals(parsed.method, Method::INITIALIZE);
            }},
            Test{[]() {
                auto input_method_initialize = R"({"jsonrpc":"2.0","id":"1","method":"initialize","params":{"processId":null,"rootUri":"file:///home/jmmut/Documents/conan/pipes/","capabilities":{"workspace":{"applyEdit":true,"workspaceEdit":{"documentChanges":true},"didChangeConfiguration":{},"didChangeWatchedFiles":{"dynamicRegistration":true},"symbol":{},"executeCommand":{},"workspaceFolders":false,"configuration":true},"textDocument":{"synchronization":{"willSave":true,"willSaveWaitUntil":true,"didSave":true},"completion":{"completionItem":{"snippetSupport":true}},"hover":{},"signatureHelp":{},"references":{},"documentHighlight":{},"formatting":{},"rangeFormatting":{},"onTypeFormatting":{},"definition":{},"codeAction":{},"rename":{"prepareSupport":true,"dynamicRegistration":false},"semanticHighlightingCapabilities":{"semanticHighlighting":false}}}}})";
                auto request = generate_message_with_header(input_method_initialize);
                auto answer = response(request);
//                std::cout << answer << std::endl;
            }},
            Test{[]() {
                auto uglified = uglify("\n{\n  \"asdf\": \"qwer\",\n  \"a\": {}\n}\n\n");
                assert_equals(uglified, R"({"asdf":"qwer","a":{}})");
            }},
            Test{[]() {
                auto initialized = "Content-Length: 52\r\n\r\n{\"jsonrpc\":\"2.0\",\"method\":\"initialized\",\"params\":{}}"s;
                auto parsed = parse(initialized);
                assert_equals(parsed.method, Method::INITIALIZED);
            }}
    }}.countFailed();

    failed += TestSuite{"hover", {
            Test{[]() {
                auto input = R"({"jsonrpc":"2.0","id":"2","method":"textDocument/hover","params":{"textDocument":{"uri":"file:///home/jmmut/Documents/conan/pipes/pipes_programs/tests/test_piped_loop.pipes"},"position":{"line":4,"character":17}}})";
                auto parsed = parse(input);
                assert_equals(parsed.method, Method::HOVER);
                assert_equals(parsed.id(), "2");
            }},
            Test{[]() {
                auto input = R"({"jsonrpc":"2.0","id":"2","method":"textDocument/hover","params": {
  "textDocument": {
    "uri": "file:///home/jmmut/Documents/conan/pipes/corelib/array/sort.pipes"
  },
  "position": {
    "line": 22,
    "character": 25
  }
}}
)";
                auto parsed = parse(input);
                assert_equals(parsed.method, Method::HOVER);
                assert_equals(parsed.location(), PositionInCode{-1, 22, 25});
            }},
            Test{[](){
                auto pos_input = R"(  "position": {    "line": 22,    "character": 25  })";
                auto pos = extract_position(pos_input);
                assert_equals(pos, PositionInCode{-1, 22, 25});
            }},
            Test{[](){
                auto contains_number = R"(ne": 22,  )";
                auto number = parse_next_number(contains_number);
                assert_equals(number, 22);
            }}
    }}.countFailed();

    failed += TestSuite{"get symbol", {
            Test{[]() {
                auto program = tokenizeAndParseAndCheckTypes(
                        "function int64 -> int64 { 3 } | function my_func {5 |my_func}");
                auto expression_opt = find_expression_at(program, PositionInCode{-1, 0, 44});
                assert_equals(expression_opt.has_value(), true);
                auto expression = expression_opt.value().get();
                assert_equals(expression.getType(), ExpressionType::LAMBDA);
                assert_equals(
                        expression.pipesType().operator*(),
                        parseSimpleType("function(my_func :function(:int64 -> :int64) -> :int64)")
                                .operator*());
            }},
            Test{[]() {
                auto input = R"({"jsonrpc":"2.0","id":"2","method":"textDocument/hover","params":{"textDocument":{"uri":"file://../../../pipes_programs/tests/test_piped_loop.pipes"},"position":{"line":4,"character":15}}})";
                auto request = parse(input);
                auto response_body = choose_response(request);
                const auto &json = response_body.message;
                assert_equals(std::count(json.begin(), json.end(), '"') % 2, 0);

                assert_equals(std::count(json.begin(), json.end(), '{'),
                              std::count(json.begin(), json.end(), '}'));
            }}
    }}.countFailed();

    return failed > 0;
}
