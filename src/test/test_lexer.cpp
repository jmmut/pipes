#include <iostream>

#include "lexer/Lexer.h"
#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::test::Test;
using randomize::test::assert_equals;

int main(int argc, char **argv) {
    randomize::log::log_level = randomize::log::parseLogLevel(argc == 2? argv[1] : "info");
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"basic lexing", {
            []() {
                auto result = tokenize("3");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::NUMBER(3)");
            },
            []() {
                auto result = tokenize("f");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::IDENTIFIER(f)");
            },
            []() {
                auto result = tokenize("(");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::OPEN_PAREN");
            },
            []() {
                auto result = tokenize(")");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::CLOSE_PAREN");
            },
            []() {
                auto result = tokenize("{");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::OPEN_BRACE");
            },
            []() {
                auto result = tokenize("}");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::CLOSE_BRACE");
            },
            []() {
                auto result = tokenize("function");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::LAMBDA");
            },
            []() {
                auto result = tokenize("|");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::CALL");
            },
            []() {
                auto result = tokenize("struct");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRUCT");
            },
            []() {
                auto result = tokenize(">");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::GREATER_THAN");
            }
    }}.countFailed();

    failed += TestSuite{"unrecognized chars", {
            []() {
                try {
                    auto result = tokenize("%");
                    ASSERT_OR_THROW_MSG(false, "should have thrown an exception");
                } catch (std::exception &e) {
                    // ok
                }
            },
    }}.countFailed();

    failed += TestSuite{"basic constructs", {
            Test{[]() {
                auto result = tokenize("3 | f");
                ASSERT_EQUALS_OR_THROW(result.size(), 3);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::NUMBER(3) "
                                          "Token::CALL "
                                          "Token::IDENTIFIER(f)");
            }},
            Test{[]() {
                auto result = tokenize("( 3)");
                ASSERT_EQUALS_OR_THROW(result.size(), 3);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::OPEN_PAREN "
                                                             "Token::NUMBER(3) "
                                                             "Token::CLOSE_PAREN");
            }},
            Test{[]() {
                auto result = tokenize("function a {b}");
                ASSERT_EQUALS_OR_THROW(result.size(), 5);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::LAMBDA "
                                          "Token::IDENTIFIER(a) "
                                          "Token::OPEN_BRACE "
                                          "Token::IDENTIFIER(b) "
                                          "Token::CLOSE_BRACE");
            }},
            Test{[]() {
                auto result = tokenize("[3] || f");
                ASSERT_EQUALS_OR_THROW(result.size(), 5);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::OPEN_BRACKET "
                                          "Token::NUMBER(3) "
                                          "Token::CLOSE_BRACKET "
                                          "Token::LOOP_CALL "
                                          "Token::IDENTIFIER(f)");
            }},
            Test{[]() {
                auto result = tokenize("[3] =|| f");
                assert_equals(result.size(), 5u);
                assert_equals(to_string(result),
                                          "Token::OPEN_BRACKET "
                                          "Token::NUMBER(3) "
                                          "Token::CLOSE_BRACKET "
                                          "Token::MUTABLE_LOOP_CALL "
                                          "Token::IDENTIFIER(f)");
            }},
            Test{[]() {
                auto result = tokenize("3 - 2");
                ASSERT_EQUALS_OR_THROW(result.size(), 3);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::NUMBER(3) "
                                          "Token::MINUS "
                                          "Token::NUMBER(2)");
            }},
            Test{[]() {
                auto result = tokenize("- 3 - 2");
                ASSERT_EQUALS_OR_THROW(result.size(), 4);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::MINUS "
                                          "Token::NUMBER(3) "
                                          "Token::MINUS "
                                          "Token::NUMBER(2)");
            }},
            Test{[]() {
                auto result = tokenize("3 - - 2");
                ASSERT_EQUALS_OR_THROW(result.size(), 4);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::NUMBER(3) "
                                          "Token::MINUS "
                                          "Token::MINUS "
                                          "Token::NUMBER(2)");
            }},
            Test{[]() {
                auto result = tokenize("a.b");
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::IDENTIFIER(a.b)");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
            }},
            Test{[]() {
                auto result = tokenize("a .b");
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::IDENTIFIER(a) "
                                          "Token::FIELD_ACCESS "
                                          "Token::FIELD(b)");
                ASSERT_EQUALS_OR_THROW(result.size(), 3);
            }},
            Test{[]() {
                auto result = tokenize("a =4");
                ASSERT_EQUALS_OR_THROW_SS(to_string(result),
                                          "Token::IDENTIFIER(a) "
                                          "Token::DEFINITION "
                                          "Token::NUMBER(4)");
                ASSERT_EQUALS_OR_THROW(result.size(), 3);
            }}
    }}.countFailed();

    failed += TestSuite{"complex constructs", {Test{[](){
        auto result = tokenize("while 1 {5}");
        randomize::test::assert_equals(to_string(result),
                                       to_string({Token::ofTokenType(Token::WHILE),
                                                  Token::ofNumber(1),
                                                  Token::ofTokenType(Token::OPEN_BRACE),
                                                  Token::ofNumber(5),
                                                  Token::ofTokenType(Token::CLOSE_BRACE)
                                                 }));
    }}}}.countFailed();

    failed += TestSuite{"strings", {
            []() {
                auto result = tokenize("\"\"");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRING()");
            },
            []() {
                auto result = tokenize("\"a\"");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRING(a)");
            },
            []() {
                auto result = tokenize("\"a\\\"b\"");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRING(a\"b)");
            },
            []() {
                auto result = tokenize("\"a\"\"b\"");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRING(ab)");
            },
            []() {
                auto result = tokenize("\"a\"\n  \"b\"");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRING(ab)");
            },
            []() {
                auto result = tokenize("\"a\"\"b\"\n  \"c\"");
                ASSERT_EQUALS_OR_THROW(result.size(), 1);
                ASSERT_EQUALS_OR_THROW_SS(to_string(result), "Token::STRING(abc)");
            },
            []() {
                ASSERT_THROWS_MSG(tokenize("\"a");, "unmatched double quote");
            }
    }}.countFailed();

    return failed > 0;
}
