#include <iostream>
#include <chrono>
#include <numeric>
#include <cmath>
#include <assembler/PipesAssembler.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using namespace std::chrono;

auto average = [](const auto &nums) {
    if (nums.size() < 1) {
        throw std::logic_error{"can't compute a standard deviation of a sequence of 0 elements"};
    }
    double init = 0.0;
    auto sum = std::accumulate(nums.begin(), nums.end(), init);
    auto avg = sum / nums.size();
    return avg;
};

auto standardDeviation = [](const auto &nums) {
    if (nums.size() < 1) {
        throw std::logic_error{"can't compute a standard deviation of a sequence of 0 elements"};
    }
    auto avg = average(nums);
    double init = 0.0;
    auto stdDev = std::accumulate(nums.begin(), nums.end(), init, [=](auto accum, auto elem) {
        auto diff = elem - avg;
        accum += diff*diff;
        return accum;
    });
    auto normalisedStdDev = sqrt(stdDev / (nums.size() - 1));
    return std::pair{avg, normalisedStdDev};
};

/**
 * on commit 5e4c675
cold with 1000 identities: 10498 µs
hot with 1000 identities (across 20 executions): 9338 µs
hot with 1000 identities, repeated 100 times: average=10462.220000 µs, standard deviation=1065.381788

 * on commit bf74a4c (fixes name collsions and uses a variant of De Bruijn indexes)
cold with 1000 identities: 5103 µs
hot with 1000 identities (across 20 executions): 2780 µs
hot with 1000 identities, repeated 100 times: average=860.730000 µs, standard deviation=566.540144

 * on commit 811be11
cold with 1000 identities: 5023 µs
hot with 1000 identities (across 20 executions): 2585 µs
hot with 1000 identities, repeated 100 times: average=847.140000 µs, standard deviation=526.841456

 * on commit 4c208c7, after refactoring into lexer, parser and evaluator, I achieved to make it 60 times slower ¬¬
cold with 1000 identities: 295513 µs

 * on commit d8a6517, after optimizing and fixing all name collision tests
cold with 1000 identities: 5866 µs       // sometimes goes as low as 1406 µs
hot with 1000 identities (across 20 executions): 4004 µs
hot with 1000 identities, repeated 100 times: average=1022.220000 µs, standard deviation=616.685447


 */
int main(int argc, char **argv) {
    randomize::log::log_level = randomize::log::parseLogLevel(argc == 2? argv[1] : "info");
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;
    failed += TestSuite{"several identities concatenated", {
            []() {
                long n = 1000;
                std::stringstream ss;
                auto input = 3;
                ss << input;
                for (int i = 0; i < n; ++i) {
                    ss << "|function x {x}";
                }
                std::string program = ss.str();

                auto begin = steady_clock::now();
                auto result = PipesAssembler{program}.getEntryPoint()(0);
                auto end = steady_clock::now();

                LOG_WARN("cold with %ld identities: %ld µs", n, duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(input, result);
            },
            []() {
                long n = 1000;
                long batchSize = 50;
                std::stringstream ss;
                auto input = 3;
                ss << input;
                for (int i = 0; i < batchSize; ++i) {
                    ss << "|function x {x}";
                }
                std::string program = ss.str();

                long result;
                auto begin = steady_clock::now();
                for (int j = 0; j < n / batchSize; ++j) {
                    result = PipesAssembler{program}.getEntryPoint()(0);
                }
                auto end = steady_clock::now();

                LOG_WARN("hot with %ld identities (across %ld executions): %ld µs", n, n/batchSize,
                        duration_cast<microseconds>(end - begin).count());

                ASSERT_EQUALS_OR_THROW(input, result);
            },
            []() {
                long n = 1000;
                std::stringstream ss;
                auto input = 3;
                ss << input;
                for (int i = 0; i < n; ++i) {
                    ss << "|function x {x}";
                }
                std::string program = ss.str();
                std::vector<long> durations;
                auto repetitions = 100;
                for (int j = 0; j < repetitions; ++j) {
                    auto begin = steady_clock::now();
                    auto result = PipesAssembler{program}.getEntryPoint()(0);
                    auto end = steady_clock::now();
                    durations.push_back(duration_cast<microseconds>(end - begin).count());
                    ASSERT_EQUALS_OR_THROW(input, result);
                }
                auto[avg, stdDeviation] = standardDeviation(durations);
                LOG_WARN("hot with %ld identities, repeated %d times: average=%f µs, standard deviation=%f",
                         n, repetitions, avg, stdDeviation);
            }
    }}.countFailed();

    /*
    failed += TestSuite{"long programs", {
            []() {
                long n = 10000;
                std::stringstream ss;
                auto input = 3;
                ss << input;
                for (int i = 0; i < n; ++i) {
                    ss << "|x->x";
                }
                std::string program = ss.str();

                auto begin = steady_clock::now();
                auto result = interpretAsNumber(program);
                auto end = steady_clock::now();

                LOG_WARN("cold with %ld identities: %ld ms", n, duration_cast<milliseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(input, result);
            },
            []() {
                long n = 100000;
                std::stringstream ss;
                auto input = 3;
                ss << input;
                for (int i = 0; i < n; ++i) {
                    ss << "|x->x";
                }
                std::string program = ss.str();

                auto begin = steady_clock::now();
                auto result = interpretAsNumber(program);
                auto end = steady_clock::now();

                LOG_WARN("cold with %ld identities: %ld ms", n, duration_cast<milliseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(input, result);
            }
    }}.countFailed();
     */

    return failed > 0;
}
