#include <iostream>

#include "assembler/Assembler.h"
#include "assembler/PipesAssembler.h"


int main(int argc, char **argv) {
    auto assembler = PipesAssembler(tokenizeAndParseAndCheckTypes("5 |function a {a}"));
    auto func = assembler.getEntryPoint();
    auto result = (*func)(0);
    return result != 4;
}
