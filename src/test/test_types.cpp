/**
 * @file test_types.cpp
 * @date 2021-05-08
 */

#include <iostream>
#include <commonlibs/SignalHandler.h>
#include <commonlibs/TestSuite.h>
#include <parser/import.h>
#include <typing/PipesType.h>
#include <typing/typing.h>
#include <typing/subtyping.h>
#include <assembler/PipesAssembler.h>
#include "typing/unify/unify.h"
#include "parser/TypeParser.h"
#include "typing/Program.h"
#include "expression/ExpressionType.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::test::Test;
using randomize::test::assert_equals;
using randomize::test::assert_throws;

#define TEST_ADD_TYPES(codeString) { \
    auto text = std::string{codeString}; \
    auto code = buildCode(text);     \
    auto parsed = tokenizeAndParse(text);\
    addInferredTypes(Program{code, parsed, {}}); \
}

#define TEST_ADD_WRONG_TYPES(codeString) { \
    auto text = std::string{codeString};   \
    auto code = buildCode(text);           \
    auto parsed = tokenizeAndParse(text);  \
    auto program = Program{code, parsed, {}}; \
    ASSERT_THROWS(                         \
        addInferredTypes(program);         \
    );                                     \
}

void test_add_types(std::string codeString, Identifiers identifiers = {}) {
    auto code = CodeFile{codeString, "pipes_programs", ""};
    auto parsed = tokenizeAndParse(code, identifiers);
    addInferredTypes(Program{code, parsed, identifiers});
}

void test_add_wrong_types(std::string codeString, Identifiers identifiers = {},
                          source_location location = source_location::current()) {
    auto code = CodeFile{codeString, "pipes_programs", ""};
    auto parsed = tokenizeAndParse(code, identifiers);
    auto program = Program{code, parsed, identifiers};
    assert_throws([=]() {
        addInferredTypes(program);
    }, "", location);
}

void test_type_equals(std::string code, std::string type,
                      source_location location = source_location::current()) {
    auto program = tokenizeAndParseAndCheckTypes(CodeFile{code, "pipes_programs", ""});
    assert_equals(program.expression.pipesType().operator*(),
                  *parseSimpleType(type),
                  "", location);
}

int main(int argc, char **argv) {
    randomize::log::log_level = randomize::log::parseLogLevel(argc == 2? argv[1] : "error");
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"parse basic types", {
            Test{[]() {
                auto identifiers = Identifiers{{"x", ofConstant(3, {})}};
                tokenizeAndParse("x :int64", identifiers);
            }},
            Test{[]() {
                auto identifiers = Identifiers{{"x", ofConstant(3, {})}};
                auto parsed = tokenizeAndParse("x :int64", identifiers);

                assert_equals(parsed.getType(), ExpressionType::IDENTIFIER);
                assert_equals(parsed.pipesType().operator*(), IntType{});
            }},
            Test{[]() {
                auto parsed = tokenizeAndParse("function x :int64 {x}");

                assert_equals(parsed.getType(), ExpressionType::LAMBDA);
                PipesType &type = parsed.pipesType().operator*();
                auto expectedParams = IntType{};
                auto expectedReturns = UnknownType{};
                auto expectedFunction = FunctionType{expectedParams.copy(), expectedReturns.copy(),
                                                     ""};
                assert_equals(type, expectedFunction);
                assert_equals(type.isCallable(), true);

                const auto &params = type.getParameterType();
                assert_equals(params, expectedParams);

                const auto &returns = type.getReturnedType();
                assert_equals(returns, expectedReturns);
            }},
            Test{[]() {
                auto parsed = tokenizeAndParse("function x -> x :int64 {x}");

                assert_equals(parsed.getType(), ExpressionType::LAMBDA);
                PipesType &type = parsed.pipesType().operator*();
                assert_equals(type.isCallable(), true);

                const auto &params = type.getParameterType();
                auto expectedParams = UnknownType{};
                assert_equals(params, expectedParams);

                const auto &returns = type.getReturnedType();
                auto expectedReturns = IntType{};
                assert_equals(returns, expectedReturns);
            }}, Test{[]() {
                test_add_types("function :function(:int64) {3}");
            }}
    }}.countFailed();


    failed += TestSuite{"subtyping", {Test{[]() {
        assert_equals(isSubType(IntType{}, IntType{}),
                      true);
    }}, Test{[]() {
        assert_equals(isSubType(IntType{},
                                FunctionType{IntType::make_ptr(""),
                                             IntType::make_ptr(""), ""}),
                      false);
    }}, Test{[]() {
        assert_equals(isSubType(IntType{}, UnknownType{}), true);
    }}, Test{[]() {
        assert_equals(isSubType(UnknownType{}, IntType{}), true);
    }}, Test{[]() {
        assert_equals(isSubType(TupleType{IntType::make_ptr(""), ""},
                                ArrayType{IntType::make_ptr(""), ""}),
                      true);
    }}, Test{[]() {
        assert_equals(isSubType(ArrayType{IntType::make_ptr(""), ""},
                                TupleType{IntType::make_ptr(""), ""}),
                      true);
    }}, Test{[]() {
        assert_equals(isSubType(IntType{},
                                TypeofType{ofIdentifier("a", UnknownType::make_ptr("")), ""}),
                      true);
    }}, Test{[]() {
        assert_equals(isMoreRestrictiveTypeAndCompatible(
                              *parseSimpleType("iterable(a :int64, b :int64)"),
                              TupleType{UnknownType::make_ptr(""), ""}),
                      true);
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        TupleType{IntType::make_ptr(""), ""},
                        TypeofType{ofIdentifier("list", TupleType::make_ptr(TypeofType::make_ptr(
                                ofIdentifier("element", UnknownType::make_ptr("")), ""
                        ), "")), ""}),
                true);
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        *parseSimpleType("struct(doThing :function(x :int64 -> x :int64), x :int64)"),
                        *parseSimpleType("iterable(f :function x, i :int64)")),
                true);
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        *parseSimpleType("iterable(a :int64, b :iterable(x :int64, y :int64))"),
                        *parseSimpleType("struct(a :int64, b :struct(x :int64, y :int64))")),
                true);
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        *parseSimpleType("struct(a :int64, b :int64)"),
                        *parseSimpleType("iterable(:int64)")),
                true, "");
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        *parseSimpleType("iterable(:int64)"),
                        *parseSimpleType("struct(a :int64, b :int64)")),
                true, "");
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        *parseSimpleType("struct(a :array, b :array)"),
                        *parseSimpleType("iterable(:array(:int64))")),
                true, "");
    }}, Test{[]() {
        assert_equals(
                isSubType(
                        *parseSimpleType("iterable(:array(:int64))"),
                        *parseSimpleType("struct(a :array, b :array)")),
                true, "");
    }}}}.countFailed();

    failed += TestSuite{"basic type checking", {Test{[]() {
        test_add_types("3 |function x :int64 {x}");
    }}, Test{[]() {
        test_add_wrong_types("function x {x} |function x :int64 {x}");
//    }}, Test{[]() {
        // bad idea to mix <> with ->
//        test_add_types("function x {x} |function<f function<x :int64 -> x :int64>, :int64> {3 |f}");
//    }}, Test{[]() {
        // maybe bad idea to mix [->] with lists [,]
//        test_add_types("function x {x} |function[f function[x :int64 -> x :int64], :int64] {3 |f}");
//    }}, Test{[]() {
        // maybe bad idea to mix {types} with {code}
//        test_add_types("function x {x} |function{f function{x :int64 -> x :int64}, :int64} {3 |f}");
//    }}, Test{[]() {
//        test_add_types("function x {x} |function(f function(x :int64 -> x :int64), :int64) {3 |f}");   // <-- best?
    }}, Test{[]() {
        test_add_types("function x {x} |function f :function x :int64 -> x :int64 {3 |f}");
    }}, Test{[]() {
        test_add_types("function x {x} |function f :function x :int64 -> x       {3 |f}");
    }}, Test{[]() {
        test_add_types("function x {x} |function f :function x       -> x :int64 {3 |f}");
    }}, Test{[]() {
        test_add_wrong_types("3 |function f :function x :int64 {3 |f}");
    }}, Test{[]() {
        test_add_wrong_types("function x -> f :function(in :int64 -> out :int64) {5}");
    }}, Test{[]() {
        test_add_wrong_types("function x :int64 -> f :function(in :int64 -> out :int64) {x}");
    }}, Test{[]() {
        test_add_wrong_types("5 |(function x {x} |function f {f |function g -> r :int64 {3 |g}})");
    }}}}.countFailed();


    failed += TestSuite{"function type associativity", {Test{[]() {
        auto parsed = tokenizeAndParse("function f :function x :int64 -> x :int64 {3 |f}");

        assert_equals(parsed.getType(), ExpressionType::LAMBDA);
        PipesType &type = parsed.pipesType().operator*();
        auto expectedParams = FunctionType{IntType::make_ptr(""), IntType::make_ptr(""), ""};
        auto expectedReturns = UnknownType{};
        auto expectedFunction = FunctionType{expectedParams.copy(), expectedReturns.copy(), ""};
        assert_equals(type, expectedFunction);
        assert_equals(type.isCallable(), true);

        const auto &params = type.getParameterType();
        assert_equals(params, expectedParams);

        const auto &returns = type.getReturnedType();
        assert_equals(returns, expectedReturns);
    }}, Test{[]() {
        auto withParens    = tokenizeAndParse("function f :function(x :int64 -> x :int64) {3 |f}");
        auto withoutParens = tokenizeAndParse("function f :function x :int64 -> x :int64  {3 |f}");
        bool equal =
                withoutParens.pipesType().operator*() == withParens.pipesType().operator*();
        assert_equals(equal, true);
    }}, Test{[]() {
        auto withParens    = tokenizeAndParse("function f :function(x :int64) -> x :int64 {3 |f}");
        auto withoutParens = tokenizeAndParse("function f :function x :int64  -> x :int64 {3 |f}");
        bool different =
                withoutParens.pipesType().operator*() != withParens.pipesType().operator*();
        assert_equals(different, true);
    }}}}.countFailed();


    failed += TestSuite{"function types with parenthesis", {Test{[]() {
        test_add_types("function x {x} |function f :function(x :int64) -> x :int64 {3 |f}");
    }}, Test{[]() {
        test_add_types("function x {x} |function(f :function(x :int64) -> x :int64) {3 |f}");
    }}, Test{[]() {
        test_add_types("function x {x} |function f :function(x :int64 -> x :int64) {3 |f}");
    }}, Test{[]() {
        test_add_types("function x {x} |function f :function(x        -> x :int64) {3 |f}");
    }}}}.countFailed();


    failed += TestSuite{"inferring types", {Test{[]() {
        auto unified = Unifier{ofNothing({}), buildCode()}
                .unify(FunctionType(IntType::make_ptr(""),
                                    UnknownType::make_ptr(""), "a"),
                       FunctionType(UnknownType::make_ptr(""),
                                    IntType::make_ptr(""), "b"));
        assert_equals(unified.operator*(),
                      FunctionType{IntType::make_ptr(""),
                                   IntType::make_ptr(""), ""});
    }}, Test{[]() {
        auto lambda = tokenizeAndParseAndCheckTypes("function x { 3 }");
        assert_equals(lambda.expression.pipesType().operator*(),
                      FunctionType{UnknownType::make_ptr("x"),
                                   IntType::make_ptr(""), ""});
        auto unified = Unifier{ofNothing({}), buildCode()}
                .unify(FunctionType(IntType::make_ptr(""),
                                    UnknownType::make_ptr(""), "a"),
                       TypeofType(lambda.expression, "b"));
        assert_equals(unified.operator*(),
                      FunctionType{IntType::make_ptr(""),
                                   IntType::make_ptr(""), ""}, "");
    }}, Test{[]() {
        test_add_types("function x {x} |function f :function(x -> x :int64) {3 |f}");
    }}, Test{[]() {
        test_add_wrong_types("function(x -> g :function(a :function (x))) {"
                             "  function a {x |a}"
                             "}|function f { 4 |(5 |f)}");
    }}, Test{[]() {
        test_add_wrong_types("function(x -> g :function(a :function (x))) {"
                             "  function b :int64 {b |x}"
                             "}");
    }}, Test{[]() {
        test_add_types("([0], [0], [0]) :struct(a :array(:int64),\n"
                       "                        b :array(:int64),\n"
                       "                        c :array(:int64))\n"
                       "    |function accum :struct(a :array,\n"
                       "                            b :array,\n"
                       "                            c :array) {0}");
    }}, Test{[]() {
        test_add_types("(0, 0, 0) :iterable(:int64)\n"
                       "|function s :struct(a :int64,\n"
                       "                    b :int64,\n"
                       "                    c :int64) {0}");
    }}}}.countFailed();


    failed += TestSuite{"types of parameters", {Test{[]() {
        test_type_equals("function f {3 |f}", "function :function :int64");
    }}, Test{[]() {
        test_add_wrong_types("function n :int64 {3 |n}");
    }}, Test{[]() {
        test_add_wrong_types("3 |function n {3 |n}");
    }}, Test{[]() {
        auto typed = tokenizeAndParseAndCheckTypes("5 |function x {x + 1}");
        auto expectedType = IntType{};
        assert_equals(typed.expression.pipesType().operator*(), expectedType);
    }}, Test{[]() {
        auto typed = tokenizeAndParseAndCheckTypes("function x {x + 1} |function inc {5 |inc}");
        auto expectedType = IntType{};
        assert_equals(typed.expression.pipesType().operator*(), expectedType);
//    }}, Test{[]() {
//        test_add_wrong_types("function x {x} |function n {3 + n}");// relying on + seems bad to rule out functions
//    }}, Test{[]() {
//        test_add_wrong_types("function n_and_f {3 + n_and_f; 3 |n_and_f}");// relying on + seems bad to rule out functions
    }}}}.countFailed();

    failed += TestSuite{"types of variables", {Test{[]() {
        test_type_equals("4 =a", "int64");
    }}, Test{[]() {
        test_type_equals("4 =a; a", "int64");
    }}, Test{[]() {
        test_type_equals("3 |function p {p =a ;a}", "int64");
    }}, Test{[]() {
        test_add_wrong_types("3 |function p {p =a ;a} ;a");
    }}, Test{[]() {
        test_type_equals("1 =a |branch {3} {4} ;a", "int64");
    }}}}.countFailed();


    // TODO this should work?
    //  func_that_will_pass_an_int(function x :unknown {function_that_takes_an_int(x)})
    // function x {x |func_that_takes_an_int} |func_that_will_pass_an_int;

    failed += TestSuite{"types of identifiers", {Test{[]() {
        Identifiers identifiers;
        importWithoutChecks("plus_3", buildCode("function x :int64 -> x_plus_3 :int64 { x + 3 }"),
                            identifiers);
        assert_throws([&]() {
                tokenizeAndParseAndCheckTypes(buildCode("function x {x} |plus_3"), identifiers);
        });
    }}, Test{[]() {
        Identifiers identifiers;
        importWithoutChecks("plus_3", buildCode("function x :int64 -> x_plus_3 :int64 { x + 3 }"),
                            identifiers);
        auto typed = tokenizeAndParseAndCheckTypes(buildCode("2 |plus_3"), identifiers);
        auto assembler = PipesAssembler{typed};
        auto result = assembler.getEntryPoint()(0);
        assert_equals(result, 5);
    }}, Test{[]() {
        Identifiers identifiers;
        importWithoutChecks("plus_3", buildCode("function x :int64 -> x_plus_3 :int64 { x + 3 }"),
                            identifiers);
        auto text = "2 |(plus_3 |function f {7 |f})";
        assert_throws([&]() {tokenizeAndParseAndCheckTypes(buildCode(text), identifiers);},
                      "f is a parameter filled with a known type (7|f returns non-function)");
    }}, Test{[]() {
        Identifiers identifiers{{"f_on_3", "function f { 3 |f}"}};
        auto text = "2 |f_on_3";
        assert_throws([&]() {tokenizeAndParseAndCheckTypes(buildCode(text), identifiers);},
                      "We couldn't infer that f_on_3 treats f as a function");
    }}, Test{[]() {
        Identifiers identifiers{{"f_on_3", "function f { 3 |f}"},
                                {"pass_f", "function f { f |f_on_3 }"}};
        assert_throws([&]() {tokenizeAndParseAndCheckTypes(buildCode("2 |pass_f"), identifiers);},
                      "We couldn't infer that f_on_3 treats f as a function "
                      "(through intermediate identifiers)");
    }}, Test{[]() {
        Identifiers identifiers;
        importWithoutChecks("int_consumer", buildCode("function n :int64 { n }"), identifiers);
        importWithoutChecks("int_dispatcher", buildCode(
                "function consumer :function(n :int64) { 3 |consumer }"), identifiers);
        auto text = "function x {x +1 |int_consumer} |int_dispatcher";
        auto typed = tokenizeAndParseAndCheckTypes(buildCode(text), identifiers);
        auto assembler = PipesAssembler{typed};
        auto result = assembler.getEntryPoint()(0);
        assert_equals(result, 4);
    }}}}.countFailed();


    failed += TestSuite{"type checking of arrays", {Test{[]() {
        test_add_types("[1, 2]");
    }}, Test{[]() {
        test_add_wrong_types("[1, function x{x}]");
    }}, Test{[]() {
        test_add_wrong_types("1 ||function x{x}");
    }}, Test{[]() {
        test_add_types("[] :iterable(number :int64) ||function x{x}");
    }}, Test{[]() {
        test_add_types("[] |function a :iterable(number :int64) {a |deref}");
    }}, Test{[]() {
        test_add_types("[] |function a :iterable {a |deref}");
    }}, Test{[]() {
        test_add_types("[3] |function a :iterable {a |deref}");
    }}, Test{[]() {
        test_add_types("[3] :iterable(n :int64) |function a :iterable {a |deref}");
    }}, Test{[]() {
        test_add_wrong_types("[3] ||function x :function(x) {x}");
    }}, Test{[]() {
        test_add_wrong_types("[[3]] ||function x { (x, 0) } :iterable(t :struct(a :int64, b :int64))");
    }}, Test{[]() {
        test_add_types("[[3]] ||function x { (x, 0) } :iterable(t :struct(a :iterable(n :int64), b :int64))");
    }}, Test{[]() {
        test_type_equals("[[3]] ||function x { (x, 0) }",
                         "array(t :iterable(a :array(n :int64), b :int64))");
    }}, Test{[]() {
        test_type_equals("(4,5,6) :struct(a,b,c) |function a :array {a}",
                         "array(:int64)");
    }}}}.countFailed();


    failed += TestSuite{"type checking of loops", {Test{[]() {
        test_add_types("[1, 2] ||function x {x +1}");
    }}, Test{[]() {
        test_add_types("[1, 2] =||function x {x +1}");
    }}, Test{[]() {
        test_add_wrong_types("[1, 2] =||function x {(x +1, 0)}");
    }}}}.countFailed();


    failed += TestSuite{"type checking of branch", {Test{[]() {
        test_add_types("1 |branch {3} {4} :int64");
    }}, Test{[]() {
        test_add_types("branch {3} {4} :function x");
    }}, Test{[]() {
        test_add_wrong_types("branch {3} {function x {x}} ");
    }}, Test{[]() {
        test_add_wrong_types("branch {function x{x}} {function x{x}} :function x -> n :int64");
    }}, Test{[]() {
        test_add_types("branch {function x{x}} {function x{x}}"
                             " :function(cond :int64 -> r :function x)");
    }}, Test{[]() {
        test_add_wrong_types("3 +(1 |branch {function x{x}} {function x{x}} :int64)");
    }}, Test{[]() {
        test_add_wrong_types("function x {x} |(1|branch {function(x :int64) {x}} {function(x :int64) {x}} :function x)");
    }}}}.countFailed();

    failed += TestSuite{"type checking of tuples", {Test{[]() {
        test_add_types("(1, 2)");
    }}, Test{[]() {
        test_add_types("(1, function x{x})");
    }}, Test{[]() {
        test_add_types("(3,) :iterable(number :int64) ||function x{x}");
    }}, Test{[]() {
        test_add_types("(2, 3) :iterable(number :int64, number :int64) ||function x{x}");
    }}, Test{[]() {
        test_add_types("(3,function x{x}) :iterable(number :int64, f :function x) ||function x{x}");
    }}, Test{[]() {
        test_add_types("(3,function x{x}) |function x :iterable {x}");
    }}, Test{[]() {
        test_add_types("function a :iterable {a |deref}");
    }}, Test{[]() {
        test_add_wrong_types("3 ||function x {x}", {});
    }}, Test{[]() {
        test_add_wrong_types("(3,) ||function x :function(x) {x}", {});
    }}}}.countFailed();


    failed += TestSuite{"type checking of intrinsics", {Test{[]() {
        test_add_types("'a' |print_char");
    }}, Test{[]() {
        test_add_wrong_types("function x{x} |print_char");
    }}, Test{[]() {
        assert_throws([]() {
            tokenizeAndParseAndCheckTypes("[1] |(deref :int64)");
        });
    }}, Test{[]() {
        test_add_types("[1] |deref");
    }}, Test{[]() {
        test_add_wrong_types("1 |deref");
    }}, Test{[]() {
        test_add_wrong_types("function x{x} |deref");
    }}, Test{[]() {
        test_add_wrong_types("1 |increment :function ");
    }}, Test{[]() {
        test_add_wrong_types("1 |increment |function(x :function(x :int64)) {x}");
    }}, Test{[]() {
        test_add_wrong_types("function x {x} |compile_time_print_type :int64");
    }}, Test{[]() {
        test_add_types("function x {x} |compile_time_print_type :function x");
    }}, Test{[]() {
        test_add_types("5 |new_array ||function x :int64 {x}");
    }}, Test{[]() {
        test_add_wrong_types("function x {x} |new_array");
    }}, Test{[]() {
        test_add_wrong_types("5 |new_array |function x :int64 {x}");
    }}}}.countFailed();

    failed += TestSuite{"type checking of binary operators", {Test{[]() {
        test_add_types("3 > 2");
    }}, Test{[]() {
        test_add_wrong_types("function x{x} > 2");
    }}, Test{[]() {
        test_add_types("3 + 2 > 4");
    }}, Test{[]() {
        test_add_wrong_types("3 > 2 :function x");
    }}}}.countFailed();


    failed += TestSuite{"type checking of struct", {Test{[]() {
        assert_throws([&]() {tokenizeAndParseAndCheckTypes("(5,) :MyStruct");});
    }}, Test{[]() {
        auto identifiers = Identifiers{{"MyStruct",
                                               ofStruct({ofField("p", IntType::make_ptr(""))})}};
        auto text = "(5,) :MyStruct";
        auto parsed = tokenizeAndParse(text, identifiers);
        auto result = addInferredTypes(Program{buildCode(text), parsed, identifiers});
        auto fields = PipesTypes{};
        fields.push_back(IntType::make_ptr("p"));
        auto expectedType = UserType::make_ptr(std::move(fields), "");
        assert_equals(result.expression.pipesType().operator*(), expectedType.operator*());
    }}, Test{[]() {
        test_add_wrong_types("(5,) :struct(p :int64) |function s :function x {s}");
    }}, Test{[]() {
        test_add_wrong_types("(5,) :struct(p :int64) |function s {"
                             "s.p |function field :function x { field }} ");
    }}, Test{[]() {
        test_add_types("(5,) :struct(p :int64) |function s {"
                             "s.p |function field :int64 { field }}");
    }}, Test{[]() {
        test_add_wrong_types("(5,) :struct(p :int64) |function s { s.asdf }");
    }}, Test{[]() {
        test_add_wrong_types("(3,4) :struct(x,y) |function s :struct(y,x) {s.x}");
    }}, Test{[]() {
        test_add_types("function s :struct(current, next) { s.next }");
    }}, Test{[]() {
        assert_throws([&]() {tokenizeAndParseAndCheckTypes(
                "(3,4) |function s :struct(current, next) { next }");
        });
    }}}}.countFailed();


    failed += TestSuite{"typeof", {Test{[]() {
        test_type_equals("function x :int64 -> result :typeof(x) {x}",
                         "function x :int64 -> result :typeof(0 :int64)");
    }}, Test{[]() {
        assert_throws([]() {
            // TODO: not implemented yet, typeof arbitrary expressions
            test_add_types("function x :int64 {x |function a :typeof(1-1) {a}}");
        });
    }}, Test{[]() {
        test_add_wrong_types("function x :function {x |function a :typeof(1-1) {a}}");
    }}, Test{[]() {
        test_type_equals("[[0]] |array/copy",
                         "iterable(:array(:int64))");
    }}, Test{[]() {
        test_type_equals("[[0]] |function lines { lines |array/copy }",
                         "iterable(:array(:int64))");
    }}, Test{[]() {
        test_add_types("3 |function x :int64 -> result :typeof(x) {[x] +8 |deref} :int64");
    }}, Test{[]() {
        test_add_wrong_types(
                "3 |function x :int64 -> result :typeof(x) {[x] +8 |deref} :function x");
    }}, Test{[]() {
        test_add_wrong_types("[12, 13, 14] append 20",
                             Identifiers{{"append", toParsedFile(tokenizeAndParseAndCheckTypes(
                                     "function elem {function list { [elem, list] }}"))}}
        );
    }}, Test{[]() {
        test_add_types("[4 |f2] |f2",
                       Identifiers{{"f2", "function a -> r :typeof(a) { a }"}}
        );
    }}, Test{[]() {
        test_add_types("function fa :int64 {3} |f2 "
//                       "| compile_time_print_type" // to see how f2 appears twice
                       ,
                       Identifiers{{"f2", toParsedFile(tokenizeAndParseAndCheckTypes(
                               "function a -> fa :array { [3] }"))}}
//                               "function a -> fa_ :array { [3] }"))}} // to see how f2 appears twice
        );
    }}, Test{[]() {
        test_add_types("[[4,5],[6,7]] array/filter function pair { pair array/get 1}"
//                       "| compile_time_print_type" // to see how 'element' appears twice
        );
    }}, Test{[]() {
        test_add_types("[12, 13, 14] append [20]",
                       Identifiers{{"append", toParsedFile(tokenizeAndParseAndCheckTypes(
                               "function elem {function list { elem; list }}"))}}
        );
    }}, Test{[]() {
        const auto &program = tokenizeAndParseAndCheckTypes(
                "function element {function list :iterable(elem :typeof(element)) { element;list }}");
        test_add_wrong_types("[12, 13, 14] append [20]",
                             Identifiers{{"append", toParsedFile(program)}}
        );
    }}, Test{[]() {
        test_add_types("4 |function a -> :struct(b :typeof(a), c :int64) {(a, 4)}");
    }}, Test{[]() {
        const auto &programGet = tokenizeAndParseAndCheckTypes(
                "function index :int64 -> f :function list :iterable(elem) -> element :typeof(elem) {"
                "    function list :iterable { index |*8 +list |deref }}");
        auto identifiers = Identifiers{{"get", toParsedFile(programGet)}};
        auto code = CodeFile{"[12, 13, 14] get 2", "pipes_programs", ""};
        auto program = tokenizeAndParseAndCheckTypes(code, identifiers);
        assert_equals(program.expression.pipesType().operator*(),
                      TypeofType(ofIdentifier("elem", IntType::make_ptr("")), ""),
                      "");
    }}, Test{[]() {
        const auto &programGet = tokenizeAndParseAndCheckTypes(
                "function index :int64 -> f :function list :iterable(elem) -> element :typeof(elem) {"
                "    function list :iterable { index |*8 +list |deref }}");
        auto identifiers = Identifiers{{"get", toParsedFile(programGet)}};
        auto code = CodeFile{"[[12, 13], [14, 15], [16, 17]] ||function pair { pair get 2 }",
                             "pipes_programs", ""};
        auto program = tokenizeAndParseAndCheckTypes(code, identifiers);
        assert_equals(program.expression.pipesType().operator*(),
                      ArrayType{TypeofType::make_ptr(ofIdentifier("elem", IntType::make_ptr("")), ""), ""},
                      "");
    }}, Test{[]() {
        test_add_wrong_types("[12, 13, 14] array/append [20]");
    }}, Test{[]() {
        test_add_types("[12, 13, 14] array/concat [20]");
    }}, Test{[]() {
        test_add_wrong_types("[12, 13, 14] array/concat 20");
    }}, Test{[]() {
        const auto &program = tokenizeAndParseAndCheckTypes(
                "function element :iterable(n :int64) {"
                "  function list :iterable(n2 :iterable(n3: int64)) { element;list }}");
        test_add_wrong_types("[12, 13, 14] append [20]",
                             Identifiers{{"append", toParsedFile(program)}}
        );
    }}, Test{[]() {
        auto program = tokenizeAndParseAndCheckTypes(
                "[1] |function list :iterable(element :int64) -> result :typeof(element) { list +8 |deref}");
        assert_equals(program.expression.pipesType().operator*(),
                      TypeofType::make_ptr(ofConstant(0, {}), "element").operator*(), "");
    }}, Test{[]() {
        // TODO: typeof that refers to virtual parameters that didn't appear yet
    //    auto program = tokenizeAndParseAndCheckTypes(
    //            "[1] |function list :iterable(element :typeof(result)) -> result :int64 { list +8 |deref}");
    //    assert_equals(program.expression.pipesType().operator*(),
    //                  IntType::make_ptr("x").operator*(), "");
    //}}, Test{[]() {
        // TODO: same name propagates type inference
    //    auto program = tokenizeAndParseAndCheckTypes(
    //            "[1] |function list :iterable(element) -> element { list +8 |deref}");
    //    assert_equals(program.expression.pipesType().operator*(),
    //                  IntType::make_ptr("element").operator*(), "");
    //}}, Test{[]() {
        auto program = tokenizeAndParseAndCheckTypes(
                "[1] |function list :iterable(element) -> result { list +8 |deref}");
        assert_equals(program.expression.pipesType().operator*(),
                      UnknownType::make_ptr("result").operator*(), "");
    }}, Test{[]() {
        test_add_types("function r :struct("
                       "            initial_value,"
                       "            iteration :function(p :struct("
                       "                current :typeof(initial_value),"
                       "                accumulated :typeof(initial_value)))) {0}");
    }}, Test{[]() {
        test_add_types("function r :struct("
                       "            initial_value,"
                       "            other_value :typeof(initial_value)) {0}");
    }}, Test{[]() {
        test_add_types("function r :iterable("
                       "            initial_value,"
                       "            other_value :typeof(initial_value)) {0}");
    }}, Test{[]() {
        auto program = tokenizeAndParseAndCheckTypes(
                "function"
                "    f :function(input :int64) "
                "    -> f2 :function(input2 :typeof(input)) { function x {x} }");

        assert_equals(program.expression.pipesType()->getParameterType(),
                      *parseSimpleType("function input :int64"));
        assert_equals(program.expression.pipesType()->getReturnedType(),
                      *parseSimpleType("function input2 :int64 -> :int64"));
    }}, Test{[]() {
        assert_throws([]() {
            auto program = tokenizeAndParseAndCheckTypes(
                    "function f :function(input :int64) {"
                    "    function f2 :function(input2 :typeof(input)) { function x {x} }}");
        }, "typeof of a virtual parameter of a different function should not be allowed");

//        }}, Test{[]() {
        // TODO same name propagates type inference
        //auto program = tokenizeAndParseAndCheckTypes(
        //        "function f :function(input :int64 -> output) -> f :function(input -> output :int64) { function x {x} }");
        //
        //assert_equals(program.expression.pipesType()->getParameterType(),
        //              FunctionType::make_ptr(
        //                      IntType::make_ptr("result"),
        //                      IntType::make_ptr("output"),
        //                      "f").operator*(),
        //              "");
        //assert_equals(program.expression.pipesType()->getReturnedType(),
        //              FunctionType::make_ptr(
        //                      IntType::make_ptr("result"),
        //                      IntType::make_ptr("output"),
        //                      "f").operator*(),
        //              "");
        //}}, Test{[]() {
            // TODO backpropagation from typeof to its reference
        //auto program = tokenizeAndParseAndCheckTypes(
        //        "function x -> result :typeof(x) { 3 }");
        //assert_equals(program.expression.pipesType().operator*(),
        //              FunctionType::make_ptr(
        //                      IntType::make_ptr("x"),
        //                      TypeofType::make_ptr(ofConstant(3, {}), "result"),
        //                      "").operator*(), "");
    }}}}.countFailed();

    failed += TestSuite{"recursive type checking", {Test{[]() {
        // this takes almost 2 seconds
//        test_add_wrong_types("function x {x} |function identity { identity |identity }");
    }}, Test{[]() {
        // TODO: unresolvable type
//        test_add_types("function x {x} :function(:? -> :?) |function identity { identity |identity }");
    }}}}.countFailed();

    // TODO: support functions with several parameters/returns
//    failed += TestSuite{"type checking of functions with tuples", {Test{[]() {
//        test_add_types("function x {x} |function(f function x :int64 -> x :int64, :int64) {3 |f}");
//    }}, Test{[]() {
//        test_add_types("function x {x} |function(f function(x :int64 -> x :int64), :int64) {3 |f}");
//    }}}}.countFailed();

    return failed != 0;
}

