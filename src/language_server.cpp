/**
 * @file language_server.cpp
 * @date 2022-12-20
 */

#include <iostream>
#include "commonlibs/log.h"
#include "commonlibs/SignalHandler.h"
#include "assembler/PipesAssembler.h"
#include "language_server/message_buffer.h"
#include "language_server/response.h"

#include <commonlibs/Walkers.h>

using std::literals::operator ""s;

int main (int argc, char **argv) {
    randomize::exception::SignalHandler::activate();
    randomize::log::setLogLevel(randomize::log::INFO_LEVEL);

    std::ofstream log;
    bool should_log_to_file = argc == 2;
    if (should_log_to_file) {
        log.open(argv[1]);
    }

    char c;
    MessageBuffer message_buffer;
    while (std::cin.get(c)) {
        if (should_log_to_file) {
            log << c;
        }
        message_buffer.handle_char(c);
        if (message_buffer.is_message_complete()) {
            if (should_log_to_file) {
                log << "\n\n###### request body:\n" << message_buffer.body() << std::endl;
            }
            try {
                auto answer = response(message_buffer.body());
                if (should_log_to_file) {
                    log << "\n\n###### answer:\n" << answer
                        << "\n\n###### waiting for next request:" << std::endl;
                }
                if (answer.should_answer) {
                    std::cout << answer << std::flush;
                }
            } catch (std::exception &e) {
                log << "\n\n###### exception: " << e.what() << std::endl;
            }

            message_buffer.reset();
        }
    }
    if (should_log_to_file) {
        log.close();
    }
    return 0;
}
