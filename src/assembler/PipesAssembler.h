/**
 * @file PipesAssembler.h
 * @date 2020-12-28
 */

#ifndef PIPES_PIPESASSEMBLER_H
#define PIPES_PIPESASSEMBLER_H

#include "assembler/Assembler.h"

#include <set>
#include <utility>
#include <fstream>
#include <cstdlib>
#include <functional>
#include <typing/typing.h>
#include "parser/Parser.h"
#include "executable/Linker.h"
#include "typing/Program.h"
#include "expression/ExpressionString.h"
#include "expression/ExpressionType.h"

/// all data types are currently this big. ints, function pointers and tuple/array/struct pointers.
static const int STACK_WIDTH_IN_BYTES = sizeof(uint64_t);

class PipesAssembler {
public:
    using EntryPoint = int(int);

    explicit PipesAssembler(const std::string &program)
            : PipesAssembler{tokenizeAndParseAndCheckTypes(program)} {
    }

    explicit PipesAssembler(const Program &program)
            : assembler{1024 * 1024}, identifiers{program.identifiers}, currentCode{&program},
              debug{true}, counter{0},
              paranoidRegisterSaving{false}, runtimeLibraries{}, relocationStart{} {
        topLevelExpression = program.expression;
        maybeCompileTopLevel(topLevelExpression);
    }

    /**
     * Creates a binary file with the machine code instructions held by this Assembler instance (after calling the
     * various emit_* functions).
     *
     * This function contrasts with getEntryPoint, which allows you to call the JIT generated code on the fly,
     * without passing through disk.
     *
     * @param path is the future file path of the binary
     */
    void generateExecutable(std::string path) const;

    EntryPoint *getEntryPoint() {
        return assembler.getEntryPoint<EntryPoint>();
    }

    Assembler &getAssembler() { return assembler; }
    const Expression &getExpression() const { return topLevelExpression; }

private:
    Assembler assembler;
    Expression topLevelExpression;
    Identifiers identifiers;
    const CodeFile *currentCode;
    struct CompiledExpression {
        ssize_t location;   ///< index to executable memory where the lambda code is
                            ///< (this is not index to closure nor pointer to closure)
        bool valid; ///< valid is only true if the expression was a lambda without closures
        ssize_t innerLambdaLocation; ///< present only if the other is a closure
        ExpressionType type;
    };
    const CompiledExpression INVALID_COMPILATION{-1, false, -1, ExpressionType::UNSET};
    CompiledExpression nonReusableExpression(ExpressionType type) {
        return CompiledExpression{-1, false, -1, type};
    }
    CompiledExpression unaddressableExpression(ExpressionType type) {
        return CompiledExpression{-1, true, -1, type};
    }
    std::map<std::string, CompiledExpression> compiledIdentifiers;

    bool debug;
    int8_t counter; // for debugging strings within the machine code
    bool paranoidRegisterSaving;

    using ParameterName = std::string;
    using OffsetFromHeapStartInBytes = int32_t;
    using Parameters = std::map<ParameterName, OffsetFromHeapStartInBytes>;
    Parameters parameterLocations;

    using LambdaId = decltype(Lambda::id);
    using EnclosingLambdaId = LambdaId;
    using BytesToClosureStart = int64_t;
    struct ParameterInstance {
        LambdaId lambdaId;
        BytesToClosureStart offset;
    };
    struct Closure {
        int argumentCount;
        LambdaId enclosingLambda;
//        LambdaId definingLambda;
        std::map<ParameterName, ParameterInstance> parameters;
    };
//    std::map<LambdaId, Closure> closuresByDefiningLambda;
    std::map<EnclosingLambdaId, Closure> closuresByEnclosingLambda;

    std::map<LambdaId, EnclosingLambdaId> parentLambdas;
    std::map<EnclosingLambdaId, std::set<LambdaId>> childLambdas;

    RuntimeLibraries runtimeLibraries;
    std::optional<int64_t> relocationStart;
    std::vector<int> variablesPerScope;
    // TODO: RegisterTracker

    static bool isIdentifier(const Expression &actual, const std::string &expected);
    static bool isLambdaIdentity(const Expression &expression);
    void maybeCompileTopLevel(Expression &expression);
    auto compileRecursive(const Expression &expression) -> CompiledExpression;
    void compileTopLevel(Expression &topLevel);
    void throwError(const std::string &message = "") const;
    void throwError(const std::string &message, const Expression &expression) const;
    void enterAndPushRdxAndCopyCurrentBufferPositionToRDX();
    void emitPipesRelocation();
    static bool isLambdaIdentity(const Lambda &lambda);
    auto compileParameter(const Expression &expression, Register64 dst) -> CompiledExpression;
    void emitNopDebugString(std::string message);
    void emitDebugString(std::string message);
    int getParameterGlobalOffset(const std::string &parameterName);
    std::string getParameterNameOfLambda(const Lambda &lambda) const;
    std::string getParameterName(const Expression &parameter) const;
    void emitPushData(Register64 src);
    void emitPushData(int64_t value);
    void emitPopData(Register64 dst);
    void emitPushLocal(Register64 src);
    void emitPopLocal(Register64 dst);
    void emitCall(Register64 ptr, PositionInCode position);
    int fillParameterOffsets(Expression &expression);
    int getParameterGlobalOffset(const Expression &parameter);
    int getParameterGlobalOffset(const Lambda &lambda);
    void emitPrepareToCallLambda();
    auto compileParameter(const Expression &expression) -> CompiledExpression;
    void emitRestoreAfterCall();
    void emitPushLocal(int64_t value);
    bool isChurchTrue(const Lambda &lambda) const;
    void compileIsZeroChurchBool();

    friend auto operator<<(std::ostream &out, const PipesAssembler::Closure &closure) -> std::ostream&;
    friend auto operator<<(std::ostream &out, const PipesAssembler::ParameterInstance &parameter) -> std::ostream&;
    const Closure &getClosureByDefiningLambda(LambdaId lambdaId);
    const Closure &getClosureByDefiningLambda(const Lambda &lambda);
    const Closure &getClosureByEnclosingLambda(LambdaId lambdaId);
    const Closure &getClosureByEnclosingLambda(const Lambda &lambda);
    auto emitPushClosures(std::optional<const Lambda *const> ptrLambda, CompiledExpression expression)
            -> CompiledExpression;
    auto compileClosureHelper(const std::function<CompiledExpression()> &emitBody,
                              std::optional<const Lambda *const> lambda) -> CompiledExpression;
    auto compileLambda(const Lambda &lambda, const PositionInCode &position) -> CompiledExpression;
    auto compileCallRecursive(const Call &call, Register64 dst,
                              PositionInCode position) -> CompiledExpression;
    auto addGlobalParameter(const std::string &parameterName) -> OffsetFromHeapStartInBytes;
    void emitPrintLastDigit();
    void emitAllocate(int byteCount);
    void emitPopLocalDiscarding();
    void emitMoveGlobalParameterToRegister(OffsetFromHeapStartInBytes offset, Register64 dst);
    void emitMoveRegisterToGlobalParameter(Register64 src, OffsetFromHeapStartInBytes offsetDst);
    void addClosuresForParameter(const Expression &parameter, long enclosingLambdaId);
    void updateLambdaMaps(const Expression &expression);
    void updateClosures(Expression &expression);
    void registerExpression(Expression &expression);
    BytesToClosureStart getParameterClosureOffset(LambdaId lambdaId, const ParameterName &paramenterName);
    void compileBranch(const Branch &branch);
    auto compileIdentifier(const Expression &expression, bool withClosure) -> CompiledExpression;
    auto compileClosureLessIdentifier(const Expression &expression) -> CompiledExpression;

    auto compileClosureHelper(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda) -> CompiledExpression;
    auto compileClosureHelper(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation,
                                               ssize_t lambdaAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda) -> CompiledExpression;
    auto compileClosureHelperWithoutClosureLoading(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation,
                                               ssize_t lambdaAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda) -> CompiledExpression;
    auto compileClosureHelper(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation,
                                               ssize_t lambdaAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda,
        bool emitClosureLoading) -> CompiledExpression;
    void emitAllocateWithMalloc(int byteCount);
    void emitAllocateWithMalloc(Register64 byteCount);
    CompiledExpression emitClosureWithinCode(const CompiledExpression &compiledLambda);
    auto compileBinaryOperation(const Expression &operation) -> CompiledExpression;
    void checkIntrinsicFunctionsNotRedefined(const Expression &expression,
                                             const Identifiers &identifiers);
    void emitReadChar();
    void emitPrintChar();
    void emitFibonacci();
    auto compileIdentifier(const Expression &expression) -> CompiledExpression;
    void emitSavingAndRestoringCallerSavedRegisters(std::function<void()> body);
    CompiledExpression compileArray(const Expression &expression);
    void emitSavingAndRestoringRegisters(std::vector<Register64> registers,
                                         std::function<void()> body);
    CompiledExpression compileLoop(const Expression &expression);
    CompiledExpression compileCallFunctionRecursive(const Call &call, Register64 dst,
                                                    CompiledExpression compiledArgument,
                                                    PositionInCode position);
    void throwSemanticError(const std::string &message = "") const;
    void throwSemanticError(const std::string &message, const Expression &expression) const;
    void emitDeallocate(int byteCount);
    void emitDeallocateWithMalloc(Register64 registerWithAddressToFree);
    PipesAssembler::CompiledExpression compileUserIdentifier(
            const Expression &expression, Expression &identifierBody, bool withClosure,
            const CodeFile &caller);
    PipesAssembler::CompiledExpression compileIntrinsicIdentifier(
            const Expression &expression);
    CompiledExpression compileTuple(const Expression &expression);
    CompiledExpression compileVariable(const Expression &expression);
    void compileIterable(const std::vector<Expression> &elements);
    bool areAllElementsLiteralValues(const std::vector<Expression> &elements) const;
    void compileLiteralIterable(const std::vector<Expression> &elements);
    void compileNonLiteralIterable(const std::vector<Expression> &elements);
    void emit_imm64_to_mem(int64_t imm64,
                           Register64 regWithPointer, int32_t displacementFromPointer);
    CompiledExpression compileFieldAccess(const Expression &expression);
    CompiledExpression compileGreaterThan(const Expression &expression);
    CompiledExpression compileDefinition(const Expression &expression);
    auto compilerBug(const std::string &message, const Expression &expression) const {
        return randomize::exception::StackTracedException{
                "Compiler bug" + (message.empty() ? std::string{"."} : ": " + message + ".")
                        + "\nTriggered with a " + quote(to_string(expression.getType()))
                        + " expression " + quote(to_string_with_types(expression))
                        + " from " + codePosToString(*currentCode, expression.position())};
    }
    auto semanticError(const std::string &message, const Expression &expression) const {
        return std::runtime_error{
                "Uncompilable expression (line "
                        + std::to_string(expression.position().line + 1) + "): "
                        + to_string(expression)
                        + (message.empty() ? std::string{""} : "\nreason: " + message)};
    }

    ScopedRegisterSaver callerSavedRegistersSaver();
    CompiledExpression compileMutableLoop(const Expression &expression);
    void emitNewArray();
    void emitCall(label_t *ptr, PositionInCode position);
    void compileParameterOfCurrentFunction(Register64 dst);
    void compileClosuredParameter(const Expression &expression, Register64 &dst);
    void emitPrintFunctionStack();
    void emitPrintCString();
    void emitPrintCurrentFunction();
    void emitGetPointerToCurrentFunction(Register64 dst);
    void emitRbpReset();
    void emitPrintCallStack();
    void emitGetPointerToReturn(Register64 dst);
    void emitDebugStringPre(std::string message);
    void checkCalledWith(ExpressionType expectedType, const Expression &expression);
    void checkCalledWith(Token::Type expectedBinaryExpression, const Expression &expression);
    CompiledExpression compileWhile(const Expression &expression);
};


#endif //PIPES_PIPESASSEMBLER_H
