/**
 * @file PipesAssembler.cpp
 * @date 2020-12-28
 */

#include <fstream>
#include <utility>
#include <commonlibs/Walkers.h>
#include "executable/PipesExecutable.h"
#include "PipesAssembler.h"
#include "commonlibs/StackTracedException.h"
#include "expression/ExpressionType.h"
#include "VariableScopeTracker.h"

// Note that this file only implements _some_ of PipesAssembler's methods.
// Mostly the assembler setup to start compiling toplevel expressions and general utility functions


/*
 # Register conventions
 ## C call convention
 The caller-saved registers are r10, r11, and any registers that parameters are put into (rdi, rsi, rdx, rcx, r8, r9)
 The callee-saved registers are RBX, RBP,and R12 through R15.

 ## Pipes call convention
 Externally, pipes should respect the C call convention, but internally it's not needed. Pipes uses some
 registers in a special way:
 - RAX: used to pass and return the main argument. Caller saved?
 - RSI: points to the data scope (lambda closure only for now). Caller saved. Look below for the definition of a closure object.
 - RDI: contains the pointer to the function to call. Caller saved?
 - RDX: pointer to the beginning of our code segment.
 - RBX: pointer to the current lambda's parameter in the stack.
 - RCX: free. Callee saved? TODO I think I'm using this as caller saved
 - RSP: pointer to the top of the call stack.
 - RSP: pointer to the bottom of the call frame (in the call stack).
 - R10: pointer to the bottom of the data buffer (chunk in the heap).
 - R11: pointer to the top of the data buffer (chunk in the heap).

 In pseudo code, a closure is defined as:
    struct {
       void *func;
       int64_t argumentCount;
       struct {
           int64_t value; /// may not be available yet
       } args[argumentCount];
    }
*/

void PipesAssembler::maybeCompileTopLevel(Expression &expression) {
    this->topLevelExpression = expression;

//    emitFibonacci();

    checkIntrinsicFunctionsNotRedefined(expression, identifiers);

    switch (expression.getType()) {
    case ExpressionType::UNSET:
    case ExpressionType::NOTHING:
    case ExpressionType::PARAMETER:
    case ExpressionType::VARIABLE:
        throwError();
    case ExpressionType::VALUE:
        assembler.emit_enter(0);    // not needed because we are not using rbp nor rsp yet
        assembler.emit_mov_imm64_to_reg(Register64::RAX, expression.getValue());
        assembler.emit_leave();     // not needed because we are not using rbp nor rsp yet
        assembler.emit_return();
        return;
    case ExpressionType::LAMBDA:
    case ExpressionType::STRUCT:
        throwSemanticError("an unused top-level " + to_string(expression.getType())
        + " doesn't make sense. Only makes sense if given an identifier and used somewhere else.");
    case ExpressionType::IDENTIFIER:
    case ExpressionType::CALL:
    case ExpressionType::BINARY_OPERATION:
    case ExpressionType::ARRAY:
    case ExpressionType::TUPLE:
    case ExpressionType::WHILE:
        compileTopLevel(expression);
        return;
    case ExpressionType::FIELD:
        throwSemanticError("It doesn't make sense to compile a top-level field (not attached to "
                           "any struct");
    case ExpressionType::BRANCH:
        throwSemanticError("unimplemented");
    }
    throwError("missing case in switch");
}

void PipesAssembler::checkIntrinsicFunctionsNotRedefined(const Expression &expression,
                                                         const Identifiers &identifiers) {
    for(const auto &intrinsic : intrinsics::FUNCTIONS) {
        if (identifiers.contains(intrinsic)) {
            throwSemanticError("The identifier " + intrinsic
                + " is a pre-defined intrinsic function, please remove your re-definition");
        }
    }
}

void PipesAssembler::compileTopLevel(Expression &topLevel) {
    auto globalParameters = fillParameterOffsets(topLevel);
    enterAndPushRdxAndCopyCurrentBufferPositionToRDX();
    emitPipesRelocation();
    emitRbpReset();
    if (paranoidRegisterSaving) {
        emitPushLocal(Register64::RDI);
        emitPushLocal(Register64::RSI);
//        emitPushLocal(Register64::RDX); pushed++; // already saved by enterAndPushRdxAndCopyCurrentBufferPositionToRDX()
        emitPushLocal(Register64::RCX);;
        // not saving r8-r9 because PipesAssembler doesn't use those at the moment
        emitPushLocal(Register64::R10);
        emitPushLocal(Register64::R11);
    }
    emitPushLocal(Register64::RBX);
    emitPushLocal(Register64::R12); // not saving r13-r15 because PipesAssembler doesn't use those at the moment


    auto heapSizeInBytes = 100 * 1024 * 1024 * STACK_WIDTH_IN_BYTES;
    emitAllocate(heapSizeInBytes);  // TODO: this just breaks for big programs.
                                    //       Either allocate each closure individually or do
                                    //       something more elaborate to allocate more buffers
//    emitAllocateWithMalloc(heapSizeInBytes); // 8 MB
    assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R10); // r10 keeps the start of our heap
    assembler.emit_mov_reg_to_reg(Register64::R10, Register64::R11); // r11 keeps the moving end of our heap

    assembler.emit_mov_reg_to_reg(Register64::RDI, Register64::RAX);   // TODO remove. just for debugging. use enter(param.size()), use move(RDI to RBP-8)
    int i = 0;
    while (true) {
        emitPushData(Register64::RAX);
        ++i;
        if (i >= globalParameters) {
            break;
        }
        assembler.emit_add(Register64::RAX, 2);
    }
    assembler.emit_mov_reg_to_reg(Register64::RDI, Register64::RAX);   // TODO remove. just for debugging

    {
        increaseScope(variablesPerScope);

        compileRecursive(topLevel);

        for (int j = 0; j < variablesPerScope.back(); ++j) {
            emitPopLocalDiscarding();
        }
        decreaseScope(variablesPerScope);
    }

    emitDeallocate(heapSizeInBytes);
//    emitDeallocateWithMalloc(Register64::R10);

    emitPopLocal(Register64::R12);
    emitPopLocal(Register64::RBX);

    if (paranoidRegisterSaving) {
        emitPopLocal(Register64::R11);
        emitPopLocal(Register64::R10);
        emitPopLocal(Register64::RCX);
//        emitPopLocal(Register64::RDX); // already saved by enterAndPushRdxAndCopyCurrentBufferPositionToRDX()
        emitPopLocal(Register64::RSI);
        emitPopLocal(Register64::RDI);
    }
//    assembler.emit_leave();
//    assembler.emit_mov_reg_to_reg(Register64::RBP, Register64::RSP);
    assembler.emit_pop(Register64::RDX);
    assembler.emit_pop(Register64::RBP);
    assembler.emit_return();
}

int PipesAssembler::fillParameterOffsets(Expression &expression) {
    int programArguments = 1;
    long offset = programArguments * STACK_WIDTH_IN_BYTES;
    parameterLocations[intrinsics::ARGUMENT] = offset;

    registerExpression(expression);
    topLevelExpression = expression;
    return parameterLocations.size();
}

void PipesAssembler::registerExpression(Expression &expression) {
    updateLambdaMaps(expression);
    updateClosures(expression);
}

void PipesAssembler::updateLambdaMaps(const Expression &expression) {
    auto lambdaIdStack = std::vector<LambdaId>{};
    recursivelyDo(expression, [this, &lambdaIdStack](const Expression &expr) {
        if (expr.getType() == ExpressionType::LAMBDA) {
            if (not lambdaIdStack.empty()) {
                childLambdas[lambdaIdStack.back()].insert(expr.getLambda().id);
                parentLambdas[expr.getLambda().id] = lambdaIdStack.back();
            }
            lambdaIdStack.push_back(expr.getLambda().id);
        }
    }, [&lambdaIdStack](const Expression &expr) {
        if (expr.getType() == ExpressionType::LAMBDA) {
            lambdaIdStack.pop_back();
        }
    });
}

void PipesAssembler::updateClosures(Expression &expression) {
    auto lambdaIdStack = std::vector<LambdaId>{};
    lambdaIdStack.clear();
    recursivelyRewrite(expression, [this, &lambdaIdStack](Expression &expr) {
        if (expr.getType() == ExpressionType::LAMBDA) {
            lambdaIdStack.push_back(expr.getLambda().id);
        }
        if (expr.getType() == ExpressionType::PARAMETER) {
            if (lambdaIdStack.empty()) {
                throwError("logic error: parameter '" + expr.getIdentifier()
                        + "' found outside of all lambdas");
            }
            if (lambdaIdStack.back() != expr.getParameter().lambdaId) {
                long enclosingLambdaId = lambdaIdStack.back();
                addClosuresForParameter(expr, enclosingLambdaId);
                expr.getParameter().enclosingLambdaId = lambdaIdStack.back();
            }
        }
        if (expr.getType() == ExpressionType::BRANCH) {

        }
    }, [&lambdaIdStack](const Expression &expr) {
        if (expr.getType() == ExpressionType::LAMBDA) {
            lambdaIdStack.pop_back();
        }
    });
}

void PipesAssembler::addClosuresForParameter(const Expression &parameter, long enclosingLambdaId) {
    for (long i = 0; i < parameter.getParameter().closureLevel; ++i) {
        auto &closure = closuresByEnclosingLambda[enclosingLambdaId];
        bool notAccountedYet = closure.parameters.count(getParameterName(parameter)) == 0;
        if (notAccountedYet) {
            closure.parameters[getParameterName(parameter)] =
                    {parameter.getParameter().lambdaId,
                    (2 + closure.argumentCount) * STACK_WIDTH_IN_BYTES};
            closure.argumentCount++;
            closure.enclosingLambda = enclosingLambdaId;
//            closure.definingLambda = parameter.parameter.lambdaId;
//            closuresByDefiningLambda[closure.definingLambda]. = closure;
        }
        auto parentLambdaIter = parentLambdas.find(enclosingLambdaId);
        if (parentLambdaIter != parentLambdas.end()) {
            enclosingLambdaId = parentLambdaIter->second;
        } else {
            if (i < parameter.getParameter().closureLevel) {
                throwError("logic error: lambda " + std::to_string(enclosingLambdaId) + " should have a parent");
            }
            break;
        }
    }
}

std::string PipesAssembler::getParameterName(const Expression &parameter) const {
    if (parameter.getType() == ExpressionType::PARAMETER) {
//        if (parameter.name == CHURCH_TRUE_PARAM or parameter.name == CHURCH_FALSE_PARAM) {
//            return parameter.name;
//        } else {
        return parameter.getParameter().name + ':'
                + std::to_string(parameter.getParameter().lambdaId);
//        }
    } else {
        throwError("argument should be a ExpressionType::Parameter, but is: " + to_string(parameter));
    }
    throwError("logic error, shouldn't reach here");
    return "";
}

/**
 * The (code) buffer start is stored in RIP when our memory block is called.
 * Do a call, which puts the next position in the stack, and then read the RBP
 */
void PipesAssembler::enterAndPushRdxAndCopyCurrentBufferPositionToRDX() {
    label_t afterCall{0, 0, false, false};
    label_t call{0, 0, false, false};
    assembler.emit_endbr64();
    assembler.emit_push(Register64::RBP);
    assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RBP);
//    assembler.emit_enter(0);
    assembler.emit_push(Register64::RDX);
    assembler.emit_call(&call);
    assembler.emit_jump(&afterCall);

    assembler.bind_label(&call);
    assembler.emit_mov_memRbp_to_reg(-2 * STACK_WIDTH_IN_BYTES, Register64::RDX);
    int8_t bytesInInstructionEndbr64 = 4;
    int8_t bytesInInstructionPushLowerReg = 1;
    int8_t bytesInInstructionMovRegReg = 3;
//    int8_t bytesInInstructionEnter = 4;
    int8_t bytesInInstructionCall = 5;
//    assembler.emit_sub(Register64::RDX,
//                       bytesInInstructionPushLowerReg +
//                               bytesInInstructionEnter +
//                               bytesInInstructionCall);
    assembler.emit_sub(Register64::RDX,
                       bytesInInstructionEndbr64 +
                               bytesInInstructionPushLowerReg +
                               bytesInInstructionPushLowerReg +
                               bytesInInstructionMovRegReg +
                               bytesInInstructionCall);
    assembler.emit_return();
    assembler.bind_label(&afterCall);
}

void PipesAssembler::emitRbpReset() {
    assembler.emit_mov_imm64_to_reg(Register64::RBP, 0);
}

auto PipesAssembler::addGlobalParameter(const std::string &parameterName)
        -> PipesAssembler::OffsetFromHeapStartInBytes {
    auto iter = parameterLocations.find(parameterName);
    bool present = iter != parameterLocations.end();
    if (not present) {
        auto offset = STACK_WIDTH_IN_BYTES * (1 + parameterLocations.size());
        parameterLocations[parameterName] = offset;
        return offset;
    }
    return iter->second;
}

int PipesAssembler::getParameterGlobalOffset(const Expression &parameter) {
    return getParameterGlobalOffset(getParameterName(parameter));
}

int PipesAssembler::getParameterGlobalOffset(const Lambda &lambda) {
    return getParameterGlobalOffset(getParameterNameOfLambda(lambda));
}

std::string PipesAssembler::getParameterNameOfLambda(const Lambda &lambda) const {
//    if (lambda.argumentName == CHURCH_TRUE_PARAM or lambda.argumentName == CHURCH_FALSE_PARAM) {
//        return lambda.argumentName;
//    } else {
        return lambda.argumentName + ':' + std::to_string(lambda.id);
//    }
}

int PipesAssembler::getParameterGlobalOffset(const std::string &parameterName) {
    auto found = parameterLocations.find(parameterName);
    if (found == parameterLocations.end()) {
        throwError("can't find location of parameter \"" + parameterName
                           + "\". Available locations: " + randomize::utils::to_string(parameterLocations));
    }
    auto offset = found->second;
    return offset;
}

auto PipesAssembler::getClosureByEnclosingLambda(const Lambda &lambda) -> const Closure & {
    return getClosureByEnclosingLambda(lambda.id);
}

auto PipesAssembler::getClosureByEnclosingLambda(EnclosingLambdaId lambdaId) -> const Closure & {
    static Closure closure{0, {}};
    auto iter = closuresByEnclosingLambda.find(lambdaId);
    if (iter == closuresByEnclosingLambda.end()) {
        return closure;
    }
    return iter->second;
}

auto PipesAssembler::getParameterClosureOffset(LambdaId lambdaId, const ParameterName &paramenterName)
        -> BytesToClosureStart {
    auto closureIter = closuresByEnclosingLambda.find(lambdaId);
    if (closureIter != closuresByEnclosingLambda.end()) {
        auto offsetIter = closureIter->second.parameters.find(paramenterName);
        if (offsetIter != closureIter->second.parameters.end()) {
            return offsetIter->second.offset;
        } else {
            std::stringstream ss;
            ss << "logic error: can't get the offset of parameter " << paramenterName << " in lambda "
               << lambdaId
               << " because the closure of that lambda doesn't have that parameter."
               << " This is the closure: "
               << closureIter->second;
            throwError(ss.str());
        }
    } else {
        std::stringstream ss;
        ss << "logic error: can't get the offset of parameter " << paramenterName << " in lambda "
           << lambdaId << ". The whole closure is missing."
           << " These are all the closures available: "
           << randomize::utils::to_string(closuresByEnclosingLambda);
        throwError(ss.str());
    }
    throw std::exception{}; // silence warning
}

// TODO: include a stack of identifiers to print codepos
void PipesAssembler::throwSemanticError(const std::string &message) const {
    throw std::runtime_error{
            "Uncompilable expression: " + to_string(topLevelExpression)
            + (message.empty()? std::string{""} : "\nreason: " + message)};
}

void PipesAssembler::throwSemanticError(const std::string &message, const Expression &expression) const {
    throw std::runtime_error{
            "Uncompilable expression (line "
            + std::to_string(expression.position().line+1) + "): " + to_string(expression)
            + (message.empty()? std::string{""} : "\nreason: " + message )};
}

void PipesAssembler::throwError(const std::string &message) const {
    throwError(message, topLevelExpression);
}

void PipesAssembler::throwError(const std::string &message, const Expression &expression) const {
    throw compilerBug(message, expression);
}

bool PipesAssembler::isIdentifier(const Expression &actual, const std::string &expected) {
    return actual.getType() == ExpressionType::IDENTIFIER
                    and actual.getIdentifier() == expected;
}

bool PipesAssembler::isLambdaIdentity(const Expression &expression) {
    return expression.getType() == ExpressionType::LAMBDA
            and expression.getLambda().body().getType() == ExpressionType::PARAMETER
            and expression.getLambda().argumentName == expression.getLambda().body().getIdentifier()
//                    and expression.getLambda().body().nestingLevel == 0
            ;
}

bool PipesAssembler::isLambdaIdentity(const Lambda &lambda) {
    return lambda.body().getType() == ExpressionType::PARAMETER
            and lambda.argumentName == lambda.body().getIdentifier()
//                    and lambda.body().nestingLevel == 0
            ;
}

bool PipesAssembler::isChurchTrue(const Lambda &lambda) const {
    return lambda.body().getType() == ExpressionType::LAMBDA
            and lambda.body().getLambda().body().getType() == ExpressionType::PARAMETER
            and lambda.body().getLambda().body().getParameter().closureLevel == 1;
}

void PipesAssembler::generateExecutable(std::string path) const {
    ::generateExecutable(std::move(path), assembler.getBuffer(), relocationStart.value());
}

auto operator<<(std::ostream &out, const PipesAssembler::Closure &closure) -> std::ostream & {
    return out << "{ args: " << closure.argumentCount << ", offsets: "
               << randomize::utils::to_string(closure.parameters);
}

auto operator<<(std::ostream &out, const PipesAssembler::ParameterInstance &parameter) -> std::ostream & {
    return out << "{lambdaId: " << parameter.lambdaId << ", offset: " << parameter.offset << "}";
}
