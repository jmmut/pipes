

// source: https://www.hanshq.net/ones-and-zeros.html
// but that blog only describes 32-bit x86, and I wanted to do 64-bit

#include "Assembler.h"
#include <fstream>


using randomize::exception::StackTracedException;

void Assembler::emit_byte64(uint64_t byte)
{
    if (byte > UINT8_MAX) {
        throw StackTracedException{"int64_t byte has value " + std::to_string(byte) + ", which doesn't fit in 8 bits"};
    }
    emit_byte(byte);
    if (buffer.size >= buffer.capacity) {
        throw StackTracedException{"buffer is full (" + std::to_string(buffer.size) + " bytes)"};
    }
    buffer.data[buffer.size++] = (Byte) byte;
}

void Assembler::emit_byte(uint8_t byte)
{
    if (buffer.size >= buffer.capacity) {
        throw StackTracedException{"buffer is full (" + std::to_string(buffer.size) + " bytes)"};
    }
    buffer.data[buffer.size++] = byte;
}

void Assembler::emit_int16(uint16_t value)
{
    /* Emit value in little-endian order. */
    uint64_t lowestByteMask = 0xff;
    emit_byte((value >>  0u) & lowestByteMask);
    emit_byte((value >>  8u) & lowestByteMask);
}

void Assembler::emit_int32(uint32_t value)
{
    /* Emit value in little-endian order. */
    uint64_t lowestByteMask = 0xff;
    emit_byte((value >>  0u) & lowestByteMask);
    emit_byte((value >>  8u) & lowestByteMask);
    emit_byte((value >> 16u) & lowestByteMask);
    emit_byte((value >> 24u) & lowestByteMask);
}

void Assembler::emit_int64(uint64_t value)
{
    /* Emit value in little-endian order. */
    uint64_t lowestByteMask = 0xff;
    emit_byte((value >>  0u) & lowestByteMask);
    emit_byte((value >>  8u) & lowestByteMask);
    emit_byte((value >> 16u) & lowestByteMask);
    emit_byte((value >> 24u) & lowestByteMask);
    emit_byte((value >> 32u) & lowestByteMask);
    emit_byte((value >> 40u) & lowestByteMask);
    emit_byte((value >> 48u) & lowestByteMask);
    emit_byte((value >> 56u) & lowestByteMask);
}

void Assembler::emit_xor(Register src, Register dst)
{
    /* 0011 000 | w=1 | 11 | src(3) | dst(3) */
    emit_byte(0x31);
    emit_byte((0x3u << 6u) | (static_cast<uint8_t>(src) << 3u) | static_cast<uint8_t>(dst));
}

void Assembler::emit_xor(Register64 src, Register64 dst)
{
    /* REX.W = 0100 1R0B | 0011 000 | w=1 | 11 | src(3) | dst(3) */
    int8_t R = static_cast<int8_t>(src) >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    int8_t B = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | R | B);
    emit_byte(0x31);
    emit_byte((0x3u << 6u) | ((0x7 & static_cast<uint8_t>(src)) << 3u) | (0x7 & static_cast<uint8_t>(dst)));
}

void Assembler::emit_set_to_zero(Register64 dst) {
    emit_xor(dst, dst);
}

void Assembler::emit_mov_imm32_to_reg(Register dst, int32_t value)
{
    if (value == 0) {
        emit_xor(dst, dst);
        return;
    }

    /* 1011 | w=1 | dst(3) | imm32 */
    emit_byte(0xB8u | static_cast<uint32_t>(dst));
    emit_int32(value);
}

void Assembler::emit_mov_imm64_to_reg(Register64 dst, int64_t value)
{
    if (value == 0) {
        emit_xor(dst, dst);
        return;
    }

    /* REX.W = 0100 1000 | 1011 | w=1 | dst(3) | imm64 */
    int8_t B = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | B);
    emit_byte(0xB8u | (0x7 &static_cast<uint8_t>(dst)));
    emit_int64(value);
}

//void Assembler::emit_mov_label_to_reg(Register64 dst, label_t *label)
//{
//    ssize_t my_addr = buffer.size;
//
//    /* REX.W = 0100 1000 | 1011 | w=1 | dst(3) | imm64 */
//    int8_t B = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
//    emit_byte(0x48 | B);
//    emit_byte(0xB8u | static_cast<uint8_t>(dst));
//
//    int64_t value = 0;
//
//    if (label->has_target) {
//        value = label->target_addr;
//    } else {
//        if (label->has_instr) {
//            throw StackTracedException{"Label already used!"};
//        }
//        label->instr_addr = my_addr;
//        label->has_instr = true;
//    }
//    emit_int64(value);
//}

void Assembler::emit_mov_reg_to_reg(Register src, Register dst)
{
    /* 1000 1001 | mod=11 | ModRM:r/m (w) = src (3 bits) | ModRM:reg (r) = dst (3 bits) */
    emit_byte(0x89u);
    emit_byte(0xC0 | (static_cast<int8_t>(src) << 3) | static_cast<uint8_t>(dst));
}

void Assembler::emit_mov_reg_to_reg(Register64 src, Register64 dst)
{
    /* REX.W =  0100 1R0B | 1000 1001 | mod=11 | ModRM:reg (r) = src (3 bits) | ModRM:r/m (w) = dst (3 bits) */
    int8_t R = static_cast<int8_t>(src) >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    int8_t B = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | R | B);
    emit_byte(0x89u);
    emit_byte(0xC0 | ((0x7 & static_cast<int8_t>(src)) << 3) | (0x7 & static_cast<uint8_t>(dst)));
}

void Assembler::emit_mov_mem_to_reg(Register src, int32_t displacement, Register dst)
{
    uint32_t mod, reg, rm;

    /* 1000 101 | w=1 | mod(2) | reg(3) | rm(3) | [sib(8)] | [displacement(8/32)] */
    emit_byte(0x8B);

    if (displacement == 0) {
        mod = 0x0;
    } else if (fitsIn8BitInt(displacement)) {
        mod = 0x1;
    } else {
        mod = 0x2;
    }

    rm = static_cast<uint8_t>(src);
    reg = static_cast<uint8_t>(dst);

    emit_byte(mod << 6u | (reg << 3u) | rm);

    if (src == Register::ESP) {
        /* Emit SIB (Scaled Index Byte). */
        /* ss=00 | index=100 | base=esp(3) */
        emit_byte((0x0u << 6u) | (0x4u << 3u) | static_cast<uint8_t>(Register::ESP));
    }

    if (mod == 0x1) {
        emit_byte((Byte)displacement);
    } else if (mod == 0x2) {
        emit_int32(displacement);
    }
}

void Assembler::emit_mov_mem_to_reg(Register64 src, int32_t displacementFromSrc, Register64 dst)
{
    uint32_t mod, reg, rm;

    /* REX.W = 0100 1RXB | 1000 101 | w=1 | mod(2) | reg(3) | rm(3) | [sib(8)] | [displacement(8/32)] */
    int8_t R = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    int8_t X = static_cast<int8_t>(src) >= static_cast<int8_t>(Register64::R8) ? 0x2 : 0x0;
    int8_t B = static_cast<int8_t>(src) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | R | X | B);
    emit_byte(0x8B);

    if (displacementFromSrc == 0) {
        mod = 0x0;
    } else if (fitsIn8BitInt(displacementFromSrc)) {
        mod = 0x1;
    } else {
        mod = 0x2;
    }

    rm = (0x7 & static_cast<uint8_t>(src));
    reg = (0x7 & static_cast<uint8_t>(dst));

    emit_byte(mod << 6u | (reg << 3u) | rm);

    if (src == Register64::RSP) {
        /* Emit SIB (Scaled Index Byte). */
        /* ss=00 | index=100 | base=esp(3) */
        emit_byte((0x0u << 6u) | (0x4u << 3u) | static_cast<uint8_t>(Register::ESP));
    }

    if (mod == 0x1) {
        emit_byte((Byte)displacementFromSrc);
    } else if (mod == 0x2) {
        emit_int32(displacementFromSrc);
    }
}

// TODO: there's a bug with using dst=R12
void Assembler::emit_mov_reg_to_mem(Register64 src, int32_t displacementFromDst, Register64 dst)
{
    uint32_t mod, reg, rm;

    /* REX.W = 0100 1RXB | 1000 100 | w=1 | mod(2) | reg(3) | rm(3) | [sib(8)] | [displacement(8/32)] */
    int8_t R = static_cast<int8_t>(src) >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
//    int8_t X = static_cast<int8_t>(dst) > static_cast<int8_t>(Register64::R8) ? 0x2 : 0x0;
    int8_t B = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | R /*| X*/ | B);
    emit_byte(0x89);

    if (displacementFromDst == 0) {
        mod = 0x0;
    } else if (fitsIn8BitInt(displacementFromDst)) {
        mod = 0x1;
    } else {
        mod = 0x2;
    }

    rm = (0x7 & static_cast<uint8_t>(dst));
    reg = (0x7 & static_cast<uint8_t>(src));

    if (rm == 0x5 and mod == 0) {
        // using mod=0 would mean a displacement32 instead of reg5 (RBP). use mod=1 and disp(8bits)=0 from reg5 or reg13
        mod = 0x1;
    }
    emit_byte(mod << 6u | (reg << 3u) | rm);

    if (dst == Register64::RSP) {
        /* Emit SIB (Scaled Index Byte). */
        /* ss=00 | index=100 | base=esp(3) */
        emit_byte(0b00'100'100);
//        emit_byte((0x0u << 6u) | (0x4u << 3u) | (0x7 & static_cast<uint8_t>(Register64::RSP)));
    }

    if (mod == 0x1) {
        emit_byte((Byte)displacementFromDst);
    } else if (mod == 0x2) {
        emit_int32(displacementFromDst);
    }
}

void Assembler::emit_mov_memRbp_to_reg(int8_t displacementFromRBP, Register64 dst)
{
    int8_t R = static_cast<int8_t>(dst) >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    auto dst32 = 0x7 & static_cast<int8_t>(dst);

    /* REX.W = 0100 1R00 | 1000 1011 | mod(2) = 01 | reg(3) | rm(3) = 101 | displacement(8 bits) */
    emit_byte(0x48 | R);
    emit_byte(0x8B);
    emit_byte(0x40 | (dst32 << 3) | 0x5);
    emit_byte(displacementFromRBP);
}

void Assembler::emit_mov_mem_rax(int64_t displacement)
{
    throw randomize::exception::StackTracedException{"unimplemented. current code doesn't work."};
    /* REX.W = 0100 1000 | 1010 0001 | displacement(64 bits) */
    emit_byte(0x48);
    emit_byte(0xa1);
    emit_int64(displacement);
}

void Assembler::emit_test(Register reg1, Register reg2)
{
    /* 1000 010 | w=1 | 11 | reg1(3) | reg2(3) */
    emit_byte(0x85);
    emit_byte((0x3u << 6u) | (static_cast<uint8_t>(reg1) << 3u) | static_cast<uint8_t>(reg2));
}

void Assembler::emit_xchg(Register reg1, Register reg2)
{
    if (reg1 == Register::EAX) {
        /* 1001 | 0 | reg2 */
        emit_byte((0x9u << 4u) | (0x0u << 3u) | static_cast<uint8_t>(reg2));
        return;
    }

    if (reg2 == Register::EAX) {
        emit_xchg(reg2, reg1);
        return;
    }

    /* 1000 011 | w=1 | 11 | reg1(3) | reg2(3) */
    emit_byte(0x87);
    emit_byte((0x3u << 6u) | (static_cast<uint8_t>(reg1) << 3u) | static_cast<uint8_t>(reg2));
}

void Assembler::emit_add(Register src, Register dst)
{
    /* 0000 000 | w=1 | 11 | src(3) | dst(3) */
    emit_byte(0x01);
    emit_byte((0x3u << 6u) | (static_cast<uint8_t>(src) << 3u) | static_cast<uint8_t>(dst));
}
void Assembler::emit_add(Register64 src, Register64 dst)
{
    /* REX.W =  0100 1R0B | 0000 000 | w=1 | 11 | reg=src(3) | r/m=dst(3) */
    auto reg = static_cast<int8_t>(src);
    int8_t R = reg >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    reg &= 0x7;

    auto rm = static_cast<int8_t>(dst);
    int8_t B = rm >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    rm &= 0x7;

    emit_byte(0x48 | R | B);
    emit_byte(0x01);
    emit_byte((0x3u << 6u) | (reg << 3u) | rm);
}

void Assembler::emit_add(Register64 reg, int8_t addition) {
    /* REX.W =  0100 1000 | 1000 0011 | Mod=11 Reg=000 RM=reg | addition (8 bits) */
    int8_t B = static_cast<int8_t>(reg) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | B);
    emit_byte(0x83);
    emit_byte(0xc0 | (0x7 & static_cast<int8_t>(reg)));
    emit_byte(addition);
}

void Assembler::emit_sub(Register64 minuendDst, Register64 subtrahend)
{
    // REX.W + 29 /r, SUB r/m64, r64
    auto reg = static_cast<int8_t>(subtrahend);
    int8_t R = reg >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    reg &= 0x7;

    auto rm = static_cast<int8_t>(minuendDst);
    int8_t B = rm >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    rm &= 0x7;

    emit_byte(0x48 | R | B);
    emit_byte(0x29);
    emit_byte((0x3u << 6u) | (reg << 3u) | rm);
}

void Assembler::emit_sub_1(Register reg) {
    emit_sub(reg, 1);
}
void Assembler::emit_sub_1(Register64 reg) {
    emit_sub(reg, 1);
}

void Assembler::emit_sub(Register reg, int8_t substract) {
    /* 1000 0011 | Mod=11 Reg=101 RM=reg | substract (8 bits) */
    emit_byte(0x83);
    emit_byte(0xe8 | static_cast<int8_t>(reg));
    emit_byte(substract);
}

void Assembler::emit_sub(Register64 reg, int8_t substract) {
    /* REX.W =  0100 100B | 1000 0011 | Mod=11 Reg=101 RM=reg | substract (8 bits) */
    int8_t B = static_cast<int8_t>(reg) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | B);
    emit_byte(0x83);
    emit_byte(0xe8 | (0x7 & static_cast<int8_t>(reg)));
    emit_byte(substract);
}

void Assembler::emit_mult_RAX_into_RDX_RAX(Register64 source) {
    //REX.W + F7 /5,IMUL r/m64

    auto rm = static_cast<int8_t>(source);
    int8_t B = rm >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    rm &= 0x7;

    emit_byte(0x48 | B);
    emit_byte(0xF7);
    emit_byte((0x3u << 6u) | (5 << 3u) | rm);
}

/**
 * The input dividend is in RDX:RAX.
 * the result quotient will be stored in RAX, and the remainder in RDX.
 */
void Assembler::emit_div_RDX_RAX_into_RAX_RDX(Register64 divisor) {
    //REX.W + F7 /6 DIV r/m64

    auto rm = static_cast<int8_t>(divisor);
    int8_t B = rm >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    rm &= 0x7;

    emit_byte(0x48 | B);
    emit_byte(0xF7);
    emit_byte((0x3u << 6u) | (6 << 3u) | rm);
}

void Assembler::emit_loop(label_t *label)
{
    /* 1110 0010 | displacement(8 bits) */

    ssize_t my_addr = buffer.size;
    ssize_t displacement = 0;

    if (label->has_target) {
        displacement = label->target_addr - (my_addr + 2);
    } else {
        if (label->has_instr) {
            throw StackTracedException{"Label already used!"};
        }
        label->instr_addr = my_addr;
        label->has_instr = true;
    }

    if (not fitsIn8BitInt(displacement)) {
        throw StackTracedException{"Value doesn't fit in an int8: displacement=" + std::to_string(displacement)};
    }
    emit_byte(0xe2);
    emit_byte((Byte) displacement);
}

bool Assembler::fitsIn8BitInt(ssize_t displacement) {
    return displacement >= INT8_MIN && displacement <= INT8_MAX;
}
bool Assembler::fitsIn32BitInt(ssize_t displacement) {
    return displacement >= INT32_MIN && displacement <= INT32_MAX;
}

void Assembler::emit_jcc(CC cc, label_t *label)
{
    /* 0000 1111 1000 | cc(4) | displacement(32) */

    ssize_t my_addr = buffer.size;
    ssize_t displacement = 0;

    if (label->has_target) {
        displacement = label->target_addr - (my_addr + 6);
    } else {
        if (label->has_instr) {
            throw StackTracedException{"Label already used!"};
        }
        label->instr_addr = my_addr;
        label->has_instr = true;
    }

    if (!(displacement >= INT32_MIN && displacement <= INT32_MAX)) {
        throw StackTracedException{"Value doesn't fit in an int32: displacement=" + std::to_string(displacement)};
    }
    emit_byte(0x0f);
    emit_byte((0x8u << 4u) | static_cast<uint32_t>(cc));
    emit_int32((uint32_t) displacement);
}

void Assembler::emit_jump(label_t *label)
{
    /* 1110 1001 | displacement(32) */

    ssize_t my_addr = buffer.size;
    ssize_t displacement = 0;

    if (label->has_target) {
        displacement = label->target_addr - (my_addr + 5);
    } else {
        if (label->has_instr) {
            throw StackTracedException{"Label already used!"};
        }
        label->instr_addr = my_addr;
        label->has_instr = true;
    }

    if (!(displacement >= INT32_MIN && displacement <= INT32_MAX)) {
        throw StackTracedException{"Value doesn't fit in an int32: displacement=" + std::to_string(displacement)};
    }
    emit_byte(0xe9);
    emit_int32((uint32_t) displacement);
}

/* Bind label to the current address and update any instruction that uses it. */
void Assembler::bind_label(label_t *label)
{
    if (label->has_target) {
        throw StackTracedException{"Label already bound!"};
    }

    auto labelAddress = static_cast<ssize_t>(buffer.size);
    label->target_addr = labelAddress;
    label->has_target = true;

    if (label->has_instr) {
        auto opcodeSizeInBytes = 0;
        auto displacementSizeInBytes = 0;
        auto instructionName = std::string{};
        /* Update the jump instruction with the displacement. */
        switch (buffer.data[label->instr_addr]) {
        case 0xe2:
            instructionName = "loop";
            opcodeSizeInBytes = 1;
            displacementSizeInBytes = 1;
            break;
        case 0x74:
            instructionName = "jump_if_zero short";
            opcodeSizeInBytes = 1;
            displacementSizeInBytes = 1;
            break;
        case 0x0f:
            instructionName = "jump_if_zero near";
            opcodeSizeInBytes = 2;
            displacementSizeInBytes = 4;
            break;
        case 0xe9:
            instructionName = "jmp";
            opcodeSizeInBytes = 1;
            displacementSizeInBytes = 4;
            break;
        case 0xe8:
            instructionName = "call";
            opcodeSizeInBytes = 1;
            displacementSizeInBytes = 4;
            break;
        default:
            //if (((buffer.data[label->instr_addr] & 0xf8) == 0x48)
            //        and (buffer.data[label->instr_addr+1] & 0xf8) == 0xb8) {
            //    instructionName = "mov imm64 to reg";
            //    opcodeSizeInBytes = 2;
            //    displacementSizeInBytes = 8;
            //} else {
            throw StackTracedException{"Binding label to unknown jump."};
            //}
        }

        auto totalInstructionSize = opcodeSizeInBytes + displacementSizeInBytes;
        auto displacement = labelAddress - (label->instr_addr + totalInstructionSize);

        auto orig_buf_size = buffer.size;
        buffer.size = label->instr_addr + opcodeSizeInBytes;
        if (displacementSizeInBytes == 1) {
            if (not fitsIn8BitInt(displacement)) {
                throw StackTracedException{"Value for instruction \"" + instructionName
                                                   + "\" doesn't fit in an int8: displacement="
                                                   + std::to_string(displacement)};
            }
            emit_byte(displacement);
        } else if (displacementSizeInBytes == 4) {
            if (not fitsIn32BitInt(displacement)) {
                throw StackTracedException{"Value for instruction \"" + instructionName
                                                   + "\" doesn't fit in an int32: displacement="
                                                   + std::to_string(displacement)};
            }
            emit_int32(displacement);
        } else {
            throw StackTracedException{
                    "logic error: unhandled displacement size: " + std::to_string(displacementSizeInBytes)};
        }
        buffer.size = orig_buf_size;
    }
}

/**
 * CALL - Call Procedure
 */
void Assembler::emit_call(label_t *func) {
    /* 1110 1000 | int32 dst */

    ssize_t my_addr = buffer.size;
    ssize_t displacement = 0;

    if (func->has_target) {
        displacement = func->target_addr - (my_addr + 5);
    } else {
        if (func->has_instr) {
            throw StackTracedException{"Label already used!"};
        }
        func->instr_addr = my_addr;
        func->has_instr = true;
    }

    if (!(displacement >= INT32_MIN && displacement <= INT32_MAX)) {
        throw StackTracedException{"Value doesn't fit in an int32: displacement=" + std::to_string(displacement)};
    }

    emit_byte(0xe8);
    emit_int32(displacement);
}

/**
 * CALL - Call Procedure
 */
void Assembler::emit_call(Register ptr) {
    /* 1111 1111 | ModRM:r/m (r) = 11 010 | reg ( 3 bits)  */

    emit_byte(0xff);
    emit_byte(0xd0 | static_cast<int8_t>(ptr));
}
/**
 * CALL - Call Procedure
 */
void Assembler::emit_call(Register64 ptr) {
    /* 1111 1111 | ModRM:r/m (r) = 11 010 | reg ( 3 bits)  */

    if (static_cast<int8_t>(ptr) >= static_cast<int8_t>(Register64::R8)) {
        throw randomize::exception::StackTracedException{"using R8-R15 registers is unimplemented"};
    }
    emit_byte(0xff);
    emit_byte(0xd0 | static_cast<int8_t>(ptr));
}

void Assembler::emit_return() {
    /* 1100 0011 */
    emit_byte(0xC3);
}

/**
 * ENTER - Make Stack Frame for Procedure Parameters
 */
void Assembler::emit_enter(uint16_t bytesInFutureFrame) {
    /*  1100 1000 | frameStorage(16) | nesting(8) */
    emit_byte(0xC8); // enter
    emit_int16(bytesInFutureFrame);
    emit_byte(0x00); // no nested scopes for now
}

/**
 * LEAVE - High Level Procedure Exit
 */
void Assembler::emit_leave() {
    /*  1100 1001  */
    emit_byte(0xC9);
}

void Assembler::emit_push(Register64 reg) {
    /* 50+rd, PUSH r64 */
    if (static_cast<int8_t>(reg) >= static_cast<int8_t>(Register64::R8)) {
        // REX.WB for registers R8 to R15
        uint8_t B = 1;
        emit_byte(0x48 | B);
    }

    emit_byte(0x50 | (0x7 & static_cast<int8_t>(reg)));
}

void Assembler::emit_push(int32_t immediate) {
    /* 0110 1000 | immediate(32 bits) */
    emit_byte(0x68);
    emit_int32(immediate);
}

void Assembler::emit_pop(Register64 reg) {
    /* 58+rd, POP r64. opcode + rd (w) */
    if (static_cast<int8_t>(reg) >= static_cast<int8_t>(Register64::R8)) {
        // REX.EB for registers R8 to R15
        uint8_t B = 1;
        emit_byte(0x48 | B);
    }
    emit_byte(0x58 | (0x7 & static_cast<int8_t>(reg)));
}
void Assembler::emit_nop(std::array<char, 5> message) {
    emit_byte(0x0f);
    emit_byte(0x1f);
    emit_byte(0x84);
    for(const auto &letter : message) {
        emit_byte(letter);
    }
}

void Assembler::emit_nop() {
    emit_byte(0x90);
}

void Assembler::emit_shift_left(Register64 reg, uint8_t n) {
    // REX.W + C1 /4 ib, SHL r/m64, imm8
    int8_t B = static_cast<int8_t>(reg) >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | B);
    emit_byte(0xC1);
    emit_byte(0xC0 | (0x4 << 3) | (0x7 & static_cast<uint8_t>(reg)));
    emit_byte(n);
}

/* bit-wise "and", discarding the result. just to set up EFLAGS. */
void Assembler::emit_test(Register64 first, Register64 second) {
    /* REX.W + 85 /r, TEST r/m64, r64 */
    auto reg = static_cast<uint8_t>(first);
    auto rm = static_cast<uint8_t>(second);

    int8_t R = reg >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    int8_t B = rm >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | R | B);
    emit_byte(0x85);
    emit_byte(0xC0 | ((0x7 & reg) << 3) | (0x7 & rm));
}

/** sets the EFLAGS of the expression (second - first) */
void Assembler::emit_compare(Register64 first, Register64 second) {
    /* REX.W + 3B /r, CMP r64, r/m64 */
    auto reg = static_cast<uint8_t>(first);
    auto rm = static_cast<uint8_t>(second);

    int8_t R = reg >= static_cast<int8_t>(Register64::R8) ? 0x4 : 0x0;
    int8_t B = rm >= static_cast<int8_t>(Register64::R8) ? 0x1 : 0x0;
    emit_byte(0x48 | R | B);
    emit_byte(0x3B);
    emit_byte(0xC0 | ((0x7 & reg) << 3) | (0x7 & rm));
}

/**
 * puts the EFLAGS of 'greater than' into RAX
 * @see emit_compare
 */
void Assembler::emit_gt() {
    emit_byte(0x0f);
    emit_byte(0x9f);
    emit_byte(0xC0);
}


void Assembler::emit_jump_if_zero(label_t *label) {
    ssize_t my_addr = buffer.size;
    ssize_t displacement = 0;

    if (label->has_target) {
        displacement = label->target_addr - (my_addr + 5);
    } else {
        if (label->has_instr) {
            throw StackTracedException{"Label already used!"};
        }
        label->instr_addr = my_addr;
        label->has_instr = true;
    }

    // we don't know if the disp will fit unless the label was already bound
    if (label->has_target and fitsIn8BitInt(displacement)) {
        /* 74 cb, JZ rel8 */
        emit_byte(0x74);
        emit_byte(displacement);
    } else {
        /* 0F 84 cd, JZ rel32 */
        emit_byte(0x0F);
        emit_byte(0x84);
        emit_int32(displacement);
    }
}

void Assembler::emit_syscall() {
    emit_byte(0x0f);
    emit_byte(0x05);
}

/** Move RCX quadwords (RCX * 64 bits) from [RSI] to [RDI].*/
void Assembler::emit_copy_range() {
    //F3 REX.W A5
    emit_byte(0xF3);
    emit_byte(0x48);
    emit_byte(0xA5);
}

/** Terminate an indirect branch */
void Assembler::emit_endbr64() {
    emit_byte(0xF3);
    emit_byte(0x0F);
    emit_byte(0x1E);
    emit_byte(0xFA);
}
