/**
 * @file VariableScopeTracker.cpp
 * @date 2022-09-12
 */

#include "VariableScopeTracker.h"

#include <utility>

VariableScopeTracker::VariableScopeTracker(ExpressionType type,
                                           std::vector<int> &variablesPerScope,
                                           std::function<void()> actionWhenScopeEnds)
        : variableScopeDefined{definesScope(type)}, variablesPerScope{variablesPerScope},
          actionWhenScopeEnds{std::move(actionWhenScopeEnds)} {
    if (variableScopeDefined) {
        increaseScope(variablesPerScope);
    }
}

VariableScopeTracker::~VariableScopeTracker() {
    if (variableScopeDefined) {
        actionWhenScopeEnds();
        decreaseScope(variablesPerScope);
    }
}
