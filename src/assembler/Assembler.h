/**
 * @file Assembler.h
 * @date 2020-12-28
 */

// source: https://www.hanshq.net/ones-and-zeros.html
// but that blog only describes 32-bit x86, and I wanted to do 64-bit

#ifndef PIPES_ASSEMBLER_H
#define PIPES_ASSEMBLER_H

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <memory>
#include <array>
#include <vector>
#include <commonlibs/StackTracedException.h>
#include "executable/ExecutableMemory.h"


enum class Register : uint8_t {
    EAX = 0x0,
    ECX = 0x1,
    EDX = 0x2,
    EBX = 0x3,
    ESP = 0x4,
    EBP = 0x5,
    ESI = 0x6,
    EDI = 0x7
};

enum class Register64 : uint8_t {
    RAX = 0x0,
    RCX = 0x1,
    RDX = 0x2,
    RBX = 0x3,
    RSP = 0x4,
    RBP = 0x5,
    RSI = 0x6,
    RDI = 0x7,
    R8  = 0x8,
    R9  = 0x9,
    R10 = 0xa,
    R11 = 0xb,
    R12 = 0xc,
    R13 = 0xd,
    R14 = 0xe,
    R15 = 0xf,
};

enum class CC {
    E = 0x0,
    Z = 0x4
};

struct label_t {
    ssize_t target_addr;
    ssize_t instr_addr;
    bool has_target;
    bool has_instr;
};

class Assembler {
public:
    explicit Assembler(size_t codeSize) : buffer{codeSize} {}

    template <typename PROTOTYPE> PROTOTYPE * getEntryPoint() const {
        if (buffer.size == 0) {
            throw randomize::exception::StackTracedException{"Can't give an entry point to an empty program! You have "
                                                             "to emit some code with the Assembler::emit_* functions"};
        }
        return buffer.getEntryPoint<PROTOTYPE>();
    }

    void emit_return();
    void emit_xor(Register src, Register dst);
    void emit_xor(Register64 src, Register64 dst);
    void emit_set_to_zero(Register64 dst);
    void emit_mov_imm32_to_reg(Register dst, int32_t value);
    void emit_mov_imm64_to_reg(Register64 dst, int64_t value);
//    void emit_mov_label_to_reg(Register64 dst, label_t *label);
    void emit_mov_mem_to_reg(Register src, int32_t displacement, Register dst);
    void emit_mov_mem_to_reg(Register64 src, int32_t displacementFromSrc, Register64 dst);
    void emit_mov_memRbp_to_reg(int8_t displacementFromRBP, Register64 dst);
    void emit_mov_mem_rax(int64_t displacement);
    void emit_mov_reg_to_mem(Register64 src, int32_t displacementFromDst, Register64 dst);
    void emit_mov_reg_to_reg(Register src, Register dst);
    void emit_mov_reg_to_reg(Register64 src, Register64 dst);
    void emit_test(Register reg1, Register reg2);
    void emit_xchg(Register reg1, Register reg2);
    void emit_add(Register src, Register dst);
    void emit_add(Register64 src, Register64 dst);
    void emit_add(Register64 reg, int8_t addition);
    void emit_sub(Register64 minuendDst, Register64 subtrahend);
    void emit_sub(Register reg, int8_t substract);
    void emit_sub(Register64 reg, int8_t substract);
    void emit_sub_1(Register reg);
    void emit_sub_1(Register64 reg);
    void emit_mult_RAX_into_RDX_RAX(Register64 source);
    void emit_div_RDX_RAX_into_RAX_RDX(Register64 divisor);
    void emit_loop(label_t *label);
    void emit_jcc(CC cc, label_t *label);
    void emit_jump(label_t *label);
    void emit_enter(uint16_t bytesInFutureFrame);
    void emit_leave();
    void emit_call(label_t *func);
    void emit_call(Register ptr);
    void emit_call(Register64 ptr);
    void emit_push(Register64 ptr);
    void emit_push(int32_t immediate);
    void emit_pop(Register64 ptr);
    void emit_nop(std::array<char, 5> message);
    void emit_nop();
    void emit_shift_left(Register64 reg, uint8_t n);
    void emit_test(Register64 first, Register64 second);
    void emit_jump_if_zero(label_t *label);
    void emit_syscall();
    void emit_compare(Register64 first, Register64 second);
    void emit_gt();
    void emit_endbr64();

    void bind_label(label_t *label);

    void emit_byte(uint8_t byte);
    void emit_byte64(uint64_t byte);
    void emit_int16(uint16_t value);
    void emit_int32(uint32_t value);
    void emit_int64(uint64_t value);

    void printMachineCode() const {
        printf("Code: ");
        for (decltype(buffer.size) i = 0; i < buffer.size; i++) {
            printf("%02x", buffer.data[i]);
        }
        printf("\n");
    }
    const ExecutableMemory &getBuffer() const { return buffer; }

    void emit_copy_range();

    static bool fitsIn8BitInt(ssize_t displacement);
    static bool fitsIn32BitInt(ssize_t displacement);
private:
    ExecutableMemory buffer;
};


void generate_fib_function(ExecutableMemory *buf);
void asm_mov_imm32(ExecutableMemory *buffer, Register dst, int32_t value);
void asm_ret(ExecutableMemory *buffer);

inline auto compile_return_42(Assembler &assembler) {
    assembler.emit_mov_imm32_to_reg(Register::EAX, 42);
    assembler.emit_return();
    return assembler.getEntryPoint<int()>();
}
inline void emit_return_42(ExecutableMemory *buf) {
    asm_mov_imm32(buf, Register::EAX, 42);
    asm_ret(buf);
}

inline auto compile_fibonacci(Assembler &assembler) {
    label_t end = {0, 0, false, false};
    label_t loop = {0, 0, false, false};

    assembler.emit_mov_reg_to_reg(Register::EDI, Register::ECX);
    assembler.emit_mov_imm32_to_reg(Register::EAX, 0);
    assembler.emit_mov_imm32_to_reg(Register::EDX, 1);
    assembler.emit_test(Register::ECX, Register::ECX);
//    assembler.emit_jcc(CC::Z, &end);
    assembler.emit_jump_if_zero(&end);

    assembler.bind_label(&loop);
    assembler.emit_xchg(Register::EAX, Register::EDX);
    assembler.emit_add(Register::EAX, Register::EDX);
    assembler.emit_loop(&loop);

    assembler.bind_label(&end);
    assembler.emit_return();
    return assembler.getEntryPoint<int32_t(int32_t)>();
}

struct ScopedRegisterSaver {
    ScopedRegisterSaver(Assembler &assembler, std::vector<Register64> registers)
            : assembler{assembler}, registers{std::move(registers)} {
        for (const auto &aRegister : this->registers) {
            assembler.emit_push(aRegister);
        }
    }
    virtual ~ScopedRegisterSaver() {
        for (auto reverseIter = registers.rbegin(); reverseIter != registers.rend(); ++reverseIter) {
            assembler.emit_pop(*reverseIter);
        }
    }
    Assembler &assembler;
    std::vector<Register64> registers;
};

#endif //PIPES_ASSEMBLER_H
