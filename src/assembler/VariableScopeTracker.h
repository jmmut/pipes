/**
 * @file VariableScopeTracker.h
 * @date 2022-09-12
 */

#ifndef PIPES_VARIABLESCOPETRACKER_H
#define PIPES_VARIABLESCOPETRACKER_H

#include <functional>
#include <vector>
#include "expression/ExpressionType.h"

class VariableScopeTracker {
public:
    explicit VariableScopeTracker(ExpressionType type,
                                  std::vector<int> &variablesPerScope,
                                  std::function<void()> actionWhenScopeEnds);
    virtual ~VariableScopeTracker();
private:
    bool variableScopeDefined;
    std::vector<int> &variablesPerScope;
    std::function<void()> actionWhenScopeEnds;
};

inline void increaseScope(std::vector<int> &variablesPerScope) {
    variablesPerScope.push_back(0);
}

inline void decreaseScope(std::vector<int> &variablesPerScope) {
    variablesPerScope.pop_back();
}

#endif //PIPES_VARIABLESCOPETRACKER_H
