

#ifndef PIPES_SYSCALLS_H
#define PIPES_SYSCALLS_H

#include "commonlibs/Walkers.h"

enum class Syscall {
    READ,
    WRITE,
    MMAP,
    MUNMAP
};
enum class Target {
    LINUX,
    MAC,
};

// maybe in the future we can cross compile easily by letting the user choose the target from the CLI
inline Target currentHost() {
#ifdef __APPLE__
    return Target::MAC;
#else
    return Target::LINUX;
#endif
}

inline std::map<Syscall, std::string> getSyscallNames() {
    std::map<Syscall, std::string> names;
    names[Syscall::READ] = "read";
    names[Syscall::WRITE] = "write";
    names[Syscall::MMAP] = "mmap";
    names[Syscall::MUNMAP] = "munmap";
    return names;
}
const std::map<Syscall, std::string> syscallNames = getSyscallNames();

inline std::string to_string(Syscall s) {
    if (contains(syscallNames, s)) {
        return syscallNames.at(s);
    } else {
        return "(syscall with code " + std::to_string(static_cast<int>(s)) + ")";
    }
}

inline std::map<Target, std::string> getTargetNames() {
    std::map<Target, std::string> names;
    names[Target::LINUX] = "linux";
    names[Target::MAC] = "mac";
    return names;
}
const std::map<Target, std::string> targetNames = getTargetNames();

inline std::string to_string(Target t) {
    if (contains(targetNames, t)) {
        return targetNames.at(t);
    } else {
        return "(target with code " + std::to_string(static_cast<int>(t)) + ")";
    }
}


/**
 * Syscall codes taken from
 * http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
 * https://opensource.apple.com/source/xnu/xnu-3789.41.3/bsd/kern/syscalls.master.auto.html
 */
inline int64_t getSyscallCode(Syscall syscall, Target target = currentHost()) {
    if (target == Target::LINUX) {
        switch (syscall) {
            case Syscall::READ:
                return 0;
            case Syscall::WRITE:
                return 1;
            case Syscall::MMAP:
                return 9;
            case Syscall::MUNMAP:
                return 11;
        }
        throw std::logic_error{"Unsupported syscall '" + to_string(syscall)
                               + "' for target OS '" + to_string(target) + "'"};
    } else if (target == Target::MAC) {
        switch (syscall) {
            case Syscall::READ:
                return 0x200'0003;
            case Syscall::WRITE:
                return 0x200'0004;
            case Syscall::MMAP:
                return 0x200'0000 + 197;
            case Syscall::MUNMAP:
                return 0x200'0000 + 73;
        }
        throw std::logic_error{"Unsupported syscall '" + to_string(syscall)
                               + "' for target OS '" + to_string(target) + "'"};
    } else {
        throw std::logic_error{"Unsupported syscall '" + to_string(syscall)
                + "' for target OS with code '" + to_string(target) + "': non-existent target"};
    }
}

const int64_t SYSCALL_STDIN = 0;
const int64_t SYSCALL_STDOUT = 1;
const int64_t SYSCALL_STDERR = 2;

#endif //PIPES_SYSCALLS_H
