/**
 * @file PipesAssemblerRecursive.cpp
 * @date 2021-05-05
 */

#include <cmath>
#include <commonlibs/RAII.h>
#include "commonlibs/Walkers.h"
#include "PipesAssembler.h"
#include "expression/ExpressionType.h"
#include "VariableScopeTracker.h"

// Note that this file only implements _some_ of PipesAssembler's methods.
// Mostly the compile* recursive, medium-level functions

auto PipesAssembler::compileRecursive(const Expression &expression) -> CompiledExpression {
//    auto tracker = VariableScopeTracker{expression.getType(), variablesPerScope, [this]() {
//        for (int i = 0; i < variablesPerScope.back(); ++i) {
//            emitPopLocalDiscarding();
//        }
//    }};
    switch (expression.getType()) {
    case ExpressionType::UNSET:
        throwError();
    case ExpressionType::NOTHING:
        throwSemanticError("unimplemented: compiling expression " + to_string(expression.getType()));
    case ExpressionType::VALUE:
        assembler.emit_mov_imm64_to_reg(Register64::RAX, expression.getValue());
        return nonReusableExpression(ExpressionType::VALUE);
    case ExpressionType::LAMBDA:
        return compileLambda(expression.getLambda(), expression.position());
    case ExpressionType::IDENTIFIER:
        return compileIdentifier(expression);
    case ExpressionType::FIELD:
        throwSemanticError("unimplemented: compiling expression " + to_string(expression.getType()));
    case ExpressionType::CALL:
        return compileCallRecursive(expression.getCall(), Register64::RAX, expression.position());
    case ExpressionType::PARAMETER:
        return compileParameter(expression);
    case ExpressionType::BRANCH:
    case ExpressionType::STRUCT:
        throwSemanticError("unimplemented: compiling expression " + to_string(expression.getType()));
        // TODO emitBranch doesn't simply work because if this result is used as function call,
        // after the branch is done, a call will jump to the wrong place
//        emitBranch(expression.getBranch());
//        return nonReusableExpression(ExpressionType::BRANCH);
    case ExpressionType::WHILE:
        return compileWhile(expression);
    case ExpressionType::BINARY_OPERATION:
        return compileBinaryOperation(expression);
    case ExpressionType::ARRAY:
        return compileArray(expression);
    case ExpressionType::TUPLE:
        return compileTuple(expression);
    case ExpressionType::VARIABLE:
        return compileVariable(expression);
    }
    throw compilerBug("missing case in switch", expression);
}

auto PipesAssembler::compileCallRecursive(const Call &call, Register64 dst,
                                          PositionInCode position) -> CompiledExpression {
    if (debug) {
        emitNopDebugString("\xca\x11\x44\x44");
    }

    auto compiledArgument = compileRecursive(call.argument());
    return compileCallFunctionRecursive(call, dst, compiledArgument, position);
}

auto PipesAssembler::compileCallFunctionRecursive(const Call &call, Register64 dst,
                                                                CompiledExpression compiledArgument,
                                                                PositionInCode position)
                                                  -> CompiledExpression {

//    Call& call = callExpression.as<ExpressionType::CALL>();
    Expression callExpression = call.asExpression(ExpressionType::CALL);
    auto throwError = [&callExpression, this](auto message) {this->throwError(message, callExpression);};
    const auto &function = call.function();
//    auto resolvedArgumentType = compiledArgument.type;
    auto callResultType = compiledArgument;
//    assembler.bind_label(&func);    // TODO don't bind if the function is not inlined
//    assembler.emit_enter(0);    // not needed because we are not using rbp nor rsp yet


    // TODO this is redundant with compileIntrinsicIdentifier, even if not identical
    if (isIdentifier(function, intrinsics::PRINT_LAST_DIGIT)) {
        emitPrintLastDigit();
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::PRINT_CHAR)) {
        emitPrintChar();
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::READ_CHAR)) {
        emitReadChar();
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::DECREMENT)) {
        assembler.emit_sub_1(Register64::RAX);
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::INCREMENT)) {
        assembler.emit_add(Register64::RAX, 1);
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::NEW_ARRAY)) {
        emitNewArray();
        callResultType = nonReusableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::DEREFERENCE)) {
        assembler.emit_mov_mem_to_reg(Register64::RAX, 0, Register64::RAX);
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::FREE)) {
        emitDeallocateWithMalloc(Register64::RAX);
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::PRINT_CURRENT_FUNCTION)) {
        emitPrintCurrentFunction();
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::PRINT_FUNCTION_STACK)) {
        emitPrintFunctionStack();
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (isIdentifier(function, intrinsics::PRINT_CALL_STACK)) {
        emitPrintCallStack();
        callResultType = unaddressableExpression(ExpressionType::VALUE);
    } else if (function.getType() == ExpressionType::CALL) {
        emitPushLocal(Register64::RSI);
//        emitPushLocal(Register64::RBX);
//        assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RBX);
        emitPushLocal(Register64::RAX); // push call argument (not current function parameter)

        auto futureFunction = compileCallRecursive(function.getCall(), Register64::RSI,
                                                   function.positionInCode);
        if (futureFunction.type == ExpressionType::LAMBDA or true) { // TODO do type checking properly
            emitPrepareToCallLambda();
            emitPopLocal(Register64::RAX);
            emitCall(Register64::RDI, position);
//            emitPopLocal(Register64::RBX);
            emitPopLocal(Register64::RSI);
//            emitRestoreAfterCall();
        } else {
            throwError("can only call identity lambdas or simple-call lambdas, call \""
                               + to_string(function)
                               + "\" resolves to a " + to_string(futureFunction.type));
        }
    } else if (function.getType() == ExpressionType::LAMBDA) {
        // TODO: don't push lambda arguments to the global pos if they are not closured?
//        int offset = getParameterGlobalOffset(function.lambda.operator*());
        if (debug) {
            emitNopDebugString("\xca\x11\x1a\xbd");
        }

        emitPushLocal(Register64::RSI);
        emitPushLocal(Register64::RAX);
        auto futureFunction = compileLambda(function.getLambda(), function.positionInCode);
        if (futureFunction.type == ExpressionType::LAMBDA or true) { // TODO do type checking properly
            if (debug) {
                emitNopDebugString("\x92\xe9\x1a\xbd");
            }
            assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RSI);
            emitPopLocal(Register64::RAX);
            assembler.emit_mov_mem_to_reg(Register64::RSI, 0, Register64::RDI);//actual lambda start
            assembler.emit_add(Register64::RDX, Register64::RDI);
//            emitPushLocal(Register64::RBX);
//            assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RBX);
//            emitPushLocal(Register64::RAX);

            emitCall(Register64::RDI, position);
//            emitPopLocalDiscarding();
//            emitPopLocal(Register64::RBX);
            emitPopLocal(Register64::RSI);
        } else {
            throwError("can only call identity lambdas or simple-call lambdas. lambda \""
                               + to_string(function)
                               + "\" resolves to a " + to_string(futureFunction.type));
        }
    } else if (function.getType() == ExpressionType::BRANCH) {
        compileBranch(function.getBranch());
    } else if (function.getType() == ExpressionType::PARAMETER) {
        emitPushLocal(Register64::RSI);
        compileParameter(function, Register64::RSI);
        emitPrepareToCallLambda();

//        emitPushLocal(Register64::RBX);
//        assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RBX);
//        emitPushLocal(Register64::RAX);

        emitCall(Register64::RDI, position);

//        emitPopLocalDiscarding();
//        emitPopLocal(Register64::RBX);
        emitPopLocal(Register64::RSI);
    } else if (function.getType() == ExpressionType::IDENTIFIER) {
        if (debug) {
            emitNopDebugString("call ident " + function.getIdentifier() + " from " +
                    codeLineToString(*this->currentCode, function.position()));
        }

        emitPushLocal(Register64::RSI);
        emitPushLocal(Register64::RAX);
        auto futureFunction = compileClosureLessIdentifier(function);
        if (futureFunction.type == ExpressionType::LAMBDA) {
            if (debug) {
                emitNopDebugString("\x92\xe9\x1d\xe7");
            }

            auto lambdaStart = label_t{futureFunction.innerLambdaLocation, 0,
                                       true, false};

            emitPopLocal(Register64::RAX);
            emitCall(&lambdaStart, position);
            emitPopLocal(Register64::RSI);
        } else {
            throwError("can only call identity lambdas or simple-call lambdas. lambda \""
                               + to_string(function)
                               + "\" resolves to a " + to_string(futureFunction.type));
        }
    } else if (function.getType() == ExpressionType::BINARY_OPERATION) {
        if (debug) {
            emitNopDebugString("\xb1\x09\xe2\xa7");
        }
        if (not contains({Token::FIELD_ACCESS, Token::STATEMENT_SEPARATOR},
                     function.getBinaryOperation().operation)) {
            throw semanticError("calling a '"
                                        + quote(to_string(function.getBinaryOperation().operation))
                                        + "' as a function is not supported", function);
        }

        emitPushLocal(Register64::RSI);
        emitPushLocal(Register64::RDI);
        emitPushLocal(Register64::RAX);
        auto futureFunction = compileBinaryOperation(function);
        if (futureFunction.type == ExpressionType::LAMBDA or true) { // TODO do type checking properly
            if (debug) {
                emitNopDebugString("\x92\xe9\x1d\xe7");
            }

            assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RSI);
            emitPopLocal(Register64::RAX);
            emitPrepareToCallLambda();

//        emitPushLocal(Register64::RBX);
//        assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RBX);
//        emitPushLocal(Register64::RAX);

            emitCall(Register64::RDI, position);
            emitPopLocal(Register64::RDI);
            emitPopLocal(Register64::RSI);
        } else {
            throwError("can only call identity lambdas or simple-call lambdas. lambda \""
                               + to_string(function)
                               + "\" resolves to a " + to_string(futureFunction.type));
        }
    } else {
        throwError("can only call lambdas or calls or an intrinsic function "
                   + randomize::utils::to_string(intrinsics::FUNCTIONS,
                                                 "(", ", ", "), but got a ")
                   + quote(to_string(function)) + " (" + to_string(function.getType()) + ")");
    }
//    assembler.emit_leave();     // not needed because we are not using rbp nor rsp yet
//    assembler.emit_return();
//    assembler.bind_label(&next);
    if (dst != Register64::RAX) { // TODO put this in every needed branch
        assembler.emit_mov_reg_to_reg(Register64::RAX, dst);
    }
    return callResultType;
}

void PipesAssembler::compileBranch(const Branch &branch) {
    auto tracker = VariableScopeTracker{ExpressionType::BRANCH, variablesPerScope, [this]() {
        for (int i = 0; i < variablesPerScope.back(); ++i) {
            emitPopLocalDiscarding();
        }
    }};
    assembler.emit_test(Register64::RAX, Register64::RAX);
    label_t zeroCase{0, 0, false, false};
    label_t end{0, 0, false, false};

    assembler.emit_jump_if_zero(&zeroCase);
    // non-zero case here
    compileRecursive(branch.yes());
    assembler.emit_jump(&end);

    assembler.bind_label(&zeroCase);
    // zero case here
    compileRecursive(branch.no());

    assembler.bind_label(&end);
}

auto PipesAssembler::compileLambda(const Lambda &lambda, const PositionInCode &position)
        -> CompiledExpression {
    if (debug) {
        emitDebugString("lambda at " + codeLineToString(*currentCode, position));
    }
    auto innerType = compileClosureHelper([&]() {
        auto tracker = VariableScopeTracker{ExpressionType::LAMBDA, variablesPerScope, [this]() {
            for (int i = 0; i < variablesPerScope.back(); ++i) {
                emitPopLocalDiscarding();
            }
        }};
        return compileRecursive(lambda.body());
    }, {&lambda});

    return innerType;
}

/**
 * @param lambda: needed if it has closured variables. Without it wouldn't know which variables were captured, so this
 * wouldn't be able to create a closure.
 */
auto PipesAssembler::compileClosureHelper(const std::function<CompiledExpression()> &emitBody,
                                          std::optional<const Lambda *const> lambda) -> CompiledExpression {
    return compileClosureHelper([&](ssize_t _) { return emitBody(); }, lambda);
}
auto PipesAssembler::compileClosureHelper(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda) -> CompiledExpression {
    return compileClosureHelper([&](ssize_t closureAssignedLocation, ssize_t _) {
        return emitBody(closureAssignedLocation);
    }, lambda);
}
auto PipesAssembler::compileClosureHelper(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation,
                                               ssize_t lambdaAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda) -> CompiledExpression {
    return compileClosureHelper(emitBody, lambda, true);
}
auto PipesAssembler::compileClosureHelperWithoutClosureLoading(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation,
                                               ssize_t lambdaAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda) -> CompiledExpression {
    return compileClosureHelper(emitBody, lambda, false);
}
auto PipesAssembler::compileClosureHelper(
        const std::function<CompiledExpression(ssize_t closureAssignedLocation,
                                               ssize_t lambdaAssignedLocation)> &emitBody,
        std::optional<const Lambda *const> lambda,
        bool emitClosureLoading) -> CompiledExpression {

    label_t afterLambda{0, 0, false, false};
    label_t lambdaPosition{0, 0, false, false};
    label_t lambdaBody{0, 0, false, false};
    label_t closurePosition{0, 0, false, false};

    // TODO/optimize: the next 3 jumps can be skipped sometimes
    assembler.emit_jump(&closurePosition);

    assembler.bind_label(&lambdaPosition);
    assembler.emit_endbr64();
    assembler.emit_jump(&lambdaBody);

    assembler.bind_label(&closurePosition);
    auto compiledLambda = CompiledExpression{
            -1, true, lambdaPosition.target_addr, ExpressionType::LAMBDA};
    auto compiledClosure = INVALID_COMPILATION;
    if (emitClosureLoading) {
        compiledClosure = emitPushClosures(lambda, compiledLambda);
    } else {
        compiledClosure = emitClosureWithinCode(compiledLambda);
    }
    assembler.emit_jump(&afterLambda);

//    auto innerType =

    assembler.bind_label(&lambdaBody);
    assembler.emit_enter(0);    // not needed because we are not using rbp nor rsp yet

    // TODO/optimize: look at the body and if it only uses the param at the beginning, don't push
    emitPushLocal(Register64::RAX);

    emitBody(compiledClosure.location, compiledLambda.innerLambdaLocation);

    emitPopLocalDiscarding();
    assembler.emit_leave();     // not needed because we are not using rbp nor rsp yet
    assembler.emit_return();

    assembler.bind_label(&afterLambda);

    //assembler.emit_mov_imm64_to_reg(Register64::RAX, static_cast<int64_t>(lambdaPosition.target_addr));

//    assembler.emit_add(Register64::RDX, Register64::RAX);
    return compiledClosure;
}

auto PipesAssembler::compileParameter(const Expression &expression) -> CompiledExpression {
    return compileParameter(expression, Register64::RAX);
}

auto PipesAssembler::compileParameter(const Expression &expression, Register64 dst) -> CompiledExpression {
    if (debug) {
        emitNopDebugString("\x9a\x2a\x37\xe2");
    }
    if (expression.getType() == ExpressionType::PARAMETER) {
        if (expression.getParameter().closureLevel > 0) {
            compileClosuredParameter(expression, dst);
        } else {
            compileParameterOfCurrentFunction(dst);
        }
        return nonReusableExpression(ExpressionType::PARAMETER);
    }
    throw randomize::exception::StackTracedException{"logic error"};
}

void PipesAssembler::compileClosuredParameter(const Expression &expression,
                                              Register64 &dst) {//            auto parentIter = parentLambdas.find(expression.getParameter().lambdaId);
//            if (parentIter != parentLambdas.end()) {
    auto offset = getParameterClosureOffset(expression.getParameter().enclosingLambdaId,
                                            getParameterName(expression));
    assembler.emit_mov_mem_to_reg(Register64::RSI, offset, dst);
//            } else {
//                throwSyntaxError("logic error: closure level of parameter '" + to_string(expression)
//                                   + "' is greater than 0 but can't find the parent lambda");
//            }
}

void PipesAssembler::compileParameterOfCurrentFunction(Register64 dst) {
    assembler.emit_mov_mem_to_reg(Register64::RBP, -8, dst);
}

auto PipesAssembler::compileIdentifier(const Expression &expression, bool withClosure) -> CompiledExpression {
    if (expression.getType() != ExpressionType::IDENTIFIER) {
        throw compilerBug("This function expected an identifier.", expression);
    } else {
        if (isIntrinsic(expression.getIdentifier())) {
            return compileIntrinsicIdentifier(expression);
        } else {
            auto identifierFile = identifiers.get_mutable_optional(expression.getIdentifier());
            if (identifierFile.isPresent()) {
                auto stacked = currentCode;
                RAII codeStack([&]() {currentCode = &(identifierFile.get().code);},
                               [&]() {currentCode = stacked;});
                return compileUserIdentifier(expression, identifierFile->expression, withClosure,
                                             *stacked);
            } else {
                throw compilerBug("undefined identifier", expression);
            }
        }
    }
}

PipesAssembler::CompiledExpression PipesAssembler::compileUserIdentifier(
        const Expression &expression, Expression &identifierBody, bool withClosure,
        const CodeFile &caller) {
    if (debug) {
        emitDebugString("identifier " + expression.getIdentifier());
    }
    registerExpression(identifierBody); // TODO is this done again for each identifier usage?
    if (identifierBody.getType() == ExpressionType::LAMBDA) {
        auto &lambda = identifierBody.getLambda();
        if (getClosureByEnclosingLambda(lambda).argumentCount > 0) {
            throwSemanticError(
                    "Compiling identifiers for lambdas with closures is not implemented");
        }
        auto compilationIter = compiledIdentifiers.find(expression.getIdentifier());
        if (compilationIter == compiledIdentifiers.end()) {
            compiledIdentifiers[expression.getIdentifier()] = nonReusableExpression(ExpressionType::LAMBDA);
            auto &compiled = compiledIdentifiers[expression.getIdentifier()];
            auto helper = &PipesAssembler::compileClosureHelperWithoutClosureLoading;
            if (withClosure) {
                helper = &PipesAssembler::compileClosureHelper;
            }
            compiled = (this->*helper)(
                    [&](ssize_t closureAssignedLocation,
                        ssize_t lambdaAssignedLocation) {
                        compiled.location = closureAssignedLocation;
                        compiled.valid = true;
                        compiled.innerLambdaLocation = lambdaAssignedLocation;
                        compiled.type = ExpressionType::LAMBDA;
                        return compileRecursive(lambda.body());
                    }, {&lambda});
            return compiled;
        } else {
            if (compilationIter->second.valid) {
                if (withClosure) {
                    if (compilationIter->second.location == -1) {
                        currentCode = &caller;
                        throw compilerBug(
                                "attempting to reuse an invalid lambda location for identifier "
                                        + quote(to_string(expression)), expression);
                    }
                    assembler.emit_mov_imm64_to_reg(Register64::RAX, compilationIter->second.location);
                    assembler.emit_add(Register64::RDX, Register64::RAX);
                }
                if (getClosureByEnclosingLambda(lambda).argumentCount > 0) {
                    throwSemanticError("unimplemented: reusing recursive closure for '"
                                               + expression.getIdentifier() + "'");
//                            emitPushClosures({&lambda});
                }
                return compilationIter->second;
            } else {
                throwError("compiler bug: compiled identifier '" + expression.getIdentifier() + "' should be valid."
                                   + " Identifiers with closures are not supported (yet?).");
            }
        }
    } else {
        if (withClosure) {
            return compileRecursive(identifierBody);
        } else {
            if (contains({ExpressionType::VALUE, ExpressionType::BRANCH, ExpressionType::IDENTIFIER},
                         identifierBody.getType())) {
                return compileRecursive(identifierBody);
            } else if (identifierBody.getType() == ExpressionType::CALL) {
                throwSemanticError("unimplemented: defining a call with identifier '"
                                           + expression.getIdentifier()
                                           + "'. Suggestion: wrap the call in a lambda. "
                                             "e.g.: 'a|b' can become "
                                             "'function x { x |(a |b) }'");
            } else {
                throwSemanticError(
                        "unimplemented: defining identifier ("
                                + expression.getIdentifier()
                                + ") with a body of type "
                                + to_string(identifierBody.getType())
                                + ". Only lambdas and values are allowed for now");
            }
        }
    }
    throw randomize::exception::StackTracedException{"should be unreachable"};
}

PipesAssembler::CompiledExpression PipesAssembler::compileIntrinsicIdentifier(
        const Expression &expression) {
    emitDebugString("intrinsic " + expression.getIdentifier());
    if (expression.getIdentifier() == intrinsics::ARGUMENT) {
        // at the beginning we pushed the argument into the stack after doing emit_enter
        auto globalOffset = getParameterGlobalOffset(intrinsics::ARGUMENT);
        emitMoveGlobalParameterToRegister(globalOffset, Register64::RAX);
        return nonReusableExpression(ExpressionType::VALUE);
    } else if (expression.getIdentifier() == intrinsics::DECREMENT) {
        return compileClosureHelper([this]() {
            assembler.emit_sub_1(Register64::RAX);
            return nonReusableExpression(ExpressionType::VALUE);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::INCREMENT) {
        return compileClosureHelper([this]() {
            assembler.emit_add(Register64::RAX, 1);
            return nonReusableExpression(ExpressionType::VALUE);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::NEW_ARRAY) {
        return compileClosureHelper([this]() {
            emitNewArray();
            return nonReusableExpression(ExpressionType::VALUE);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::DEREFERENCE) {
        return compileClosureHelper([this]() {
            assembler.emit_mov_mem_to_reg(Register64::RAX, 0, Register64::RAX);
            return nonReusableExpression(ExpressionType::VALUE);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::FREE) {
        return compileClosureHelper([this]() {
            emitDeallocateWithMalloc(Register64::RAX);
            return nonReusableExpression(ExpressionType::VALUE);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::PRINT_LAST_DIGIT) {
        return compileClosureHelper([this]() {
            emitPrintLastDigit();
            return nonReusableExpression(ExpressionType::LAMBDA);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::PRINT_CHAR) {
        return compileClosureHelper([this]() {
            emitPrintChar();
            return nonReusableExpression(ExpressionType::LAMBDA);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::READ_CHAR) {
        return compileClosureHelper([this]() {
            emitReadChar();
            return nonReusableExpression(ExpressionType::LAMBDA);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::PRINT_CURRENT_FUNCTION) {
        return compileClosureHelper([this]() {
            emitPrintCurrentFunction();
            return nonReusableExpression(ExpressionType::LAMBDA);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::PRINT_FUNCTION_STACK) {
        return compileClosureHelper([this]() {
            emitPrintFunctionStack();
            return nonReusableExpression(ExpressionType::LAMBDA);
        }, {});
    } else if (expression.getIdentifier() == intrinsics::PRINT_CALL_STACK) {
        return compileClosureHelper([this]() {
            emitPrintCallStack();
            return nonReusableExpression(ExpressionType::LAMBDA);
        }, {});
    } else {
        throwError("undefined intrinsic identifier", expression);
    }
    throw randomize::exception::StackTracedException{"should be unreachable"};
}

auto PipesAssembler::compileIdentifier(const Expression &expression) -> CompiledExpression {
    return compileIdentifier(expression, true);
}

auto PipesAssembler::compileClosureLessIdentifier(const Expression &expression) -> CompiledExpression {
    return compileIdentifier(expression, false);
}

auto PipesAssembler::compileBinaryOperation(const Expression &operationExpression) -> CompiledExpression {
    if (operationExpression.getType() != ExpressionType::BINARY_OPERATION) {
        throwError("This function expected a binary operator.", operationExpression);
    } else {
        auto &operation = operationExpression.getBinaryOperation();
        if (operation.operation == Token::PLUS) {
            if (operation.left.getType() == ExpressionType::VALUE
                    and Assembler::fitsIn8BitInt(operation.left.getValue())) {
                compileRecursive(operation.right);
                assembler.emit_add(Register64::RAX, static_cast<int8_t>(operation.left.getValue()));
            } else if (operation.right.getType() == ExpressionType::VALUE
                    and Assembler::fitsIn8BitInt(operation.right.getValue())) {
                compileRecursive(operation.left);
                assembler.emit_add(Register64::RAX, static_cast<int8_t>(operation.right.getValue()));
            } else {
                compileRecursive(operation.left);
                emitPushLocal(Register64::RAX);
                compileRecursive(operation.right);
                emitPopLocal(Register64::R12);
                assembler.emit_add(Register64::R12, Register64::RAX);
            }
            return nonReusableExpression(ExpressionType::BINARY_OPERATION);
        } else if (operation.operation == Token::MINUS) {
            if (operation.right.getType() == ExpressionType::VALUE
                    and Assembler::fitsIn8BitInt(operation.right.getValue())) {
                compileRecursive(operation.left);
                assembler.emit_sub(Register64::RAX, static_cast<int8_t>(operation.right.getValue()));
            } else {
                compileRecursive(operation.left);
                emitPushLocal(Register64::RAX);
                compileRecursive(operation.right);
                assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R12);
                emitPopLocal(Register64::RAX);
                assembler.emit_sub(Register64::RAX, Register64::R12);
            }
            return nonReusableExpression(ExpressionType::BINARY_OPERATION);
        } else if (operation.operation == Token::MULTIPLY) {
            ScopedRegisterSaver saver{assembler, {Register64::RDX}};
            compileRecursive(operation.left);
            emitPushLocal(Register64::RAX);
            compileRecursive(operation.right);
            emitPopLocal(Register64::R12);
            assembler.emit_mult_RAX_into_RDX_RAX(Register64::R12);
            return nonReusableExpression(ExpressionType::BINARY_OPERATION);
        } else if (operation.operation == Token::DIVIDE
                or operation.operation == Token::MODULO) {
            ScopedRegisterSaver saver{assembler, {Register64::RDX}};
            compileRecursive(operation.left);
            emitPushLocal(Register64::RAX);
            compileRecursive(operation.right);
            assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R12);
            emitPopLocal(Register64::RAX);
            assembler.emit_xor(Register64::RDX, Register64::RDX);
            assembler.emit_div_RDX_RAX_into_RAX_RDX(Register64::R12);
            if (operation.operation == Token::MODULO) {
                assembler.emit_mov_reg_to_reg(Register64::RDX, Register64::RAX);
            }
            return nonReusableExpression(ExpressionType::BINARY_OPERATION);
        } else if (operation.operation == Token::STATEMENT_SEPARATOR) {
            compileRecursive(operation.left);
            return compileRecursive(operation.right);
        } else if (operation.operation == Token::LOOP_CALL) {
            return compileLoop(operationExpression);
        } else if (operation.operation == Token::MUTABLE_LOOP_CALL) {
            return compileMutableLoop(operationExpression);
        } else if (operation.operation == Token::FIELD_ACCESS) {
            return compileFieldAccess(operationExpression);
        } else if (operation.operation == Token::GREATER_THAN) {
            return compileGreaterThan(operationExpression);
        } else if (operation.operation == Token::DEFINITION) {
            return compileDefinition(operationExpression);
        } else {
            throwSemanticError(
                    "unsupported binary operation " + to_type_string(operation.operation));
        }
    }
    return INVALID_COMPILATION;
}

PipesAssembler::CompiledExpression PipesAssembler::compileArray(const Expression &expression) {
    if (expression.getType() != ExpressionType::ARRAY) {
        throwError("This function expected an array.", expression);
    } else {
        const auto &iterable = expression.getArray();
        compileIterable(iterable.getElements());
        return CompiledExpression{-1, true, -1, ExpressionType::ARRAY};
    }
    return INVALID_COMPILATION;
}

PipesAssembler::CompiledExpression PipesAssembler::compileTuple(const Expression &expression) {
    if (expression.getType() != ExpressionType::TUPLE) {
        throwError("This function expected an tuple.", expression);
    } else {
        const auto &iterable = expression.getTuple();
        compileIterable(iterable.getElements());
        return CompiledExpression{-1, true, -1, ExpressionType::TUPLE};
    }
    return INVALID_COMPILATION;
}

void PipesAssembler::compileIterable(const std::vector<Expression> &elements) {
    emitAllocateWithMalloc(static_cast<int>((elements.size() + 1) * STACK_WIDTH_IN_BYTES));
    emit_imm64_to_mem(static_cast<int64_t>(elements.size()), Register64::RAX, 0);
    if (not elements.empty()) {
        // for literal arrays this copy might seem unnecessary, given that we are already putting
        // the array in the code section, but we allow modifying the elements of arrays, so we need
        // a new array anyway
        if (areAllElementsLiteralValues(elements)) {
            compileLiteralIterable(elements);
        } else {
            compileNonLiteralIterable(elements);
        }
    }
}

bool PipesAssembler::areAllElementsLiteralValues(const std::vector<Expression> &elements) const {
    bool allLiteral = true;
    for (const auto &element : elements) {
        if (element.getType() != ExpressionType::VALUE) {
            allLiteral = false;
            break;
        }
    }
    return allLiteral;
}

void PipesAssembler::compileLiteralIterable(const std::vector<Expression> &elements) {
    ScopedRegisterSaver saver{assembler, {Register64::RDI, Register64::RSI}};
    assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RDI);
    assembler.emit_add(Register64::RDI, STACK_WIDTH_IN_BYTES);

    label_t start{0, 0, false, false};
    label_t afterArray{0, 0, false, false};
    assembler.emit_jump(&afterArray);
    assembler.bind_label(&start);
    for (const auto &element : elements) {
        if (element.getType() == ExpressionType::VALUE) {
            assembler.emit_int64(element.getValue());
        } else {
            throwError("Expected an iterable whose elements are literal integers. "
                       "Offending array element is a " + quote(to_string(element.getType()))
                            + " of type: " + element.pipesType()->to_string()
                            + "\n(Should be " + quote(to_string(ExpressionType::VALUE))
                            + " of type: " + IntType{}.to_string() + ")");
        }
    }
    assembler.bind_label(&afterArray);
    assembler.emit_mov_imm64_to_reg(Register64::RSI, static_cast<int64_t>(start.target_addr));
    assembler.emit_add(Register64::RDX, Register64::RSI);
    assembler.emit_mov_imm64_to_reg(Register64::RCX, static_cast<int64_t>(elements.size()));
    assembler.emit_copy_range();
}

void PipesAssembler::compileNonLiteralIterable(const std::vector<Expression> &elements) {
    // Note the order! put RAX == &array on the top of the stack
    ScopedRegisterSaver saver{assembler, {Register64::R9, Register64::RAX}};

    int i = 1;
    for (const auto &element : elements) {
        compileRecursive(element);

        // load &array from the top of the stack into R12, copy RAX to array[i]
        assembler.emit_mov_mem_to_reg(Register64::RSP, 0, Register64::R9);
        assembler.emit_mov_reg_to_mem(Register64::RAX, i * STACK_WIDTH_IN_BYTES, Register64::R9);
        ++i;
    }
}

// TODO: this is very wrong
PipesAssembler::CompiledExpression PipesAssembler::compileVariable(const Expression &expression) {
    checkCalledWith(ExpressionType::VARIABLE, expression);
    assembler.emit_mov_mem_to_reg(Register64::RSP, 0, Register64::RAX);
    return CompiledExpression{-1, true, -1, ExpressionType::VARIABLE};
}

PipesAssembler::CompiledExpression PipesAssembler::compileLoop(const Expression &expression) {
    checkCalledWith(Token::LOOP_CALL, expression);
    const BinaryOperation &loop = expression.getBinaryOperation();

    if (not loop.left.pipesType()->isIterable()) {
        throw compilerBug("Expression to be iterated is not iterable", expression);
    } else if (not loop.right.pipesType()->isCallable()) {
        throw compilerBug("Function to be called in a loop is not callable", expression);
    } else {
        compileRecursive(loop.left);

        {
            ScopedRegisterSaver saver{assembler, {Register64::R8,
                                                  Register64::R9,
                                                  Register64::R13}};

            label_t loopStart{0, 0, false, false};
            label_t afterLoop{0, 0, false, false};

            assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R8);
            // r8 has the pointer to the array (to the first address, which contains the size)

            assembler.emit_mov_mem_to_reg(Register64::R8, 0, Register64::RAX);
            // RAX contains the size in elements

            assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R9);
            assembler.emit_add(Register64::R9, 1);

            // *rax + 1 is the count in STACK_WIDTH_IN_BYTES (which is 8) until the elem
            // past-the-end so need to multiply by STACK_WIDTH_IN_BYTES, which is the same as
            // left shift by log_2(STACK_WIDTH_IN_BYTES) == 3
            assembler.emit_shift_left(Register64::R9, 3);
            // r9 contains the displacement in bytes until past-the-last-element

            assembler.emit_add(Register64::R8, Register64::R9);
            // r9 contains the pointer to past-the-last-element
            //TODO would all this be doable with lea? lea r9 <- (rax[0] + 1)*8 + rax

            emitNewArray();
            // rax contains a pointer to the new array, like r8 has for the old

            {
                ScopedRegisterSaver saver_2{assembler, {Register64::RAX}};

                assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R13);
                // r13 contains a pointer to the new array, like r8 has for the old

                assembler.bind_label(&loopStart);
                assembler.emit_add(Register64::R8, STACK_WIDTH_IN_BYTES);
                assembler.emit_add(Register64::R13, STACK_WIDTH_IN_BYTES);
                assembler.emit_compare(Register64::R8, Register64::R9);
                assembler.emit_jump_if_zero(&afterLoop);
                assembler.emit_mov_mem_to_reg(Register64::R8, 0, Register64::RAX);
                const Expression &callWrapper = ofCall(ofConstant(0, {}), Expression{loop.right},
                                                       expression.position());
                compileCallFunctionRecursive(callWrapper.getCall(),
                                             Register64::RAX,
                                             nonReusableExpression(ExpressionType::VALUE),
                                             expression.position());

                assembler.emit_mov_reg_to_mem(Register64::RAX, 0, Register64::R13);
                assembler.emit_jump(&loopStart);
                assembler.bind_label(&afterLoop);

                // TODO 'else' section when loop makes 0 iterations? might convert it to a do-while

                return CompiledExpression{-1, true, -1, ExpressionType::BINARY_OPERATION};
            }
        }
    }
}

PipesAssembler::CompiledExpression PipesAssembler::compileMutableLoop(
        const Expression &expression) {
    checkCalledWith(Token::MUTABLE_LOOP_CALL, expression);
    const BinaryOperation &loop = expression.getBinaryOperation();
    if (not loop.left.pipesType()->isIterable()) {
        throw compilerBug("Expression to be iterated is not iterable", expression);
    } else if (not loop.right.pipesType()->isCallable()) {
        throw compilerBug("Function to be called in a loop is not callable", expression);
    } else {
        compileRecursive(loop.left);

        {
            ScopedRegisterSaver saver{assembler, {Register64::RAX,
//                                                      Register64::RSI,
                                                  Register64::R8,
                                                  Register64::R9}};

            label_t loopStart{0, 0, false, false};
            label_t afterLoop{0, 0, false, false};

            assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R8);
            assembler.emit_mov_mem_to_reg(Register64::RAX, 0, Register64::R9);
            assembler.emit_add(Register64::R9, 1);

            // *rax + 1 is the count in STACK_WIDTH_IN_BYTES (which is 8) until the elem
            // past-the-end so need to multiply by STACK_WIDTH_IN_BYTES, which is the same as
            // left shift by log_2(STACK_WIDTH_IN_BYTES) == 3
            assembler.emit_shift_left(Register64::R9, 3);

            assembler.emit_add(Register64::RAX, Register64::R9);
            //TODO would all this be doable with lea? lea r9 <- (rax[0] + 1)*8 + rax

            assembler.bind_label(&loopStart);
            assembler.emit_add(Register64::R8, STACK_WIDTH_IN_BYTES);
            assembler.emit_compare(Register64::R8, Register64::R9);
            assembler.emit_jump_if_zero(&afterLoop);
            assembler.emit_mov_mem_to_reg(Register64::R8, 0, Register64::RAX);
            const Expression &callWrapper = ofCall(ofConstant(0, {}), Expression{loop.right},
                                                   expression.position());
            compileCallFunctionRecursive(callWrapper.getCall(),
                                         Register64::RAX,
                                         nonReusableExpression(ExpressionType::VALUE),
                                         expression.position());

            assembler.emit_mov_reg_to_mem(Register64::RAX, 0, Register64::R8);
            assembler.emit_jump(&loopStart);
            assembler.bind_label(&afterLoop);

            // TODO 'else' section when loop makes 0 iterations? might convert it to a do-while

            return CompiledExpression{-1, true, -1, ExpressionType::BINARY_OPERATION};
        }
    }
}

PipesAssembler::CompiledExpression PipesAssembler::compileFieldAccess(
        const Expression &expression) {
    checkCalledWith(Token::FIELD_ACCESS, expression);
    const BinaryOperation &access = expression.getBinaryOperation();
    bool isTypeofStruct = is<TypeofType>(access.left.pipesType())
            and is<UserType>(access.left.pipesType()->getNestedType());
    if (not is<UserType>(access.left.pipesType()) and not isTypeofStruct) {
        throw compilerBug("Expression to be accessed is not a struct", access.left);
    } else if (access.right.getType() != ExpressionType::FIELD) {
        throw compilerBug("A field access should have a FIELD as second operator",
                          access.right);
    } else {
        compileRecursive(access.left);
        auto &structure = dynamic_cast<const UserType &>(
                isTypeofStruct
                ? access.left.pipesType()->getNestedType()
                : access.left.pipesType().operator*());
        auto index = structure.getFieldIndex(access.right.getField());
        if (not index.has_value()) {
            throw compilerBug("field " + quote(access.right.getField())
                                      + " not present in struct "
                                      + quote(to_string(access.left.pipesType())), expression);
        }
        auto displacement = int32_t{(index.value() + 1) * STACK_WIDTH_IN_BYTES};
        assembler.emit_mov_mem_to_reg(Register64::RAX, displacement, Register64::RAX);
        return nonReusableExpression(ExpressionType::FIELD);
    }
}

PipesAssembler::CompiledExpression PipesAssembler::compileGreaterThan(
        const Expression &expression) {
    checkCalledWith(Token::GREATER_THAN, expression);
    const BinaryOperation &gt = expression.getBinaryOperation();
    if (not is<IntType>(gt.left.pipesType()) or not is<IntType>(gt.right.pipesType())) {
        throw compilerBug("Expression '>' needs 2 ints, got: ", expression);
    } else {
        auto saver = ScopedRegisterSaver{assembler, {Register64::R8}};
        compileRecursive(gt.left);
        emitPushLocal(Register64::RAX);
        compileRecursive(gt.right);
        assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R8);
        assembler.emit_set_to_zero(Register64::RAX);
        emitPopLocal(Register64::R12);
        assembler.emit_compare(Register64::R12, Register64::R8);
        assembler.emit_gt();
        return nonReusableExpression(ExpressionType::FIELD);
    }
}

PipesAssembler::CompiledExpression PipesAssembler::compileDefinition(const Expression &expression) {
    checkCalledWith(Token::DEFINITION, expression);
    const BinaryOperation &gt = expression.getBinaryOperation();
    if (not is<IntType>(gt.left.pipesType()) or not is<IntType>(gt.right.pipesType())) {
        throw compilerBug("Expression '>' needs 2 ints, got: ", expression);
    } else {
        auto saver = ScopedRegisterSaver{assembler, {Register64::R8}};
        compileRecursive(gt.left);
        assembler.emit_push(Register64::RAX);
        ++variablesPerScope.back();
        return nonReusableExpression(ExpressionType::BINARY_OPERATION);
    }
}

void PipesAssembler::checkCalledWith(ExpressionType expectedType, const Expression &expression) {
    if (expression.getType() != expectedType) {
        throw compilerBug("This function expected a " + to_string(expectedType)
                                  + " expression.", expression);
    }
}

void PipesAssembler::checkCalledWith(Token::Type expectedBinaryExpression,
                                     const Expression &expression) {
    if (expression.getType() != ExpressionType::BINARY_OPERATION
            or expression.getBinaryOperation().operation != expectedBinaryExpression) {
        throw compilerBug("This function expected a "
                                  + quote(to_string(expectedBinaryExpression))
                                  + " (binary operation "
                                  + to_type_string(expectedBinaryExpression)
                                  + ") expression", expression);
    }
}

PipesAssembler::CompiledExpression PipesAssembler::compileWhile(
        const Expression &expression) {
    checkCalledWith(ExpressionType::WHILE, expression);

    auto tracker = VariableScopeTracker{ExpressionType::WHILE, variablesPerScope, [this]() {
        for (int i = 0; i < variablesPerScope.back(); ++i) {
            emitPopLocalDiscarding();
        }
    }};
    const While &w = expression.getWhile();

    label_t loopStart{0, 0, false, false};
    label_t zeroCase{0, 0, false, false};
    label_t end{0, 0, false, false};

    assembler.bind_label(&loopStart);
    compileRecursive(w.condition());
    assembler.emit_test(Register64::RAX, Register64::RAX);
    assembler.emit_jump_if_zero(&zeroCase);
    // non-zero case here
    compileRecursive(w.body());
    assembler.emit_jump(&loopStart);

    assembler.bind_label(&zeroCase);
    assembler.bind_label(&end);

    return nonReusableExpression(ExpressionType::WHILE);
}

