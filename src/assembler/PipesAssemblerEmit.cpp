/**
 * @file PipesAssemblerEmit.cpp
 * @date 2021-05-04
 */

#include <utility>

#include "assembler/PipesAssembler.h"
#include "assembler/syscalls.h"
#include "expression/ExpressionType.h"

// Note that this file only implements _some_ of PipesAssembler's methods.
// Mostly the emit* recursion-less, assembly-like, low-level functions just above the assembler.

void PipesAssembler::emitFibonacci() {
    label_t fib{0, 0, false, false};
    assembler.bind_label(&fib);
    assembler.emit_test(Register64::RDI, Register64::RDI);
    label_t zeroCase1{0, 0, false, false};
    label_t zeroCase2{0, 0, false, false};

    assembler.emit_jump_if_zero(&zeroCase1);
        // non-zero case here
        assembler.emit_sub_1(Register64::RDI);
        assembler.emit_test(Register64::RDI, Register64::RDI);

        assembler.emit_jump_if_zero(&zeroCase2);
            // non-zero case here
            assembler.emit_push(Register64::RDI);
            assembler.emit_call(&fib);
            assembler.emit_pop(Register64::RDI);
            assembler.emit_push(Register64::RAX);
            assembler.emit_sub_1(Register64::RDI);
            assembler.emit_call(&fib);
            assembler.emit_pop(Register64::RDI);
            assembler.emit_add(Register64::RDI, Register64::RAX);
            assembler.emit_return();

    assembler.bind_label(&zeroCase1);
    assembler.bind_label(&zeroCase2);
    // zero case here
    assembler.emit_mov_imm64_to_reg(Register64::RAX, 1);
    assembler.emit_return();
}

void PipesAssembler::emitPipesRelocation() {
    label_t afterRelocation{0, 0, false, false};

    assembler.emit_jump(&afterRelocation);
    relocationStart = static_cast<int64_t>(assembler.getBuffer().size);
    auto pointerCount = sizeof(RuntimeLibraries) / STACK_WIDTH_IN_BYTES;
    auto rawPtr = static_cast<void *>(&runtimeLibraries);
    auto ptrInt64 = static_cast<uint64_t *>(rawPtr);
    for (size_t i = 0; i < pointerCount; ++i) {
        auto ptrInt64ToMember = ptrInt64 + i;
        auto memberContent = *ptrInt64ToMember;
        assembler.emit_int64(memberContent);
    }
    assembler.emit_nop({0, 0, 0, 0, 0}); // help the disassembler to sync with the instructions

    assembler.bind_label(&afterRelocation);
}

void PipesAssembler::emit_imm64_to_mem(int64_t imm64,
                                       Register64 regWithPointer, int32_t displacementFromPointer) {
    assembler.emit_mov_imm64_to_reg(Register64::R12, imm64);
    assembler.emit_mov_reg_to_mem(Register64::R12, displacementFromPointer, regWithPointer);
}

void PipesAssembler::emitPushData(Register64 src) {
    assembler.emit_add(Register64::R11, STACK_WIDTH_IN_BYTES);
    assembler.emit_mov_reg_to_mem(src, 0, Register64::R11);
}

void PipesAssembler::emitPushData(int64_t value) {
    assembler.emit_add(Register64::R11, STACK_WIDTH_IN_BYTES);
    emit_imm64_to_mem(value, Register64::R11, 0);
}

void PipesAssembler::emitPopData(Register64 dst) {
    assembler.emit_mov_mem_to_reg(Register64::R11, 0, dst);
    assembler.emit_sub(Register64::R11, STACK_WIDTH_IN_BYTES);
}

void PipesAssembler::emitPushLocal(Register64 src) {
    assembler.emit_push(src);
}
void PipesAssembler::emitPushLocal(int64_t value) {
    assembler.emit_push(value);
}

void PipesAssembler::emitPopLocal(Register64 dst) {
    assembler.emit_pop(dst);
}

void PipesAssembler::emitPopLocalDiscarding() {
    assembler.emit_add(Register64::RSP, STACK_WIDTH_IN_BYTES);
}

void PipesAssembler::emitCall(Register64 ptr, PositionInCode position) {
    emitPushLocal(ptr);
//    emitPushLocal(Register64::R8);
//    emitPushLocal(Register64::R9);
//    assembler.emit_sub(Register64::RSP, 1 * STACK_WIDTH_IN_BYTES);
    assembler.emit_call(ptr);
    if (debug) {
        emitDebugStringPre("called from " + codeLineToString(*currentCode, position));
    }
//    assembler.emit_add(Register64::RSP, 1 * STACK_WIDTH_IN_BYTES);
//    emitPopLocal(Register64::R9);
//    emitPopLocal(Register64::R8);
    emitPopLocalDiscarding();
}

void PipesAssembler::emitCall(label_t *ptr, PositionInCode position) {
    assembler.emit_mov_imm64_to_reg(Register64::R12, ptr->target_addr);
    assembler.emit_add(Register64::RDX, Register64::R12);
    emitPushLocal(Register64::R12);
//    emitPushLocal(Register64::R8);
//    emitPushLocal(Register64::R9);
//    assembler.emit_sub(Register64::RSP, 1 * STACK_WIDTH_IN_BYTES);
    assembler.emit_call(ptr);
    if (debug) {
        emitDebugStringPre("called from " + codeLineToString(*currentCode, position));
    }
//    assembler.emit_add(Register64::RSP, 1 * STACK_WIDTH_IN_BYTES);
//    emitPopLocal(Register64::R9);
//    emitPopLocal(Register64::R8);
    emitPopLocalDiscarding();
}

void PipesAssembler::emitSavingAndRestoringRegisters(std::vector<Register64> registers,
                                                     std::function<void()> body) {
    ScopedRegisterSaver saver{assembler, std::move(registers)};
    body();
}

std::vector<Register64> callerSavedRegisters() {
    return {Register64::RDI,
            Register64::RSI,
            Register64::RCX,
            Register64::RDX,
            Register64::R8,
            Register64::R9,
            Register64::R10,
            Register64::R11};
}

ScopedRegisterSaver PipesAssembler::callerSavedRegistersSaver() {
    return ScopedRegisterSaver{assembler, callerSavedRegisters()};
}

void PipesAssembler::emitSavingAndRestoringCallerSavedRegisters(std::function<void()> body) {
    emitSavingAndRestoringRegisters(callerSavedRegisters(), std::move(body));
}

/**
 * man mmap
 * addr is NULL,
 * length is the size of the block
 * prot: PROT_EXEC Pages may be executed.
 *       PROT_READ Pages may be read.
 *       PROT_WRITE Pages may be written.
 *       PROT_NONE Pages may not be accessed.
 * flags: the only ones we care about are: MAP_PRIVATE | MAP_ANONYMOUS
 * fd is the file being mapped -> unused in our case, -1?
 * offset is the offset to the beginning of the file -> 0 in our case
 *
 * https://callumscode.com/blog/2010/jan/x86-64-linux-syscalls
 * http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
 * sys_mmap code            %rax
 * unsigned long addr       %rdi
 * unsigned long len        %rsi
 * unsigned long prot       %rdx
 * unsigned long flags      %r10
 * unsigned long fd         %r8
 * unsigned long off        %r9
 */
void PipesAssembler::emitAllocate(int byteCount) {
//    auto saver = callerSavedRegistersSaver();
    emitSavingAndRestoringCallerSavedRegisters([this, byteCount]() {
        assembler.emit_mov_imm64_to_reg(Register64::RAX, getSyscallCode(Syscall::MMAP));
        assembler.emit_mov_imm64_to_reg(Register64::RDI, 0); // 0: we don't give a hint for the memory address. OS chosen.
        assembler.emit_mov_imm64_to_reg(Register64::RSI, byteCount);
        assembler.emit_mov_imm64_to_reg(Register64::RDX, PROT_READ | PROT_WRITE);
        assembler.emit_mov_imm64_to_reg(Register64::R10, MAP_PRIVATE | MAP_ANONYMOUS);
        assembler.emit_mov_imm64_to_reg(Register64::R8, -1);
        assembler.emit_mov_imm64_to_reg(Register64::R9, 0);
        assembler.emit_syscall();
    });
}
/**
 * http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
 * sys_munmap code        %rax
 * unsigned long addr     %rdi
 * size_t len             %rsi
 */
void PipesAssembler::emitDeallocate(int byteCount) {
    emitSavingAndRestoringCallerSavedRegisters([this, byteCount]() {
        ScopedRegisterSaver saver{assembler, {Register64::RAX}};
        assembler.emit_mov_imm64_to_reg(Register64::RAX, getSyscallCode(Syscall::MUNMAP));

        // R10 has the start of our heap
        assembler.emit_mov_reg_to_reg(Register64::R10, Register64::RDI);
        assembler.emit_mov_imm64_to_reg(Register64::RSI, byteCount);
        assembler.emit_syscall();
    });
}

// TODO do we want to allow allocations bigger than 2^32? at least not until pipes is more stable?
void PipesAssembler::emitAllocateWithMalloc(int byteCount) {
    emitSavingAndRestoringCallerSavedRegisters([this, byteCount]() {
        assembler.emit_mov_imm64_to_reg(Register64::RDI, byteCount);
        assembler.emit_mov_mem_to_reg(Register64::RDX, static_cast<int32_t>(relocationStart.value()),
                                      Register64::RAX);
        assembler.emit_call(Register64::RAX);
    });
}
void PipesAssembler::emitAllocateWithMalloc(Register64 byteCount) {
    emitSavingAndRestoringCallerSavedRegisters([this, byteCount]() {
        if (byteCount != Register64::RDI) {
            assembler.emit_mov_reg_to_reg(byteCount, Register64::RDI);
        }
        assembler.emit_mov_mem_to_reg(Register64::RDX, static_cast<int32_t>(relocationStart.value()),
                                      Register64::RAX);
        assembler.emit_call(Register64::RAX);
    });
}
void PipesAssembler::emitDeallocateWithMalloc(Register64 registerWithAddressToFree) {
    emitSavingAndRestoringCallerSavedRegisters([this, registerWithAddressToFree]() {
        ScopedRegisterSaver saver{assembler, {Register64::RAX}};
        if (registerWithAddressToFree != Register64::RDI) {
            assembler.emit_mov_reg_to_reg(registerWithAddressToFree, Register64::RDI);
        }
        assembler.emit_mov_mem_to_reg(Register64::RDX,
                                      static_cast<int32_t>(relocationStart.value() + 8),
// TODO: less magic. do something like (&((RuntimeLibraries*)relocation).free))
//                                      reinterpret_cast<int32_t>(static_cast<RuntimeLibraries*>
//
//                                      (reinterpret_cast<void *>(relocationStart
//                                              .value()))->freePtr),
                                      Register64::RAX);
        assembler.emit_call(Register64::RAX);
    });
}

void PipesAssembler::emitReadChar() {
    emitSavingAndRestoringCallerSavedRegisters([this]() {
        emitPushLocal(Register64::RAX);

        Target target = currentHost();
        assembler.emit_mov_imm64_to_reg(Register64::RAX, getSyscallCode(Syscall::READ, target));
        assembler.emit_mov_imm64_to_reg(Register64::RDI, static_cast<int>(SYSCALL_STDIN));

        // *buf is in the top of the stack, so that we can pop the char that was read
        assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RSI);

        assembler.emit_mov_imm64_to_reg(Register64::RDX, 1); // read 1 byte(s) into *buf
        assembler.emit_syscall();

        // this is useful to put watchpoints while debugging
//        assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R9);

        assembler.emit_test(Register64::RAX, Register64::RAX);
        label_t zeroCase{0, 0, false, false};
        label_t end{0, 0, false, false};

        assembler.emit_jump_if_zero(&zeroCase);
        // non-zero case here
        emitPopLocal(Register64::RAX);  // put the char in RAX
        assembler.emit_jump(&end);

        assembler.bind_label(&zeroCase);
        // zero case here
        emitPopLocalDiscarding();

        assembler.bind_label(&end);
    });
}

/**
 * @return the input char. Like `function x {x}` with the side effect that x gets printed
 */
void PipesAssembler::emitPrintChar() {
    emitSavingAndRestoringCallerSavedRegisters([this]() {
        emitPushLocal(Register64::RAX);

        Target target = currentHost();
        assembler.emit_mov_imm64_to_reg(Register64::RAX, getSyscallCode(Syscall::WRITE, target));
        assembler.emit_mov_imm64_to_reg(Register64::RDI, SYSCALL_STDOUT);
        assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RSI); // *buf
        assembler.emit_mov_imm64_to_reg(Register64::RDX, 1); // write 1 byte(s) from *buf
//        assembler.emit_mov_imm64_to_reg(Register64::RCX, 0); // write 1 byte(s) from *buf
        assembler.emit_syscall();

        // this is useful to put watchpoints while debugging
//        assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::R9);

        emitPopLocal(Register64::RAX);  // return the input
    });
}

void PipesAssembler::emitPrintLastDigit() {
    emitSavingAndRestoringCallerSavedRegisters([this]() {
        emitPushLocal(Register64::RAX);

        // +48: make 0 a '0'. this hack only works for single-digit numbers.
        // '\n'*256: make the next char a newline
        assembler.emit_mov_imm64_to_reg(Register64::RDI, 48 + '\n' * 256);
        assembler.emit_add(Register64::RDI, Register64::RAX);
        emitPushLocal(Register64::RAX);

        Target target = currentHost();
        assembler.emit_mov_imm64_to_reg(Register64::RAX, getSyscallCode(Syscall::WRITE, target));
        assembler.emit_mov_imm64_to_reg(Register64::RDI, SYSCALL_STDOUT);
        assembler.emit_mov_reg_to_reg(Register64::RSP, Register64::RSI); // *buf
        assembler.emit_mov_imm64_to_reg(Register64::RDX, 2); // wrddvite 2 byte(s) from *buf
        assembler.emit_syscall();

        emitPopLocalDiscarding();
        emitPopLocal(Register64::RAX);
    });
}

/**
 * the pointer to the string should be in RSI, and the size in bytes should be in RDX.
 */
void PipesAssembler::emitPrintCString() {
    auto registersToSave = callerSavedRegisters();
    std::erase_if(registersToSave, [](Register64 reg) {
        return reg == Register64::RSI or reg == Register64::RDX;
    });
    auto saver = ScopedRegisterSaver{assembler, registersToSave};

    Target target = currentHost();
    assembler.emit_mov_imm64_to_reg(Register64::RAX, getSyscallCode(Syscall::WRITE, target));
    assembler.emit_mov_imm64_to_reg(Register64::RDI, SYSCALL_STDOUT);
    assembler.emit_syscall();
}

void PipesAssembler::emitNewArray() {
    auto registerSaver = ScopedRegisterSaver{assembler, {Register64::RDI}};
    assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RDI);
    assembler.emit_add(Register64::RAX, 1);

    // *rax + 1 is the array size in STACK_WIDTH_IN_BYTES (which is 8),
    // but malloc counts in bytes (not in stack_widths)
    assembler.emit_shift_left(Register64::RAX, 3);

    emitAllocateWithMalloc(Register64::RAX);
    assembler.emit_mov_reg_to_mem(Register64::RDI, 0, Register64::RAX);
}

/**
 * Assumes the closure pointer is in RSI, and emits assembler that puts the actual lambda pointer in RDI.
 */
void PipesAssembler::emitPrepareToCallLambda() {
    if(debug) {
        emitNopDebugString("\x92\xe9\xa2\xec");
    }
    assembler.emit_mov_mem_to_reg(Register64::RSI, 0, Register64::RDI);// actual lambda start
    assembler.emit_add(Register64::RDX, Register64::RDI);
}

void PipesAssembler::emitMoveGlobalParameterToRegister(OffsetFromHeapStartInBytes offsetSrc, Register64 dst) {
    assembler.emit_mov_mem_to_reg(Register64::R10, offsetSrc, dst);
}

void PipesAssembler::emitMoveRegisterToGlobalParameter(Register64 src, OffsetFromHeapStartInBytes offsetDst) {
    assembler.emit_mov_reg_to_mem(src, offsetDst, Register64::R10);
}

void PipesAssembler::emitNopDebugString(std::string message) {
    if (message.size() != 4) {
        std::vector<std::string> parts;
        parts.emplace_back();
        for (size_t i = 0; i < message.size(); ++i) {
            if (i %4 == 0) {
                parts.emplace_back();
            }
            parts.back().push_back(message[i]);
        }
        for (size_t i = 0; i < 4 - parts.back().size(); ++i) {
            parts.back().push_back(' ');
        }
        for (const auto &part: parts) {
            assembler.emit_nop({part[0], part[1], part[2], part[3], counter});
        }
    } else {
//    label_t after{0, 0, false, false};
//    assembler.emit_jump(&after);
        assembler.emit_nop({message[3], message[2], message[1], message[0], counter});
//    assembler.bind_label(&after);
    }
}

void PipesAssembler::emitDebugString(std::string message) {
    label_t afterMessage{0, 0, false, false};
    assembler.emit_jump(&afterMessage);
    message += '\0'; // make it a c-string
    for (char letter : message) {
        assembler.emit_byte(letter);
    }
    assembler.emit_int64(message.size());
    assembler.bind_label(&afterMessage);
    counter++;
}

void PipesAssembler::emitDebugStringPre(std::string message) {
    label_t afterMessage{0, 0, false, false};
    assembler.emit_jump(&afterMessage);
    message += '\0'; // make it a c-string
    assembler.emit_int64(message.size());
    for (char letter : message) {
        assembler.emit_byte(letter);
    }
    assembler.bind_label(&afterMessage);
    counter++;
}

auto PipesAssembler::emitPushClosures(std::optional<const Lambda *const> ptrLambda, CompiledExpression compiledLambda)
        -> CompiledExpression {
    if (debug) {
        emitNopDebugString("\xca\x97\x2e\x44");
    }

    if (not ptrLambda.has_value()) {
        return emitClosureWithinCode(compiledLambda);
    } else {
        auto &lambda = *(ptrLambda.value());
        auto &closure = getClosureByEnclosingLambda(lambda);
        auto closuredParameterCount = closure.argumentCount;
        if (closuredParameterCount == 0) {
            return emitClosureWithinCode(compiledLambda);
        } else {
            emitPushData(compiledLambda.innerLambdaLocation); // lambda start, relative to code section
            assembler.emit_mov_reg_to_reg(Register64::R11, Register64::RAX);// put in rax the pointer to the start of the closure

            emitPushData(static_cast<int64_t>(closuredParameterCount)); // number of closured parameters

            assembler.emit_add(Register64::R11,
                               closuredParameterCount * STACK_WIDTH_IN_BYTES); // reserve space for params
            emitPushLocal(Register64::RCX); // TODO maybe unneeded? (and pop too)
            // move current global values to closure
            for (const auto &[closuredParameterName, lambdaIdAndOffset] : closure.parameters) {
                auto parentIter = parentLambdas.find(lambda.id);
                if (parentIter != parentLambdas.end()) {
                    if (lambdaIdAndOffset.lambdaId == parentIter->second) {
                        // closuredParameterName is defined by the enclosing lambda
                        compileParameterOfCurrentFunction(Register64::RCX);
                        assembler.emit_mov_reg_to_mem(Register64::RCX, lambdaIdAndOffset.offset, Register64::RAX);
                    } else {
                        auto offset = getParameterClosureOffset(parentIter->second, closuredParameterName);
                        assembler.emit_mov_mem_to_reg(Register64::RSI, offset, Register64::RCX);
                        assembler.emit_mov_reg_to_mem(Register64::RCX, lambdaIdAndOffset.offset, Register64::RAX);
                    }
                } else {
                    throwError("can't capture " + closuredParameterName + " for compiling lambda "
                                       + std::to_string(lambda.id) + ". Seems a top-level lambda.");
                }
            }
            emitPopLocal(Register64::RCX);

            // as the closure has captured variables, the closure is in the heap,
            // so can't know its location at compile time, so return an invalid location
            return nonReusableExpression(ExpressionType::LAMBDA);
        }
    }
}

auto PipesAssembler::emitClosureWithinCode(const PipesAssembler::CompiledExpression &compiledLambda)
        -> CompiledExpression {
    label_t afterClosure{0, 0, false, false};
    label_t closureStart{0, 0, false, false};

    assembler.emit_jump(&afterClosure);
    assembler.bind_label(&closureStart);
    assembler.emit_int64(compiledLambda.innerLambdaLocation);
    assembler.emit_int64(0);    // no arguments
    assembler.emit_nop({0, 0, 0, 0, 0}); // help the disassembler to sync with the instructions

    assembler.bind_label(&afterClosure);
    assembler.emit_mov_imm64_to_reg(Register64::RAX, static_cast<int64_t>(closureStart.target_addr));
    assembler.emit_add(Register64::RDX, Register64::RAX);

    return CompiledExpression{
            closureStart.target_addr, true, compiledLambda.innerLambdaLocation, ExpressionType::LAMBDA};
}

void PipesAssembler::emitPrintFunctionStack() {
    auto saver = ScopedRegisterSaver{assembler,
                                     {Register64::RAX, Register64::RDX, Register64::RSI,
                                      Register64::RBP}};

    label_t loopStart{0, 0, false, false};
    label_t zeroCase{0, 0, false, false};

    assembler.bind_label(&loopStart);
    assembler.emit_test(Register64::RBP, Register64::RBP);
    assembler.emit_jump_if_zero(&zeroCase);
    // non-zero case here

    emitGetPointerToCurrentFunction(Register64::RAX);
    // rax has the pointer to the current function

    assembler.emit_add(Register64::RAX, -13);
    // go back 2 instructions: size of jmp is 5 bytes, size of the function name is an int64.
    // rax has the pointer to the name offset

    assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RSI);

    assembler.emit_mov_mem_to_reg(Register64::RAX, 0, Register64::RDX);
    // rdx has the function name offset

    assembler.emit_sub(Register64::RSI, Register64::RDX);
    // rsi has the pointer to the function name

    assembler.emit_sub_1(Register64::RDX);
    // rax has the function name size

    emitPrintCString();

    assembler.emit_mov_imm64_to_reg(Register64::RAX, '\n');
    emitPrintChar();

    assembler.emit_mov_memRbp_to_reg(0, Register64::RBP);
    // now do the same with the upper frame

    assembler.emit_jump(&loopStart);

    assembler.bind_label(&zeroCase);
}

void PipesAssembler::emitPrintCallStack() {
    emitPrintCurrentFunction();
    auto saver = ScopedRegisterSaver{assembler,
                                     {Register64::RAX, Register64::RDX, Register64::RSI,
                                      Register64::RBP}};

    label_t loopStart{0, 0, false, false};
    label_t zeroCase{0, 0, false, false};

    assembler.bind_label(&loopStart);
    assembler.emit_mov_imm64_to_reg(Register64::RAX, '\n');
    emitPrintChar();
    assembler.emit_test(Register64::RBP, Register64::RBP);
    assembler.emit_jump_if_zero(&zeroCase);
    // non-zero case here

    emitGetPointerToReturn(Register64::RAX);
    // rax has the pointer to the return position after the caller

    assembler.emit_add(Register64::RAX, 5);
    // go forward 1 instructions: size of jmp is 5 bytes
    // rax has the pointer to the name offset

    assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RSI);

    assembler.emit_mov_mem_to_reg(Register64::RAX, 0, Register64::RDX);
    // rdx has the function name offset

    assembler.emit_add(Register64::RSI, STACK_WIDTH_IN_BYTES);
    // rsi has the pointer to the function name

    assembler.emit_sub_1(Register64::RDX);
    // rax has the function name size

    emitPrintCString();

    assembler.emit_mov_memRbp_to_reg(0, Register64::RBP);
    // now do the same with the upper frame

    assembler.emit_jump(&loopStart);

    assembler.bind_label(&zeroCase);
}

void PipesAssembler::emitGetPointerToCurrentFunction(Register64 dst) {
    assembler.emit_mov_memRbp_to_reg(16, dst);
}
void PipesAssembler::emitGetPointerToReturn(Register64 dst) {
    assembler.emit_mov_memRbp_to_reg(8, dst);
}

void PipesAssembler::emitPrintCurrentFunction() {
    auto saver = ScopedRegisterSaver{assembler, {Register64::RAX, Register64::RDX, Register64::RSI}};

    emitGetPointerToCurrentFunction(Register64::RAX);
    // rax has the pointer to the current function

    assembler.emit_add(Register64::RAX, -13);
    // go back 2 instructions: size of jmp is 5 bytes, size of the function name is an int64.
    // rax has the pointer to the name offset

    assembler.emit_mov_reg_to_reg(Register64::RAX, Register64::RSI);

    assembler.emit_mov_mem_to_reg(Register64::RAX, 0, Register64::RDX);
    // rdx has the function name offset

    assembler.emit_sub(Register64::RSI, Register64::RDX);
    // rdi has the pointer to the function name

    assembler.emit_sub_1(Register64::RDX);
    // rax has the function name size

    emitPrintCString();
}
