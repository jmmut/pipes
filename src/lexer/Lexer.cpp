#include <map>
#include "platform/source_location.h"
#include "Lexer.h"
#include "commonlibs/StackTracedException.h"
#include "commonlibs/Walkers.h"

using namespace std::string_literals;

std::string to_type_string(Token::Type type) {
    switch (type) {
    case Token::OPEN_PAREN:
        return "Token::OPEN_PAREN";
    case Token::CLOSE_PAREN:
        return "Token::CLOSE_PAREN";
    case Token::OPEN_BRACE:
        return "Token::OPEN_BRACE";
    case Token::CLOSE_BRACE:
        return "Token::CLOSE_BRACE";
    case Token::OPEN_BRACKET:
        return "Token::OPEN_BRACKET";
    case Token::CLOSE_BRACKET:
        return "Token::CLOSE_BRACKET";
    case Token::LAMBDA:
        return "Token::LAMBDA";
    case Token::LAMBDA_RETURN:
        return "Token::LAMBDA_RETURN";
    case Token::IDENTIFIER:
        return "Token::IDENTIFIER";
    case Token::CALL:
        return "Token::CALL";
    case Token::LOOP_CALL:
        return "Token::LOOP_CALL";
    case Token::MUTABLE_LOOP_CALL:
        return "Token::MUTABLE_LOOP_CALL";
    case Token::BRANCH:
        return "Token::BRANCH";
    case Token::WHILE:
        return "Token::WHILE";
    case Token::COMMA:
        return "Token::COMMA";
    case Token::STATEMENT_SEPARATOR:
        return "Token::STATEMENT_SEPARATOR";
    case Token::NUMBER:
        return "Token::NUMBER";
    case Token::PLUS:
        return "Token::PLUS";
    case Token::MINUS:
        return "Token::MINUS";
    case Token::MULTIPLY:
        return "Token::MULTIPLY";
    case Token::DIVIDE:
        return "Token::DIVIDE";
    case Token::MODULO:
        return "Token::MODULUS";
    case Token::TYPE_MARKER:
        return "Token::TYPE_MARKER";
    case Token::QUOTE:
        return "Token::QUOTE";
    case Token::STRING:
        return "Token::STRING";
    case Token::STRUCT:
        return "Token::STRUCT";
    case Token::FIELD_ACCESS:
        return "Token::FIELD_ACCESS";
    case Token::FIELD:
        return "Token::FIELD";
    case Token::GREATER_THAN:
        return "Token::GREATER_THAN";
    case Token::DEFINITION:
        return "Token::DEFINITION";
    }
    throw randomize::exception::StackTracedException{"missing Token::Type case with code "
            + std::to_string(type)};
}
std::string to_string(Token::Type type) {
    switch (type) {
    case Token::OPEN_PAREN:
        return "(";
    case Token::CLOSE_PAREN:
        return ")";
    case Token::OPEN_BRACE:
        return "{";
    case Token::CLOSE_BRACE:
        return "}";
    case Token::OPEN_BRACKET:
        return "[";
    case Token::CLOSE_BRACKET:
        return "]";
    case Token::LAMBDA:
        return "function";
    case Token::LAMBDA_RETURN:
        return "->";
    case Token::IDENTIFIER:
        return "Token::IDENTIFIER";
    case Token::CALL:
        return "|";
    case Token::LOOP_CALL:
        return "||";
    case Token::MUTABLE_LOOP_CALL:
        return "=||";
    case Token::BRANCH:
        return "branch";
    case Token::WHILE:
        return "while";
    case Token::COMMA:
        return ",";
    case Token::STATEMENT_SEPARATOR:
        return ";";
    case Token::NUMBER:
        return "Token::NUMBER";
    case Token::PLUS:
        return "+";
    case Token::MINUS:
        return "-";
    case Token::MULTIPLY:
        return "|*";
    case Token::DIVIDE:
        return "|/";
    case Token::MODULO:
        return "|%";
    case Token::TYPE_MARKER:
        return ":";
    case Token::QUOTE:
        return "'";
    case Token::STRING:
        return "Token::STRING";
    case Token::STRUCT:
        return "struct";
    case Token::FIELD_ACCESS:
        return ".";
    case Token::FIELD:
        return "Token::FIELD";
    case Token::GREATER_THAN:
        return ">";
    case Token::DEFINITION:
        return "=";
    }
    throw randomize::exception::StackTracedException{"missing Token::Type case with code "
            + std::to_string(type)};
}

std::string to_string(const Token &token) {
    std::string result = to_type_string(token.type);
    if (token.type == Token::IDENTIFIER or token.type == Token::FIELD) {
        result += "(" + token.name + ")";
    } else if (token.type == Token::NUMBER) {
        result += "(" + std::to_string(token.number) + ")";
    } else if (token.type == Token::STRING) {
        result += "(" + token.name + ")";
    }
    return result;
}

std::string to_string(const Tokens &tokens) {
    std::stringstream ss;
    for (const auto &token : tokens) {
        ss << to_string(token) << " ";
    }
    auto tokensString = ss.str();
    tokensString.pop_back();
    return tokensString;
}

Token Token::ofIdentifier(std::string name, PositionInCode positionInCode) {
    return {Token::IDENTIFIER, 0L, name, positionInCode};
}

Token Token::ofField(std::string name, PositionInCode positionInCode) {
    return {Token::FIELD, 0L, name, positionInCode};
}

Token Token::ofNumber(long number, PositionInCode positionInCode) {
    return {Token::NUMBER, number, "", positionInCode};
}

Token Token::ofString(std::string message, PositionInCode positionInCode) {
    return {Token::STRING, 0L, message, positionInCode};
}

Token Token::ofTokenType(Token::Type type, PositionInCode positionInCode) {
    if (type == Token::IDENTIFIER or type == Token::NUMBER) {
        throw randomize::exception::StackTracedException{
                "wrong usage. for Token::NUMBER or Token::IDENTIFIER use ofNumber or ofIdentifier"};
    } else {
        return {type, 0L, "", positionInCode};
    }
}

bool isNewLine(char c) {
    return c == '\n';
}
bool isSpace(char c) {
    return c == ' ' or c == '\t' or isNewLine(c);
}
bool isNumber(char c) {
    return c >= '0' and c <= '9';
}
bool isLetter(char c) {
    return (c >= 'a' and c <= 'z')
            or (c >= 'A' and c <= 'Z');
}
bool isIdentifierCharacter(char c) {
    return isLetter(c)
            or c == '_'
            or c == NAMESPACE_ACCESS_TOKEN;
}
bool isFunctionCharacter(char c) {
    return isIdentifierCharacter(c);
}

auto consumeUntilAfter(const std::string &code, PositionInCode &positionInCode, char delimiter) {
    auto size = static_cast<long>(code.size());
    while (positionInCode.absoluteChar < size and code[positionInCode.absoluteChar] != delimiter) {
        if (isNewLine(code[positionInCode.absoluteChar])) {
            positionInCode.incrementLine();
        } else {
            positionInCode.incrementChar();
        }
    }
    if (positionInCode.absoluteChar < size) {
        if (isNewLine(code[positionInCode.absoluteChar])) {
            positionInCode.incrementLine();
        } else {
            positionInCode.incrementChar();
        }
    }
}
void consumeSpace(const std::string &code, PositionInCode &positionInCode) {
    while (positionInCode.absoluteChar < static_cast<long>(code.size()) and isSpace(code[positionInCode.absoluteChar])) {
        if (isNewLine(code[positionInCode.absoluteChar])) {
            positionInCode.incrementLine();
        } else {
            positionInCode.incrementChar();
        }
    }
}
auto consumeNumber(const std::string &code, PositionInCode &positionInCode) -> long {
    auto &index = positionInCode.absoluteChar;
    long n = 0;
    while (isNumber(code[index])) {
        char c = code[index];
        n *= 10; // assume base-10
        n += c - '0';
        positionInCode.incrementChar();
    }
    return n;
}

//auto consumeNumber(const std::string &code) {
//    long mockIndex = 0;
//    return consumeNumber(code, mockIndex);
//}
auto consumeIdentifier(const std::string &code, PositionInCode &positionInCode) -> std::string {
    auto &index = positionInCode.absoluteChar;
    auto start = index;
    if (isFunctionCharacter(code[index])) {
        positionInCode.incrementChar();
    }
    // TODO: allow "[4,5,6] #3". right now it will take #3 as an identifier
    while (isFunctionCharacter(code[index])
            or isNumber(code[index])
            or code[index] == to_string(Token::FIELD_ACCESS)[0]
            or code[index] == CLOSURE_LEVEL_TOKEN) {
        positionInCode.incrementChar();
    }
    std::string name{&(code[start]), &(code[index])};
    return name;
}

auto throwLexerErrorStack(const CodeFile &code, PositionInCode index, std::string message = "") {
    throw randomize::exception::StackTracedException{
            message + (message.empty() ? "" : ". ") + "Unexpected character with code "
                    + std::to_string(static_cast<unsigned char>(code.text[index.absoluteChar]))
                    + " at " + codePosToString(code, index)};
}
auto throwLexerError(const CodeFile &code, PositionInCode index, std::string message = "") {
    throw std::runtime_error{
            message + (message.empty() ? "" : ". ") + "Unexpected character with code "
                    + std::to_string(static_cast<unsigned char>(code.text[index.absoluteChar]))
                    + " at " + codePosToString(code, index)};
}
auto lexerError(const CodeFile &code, PositionInCode index, std::string message = "") {
    return std::runtime_error{
            message + (message.empty() ? "" : ". ") + "Unexpected character with code "
                    + std::to_string(static_cast<unsigned char>(code.text[index.absoluteChar]))
                    + " at " + codePosToString(code, index)};
}

void checkEOF(const CodeFile &code, PositionInCode &positionInCode) {
    auto textSize = static_cast<long>(code.text.size());
    if (textSize <= positionInCode.absoluteChar) {
        auto path = code.path + '/' + code.file;
        if (code.text.empty()) {
            throw std::runtime_error{"unexpected end-of-file: empty file " + quote(path)};
        } else {
            auto diff = positionInCode.absoluteChar - (textSize -1);
            positionInCode.absoluteChar -= diff;
            positionInCode.charInLine -= diff;
            if (positionInCode.absoluteChar < 0 or positionInCode.charInLine < 0) {
                throw std::runtime_error{"unexpected end-of-file in " + quote(path)};
            } else {
                throw std::runtime_error{
                        "unexpected end-of-file after " + codePosToString(code, positionInCode)};
            }
        }
    }
}

auto consumeLetter(const CodeFile &code, PositionInCode &positionInCode) -> char {
    auto &index = positionInCode.absoluteChar;
    if (code.text[index] == '\\') {
        positionInCode.incrementChar();
        checkEOF(code, positionInCode);
        if (code.text[index] == 'n') {
            positionInCode.incrementChar();
            return '\n';
        } else if (code.text[index] == '"') {
            positionInCode.incrementChar();
            return '\"';
        } else {
            throwLexerError(code, positionInCode,
                    R"(Error: Only '\n' or '\"' are supported as escaped characters.)");
        }
    } else {
        auto letter = code.text[index];
        positionInCode.incrementChar();
        return letter;
    }
    throw randomize::exception::AssertionException{} << "should be unreachable";
}

auto consumeChar(const CodeFile &code, PositionInCode &positionInCode) -> char {
    auto &index = positionInCode.absoluteChar;
    positionInCode.incrementChar();
    checkEOF(code, positionInCode);
    auto letter = consumeLetter(code, positionInCode);
    if (code.text[index] == to_string(Token::QUOTE)[0]) {
        positionInCode.incrementChar(); // leave the index past our consumed char
        return letter;
    } else {
        throw lexerError(code, positionInCode,
                        "chars should start and end with a " + to_string(Token::QUOTE));
    }
}

auto consumeString(const CodeFile &code, PositionInCode &positionInCode) -> std::string {
    auto &index = positionInCode.absoluteChar;
    if (code.text[index] != STRING_TOKEN) {
        throwLexerErrorStack(code, positionInCode,
                             "Strings must start with " + std::to_string(STRING_TOKEN));
    }
    positionInCode.incrementChar();
    checkEOF(code, positionInCode);
    std::stringstream message;
    while (code.text[index] != STRING_TOKEN) {
        auto c = consumeLetter(code, positionInCode);
        message << c;
        checkEOF(code, positionInCode);
    }
    positionInCode.incrementChar();
    consumeSpace(code.text, positionInCode);
    if (static_cast<long>(code.text.size()) > index and code.text[index] == STRING_TOKEN) {
        message << consumeString(code, positionInCode);
        consumeSpace(code.text, positionInCode);
    }
    return message.str();
}
struct TokenName {
    Token::Type type;
    std::string name;
};
using NormalTokens = std::map<char, std::vector<TokenName>>;

/**
 * These are all tokens that just need to do ofTokenType and advance the input position.
 * the mapped type is a vector, sorted by decreasing token length, so that input "||" doesn't get
 * tokenized as two calls "|", but a single loop call "||".
 */
NormalTokens buildNormalTokens() {
    auto normalTokens = NormalTokens{};
    auto addNormalToken = [&normalTokens](Token::Type token) {
        auto tokenString = to_string(token);
        normalTokens[tokenString[0]].push_back({token, tokenString});
    };
    addNormalToken(Token::OPEN_PAREN);
    addNormalToken(Token::CLOSE_PAREN);
    addNormalToken(Token::OPEN_BRACE);
    addNormalToken(Token::CLOSE_BRACE);
    addNormalToken(Token::OPEN_BRACKET);
    addNormalToken(Token::CLOSE_BRACKET);
    addNormalToken(Token::LAMBDA_RETURN);
    addNormalToken(Token::CALL);
    addNormalToken(Token::LOOP_CALL);
    addNormalToken(Token::MUTABLE_LOOP_CALL);
    addNormalToken(Token::COMMA);
    addNormalToken(Token::STATEMENT_SEPARATOR);
    addNormalToken(Token::PLUS);
    addNormalToken(Token::MINUS);
    addNormalToken(Token::MULTIPLY);
    addNormalToken(Token::DIVIDE);
    addNormalToken(Token::MODULO);
    addNormalToken(Token::TYPE_MARKER);
    addNormalToken(Token::GREATER_THAN);
    addNormalToken(Token::DEFINITION);

    for (auto &[firstChar, tokenNames] : normalTokens) {
        std::sort(tokenNames.begin(), tokenNames.end(), [](const TokenName &first,
                                                           const TokenName &second) {
            return first.name.size() > second.name.size();
        });
    }
    return normalTokens;
}

std::vector<std::string> orderByDecreasingLength(std::vector<std::string> strings) {
    std::sort(strings.begin(), strings.end(), [](const auto &left, const auto &right) {
        return left.size() > right.size();
    });
    return strings;
}

/**
 * These are operators that are easier to define as part of the Pipes Corelib than in the compiler.
 * At the moment the lexer will reject any unknown symbol. This could be changed to allow users to
 * define their own operators without compiler changes, but seems prone to confusion and worse
 * error messages.
 */
std::vector<std::string> getOperatorIdentifiers() {
    static auto ids = orderByDecreasingLength(
            std::vector<std::string>{"#", "++", "=?", "<", ">=", "<="});
    return ids;
}

bool stringMatches(const std::string &word, const std::string &fullText, long index) {
    if (word.size() + index > fullText.size()) {
        return false;
    } else {
        return strncmp(word.c_str(), fullText.c_str() + index, word.size()) == 0;
    }
}

std::pair<bool, std::string> isOperatorIdentifier(const std::string &codeText, long &index) {
    for (const auto &operat : getOperatorIdentifiers()) {
        if (stringMatches(operat, codeText, index)) {
            return {true, operat};
        }
    }
    return {false, ""};
}

Tokens tokenize(const CodeFile &code) {
    Tokens tokens;
    static auto normalTokens = buildNormalTokens();

    auto reservedWords = std::map<std::string, Token::Type>{};
    auto addReservedWord = [&reservedWords](Token::Type token) {
        reservedWords[to_string(token)] = token;
    };
    addReservedWord(Token::LAMBDA);
    addReservedWord(Token::BRANCH);
    addReservedWord(Token::STRUCT);
    addReservedWord(Token::WHILE);

    PositionInCode positionInCode{0, 0, 0};
    auto &index = positionInCode.absoluteChar;
    for (; consumeSpace(code.text, positionInCode), index < static_cast<long>(code.text.size()); ) {
        auto posAtBeginningOfToken = positionInCode;
        if (stringMatches(COMMENT_TOKEN, code.text, index)) {
            consumeUntilAfter(code.text, positionInCode, '\n');
        } else if (isIdentifierCharacter(code.text[index])) {
            auto identifier = consumeIdentifier(code.text, positionInCode);
            if (auto reservedWord = reservedWords.find(identifier);
                    reservedWord != reservedWords.end()) {
                tokens.push_back(Token::ofTokenType(reservedWord->second, posAtBeginningOfToken));
            } else {
                tokens.push_back(Token::ofIdentifier(identifier, posAtBeginningOfToken));
            }
        } else if (auto [found, identifier] = isOperatorIdentifier(code.text, index); found) {
            index += static_cast<long>(identifier.size());
            tokens.push_back(Token::ofIdentifier(identifier, posAtBeginningOfToken));
        } else if (isNumber(code.text[index])) {
            tokens.push_back(Token::ofNumber(consumeNumber(code.text, positionInCode), posAtBeginningOfToken));
        } else if (code.text[index] == to_string(Token::QUOTE)[0]) {
            tokens.push_back(
                    Token::ofNumber(consumeChar(code, positionInCode), posAtBeginningOfToken));
        } else if (code.text[index] == STRING_TOKEN) {
            tokens.push_back(
                    Token::ofString(consumeString(code, positionInCode), posAtBeginningOfToken));
        } else if (stringMatches(to_string(Token::FIELD_ACCESS), code.text, index)) {
            tokens.push_back(Token::ofTokenType(Token::FIELD_ACCESS, posAtBeginningOfToken));
            positionInCode.incrementChar();
            auto fieldPos = positionInCode;
            auto identifier = consumeIdentifier(code.text, positionInCode);
            tokens.push_back(Token::ofField(identifier, fieldPos));
        } else {
            bool tokenDetected = false;
            auto found = normalTokens.find(code.text[index]);
            if (found != normalTokens.end()) {
                for (const auto &tokenName : found->second) {
                    if (index + tokenName.name.size() <= code.text.size()) {
                        if (code.text.substr(index, tokenName.name.size()) == tokenName.name) {
                            tokens.push_back(Token::ofTokenType(tokenName.type,
                                                                posAtBeginningOfToken));
                            positionInCode.incrementChar(static_cast<long>(tokenName.name.size()));
                            tokenDetected = true;
                        }
                    }
                }
            }

            if (not tokenDetected) {
                throwLexerError(code, posAtBeginningOfToken);
            }
        }
    }
    return tokens;
}

Tokens tokenize(std::string code) {
    return tokenize(CodeFile{std::move(code), ".", ""});
}
