#ifndef PIPES_LEXER_H
#define PIPES_LEXER_H

#include <utility>
#include <vector>
#include <iostream>
#include <fstream>
#include <filesystem>
#include "commonlibs/StackTracedException.h"
#include "lexer/pipesCodeFile.h"

static const char CLOSURE_LEVEL_TOKEN = '@';
static const char NAMESPACE_ACCESS_TOKEN = '/';
static const char ARRAY_ACCESS_TOKEN = '#';
static const char STRING_TOKEN = '"';
static const char *const COMMENT_TOKEN = "//";


struct Token {
    // if you add a type, add the case in to_type_string(Token::Type) and to_string (Token::Type)
    enum Type {
        OPEN_PAREN, CLOSE_PAREN,
        OPEN_BRACE, CLOSE_BRACE,
        OPEN_BRACKET, CLOSE_BRACKET,
        LAMBDA, // the lambda start string: "function"
        LAMBDA_RETURN, // separator of lambda parameters and returns: "->"
        CALL, // what splits the arguments and the function being called: "|"
        LOOP_CALL,
        MUTABLE_LOOP_CALL,
        IDENTIFIER,
        NUMBER,
        BRANCH,
        WHILE,
        COMMA,
        STATEMENT_SEPARATOR,
        PLUS,
        MINUS,
        MULTIPLY,
        DIVIDE,
        MODULO,
        TYPE_MARKER,
        QUOTE,
        STRING,
        STRUCT,
        FIELD_ACCESS,
        FIELD,
        GREATER_THAN,
        DEFINITION
    };

    Type type;
    long number;
    std::string name;
    PositionInCode positionInCode;

    static Token ofIdentifier(std::string name, PositionInCode positionInCode = {});
    static Token ofField(std::string name, PositionInCode positionInCode = {});
    static Token ofNumber(long number, PositionInCode positionInCode = {});
    static Token ofString(std::string message, PositionInCode positionInCode = {});
    static Token ofTokenType(Token::Type type, PositionInCode positionInCode = {});
};

using Tokens = std::vector<Token>;

struct TokenedFile : public CodeFile {
    const Tokens tokens;
};

Tokens tokenize(std::string code);
Tokens tokenize(const CodeFile &code);


std::string to_type_string(Token::Type type);
std::string to_string(Token::Type type);
std::string to_string(const Token &token);
std::string to_string(const Tokens &tokens);
inline std::string double_quote(std::string inner) {
    return '"' + inner + '"';
}
inline std::string backtick_quote(std::string inner) {
    return '`' + inner + '`';
}
inline std::string quote(std::string inner) {
    return "'" + inner + "'";
}
inline std::string quote(char inner) {
    return std::string{"'"} + inner + "'";
}

inline std::string parenthesize(std::string inner) {
    return "(" + inner + ")";
}

inline std::ostream &operator<<(std::ostream &o, const Token &token) {
    return o << to_string(token);
}

auto codePosToString(const CodeFile &code, PositionInCode positionInCode) -> std::string;
bool isSpace(char c);
bool isNumber(char c);
bool isPipe(char c);
bool isIdentifierCharacter(char c);
bool isFunctionCharacter(char c);

void consumeSpace(const std::string &code, PositionInCode &positionInCode);
auto consumeNumber(const std::string &code, PositionInCode &positionInCode) -> long;
auto consumeIdentifier(const std::string &code, PositionInCode &positionInCode) -> std::string;


#endif //PIPES_LEXER_H
