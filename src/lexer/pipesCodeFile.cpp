/**
 * @file pipesCodeFile.cpp
 * @date 2022-02-26
 */

#include "lexer/pipesCodeFile.h"

std::string expand_user(std::string path) {
    if (not path.empty() and path[0] == '~' and (path.size() == 1 or path[1] == '/')) {
        char const* home = getenv("HOME");
        if (home or ((home = getenv("USERPROFILE")))) {
            path.replace(0, 1, home);
        } else {
            char const *hdrive = getenv("HOMEDRIVE");
            char const *hpath = getenv("HOMEPATH");
            if (hdrive and hpath) {
                path.replace(0, 1, std::string(hdrive) + hpath);
            }
        }
    }
    return path;
}

std::string resolveFromCorelibPath(const std::string &path) {
    return expand_user(CORELIB_PATH + path);
}

std::string readWholeFile(std::string filePath) {
    auto file = std::ifstream{filePath};
    if (not file) {
        throw std::runtime_error{"Can't read file: " + filePath};
    }
    auto wholeTextProgram = std::string{std::istreambuf_iterator<char>{file},
                                        std::istreambuf_iterator<char>{}};
    return wholeTextProgram;
}

std::string findPath(const std::string &masterPath,
                            const std::string &fileRelativePath) {
    auto localPath = masterPath + "/" + fileRelativePath;
    auto pathInCorelib = resolveFromCorelibPath(fileRelativePath);
    if (std::filesystem::exists(localPath)) {
        return localPath;
    } else if (std::filesystem::exists(pathInCorelib)) {
        return pathInCorelib;
    } else {
        throw std::runtime_error{
                "Can't read file because it doesn't exist: " + fileRelativePath
                        + ". Didn't find it in " + localPath + " nor in the pipes corelib folder "
                        + pathInCorelib};

    }
}

CodeFile getCodeFromRelativePath(const std::string &masterPath,
                                 const std::string &fileRelativePath) {
    checkHasPipesSuffix(fileRelativePath);
    auto actualPath = findPath(masterPath, fileRelativePath);
    auto wholeTextProgram = readWholeFile(actualPath);
    auto code = CodeFile{wholeTextProgram, masterPath, fileRelativePath};
    return code;
}

CodeFile getCodeFromPath(const std::string &path) {
    unsigned long lastFolderPos = path.find_last_of('/');
    if (lastFolderPos != std::string::npos) {
        auto folder = path.substr(0, lastFolderPos);
        auto filename = path.substr(lastFolderPos + 1);
        auto code = getCodeFromRelativePath(folder, filename);
        return code;
    } else {
        auto code = getCodeFromRelativePath(".", path);
        return code;
    }
}
