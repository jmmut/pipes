/**
 * @file pipesFile.h
 * @date 2021-06-09
 */

#ifndef PIPES_PIPESCODEFILE_H
#define PIPES_PIPESCODEFILE_H

#include <cstring>
#include <cstdlib>
#include <string>
#include <filesystem>
#include <fstream>
#include "commonlibs/StackTracedException.h"

static const char *const PIPES_SUFFIX = ".pipes";

// TODO: read environment variable XDG_DATA_HOME, and use this as default
static const char *const CORELIB_PATH = "~/.local/share/pipes/corelib/";

struct CodeFile {
    const std::string text;
    const std::string path;
    const std::string file;
};
inline CodeFile buildCode(const std::string &text) {
    return CodeFile{text, ".", ""};
}
inline CodeFile buildCode() {
    return CodeFile{"", ".", ""};
}

struct PositionInCode {
    PositionInCode(long absoluteChar, long line, long charInLine)
            : absoluteChar{absoluteChar}, line{line}, charInLine{charInLine} {}
    PositionInCode() : PositionInCode{-1, -1, -1} {}
    PositionInCode(const PositionInCode &other) = default;
    long absoluteChar;
    long line;
    long charInLine;
    void incrementChar(long count = 1) {
        absoluteChar += count;
        charInLine += count;
    }
    void incrementLine(long lineCount = 1, long newLineCharCount = 1) {
        absoluteChar += newLineCharCount;
        line += lineCount;
        charInLine = 0;
    }
    bool valid() const { return absoluteChar != -1 and line != -1 and charInLine != -1; }
};

inline bool operator==(const PositionInCode &first, const PositionInCode &second) {
    return first.absoluteChar == second.absoluteChar
            and first.line == second.line
            and first.charInLine == second.charInLine;
}

inline std::ostream &operator<<(std::ostream &out, const PositionInCode &pos) {
    return out << "PositionInCode{ absoluteChar: " << pos.absoluteChar
               << ", line: " << pos.line
               << ", charInLine: " << pos.charInLine << "}";
}

inline bool hasPipesSuffix(const std::string &path) {
    auto pipesSuffix = std::string{PIPES_SUFFIX};
    bool longerThanJustPipesSuffix = path.size() >= pipesSuffix.size();
    bool hasPipesSuffix = false;
    if (longerThanJustPipesSuffix) {
        // note that the next subtraction has to be done after checking path is longer than suffix
        hasPipesSuffix = path.substr(path.size() - pipesSuffix.size()) == pipesSuffix;
    }
    return hasPipesSuffix;
}
inline void checkHasPipesSuffix(const std::string &path) {
    if (not hasPipesSuffix(path)) {
        throw std::runtime_error{
            "A pipes program file should have extension '.pipes'. Wrong path: " + path};
    }
}


CodeFile getCodeFromRelativePath(const std::string &masterPath,
                                 const std::string &fileRelativePath);
CodeFile getCodeFromPath(const std::string &path);

inline std::string getExecutablePath(const std::string &pipesCodePath) {
    return pipesCodePath.substr(0, pipesCodePath.size() - strlen(PIPES_SUFFIX));
}

inline bool isFile(std::string filePath) {
//    return std::ifstream{filePath}.operator bool();
    return std::filesystem::exists(filePath);
}
inline bool isPipesCodeFile(std::string filePath) {
//    return std::ifstream{filePath}.operator bool();
    return std::filesystem::exists(filePath) and hasPipesSuffix(filePath);
}

inline std::string codeLineToString(const CodeFile &code, PositionInCode positionInCode) {
    return  code.file
            + (code.file.empty() ? "line " : ":") + std::to_string(positionInCode.line+1)
            + ":" + std::to_string(positionInCode.charInLine+1);
}

inline auto codePosToString(const CodeFile &code, PositionInCode positionInCode) -> std::string {
    if (not positionInCode.valid()) {
        return "(unknown location)";
    }
    if (positionInCode.absoluteChar >= static_cast<long>(code.text.size())) {
        throw randomize::exception::StackTracedException{"logic error: code is " + std::to_string(code.text.size())
                                                         + " letters long, but tried to print position "
                                                         + std::to_string(positionInCode.absoluteChar)};
    }
    auto lineStartIndex = positionInCode.absoluteChar - positionInCode.charInLine;
    auto lineEndIndex = static_cast<long>(code.text.find('\n', positionInCode.absoluteChar));
    auto relevantLine = code.text.substr(lineStartIndex, lineEndIndex - lineStartIndex);
    auto spaceFill = std::string(positionInCode.charInLine, ' ');
    return codeLineToString(code, positionInCode) + ": \n"
            + relevantLine + "\n"
            + spaceFill + "^";
}

inline auto compilerBug(const CodeFile &input, PositionInCode positionInCode, std::string message = "") {
    return randomize::exception::StackTracedException{
            "Compiler bug" + (message.empty()? "." : ": " + message)
            + ".\nDetected at " + codePosToString(input, positionInCode)};
}

#endif //PIPES_PIPESCODEFILE_H
