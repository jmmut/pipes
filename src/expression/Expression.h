#ifndef HELLOWORLD_EXPRESSION_H
#define HELLOWORLD_EXPRESSION_H

#include <memory>
#include <functional>
#include <utility>
#include "platform/source_location.h"
#include <typing/PipesType.h>
#include <commonlibs/Walkers.h>
#include <variant>
#include "lexer/Lexer.h"
#include "commonlibs/StackTracedException.h"
#include "expression/ExpressionVariants.h"
#include "expression/ExpressionType.h"


/** Main Abstract Syntax Tree (AST) */
struct Expression {
private:
    ExpressionType type;
    std::variant<
        Empty,
        Value,
        Name,
        Parameter,
        std::unique_ptr<Lambda>,
        std::unique_ptr<BinaryOperation>,
        SubExpressions
    > innerVariant;

public:
    Expression(const Expression &e);    // copies
    Expression(Expression &&e) noexcept;    // moves
    Expression();   // empty state
    ~Expression();
private:
    void destroy();
    void checkType(ExpressionType type, source_location location = {}) const;
    void checkTypeAnyOf(std::initializer_list<ExpressionType> types,
                        source_location location) const;
    bool isBareExpression() const {
        using enum ExpressionType;
        return contains({UNSET, NOTHING}, type);
    }

public:
    Expression &operator=(const Expression &e); // copies
    Expression &operator=(Expression &&e) noexcept; // moves
    bool operator==(const Expression &other) const;
    bool operator!=(const Expression &other) const;
    std::string debugEquals(const Expression &other) const;

    // getters
    template<ExpressionType type>
    const auto &as(source_location location = {}) const {
        checkType(type, location);
        if constexpr (type == ExpressionType::UNSET
                or type == ExpressionType::NOTHING) {
            return get<Empty>(innerVariant);
        } else if constexpr (type == ExpressionType::VALUE) {
            return get<Value>(innerVariant);
        } else if constexpr (type == ExpressionType::FIELD
                or type == ExpressionType::IDENTIFIER
                or type == ExpressionType::VARIABLE) {
            return get<Name>(innerVariant);
        } else if constexpr (type == ExpressionType::PARAMETER) {
            return get<Parameter>(innerVariant);
        } else if constexpr (type == ExpressionType::LAMBDA) {
            return get<std::unique_ptr<Lambda>>(innerVariant).operator*();
        } else if constexpr (type == ExpressionType::BINARY_OPERATION) {
            return get<std::unique_ptr<BinaryOperation>>(innerVariant).operator*();
        } else if constexpr (type == ExpressionType::CALL
                or type == ExpressionType::BRANCH
                or type == ExpressionType::WHILE
                or type == ExpressionType::ARRAY
                or type == ExpressionType::TUPLE
                or type == ExpressionType::STRUCT) {
            return get<SubExpressions>(innerVariant);
        }
    }

    template<ExpressionType type>
    auto &as(source_location location = {}) {
        if constexpr (type == ExpressionType::FIELD or type == ExpressionType::IDENTIFIER) {
            checkTypeAnyOf({ExpressionType::FIELD, ExpressionType::IDENTIFIER}, location);
            // TODO remove this if. some struct tests require it
        } else {
            checkType(type, location);
        }
        if constexpr (type == ExpressionType::UNSET
                or type == ExpressionType::NOTHING) {
            return get<Empty>(innerVariant);
        } else if constexpr (type == ExpressionType::VALUE) {
            return get<Value>(innerVariant);
        } else if constexpr (type == ExpressionType::FIELD
                or type == ExpressionType::IDENTIFIER
                or type == ExpressionType::VARIABLE) {
            return get<Name>(innerVariant);
        } else if constexpr (type == ExpressionType::PARAMETER) {
            return get<Parameter>(innerVariant);
        } else if constexpr (type == ExpressionType::LAMBDA) {
            return get<std::unique_ptr<Lambda>>(innerVariant).operator*();
        } else if constexpr (type == ExpressionType::BINARY_OPERATION) {
            return get<std::unique_ptr<BinaryOperation>>(innerVariant).operator*();
        } else if constexpr (type == ExpressionType::CALL
                or type == ExpressionType::BRANCH
                or type == ExpressionType::WHILE
                or type == ExpressionType::ARRAY
                or type == ExpressionType::TUPLE
                or type == ExpressionType::STRUCT) {
            return get<SubExpressions>(innerVariant);
        }
    }

    ExpressionType getType() const {return type;}
    long getValue(source_location = {}) const;
    const std::string &getIdentifier(source_location = {}) const;
    const std::string &getField(source_location = {}) const;
    const std::string &getVariable(source_location = {}) const;
    const Parameter &getParameter(source_location = {}) const;
    const Lambda &getLambda(source_location = {}) const;
    const Call &getCall(source_location = {}) const;
    const Branch &getBranch(source_location = {}) const;
    const While &getWhile(source_location = {}) const;
    const BinaryOperation &getBinaryOperation(source_location = {}) const;
    const Array &getArray(source_location = {}) const;
    const Tuple &getTuple(source_location = {}) const;
    const Struct &getStructure(source_location = {}) const;

    ExpressionType &getType() {return type;}
    long &getValue(source_location = {});
    std::string &getIdentifier(source_location = {});
    std::string &getField(source_location = {});
    std::string &getVariable(source_location = {});
    Parameter &getParameter(source_location = {});
    Lambda &getLambda(source_location = {});
    Call &getCall(source_location = {});
    Branch &getBranch(source_location = {});
    While &getWhile(source_location = {});
    BinaryOperation &getBinaryOperation(source_location = {});
    Array &getArray(source_location = {});
    Tuple &getTuple(source_location = {});
    Struct &getStructure(source_location = {});

    // builders
    static Expression ofNothing(PositionInCode positionInCode);
    static Expression ofConstant(long n, PositionInCode positionInCode = {});

    static Expression ofIdentifier(std::string name, PtrPipesType &&type,
                                   PositionInCode positionInCode = {});
    static Expression ofField(std::string name, PtrPipesType &&pipesType,
                              PositionInCode positionInCode = {});
    static Expression ofVariable(std::string name, PtrPipesType &&pipesType,
                                 PositionInCode positionInCode = {});
    static Expression ofParameter(std::string name, long closureLevel, long nestingLevel, long lambdaId,
                                  long enclosingLambdaId, PositionInCode positionInCode);
    static Expression ofVirtualParameter(std::string name, long closureLevel, long nestingLevel, long lambdaId,
                                         long enclosingLambdaId, PositionInCode positionInCode);
    static Expression ofLambda(const Lambda &f, PositionInCode positionInCode = {});
    static Expression ofLambda(long lambdaId, std::string &&argumentName, long lambdaLevel, Expression &&body,
                               PositionInCode positionInCode = {});
    static Expression ofCall(Expression &&argument, Expression &&function, PositionInCode positionInCode);
    static Expression ofBranch(Expression &&yes, Expression &&no, PositionInCode positionInCode);
    static Expression ofWhile(Expression &&condition, Expression &&body, PositionInCode positionInCode);
    static Expression ofBinaryOperation(Token::Type operation, Expression &&left, Expression &&right,
                                        PositionInCode positionInCode = {});
    static Expression ofArray(Expressions &&expressions, PositionInCode positionInCode = {});
    static Expression ofTuple(Expressions &&expressions, PositionInCode positionInCode = {});
    static Expression ofStruct(Expressions &&identifiers, PositionInCode positionInCode = {});

    PtrPipesType &pipesType();
    const PtrPipesType &pipesType() const;
    PositionInCode &position();
    const PositionInCode &position() const;
    PositionInCode positionInCode;

private:
    const ExpressionBase &getExpressionBase() const;
    ExpressionBase &getExpressionBase();
};

// pulling aliases out of Expression
// c++ I hate you. this doesn't work with overloads nor default parameters:
// constexpr inline auto &ofIdentifier = Expression::ofIdentifier;
constexpr inline auto &ofNothing = Expression::ofNothing;
constexpr inline auto &ofCall = Expression::ofCall;
constexpr inline auto &ofBranch = Expression::ofBranch;
constexpr inline auto &ofWhile = Expression::ofWhile;
constexpr inline auto &ofBinaryOperation = Expression::ofBinaryOperation;
constexpr inline auto &ofArray = Expression::ofArray;
constexpr inline auto &ofTuple = Expression::ofTuple;

inline Expression ofConstant(long n, PositionInCode positionInCode = PositionInCode{}) {
    return Expression::ofConstant(n, positionInCode);
}
inline Expression ofIdentifier(std::string name, PtrPipesType &&type,
                               PositionInCode positionInCode = {}) {
    return Expression::ofIdentifier(std::move(name), std::move(type), positionInCode);
}
inline Expression ofParameter(std::string name, long closureLevel = 0, long nestingLevel = -1,
                              long lambdaId = -1,
                              long enclosingLambdaId = -1, PositionInCode positionInCode = PositionInCode{}) {
    return Expression::ofParameter(std::move(name), closureLevel, nestingLevel, lambdaId, enclosingLambdaId,
                                   positionInCode);
}
inline Expression ofVirtualParameter(std::string name, long closureLevel, long nestingLevel = -1,
                                     long lambdaId = -1, long enclosingLambdaId = -1,
                                     PositionInCode positionInCode = PositionInCode{}) {
    return Expression::ofVirtualParameter(std::move(name), closureLevel, nestingLevel, lambdaId,
                                          enclosingLambdaId, positionInCode);
}
inline Expression ofLambda(long lambdaId, std::string argumentName, long lambdaLevel, Expression &&body,
                    PositionInCode positionInCode = {}) {
    return Expression::ofLambda(lambdaId, std::move(argumentName), lambdaLevel, std::move(body), positionInCode);
}
inline Expression ofLambda(const Lambda &lambda, PositionInCode positionInCode = {}) {
    return Expression::ofLambda(lambda, positionInCode);
}

inline Expression ofStruct(Expressions &&identifiers, PositionInCode positionInCode = {}) {
    return Expression::ofStruct(std::move(identifiers), positionInCode);
}

inline Expression ofField(std::string name, PositionInCode positionInCode = {}) {
    return Expression::ofField(std::move(name), UnresolvedType::make_ptr(""), positionInCode);
}
inline Expression ofField(std::string name, PtrPipesType &&pipesType,
                          PositionInCode positionInCode = {}) {
    return Expression::ofField(std::move(name), std::move(pipesType), positionInCode);
}

inline Expression ofVariable(std::string name, PtrPipesType &&pipesType,
                             PositionInCode positionInCode = {}) {
    return Expression::ofVariable(std::move(name), std::move(pipesType), positionInCode);
}

struct Lambda : public ExpressionBase {
    LambdaId id;
    std::string argumentName;
    long lambdaLevel;
    Expression m_body;
    Expression &body() {return m_body; }
    const Expression &body() const {return m_body; }
};
bool operator==(const Lambda &lhs, const Lambda &rhs);
bool operator!=(const Lambda &lhs, const Lambda &rhs);

struct BinaryOperation : public ExpressionBase {
    Token::Type operation;
    Expression left;
    Expression right;
};

inline bool operator==(const BinaryOperation &lhs, const BinaryOperation &rhs);
inline bool operator!=(const BinaryOperation &lhs, const BinaryOperation &rhs);


void recursivelyDo(const Expression &expression,
                   const std::function<void(const Expression &subnode)> &action);
void recursivelyDo(const Expression &expression,
                   const std::function<void(const Expression &)> &preAction,
                   const std::function<void(const Expression &)> &postAction);

void recursivelyRewrite(Expression &expression,
                        const std::function<void(Expression &)> &preAction);
void recursivelyRewrite(Expression &expression,
                        const std::function<void(Expression &)> &preAction,
                        const std::function<void(Expression &)> &postAction);


struct ExpressionOperation {
    ExpressionOperation() = default;
    explicit ExpressionOperation(const Expression &expression);
    virtual void operate(const Expression &expression) = 0; ///< for PipesType::NOTHING and TYPE::UNSET
    virtual void operate(const long &value) = 0;
    virtual void operate(const std::string &identifier) = 0;
    virtual void operate(const Parameter &parameter) = 0;
    virtual void operate(const Lambda &lambda) = 0;
    virtual void operate(const Call &call) = 0;
    virtual void operate(const Branch &branch) = 0;
    virtual void operate(const While &while_loop) = 0;
    virtual void operate(const BinaryOperation &binaryOperation) = 0;
    virtual void operate(const Array &array) = 0;
    virtual void operate(const Tuple &tuple) = 0;
    virtual void operate(const Struct &structure) = 0;
};
struct MutableExpressionOperation {
    virtual void operate(Expression &expression) = 0; ///< for PipesType::NOTHING and TYPE::UNSET
    virtual void operate(long &value) = 0;
    virtual void operate(Parameter &parameter) = 0;
    virtual void operate(std::string &identifier) = 0;
    virtual void operate(std::unique_ptr<Lambda> &lambda) = 0;
    virtual void operate(std::unique_ptr<Call> &call) = 0;
    virtual void operate(std::unique_ptr<Branch> &branch) = 0;
    virtual void operate(std::unique_ptr<While> &while_loop) = 0;
    virtual void operate(std::unique_ptr<BinaryOperation> &binaryOperation) = 0;
    virtual void operate(std::unique_ptr<Array> &array) = 0;
    virtual void operate(std::unique_ptr<Tuple> &tuple) = 0;
    virtual void operate(std::unique_ptr<Struct> &structure) = 0;
};

void operate(const Expression &expression, ExpressionOperation &operation);

//inline void operate(Expression &expression, MutableExpressionOperation &operation) {
//    switch (expression.getType()) {
//    case ExpressionType::UNSET:
//    case ExpressionType::NOTHING:
//        operation.operate(expression);
//        return;
//    case ExpressionType::VALUE:
//        operation.operate(expression.getValue());
//        return;
//    case ExpressionType::IDENTIFIER:
//    case ExpressionType::FIELD:
//        operation.operate(expression.getIdentifier());
//        return;
//    case ExpressionType::PARAMETER:
//        operation.operate(expression.getParameter());
//        return;
//    case ExpressionType::LAMBDA:
//        operation.operate(expression.lambda);
//        return;
//    case ExpressionType::CALL:
//        operation.operate(expression.call);
//        return;
//    case ExpressionType::BRANCH:
//        operation.operate(expression.branch);
//        return;
//    case ExpressionType::WHILE:
//        operation.operate(expression.while_loop);
//        return;
//    case ExpressionType::BINARY_OPERATION:
//        operation.operate(expression.binaryOperation);
//        return;
//    case ExpressionType::ARRAY:
//        operation.operate(expression.array);
//        return;
//    case ExpressionType::TUPLE:
//        operation.operate(expression.tuple);
//        return;
//    case ExpressionType::STRUCT:
//        operation.operate(expression.structure);
//        return;
//    }
//    throw randomize::exception::StackTracedException{"unhandled case " + to_string(expression.getType())};
//}


inline ExpressionOperation::ExpressionOperation(const Expression &expression) {
    ::operate(expression, *this);
}

//void MutableExpressionOperation::apply(Expression &expr) {
//    operate(expr, *this);
//}
#endif //HELLOWORLD_EXPRESSION_H
