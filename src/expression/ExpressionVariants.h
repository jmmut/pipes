/**
 * @file ExpressionVariants.h
 * @date 2022-03-13
 */

#ifndef PIPES_EXPRESSIONVARIANTS_H
#define PIPES_EXPRESSIONVARIANTS_H

#include "lexer/pipesCodeFile.h"
#include "typing/PipesType.h"
#include "expression/ExpressionType.h"


class ExpressionBase {
public:
    virtual ~ExpressionBase() = default;

    ExpressionBase() : ptrPipesType{UnknownType::make_ptr("")} {}

    ExpressionBase(PositionInCode positionInCode, const PipesType &ptrPipesType)
            : positionInCode{positionInCode}, ptrPipesType{ptrPipesType.copy()} {}
    ExpressionBase(PositionInCode positionInCode, PtrPipesType ptrPipesType)
            : positionInCode{positionInCode}, ptrPipesType{std::move(ptrPipesType)} {}
    ExpressionBase(const ExpressionBase &other)
            : ExpressionBase{other.position(), copy(other.pipesType())} {}
    ExpressionBase(ExpressionBase &&other) noexcept
            : ExpressionBase{other.position(), std::move(other.pipesType())} {}

    ExpressionBase &operator=(const ExpressionBase &e);
    ExpressionBase &operator=(ExpressionBase &&e) noexcept;

    virtual PositionInCode &position();
    virtual const PositionInCode &position() const;
    virtual PtrPipesType &pipesType();
    virtual const PtrPipesType &pipesType() const;
    Expression asExpression(ExpressionType type) const;

private:
    PositionInCode positionInCode;
    PtrPipesType ptrPipesType;
};

struct Expression;
using Expressions = std::vector<Expression>;
using PtrExpression = std::unique_ptr<Expression>;

struct Lambda;
struct BinaryOperation;

using LambdaId = long;

struct Parameter : public ExpressionBase {
    std::string name;
    long nestingLevel;
    long closureLevel;  ///< closure level is the number of lambdas you
                        ///< have to skip to match the correct lambda. examples:
                        ///< function x{x_0}
                        ///< function a { function b {a_1}}
    LambdaId lambdaId;
    long enclosingLambdaId;
    bool isVirtual;

    Parameter(const PositionInCode &positionInCode, PtrPipesType &&ptrPipesType);
};

bool operator==(const Parameter &lhs, const Parameter &rhs);
bool operator!=(const Parameter &lhs, const Parameter &rhs);



inline auto &moveAssignment(auto *thisPtr, ExpressionBase &&other) {
    if (thisPtr != &other) {
        dynamic_cast<ExpressionBase &>(*thisPtr) =
                std::move(dynamic_cast<ExpressionBase &>(other));
    }
    return *thisPtr;
}

struct ISubExpression : public virtual ExpressionBase {
    virtual const Expressions &getElements() const = 0;
    virtual Expressions &getElements() = 0;

    ISubExpression &operator=(ISubExpression &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};
bool operator==(const ISubExpression &lhs, const ISubExpression &rhs);
bool operator!=(const ISubExpression &lhs, const ISubExpression &rhs);

struct Array : public virtual ISubExpression {
    Array &operator=(Array &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};
struct Tuple : public virtual ISubExpression {
    Tuple &operator=(Tuple &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};
struct Struct : public virtual ISubExpression {
    Struct &operator=(Struct &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};


struct IBiExpression : public virtual ExpressionBase {
    virtual Expression &getFirst() = 0;
    virtual Expression &getSecond() = 0;
    virtual const Expression &getFirst() const = 0;
    virtual const Expression &getSecond() const = 0;

    IBiExpression &operator=(IBiExpression &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};
bool operator==(const IBiExpression &lhs, const IBiExpression &rhs);
bool operator!=(const IBiExpression &lhs, const IBiExpression &rhs);


struct Call : public virtual IBiExpression {
    Expression &argument() { return getFirst(); }
    Expression &function() { return getSecond(); }
    const Expression &argument() const { return getFirst(); }
    const Expression &function() const { return getSecond(); }
    Call &operator=(Call &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};

struct Branch : public virtual IBiExpression {
    Expression &yes() { return getFirst(); }
    Expression &no() { return getSecond(); }
    const Expression &yes() const { return getFirst(); }
    const Expression &no() const { return getSecond(); }
    Branch &operator=(Branch &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};

struct While : public virtual IBiExpression {
    Expression &condition() { return getFirst(); }
    Expression &body() { return getSecond(); }
    const Expression &condition() const { return getFirst(); }
    const Expression &body() const { return getSecond(); }
    While &operator=(While &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};

struct SubExpressions : public virtual Array, public virtual Tuple, public virtual Struct,
        public virtual Call, public virtual Branch, public virtual While {
    Expressions elements;
    Expressions &getElements() override;
    const Expressions &getElements() const override;

    Expression &getFirst() override { return elements.at(0); }
    Expression &getSecond() override { return elements.at(1); }
    const Expression &getFirst() const override { return elements.at(0); }
    const Expression &getSecond() const override { return elements.at(1); }

    SubExpressions() {}
    SubExpressions(const SubExpressions &other);
    SubExpressions(SubExpressions &&other);
    SubExpressions &operator=(SubExpressions &&other) noexcept;
};


struct IName : public virtual ExpressionBase {
    virtual std::string &name() = 0;
    virtual const std::string &name() const = 0;
    IName &operator=(IName &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};

struct Identifier : public virtual IName {
    Identifier &operator=(Identifier &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};
struct Field : public virtual IName {
    Field &operator=(Field &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};
struct Variable : public virtual IName {
    Variable &operator=(Variable &&other) noexcept {
        return moveAssignment(this, std::move(other));
    };
};

struct Name : public virtual Identifier, public virtual Field, public virtual Variable {
    std::string &name() override { return name_; }
    const std::string &name() const override { return name_; }

    explicit Name(std::string other) :name_{std::move(other)} {}
    Name(const Name &other);
    Name(Name &&other);
    Name &operator=(Name &&other) noexcept;
private:
    std::string name_;
};

struct Value : public ExpressionBase {
    explicit Value(long other) : value_{other} {}
    Value(const Value &other);
    Value(Value &&other);
    Value &operator=(Value &&other) noexcept;

    long value() const {return value_;}
    long &value() {return value_;}
private:
    long value_;
};

struct Empty : public ExpressionBase {
};

#endif //PIPES_EXPRESSIONVARIANTS_H
