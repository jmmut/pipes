/**
 * @file ExpressionString.h
 * @date 2022-03-13
 */

#ifndef PIPES_EXPRESSIONSTRING_H
#define PIPES_EXPRESSIONSTRING_H

#include "expression/ExpressionVariants.h"
#include "expression/Expression.h"
#include "expression/ExpressionType.h"

std::string to_string(const Lambda &f);
std::string to_string(const Call &call);
std::string to_string(const Branch &branch);
std::string to_string(const While &while_loop);
std::string to_string(const BinaryOperation &binaryOperation);
std::string to_string(const Array &array);
std::string to_string(const Tuple &tuple);
std::string to_string(const Struct &structure);
std::string to_string(const Expression &e);
//std::string to_string(const ExpressionBase &e);
std::string to_string(ExpressionType type);
std::string to_string_with_types(const Lambda &lambda, const PipesType &type);
std::string to_string_with_types(const Expression &e);

std::string indent(int nesting);
std::string to_pretty_string_with_types(const Expression &e, int nesting);


inline std::ostream &operator<<(std::ostream &o, const Expression &expression) {
    return o << to_string(expression);
}

inline std::ostream &operator<<(std::ostream &o, const ExpressionType &type) {
    return o << to_string(type);
}

#endif //PIPES_EXPRESSIONSTRING_H
