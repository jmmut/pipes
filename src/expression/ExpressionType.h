/**
 * @file ExpressionType.h
 * @date 2022-09-12
 */

#ifndef PIPES_EXPRESSIONTYPE_H
#define PIPES_EXPRESSIONTYPE_H

#include "commonlibs/Walkers.h"

enum class ExpressionType {
    UNSET, NOTHING,
    VALUE, LAMBDA, IDENTIFIER, CALL, PARAMETER, BRANCH, WHILE, BINARY_OPERATION,
    ARRAY, TUPLE, STRUCT, FIELD, VARIABLE
};

inline bool definesScope(ExpressionType type) {
    auto definingTypes = std::set<ExpressionType>{ExpressionType::LAMBDA,
                                                  ExpressionType::WHILE,
                                                  ExpressionType::BRANCH};
    return contains(definingTypes, type);
}

#endif //PIPES_EXPRESSIONTYPE_H
