/**
 * @file ExpressionString.cpp
 * @date 2022-03-13
 */

#include "expression/ExpressionString.h"
#include "expression/ExpressionType.h"

static const int INDENTATION_SPACES = 2;

std::string to_string(const Lambda &f) {
    return to_string(Token::LAMBDA) + " " + f.argumentName + (f.argumentName.empty() ? "" : " ")
            + "{" + to_string(f.body()) + "}";
}

std::string parenthesizeIfBinaryOp(const Expression &expression) {
    auto serialized = to_string(expression);
    if (contains({ExpressionType::CALL, ExpressionType::BINARY_OPERATION}, expression.getType())) {
        serialized = "(" + serialized + ")";
    }
    return serialized;
}

std::string to_string(const Call &call) {
    std::string arg = parenthesizeIfBinaryOp(call.argument());
    std::string func = parenthesizeIfBinaryOp(call.function());
    return arg + " |" + func;
}

std::string to_string(const Branch &branch) {
    std::string yes = parenthesizeIfBinaryOp(branch.yes());
    std::string no = parenthesizeIfBinaryOp(branch.no());
    return to_string(Token::BRANCH) + " {" + yes + "} {" + no + "}";
}

std::string to_string(const While &while_loop) {
    std::string condition = to_string(while_loop.condition());
    std::string body = to_string(while_loop.body());
    return to_string(Token::WHILE) + " " + condition + " { " + body + " }";
}

std::string to_string(const BinaryOperation &op) {
    if (op.operation == Token::FIELD_ACCESS) {
        return to_string(op.left) + " " + to_string(op.operation) + to_string(op.right);
    } else {
        return to_string(op.left) + " " + to_string(op.operation)
                + " " + parenthesizeIfBinaryOp(op.right);
    }
}
std::string to_string(const Array &array) {
    return randomize::utils::to_string(array.getElements());
}
std::string to_string(const Tuple &tuple) {
    if (tuple.getElements().size() == 1) {
        return "(" + to_string(tuple.getElements()[0]) + ",)";
    } else {
        return randomize::utils::to_string(tuple.getElements(), "(", ", ", ")");
    }
}
std::string to_string(const Struct &structure) {
    if (structure.getElements().size() == 1) {
        return to_string(Token::STRUCT) + " (" + to_string(structure.getElements()[0]) + ",)";
    } else {
        return randomize::utils::to_string(structure.getElements(),
                                           to_string(Token::STRUCT) + " (", ", ", ")");
    }
}

std::string to_string(const Expression &e) {
    switch (e.getType()) {
    case ExpressionType::UNSET:
        return "(UNSET expression! this is likely an error in the compiler/parser code)";
    case ExpressionType::NOTHING:
        return "Nothing";
    case ExpressionType::VALUE:
        return std::to_string(e.getValue());
    case ExpressionType::LAMBDA:
        return to_string(e.getLambda());
    case ExpressionType::CALL:
        return to_string(e.getCall());
    case ExpressionType::BRANCH:
        return to_string(e.getBranch());
    case ExpressionType::WHILE:
        return to_string(e.getWhile());
    case ExpressionType::IDENTIFIER:
        return e.getIdentifier();
    case ExpressionType::FIELD:
        return e.getField();
    case ExpressionType::VARIABLE:
        return e.getVariable();
    case ExpressionType::PARAMETER:
        return e.getParameter().name + (e.getParameter().closureLevel > 0?
                CLOSURE_LEVEL_TOKEN + std::to_string(e.getParameter().closureLevel)
                : std::string{});
    case ExpressionType::BINARY_OPERATION:
        return to_string(e.getBinaryOperation());
    case ExpressionType::ARRAY:
        return to_string(e.getArray());
    case ExpressionType::TUPLE:
        return to_string(e.getTuple());
    case ExpressionType::STRUCT:
        return to_string(e.getStructure());
    }
    throw randomize::exception::StackTracedException{
            "Unhandled case ExpressionType::Type " + to_string(e.getType())};
}

std::string to_string(ExpressionType type) {
    switch (type) {
    case ExpressionType::UNSET:
        return "UNSET";
    case ExpressionType::NOTHING:
        return "NOTHING";
    case ExpressionType::VALUE:
        return "VALUE";
    case ExpressionType::LAMBDA:
        return "LAMBDA";
    case ExpressionType::IDENTIFIER:
        return "IDENTIFIER";
    case ExpressionType::FIELD:
        return "FIELD";
    case ExpressionType::VARIABLE:
        return "VARIABLE";
    case ExpressionType::CALL:
        return "CALL";
    case ExpressionType::BRANCH:
        return "BRANCH";
    case ExpressionType::WHILE:
        return "WHILE";
    case ExpressionType::PARAMETER:
        return "PARAMETER";
    case ExpressionType::BINARY_OPERATION:
        return "BINARY_OPERATION";
    case ExpressionType::ARRAY:
        return "ARRAY";
    case ExpressionType::TUPLE:
        return "TUPLE";
    case ExpressionType::STRUCT:
        return "STRUCT";
    }
    throw randomize::exception::StackTracedException{
        "missing ExpressionType::Type case " + std::to_string(static_cast<int>(type))};
}

std::string to_string_with_types(const Lambda &lambda, const PipesType &type) {
    return to_string(Token::LAMBDA) + "("
            + type.getParameterType().to_string()
            + " " + to_string(Token::LAMBDA_RETURN) + " "
            + type.getReturnedType().to_string() + ") {"
            + to_string_with_types(lambda.body()) + "}";
}
std::string to_string_with_types(const Call &call, const PipesType &type) {
    return to_string_with_types(call.argument())
            + " " + to_string(Token::CALL)
            + "(" + to_string_with_types(call.function()) + ") "
            + to_string(Token::TYPE_MARKER) + type.to_string_without_toplevel_name();
}

std::string to_string_with_types(const Expression &e) {
    switch (e.getType()) {
    case ExpressionType::UNSET:
    case ExpressionType::NOTHING:
    case ExpressionType::BINARY_OPERATION:
    case ExpressionType::STRUCT:
        return to_string(e);
    case ExpressionType::BRANCH:
    case ExpressionType::WHILE:
    case ExpressionType::VALUE:
    case ExpressionType::IDENTIFIER:
    case ExpressionType::FIELD:
    case ExpressionType::VARIABLE:
    case ExpressionType::PARAMETER:
    case ExpressionType::ARRAY:
    case ExpressionType::TUPLE:
        return to_string(e) + " :" + e.pipesType()->to_string_without_toplevel_name();
    case ExpressionType::LAMBDA:
        return to_string_with_types(e.getLambda(), e.pipesType().operator*());
    case ExpressionType::CALL:
        return to_string_with_types(e.getCall(), e.pipesType().operator*());
    }
    throw randomize::exception::StackTracedException{
            "Unhandled case ExpressionType::Type " + to_string(e.getType())};
}


std::string to_pretty_string_with_types(const Lambda &lambda, const PipesType &type, int nesting) {
    return to_string(Token::LAMBDA) + "("
            + type.getParameterType().to_string()
            + (type.getParameterType().isCallable() and type.getParameterType() != UnknownType{} ?
               "\n" + indent(nesting + 2) : " ")
            + to_string(Token::LAMBDA_RETURN) + " "
            + type.getReturnedType().to_string() + ") {\n"
            + indent(nesting + 1)
            + to_pretty_string_with_types(lambda.body(), nesting + 1)
            + "\n" + indent(nesting) + "}\n";
}
std::string to_pretty_string_with_types(const Call &call, const PipesType &type, int nesting) {
    return to_pretty_string_with_types(call.argument(), nesting)
            + "\n" + indent(nesting) + to_string(Token::CALL) + "(\n"
            + indent(nesting+1) + to_pretty_string_with_types(call.function(), nesting + 1)
            + "\n" + indent(nesting) + ")\n"
            + indent(nesting) + ":" + type.to_string_without_toplevel_name();
}

std::string to_pretty_string_with_types(const Expression &e, int nesting) {
    switch (e.getType()) {
    case ExpressionType::UNSET:
    case ExpressionType::NOTHING:
    case ExpressionType::STRUCT:
        return to_string(e);
    case ExpressionType::VALUE:
    case ExpressionType::BINARY_OPERATION:
    case ExpressionType::IDENTIFIER:
    case ExpressionType::FIELD:
    case ExpressionType::VARIABLE:
    case ExpressionType::PARAMETER:
    case ExpressionType::ARRAY:
    case ExpressionType::TUPLE:
        return to_string(e) + " :" + e.pipesType()->to_string_without_toplevel_name();
    case ExpressionType::BRANCH:
    case ExpressionType::WHILE:
        return to_string(e) + "\n"
        + indent(nesting) + ":" + e.pipesType()->to_string_without_toplevel_name();
    case ExpressionType::LAMBDA:
        return to_pretty_string_with_types(e.getLambda(), e.pipesType().operator*(),
                                           nesting);
    case ExpressionType::CALL:
        return to_pretty_string_with_types(e.getCall(), e.pipesType().operator*(), nesting);
    }
    throw randomize::exception::StackTracedException{
            "Unhandled case ExpressionType::Type " + to_string(e.getType())};
}

std::string indent(int nesting) {
    return std::string(nesting * INDENTATION_SPACES, ' ');
}

