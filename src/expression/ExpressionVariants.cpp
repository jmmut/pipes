/**
 * @file ExpressionVariants.cpp
 * @date 2022-03-13
 */

#include "expression/ExpressionVariants.h"
#include "expression/Expression.h"
#include "expression/ExpressionString.h"
#include "expression/ExpressionType.h"


////////////////// ExpressionBase

PositionInCode &ExpressionBase::position() {
    return positionInCode;
}

const PositionInCode &ExpressionBase::position() const {
    return positionInCode;
}

PtrPipesType &ExpressionBase::pipesType() {
    return ptrPipesType;
}

const PtrPipesType &ExpressionBase::pipesType() const {
    return ptrPipesType;
}

ExpressionBase &ExpressionBase::operator=(const ExpressionBase &e) {
    if (&e != this) {
        pipesType() = copy(e.pipesType());
        position() = e.position();
    }
    return *this;
}

ExpressionBase &ExpressionBase::operator=(ExpressionBase &&e) noexcept {
    if (&e != this) {
        pipesType() = std::move(e.pipesType());
        position() = e.position();
    }
    return *this;
}

Expression ExpressionBase::asExpression(ExpressionType type) const {
    switch (type) {
    case ExpressionType::UNSET:
        return Expression{};
    case ExpressionType::NOTHING:
        return ofNothing(position());
    case ExpressionType::VALUE:
        return ofConstant(dynamic_cast<const Expression *>(this)->getValue(), position());
    case ExpressionType::IDENTIFIER: {
        auto casted = dynamic_cast<const Expression *>(this);
        return ofIdentifier(casted->getIdentifier(), casted->pipesType()->copy(), position());
    }
    case ExpressionType::FIELD: {
        auto casted = dynamic_cast<const Expression *>(this);
        return ofField(casted->getField(), casted->pipesType()->copy(), position());
    }
    case ExpressionType::VARIABLE: {
        auto casted = dynamic_cast<const Expression *>(this);
        return ofField(casted->getVariable(), casted->pipesType()->copy(), position());
    }
    case ExpressionType::PARAMETER: {
        auto casted = dynamic_cast<const Expression *>(this);
        Parameter p = casted->getParameter();
        return ofParameter(p.name, p.closureLevel, p.nestingLevel, p.lambdaId, p.enclosingLambdaId,
                           position());
    }
    case ExpressionType::LAMBDA: {
        auto casted = dynamic_cast<const Lambda *>(this);
        return ofLambda(*casted, position());
    }
    case ExpressionType::CALL: {
        auto casted = dynamic_cast<const Call *>(this);
        return ofCall(Expression{casted->argument()}, Expression{casted->function()}, position());
    }
    case ExpressionType::BRANCH: {
        auto casted = dynamic_cast<const Branch *>(this);
        return ofBranch(Expression{casted->yes()}, Expression{casted->no()}, position());
    }
    case ExpressionType::WHILE: {
        auto casted = dynamic_cast<const While *>(this);
        return ofWhile(Expression{casted->condition()}, Expression{casted->body()}, position());
    }
    case ExpressionType::BINARY_OPERATION: {
        auto casted = dynamic_cast<const BinaryOperation *>(this);
        return ofBinaryOperation(casted->operation,
                                 Expression{casted->left},
                                 Expression{casted->right},
                                 position());
    }
    case ExpressionType::ARRAY:
        return ofArray(Expressions{(dynamic_cast<const SubExpressions *>(this))->getElements()}, position());
    case ExpressionType::TUPLE:
        return ofTuple(Expressions{(dynamic_cast<const SubExpressions *>(this))->getElements()}, position());
    case ExpressionType::STRUCT:
        return ofStruct(Expressions{(dynamic_cast<const SubExpressions *>(this))->getElements()}, position());
    }
    throw randomize::exception::StackTracedException{"unhandled case " + to_string(type)};
}

////////////////// Typed Expressions

Parameter::Parameter(const PositionInCode &positionInCode, PtrPipesType &&ptrPipesType)
        : ExpressionBase(positionInCode, std::move(ptrPipesType)) {}

bool operator==(const Parameter &lhs, const Parameter &rhs) {
    return lhs.closureLevel == rhs.closureLevel;
}

bool operator!=(const Parameter &lhs, const Parameter &rhs) {
    return not (lhs == rhs);
}


bool operator==(const ISubExpression &lhs, const ISubExpression &rhs) {
    return lhs.getElements() == rhs.getElements();
}
bool operator!=(const ISubExpression &lhs, const ISubExpression &rhs) {
    return not (lhs == rhs);
}

bool operator==(const IBiExpression &lhs, const IBiExpression &rhs) {
    return lhs.getFirst() == rhs.getFirst() and lhs.getSecond() == rhs.getSecond();
}
bool operator!=(const IBiExpression &lhs, const IBiExpression &rhs) {
    return not (lhs == rhs);
}

Expressions &SubExpressions::getElements() {
    return elements;
}
const Expressions &SubExpressions::getElements() const {
    return elements;
}

SubExpressions::SubExpressions(const SubExpressions &other)
        : ExpressionBase{other}, elements{other.elements} {}
SubExpressions::SubExpressions(SubExpressions &&other)
        : ExpressionBase{other}, elements{std::move(other.elements)} {} // TODO copies ExBase

SubExpressions &SubExpressions::operator=(SubExpressions &&other) noexcept {
    if (this != &other) {
        elements = std::move(other.elements);
        dynamic_cast<ExpressionBase &>(*this) =
                std::move(dynamic_cast<ExpressionBase &>(other));
    }
    return *this;
}

Name::Name(const Name &other)
        : ExpressionBase{other}, name_{other.name_} {}
Name::Name(Name &&other)
        : ExpressionBase{other}, name_{std::move(other.name_)} {} // TODO copies ExBase

Name &Name::operator=(Name &&other) noexcept {
    if (this != &other) {
        name_ = std::move(other.name_);
        dynamic_cast<ExpressionBase &>(*this) =
                std::move(dynamic_cast<ExpressionBase &>(other));
    }
    return *this;
}


Value::Value(const Value &other) : ExpressionBase{other}, value_{other.value_} {}
Value::Value(Value &&other) : ExpressionBase{other}, value_{other.value_} {}

Value &Value::operator=(Value &&other) noexcept {
    if (this != &other) {
        value_ = other.value_;
        dynamic_cast<ExpressionBase &>(*this) =
                std::move(dynamic_cast<ExpressionBase &>(other));
    }
    return *this;
}


//Empty::Empty(const Empty &other) : ExpressionBase{other} {}
//Empty::Empty(Empty &&other) : ExpressionBase{other} {}
//
//Empty &Empty::operator=(Empty &&other) noexcept {
//    return moveAssignment(this, std::move(other));
//}
