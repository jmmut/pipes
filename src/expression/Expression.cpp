
#include "Expression.h"
#include <utility>
#include <functional>
#include <commonlibs/RAII.h>
#include "commonlibs/StackTracedException.h"
#include "commonlibs/Walkers.h"
#include "lexer/Lexer.h"
#include "expression/ExpressionString.h"
#include "expression/ExpressionType.h"


////////////////// Expression
Expression::Expression(const Expression &e)
        : type{e.type}
//        , innerVariant{e.innerVariant}
        {
    switch (type) {
    case ExpressionType::UNSET:
    case ExpressionType::NOTHING:
        innerVariant = std::get<Empty>(e.innerVariant);
        return;
    case ExpressionType::VALUE:
        innerVariant = std::get<Value>(e.innerVariant);
        return;
    case ExpressionType::IDENTIFIER:
    case ExpressionType::FIELD:
    case ExpressionType::VARIABLE:
        innerVariant = std::get<Name>(e.innerVariant);
        return;
    case ExpressionType::PARAMETER:
        innerVariant = std::get<Parameter>(e.innerVariant);
        return;
    case ExpressionType::LAMBDA:
        innerVariant = std::make_unique<Lambda>(e.getLambda());
        return;
    case ExpressionType::BINARY_OPERATION:
        innerVariant = std::make_unique<BinaryOperation>(e.getBinaryOperation());
        return;
    case ExpressionType::CALL:
    case ExpressionType::BRANCH:
    case ExpressionType::WHILE:
    case ExpressionType::ARRAY:
    case ExpressionType::TUPLE:
    case ExpressionType::STRUCT:
        innerVariant = std::get<SubExpressions>(e.innerVariant);
        return;
    }
    throw randomize::exception::StackTracedException{"unhandled case " + to_string(e.type)};
}

Expression::Expression(Expression &&e) noexcept
        : type{e.type}, innerVariant{std::move(e.innerVariant)} {
}


Expression::Expression() : type{ExpressionType::UNSET}, innerVariant{Empty{}} {
}

Expression::~Expression() {
    destroy();
}

void Expression::destroy() {
}

Expression &Expression::operator=(const Expression &e) {
    if (&e != this) {
        destroy();
        new((void *) this) Expression{e};
    }
    return *this;
}

Expression &Expression::operator=(Expression &&e) noexcept {
    if (&e != this) {
        destroy();
        new((void *) this) Expression{std::move(e)};
    }
    return *this;
}

bool Expression::operator==(const Expression &other) const {
    if (type != other.type) {
        return false;
    }

    struct Compare : ExpressionOperation {
        bool equal;
        const Expression &other;

        Compare(bool equal, const Expression &other) : equal(equal), other(other) {}

        void operate(const Expression &expression) override {}

        void operate(const long &value) override {
            equal = value == other.getValue();
        }
        void operate(const std::string &identifier) override {
            equal = identifier == (other.type == ExpressionType::IDENTIFIER
                                   ? other.getIdentifier()
                                   : (other.type == ExpressionType::FIELD
                                      ? other.getField()
                                      : other.getVariable()));
        }
        void operate(const Parameter &parameter) override {
            equal = parameter == other.getParameter();
        }
        void operate(const Lambda &lambda) override {
            equal = lambda == other.getLambda();
        }
        void operate(const Call &call) override {
            equal = call == other.getCall();
        }
        void operate(const Branch &branch) override {
            equal = branch == other.getBranch();
        }
        void operate(const While &while_loop) override {
            equal = while_loop == other.getWhile();
        }
        void operate(const BinaryOperation &binaryOperation) override {
            equal = binaryOperation == other.getBinaryOperation();
        }
        void operate(const Array &array) override {
            equal = array == other.getArray();
        };
        void operate(const Tuple &tuple) override {
            equal = tuple == other.getTuple();
        };
        void operate(const Struct &structure) override {
            equal = structure == other.getStructure();
        };
    } compare{false, other};
    operate(*this, compare);
    return compare.equal;
}

bool Expression::operator!=(const Expression &other) const {
    return !(*this == other);
}
std::string Expression::debugEquals(const Expression &other) const {
    if (*this == other) {
        return "expressions are equal";
    }

    if (type != other.type) {
        return "type is different: " + to_string(type) + " != " + to_string(other.type);
    }

    struct Compare : ExpressionOperation {
        std::string equal;
        const Expression &other;

        Compare(std::string defaultMessage, const Expression &other) : equal(defaultMessage), other(other) {}

        void operate(const Expression &expression) override {}

        void operate(const long &value) override {
            if (value != other.getValue()) {
                equal = "value is different: " + std::to_string(value) + " != " + std::to_string(other.getValue());
            }
        }

        void operate(const std::string &identifier) override {
            if (other.getType() == ExpressionType::IDENTIFIER) {
                if (identifier != other.getIdentifier()) {
                    equal = "identifier is different: " + identifier + " != " + other.getIdentifier();
                }
            } else if (other.getType() == ExpressionType::FIELD) {
                if (identifier != other.getField()) {
                    equal = "field is different: " + identifier + " != " + other.getField();
                }
            } else {
                if (identifier != other.getField()) {
                    equal = "variable is different: " + identifier + " != " + other.getVariable();
                }
            }
        }

        void operate(const Parameter &parameter) override {
            if (parameter.closureLevel != other.getParameter().closureLevel) {
                equal = "closure level is different: " + std::to_string(parameter.closureLevel) + " != "
                        + std::to_string(other.getParameter().closureLevel);
            }
        }

        void operate(const Lambda &lambda) override {
            equal = "looking at inner lambda: {"
                    + (lambda.lambdaLevel == other.getLambda().lambdaLevel ?
                       lambda.body().debugEquals(other.getLambda().body()) :
                       "lambda levels are different: "
                               + std::to_string(lambda.lambdaLevel) + " != "
                               + std::to_string(other.getLambda().lambdaLevel))
                    + "}";
        }

        void operate(const Call &call) override {
            equal = "looking at inner call: argument: {" + call.argument().debugEquals(other.getCall().argument())
                    + "}, function: {" + call.function().debugEquals(other.getCall().function()) + "}";
        }

        void operate(const Branch &branch) override {
            equal = "looking at inner branch: yes: {" + branch.yes().debugEquals(other.getBranch().yes())
                    + "}, no: {" + branch.no().debugEquals(other.getBranch().no()) + "}";
        }
        void operate(const While &while_loop) override {
            equal = "looking at inner while: condition: {" + while_loop.condition().debugEquals(other
                    .getWhile().condition())
                    + "}, body: {" + while_loop.body().debugEquals(other.getWhile().body()) + "}";
        }
        void operate(const BinaryOperation &binaryOperation) override {
            equal = "looking at inner binaryOperation " + to_type_string(binaryOperation.operation) + ": left: {"
                    + binaryOperation.left.debugEquals(other.getBinaryOperation().left)
                    + "}, right: {" + binaryOperation.right.debugEquals(other.getBinaryOperation().right) + "}";
        }
        void operate(const Array &array) override {
            if (array.getElements().size() != other.getArray().getElements().size()) {
                equal = "looking at array: different sizes! "
                        + std::to_string(array.getElements().size()) + " vs "
                        + std::to_string(other.getArray().getElements().size()) + ": "
                        + to_string(array) + ", " + to_string(other);
            } else {
                equal = "looking at inner array: {";
                for (size_t i = 0; i < array.getElements().size(); ++i) {
                    equal += array.getElements()[i].debugEquals(other.getArray().getElements()[i]) + ", ";
                }
                equal += "}";
            }
        }
        void operate(const Tuple &tuple) override {
            if (tuple.getElements().size() != other.getTuple().getElements().size()) {
                equal = "looking at tuple: different sizes! "
                        + std::to_string(tuple.getElements().size()) + " vs "
                        + std::to_string(other.getTuple().getElements().size()) + ": "
                        + to_string(tuple) + ", " + to_string(other);
            } else {
                equal = "looking at inner tuple: {";
                for (size_t i = 0; i < tuple.getElements().size(); ++i) {
                    equal += tuple.getElements()[i].debugEquals(other.getTuple().getElements()[i]) + ", ";
                }
                equal += "}";
            }
        }
        void operate(const Struct &structure) override {
            if (structure.getElements().size() != other.getStructure().getElements().size()) {
                equal = "looking at struct: different sizes! "
                        + std::to_string(structure.getElements().size()) + " vs "
                        + std::to_string(other.getStructure().getElements().size()) + ": "
                        + to_string(structure) + ", " + to_string(other);
            } else {
                equal = "looking at inner struct: {";
                for (size_t i = 0; i < structure.getElements().size(); ++i) {
                    equal += structure.getElements()[i].debugEquals(other.getStructure().getElements()[i]) + ", ";
                }
                equal += "}";
            }
        }
    } compare{"??? there's something wrong", other};
    operate(*this, compare);
    return compare.equal;
}

////////////// Getters

void Expression::checkType(ExpressionType type, source_location location) const {
    if (this->type != type) {
        throw randomize::exception::StackTracedException() << "Compiler bug: expected a " << type
                << " but expression is of type " << this->type << " at " << location;
    }
}
void Expression::checkTypeAnyOf(std::initializer_list<ExpressionType> types,
                                source_location location) const {
    if (not std::any_of(types.begin(), types.end(),
                        [myType = this->type](auto t) { return t == myType; })) {
        throw randomize::exception::StackTracedException()
                << "Compiler bug: expected any of " << randomize::utils::to_string(types)
                << " but expression is of type " << this->type << " at " << location;
    }
}

long Expression::getValue(source_location location) const {
    return as<ExpressionType::VALUE>().value();
}
const std::string &Expression::getIdentifier(source_location location) const {
    return as<ExpressionType::IDENTIFIER>(location).name();
}
const std::string &Expression::getField(source_location location) const {
    return as<ExpressionType::FIELD>(location).name();
}
const std::string &Expression::getVariable(source_location location) const {
    return as<ExpressionType::VARIABLE>(location).name();
}
const Parameter &Expression::getParameter(source_location location) const {
    return as<ExpressionType::PARAMETER>(location);
}
const Lambda &Expression::getLambda(source_location location) const {
    return as<ExpressionType::LAMBDA>(location);
}
const Call &Expression::getCall(source_location location) const {
    return as<ExpressionType::CALL>(location);
}
const Branch &Expression::getBranch(source_location location) const {
    return as<ExpressionType::BRANCH>(location);
}
const While &Expression::getWhile(source_location location) const {
    return as<ExpressionType::WHILE>(location);
}
const BinaryOperation &Expression::getBinaryOperation(source_location location) const {
    return as<ExpressionType::BINARY_OPERATION>(location);
}
const Array &Expression::getArray(source_location location) const {
    return as<ExpressionType::ARRAY>(location);
}
const Tuple &Expression::getTuple(source_location location) const {
    return as<ExpressionType::TUPLE>(location);
}
const Struct &Expression::getStructure(source_location location) const {
    return as<ExpressionType::STRUCT>(location);
}

long &Expression::getValue(source_location location) {
    return as<ExpressionType::VALUE>(location).value();
}
std::string &Expression::getIdentifier(source_location location) {
    return as<ExpressionType::IDENTIFIER>(location).name();
}
std::string &Expression::getField(source_location location) {
    return as<ExpressionType::FIELD>(location).name();
}
std::string &Expression::getVariable(source_location location) {
    return as<ExpressionType::VARIABLE>(location).name();
}
Parameter &Expression::getParameter(source_location location) {
    return as<ExpressionType::PARAMETER>(location);
}
Lambda &Expression::getLambda(source_location location) {
    return as<ExpressionType::LAMBDA>(location);
}
Call &Expression::getCall(source_location location) {
    return as<ExpressionType::CALL>(location);
}
Branch &Expression::getBranch(source_location location) {
    return as<ExpressionType::BRANCH>(location);
}
While &Expression::getWhile(source_location location) {
    return as<ExpressionType::WHILE>(location);
}
BinaryOperation &Expression::getBinaryOperation(source_location location) {
    return as<ExpressionType::BINARY_OPERATION>(location);
}
Array &Expression::getArray(source_location location) {
    return as<ExpressionType::ARRAY>(location);
}
Tuple &Expression::getTuple(source_location location) {
    return as<ExpressionType::TUPLE>(location);
}
Struct &Expression::getStructure(source_location location) {
    return as<ExpressionType::STRUCT>(location);
}


////////////// Builders
Expression Expression::ofNothing(PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::NOTHING;
    expression.innerVariant = Empty{};
    expression.position() = positionInCode;
    return expression;
}

Expression Expression::ofConstant(long n, PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::VALUE;
    expression.innerVariant = Value{n};
    expression.position() = positionInCode;
    expression.pipesType() = IntType::make_ptr("");
    return expression;
}

Expression Expression::ofIdentifier(std::string name, PtrPipesType &&type,
                                    PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::IDENTIFIER;
    auto e = Name{std::move(name)};
    e.position() = positionInCode;
    e.pipesType() = std::move(type);
    expression.innerVariant = e;
    return expression;
}

Expression Expression::ofField(std::string name, PtrPipesType &&pipesType,
                               PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::FIELD;
    expression.innerVariant = Name{std::move(name)};
    expression.position() = positionInCode;
    expression.pipesType() = std::move(pipesType);
    return expression;
}

Expression Expression::ofVariable(std::string name, PtrPipesType &&pipesType,
                                  PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::VARIABLE;
    expression.innerVariant = Name{std::move(name)};
    expression.position() = positionInCode;
    expression.pipesType() = std::move(pipesType);
    return expression;
}

Expression Expression::ofParameter(std::string name, long closureLevel, long nestingLevel,
                                   long lambdaId, long enclosingLambdaId,
                                   PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::PARAMETER;

    auto parameter = Parameter{positionInCode, UnknownType::make_ptr("")};
    parameter.name = std::move(name);
    parameter.nestingLevel = nestingLevel;
    parameter.closureLevel = closureLevel;
    parameter.lambdaId = lambdaId;
    parameter.enclosingLambdaId = enclosingLambdaId;
    parameter.isVirtual = false;

    expression.innerVariant = std::move(parameter);
    return expression;
}

Expression Expression::ofVirtualParameter(std::string name, long closureLevel, long nestingLevel,
                                   long lambdaId, long enclosingLambdaId,
                                   PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::PARAMETER;
    auto parameter = Parameter{positionInCode, UnknownType::make_ptr("")};
    parameter.name = std::move(name);
    parameter.nestingLevel = nestingLevel;
    parameter.closureLevel = closureLevel;
    parameter.lambdaId = lambdaId;
    parameter.enclosingLambdaId = enclosingLambdaId;
    parameter.isVirtual = true;

    expression.innerVariant = std::move(parameter);
    return expression;
}

Expression Expression::ofLambda(const Lambda &f, PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::LAMBDA;
    expression.innerVariant = std::make_unique<Lambda>(f);
    expression.getLambda().position() = positionInCode;
    expression.getLambda().pipesType() = FunctionType::make_ptr(UnknownType::make_ptr(f.argumentName),
                                                    UnknownType::make_ptr(""), "");
    return expression;
}

Expression Expression::ofLambda(long lambdaId, std::string &&argumentName, long lambdaLevel, Expression &&body,
                                PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::LAMBDA;
    Lambda f;
    f.id = lambdaId;
    f.argumentName = std::move(argumentName);
    f.lambdaLevel = lambdaLevel;
    f.body() = std::move(body);
    expression.innerVariant = std::make_unique<Lambda>(f);
    expression.getLambda().position() = positionInCode;
    expression.getLambda().pipesType() = FunctionType::make_ptr(
            UnknownType::make_ptr(expression.getLambda().argumentName),
            UnknownType::make_ptr(""), ""); // TODO body.pipesType() ?
    return expression;
}

SubExpressions buildSubExpressions(Expressions &&expressions
                                   , PositionInCode positionInCode, PtrPipesType &&pipesType
                                   ) {
    SubExpressions e;
    e.elements = std::move(expressions);
    e.position() = positionInCode;
    e.pipesType() = std::move(pipesType);
    return e;
}

Expression Expression::ofCall(Expression &&argument, Expression &&function, PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::CALL;
    expression.innerVariant = buildSubExpressions({std::move(argument), std::move(function)},
                                                  positionInCode, UnknownType::make_ptr(""));
    return expression;
}

Expression Expression::ofBranch(Expression &&yes, Expression &&no, PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::BRANCH;
    expression.innerVariant = buildSubExpressions(
            {std::move(yes), std::move(no)}, positionInCode,
            FunctionType::make_ptr(IntType::make_ptr(""), UnknownType::make_ptr(""), ""));
    return expression;
}

Expression Expression::ofWhile(Expression &&condition, Expression &&body,
                               PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::WHILE;
    expression.innerVariant = buildSubExpressions({std::move(condition), std::move(body)},
                                                  positionInCode, IntType::make_ptr(""));
    // TODO: NothingType? return number of iterations?
    return expression;
}

Expression Expression::ofBinaryOperation(Token::Type operation, Expression &&left, Expression &&right,
                                         PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::BINARY_OPERATION;
    BinaryOperation bo;
    bo.operation = operation;
    bo.left = std::move(left);
    bo.right = std::move(right);
    expression.innerVariant = std::make_unique<BinaryOperation>(bo);
    expression.getBinaryOperation().position() = positionInCode;
    return expression;
}


Expression Expression::ofArray(Expressions &&expressions, PositionInCode positionInCode) {
    Expression expression;
    expression.getType() = ExpressionType::ARRAY;
    expression.innerVariant = buildSubExpressions(std::move(expressions), positionInCode,
                                                  ArrayType::make_ptr(UnknownType::make_ptr(""), ""));
    return expression;
}
Expression Expression::ofTuple(Expressions &&expressions, PositionInCode positionInCode) {
    auto types = PipesTypes{};
    for (const auto &expr : expressions) {
        types.push_back(expr.pipesType()->copy());
    }
    Expression expression;
    expression.getType() = ExpressionType::TUPLE;
    expression.innerVariant = buildSubExpressions(std::move(expressions), positionInCode,
                                                  TupleType::make_ptr(std::move(types), ""));
    return expression;
}
Expression Expression::ofStruct(Expressions &&identifiers, PositionInCode positionInCode) {
    PipesTypes types;
    for (const auto &identifier : identifiers) {
        if (identifier.type != ExpressionType::FIELD) {
            throw randomize::exception::StackTracedException{
                    "expected " + to_string(ExpressionType::FIELD) + ", got "
                            + to_string(identifier.type) + " "
                            + quote(to_string_with_types(identifier))};
        }
        types.push_back(identifier.pipesType()->copy());
    }
    Expression expression;
    expression.getType() = ExpressionType::STRUCT;
    expression.innerVariant = buildSubExpressions(std::move(identifiers), positionInCode,
                                                  UserType::make_ptr(std::move(types), ""));
    return expression;
}

PositionInCode &Expression::position() {
    return getExpressionBase().ExpressionBase::position();
}

const PositionInCode &Expression::position() const {
    return getExpressionBase().ExpressionBase::position();
}

PtrPipesType &Expression::pipesType() {
    return getExpressionBase().ExpressionBase::pipesType();
}

const PtrPipesType &Expression::pipesType() const {
    return getExpressionBase().ExpressionBase::pipesType();
}

const ExpressionBase &Expression::getExpressionBase() const {
    switch(type) {
    case ExpressionType::UNSET:
        return as<ExpressionType::UNSET>();
    case ExpressionType::NOTHING:
        return as<ExpressionType::NOTHING>();
    case ExpressionType::VALUE:
        return as<ExpressionType::VALUE>();
    case ExpressionType::LAMBDA:
        return as<ExpressionType::LAMBDA>();
    case ExpressionType::IDENTIFIER:
        return as<ExpressionType::IDENTIFIER>();
    case ExpressionType::CALL:
        return as<ExpressionType::CALL>();
    case ExpressionType::PARAMETER:
        return as<ExpressionType::PARAMETER>();
    case ExpressionType::BRANCH:
        return as<ExpressionType::BRANCH>();
    case ExpressionType::WHILE:
        return as<ExpressionType::WHILE>();
    case ExpressionType::BINARY_OPERATION:
        return as<ExpressionType::BINARY_OPERATION>();
    case ExpressionType::ARRAY:
        return as<ExpressionType::ARRAY>();
    case ExpressionType::TUPLE:
        return as<ExpressionType::TUPLE>();
    case ExpressionType::STRUCT:
        return as<ExpressionType::STRUCT>();
    case ExpressionType::FIELD:
        return as<ExpressionType::FIELD>();
    case ExpressionType::VARIABLE:
        return as<ExpressionType::VARIABLE>();
    }
    throw randomize::exception::StackTracedException{}
            << "missing case in switch at "
            << source_location{};
}

/** see https://stackoverflow.com/a/47369227 */
ExpressionBase &Expression::getExpressionBase() {
    return const_cast<ExpressionBase&>(std::as_const(*this).getExpressionBase());
}

////////////////// LAMBDA
bool operator==(const Lambda &lhs, const Lambda &rhs) {
    return lhs.lambdaLevel == rhs.lambdaLevel &&
           lhs.body() == rhs.body();
}

bool operator!=(const Lambda &lhs, const Lambda &rhs) {
    return !(rhs == lhs);
}

////////////////// BinaryOperation
bool operator==(const BinaryOperation &lhs, const BinaryOperation &rhs) {
    return lhs.operation == rhs.operation &&
            lhs.left == rhs.left &&
            lhs.right == rhs.right;
}
bool operator!=(const BinaryOperation &lhs, const BinaryOperation &rhs) {
    return !(rhs == lhs);
}


void recursivelyDo(const Expression &expression, const std::function<void(const Expression &)> &preAction,
                   const std::function<void(const Expression &)> &postAction) {
    RAII actioner([&]() {preAction(expression);},
                  [&]() {postAction(expression);});
    switch (expression.getType()) {
    case ExpressionType::UNSET:
    case ExpressionType::NOTHING:
    case ExpressionType::VALUE:
    case ExpressionType::IDENTIFIER:
    case ExpressionType::FIELD:
    case ExpressionType::VARIABLE:
    case ExpressionType::PARAMETER:
        return;
    case ExpressionType::LAMBDA:
        recursivelyDo(expression.getLambda().body(), preAction, postAction);
        return;
    case ExpressionType::CALL:
        recursivelyDo(expression.getCall().argument(), preAction, postAction);
        recursivelyDo(expression.getCall().function(), preAction, postAction);
        return;
    case ExpressionType::BRANCH:
        recursivelyDo(expression.getBranch().yes(), preAction, postAction);
        recursivelyDo(expression.getBranch().no(), preAction, postAction);
        return;
    case ExpressionType::WHILE:
        recursivelyDo(expression.getWhile().condition(), preAction, postAction);
        recursivelyDo(expression.getWhile().body(), preAction, postAction);
        return;
    case ExpressionType::BINARY_OPERATION:
        recursivelyDo(expression.getBinaryOperation().left, preAction, postAction);
        recursivelyDo(expression.getBinaryOperation().right, preAction, postAction);
        return;
    case ExpressionType::ARRAY:
        for (const auto &element : expression.getArray().getElements()) {
            recursivelyDo(element, preAction, postAction);
        }
        return;
    case ExpressionType::TUPLE:
        for (const auto &element : expression.getTuple().getElements()) {
            recursivelyDo(element, preAction, postAction);
        }
        return;
    case ExpressionType::STRUCT:
        for (const auto &element : expression.getStructure().getElements()) {
            recursivelyDo(element, preAction, postAction);
        }
        return;
    }
    throw randomize::exception::StackTracedException{"unhandled Expression case"};
}
void recursivelyDo(const Expression &expression, const std::function<void(const Expression &)> &action) {
    recursivelyDo(expression, action, [](const Expression&) {});
}

void addBichildRewritesToStack(Expression &left, Expression &right,
                               std::vector<Expression *> &preStack,
                               std::vector<Expression *> &postStack) {
    preStack.push_back(&right);
    preStack.push_back(&left);
    postStack.push_back(&right);
    postStack.push_back(nullptr);
    postStack.push_back(&left);
}

void addElementRewritesToStack(std::vector<Expression> &elements,
                               std::vector<Expression *> &preStack,
                               std::vector<Expression *> &postStack) {
    if (elements.empty()) {
        preStack.push_back(nullptr);
    } else {
        auto iter = elements.rbegin();
        preStack.push_back(&*iter);
        postStack.push_back(&*iter);
        for (++iter; iter != elements.rend(); ++iter) {
            preStack.push_back(&*iter);
            postStack.push_back(nullptr);
            postStack.push_back(&*iter);
        }
    }
}

void addInnerRewritesToStack(Expression &expression, std::vector<Expression *> &preStack,
                             std::vector<Expression *> &postStack) {
    switch (expression.getType()) {
        case ExpressionType::UNSET:
        case ExpressionType::NOTHING:
        case ExpressionType::VALUE:
        case ExpressionType::IDENTIFIER:
        case ExpressionType::FIELD:
        case ExpressionType::VARIABLE:
        case ExpressionType::PARAMETER:
            preStack.push_back(nullptr);
            return;
        case ExpressionType::LAMBDA:
            preStack.push_back(&expression.getLambda().body());
            postStack.push_back(&expression.getLambda().body());
            return;
        case ExpressionType::CALL:
            addBichildRewritesToStack(expression.getCall().argument(), expression.getCall().function(),
                                      preStack, postStack);
            return;
        case ExpressionType::BRANCH:
            addBichildRewritesToStack(expression.getBranch().yes(), expression.getBranch().no(),
                                      preStack, postStack);
            return;
        case ExpressionType::WHILE:
            addBichildRewritesToStack(expression.getWhile().condition(), expression.getWhile().body(),
                                      preStack, postStack);
            return;
        case ExpressionType::BINARY_OPERATION:
            addBichildRewritesToStack(expression.getBinaryOperation().left,
                                      expression.getBinaryOperation().right,
                                      preStack, postStack);
            return;
        case ExpressionType::ARRAY:
            addElementRewritesToStack(expression.getArray().getElements(), preStack, postStack);
            return;
        case ExpressionType::TUPLE:
            addElementRewritesToStack(expression.getTuple().getElements(), preStack, postStack);
            return;
        case ExpressionType::STRUCT:
            addElementRewritesToStack(expression.getStructure().getElements(), preStack, postStack);
            return;
    }
    throw randomize::exception::StackTracedException{"unhandled Expression case"};
}

void recursivelyRewrite(Expression &expression,
                        const std::function<void(Expression &)> &preAction,
                        const std::function<void(Expression &)> &postAction) {
    std::vector<Expression*> preStack;
    std::vector<Expression*> postStack;
    preStack.push_back(&expression);
    postStack.push_back(&expression);
    while (not preStack.empty() and not postStack.empty()) {
        if (preStack.back() != nullptr) {
            // going down
            Expression *current = preStack.back();
            preAction(*current);
            preStack.pop_back();
            addInnerRewritesToStack(*current, preStack, postStack);
        } else if (postStack.back() != nullptr) {
            // going up. a nullptr in preStack means go up until there's a nullptr in the postStack
            postAction(*postStack.back());
            postStack.pop_back();
        } else {
            // go down again
            preStack.pop_back();
            postStack.pop_back();
        }
    }
}

void recursivelyRewrite(Expression &expression, const std::function<void(Expression &)> &preAction) {
    recursivelyRewrite(expression, preAction, [](Expression&) {});
}

void operate(const Expression &expression, ExpressionOperation &operation) {
    switch (expression.getType()) {
    case ExpressionType::UNSET:
    case ExpressionType::NOTHING:
        operation.operate(expression);
        return;
    case ExpressionType::VALUE:
        operation.operate(expression.getValue());
        return;
    case ExpressionType::IDENTIFIER:
        operation.operate(expression.getIdentifier());
        return;
    case ExpressionType::FIELD:
        operation.operate(expression.getField());
        return;
    case ExpressionType::VARIABLE:
        operation.operate(expression.getVariable());
        return;
    case ExpressionType::PARAMETER:
        operation.operate(expression.getParameter());
        return;
    case ExpressionType::LAMBDA:
        operation.operate(expression.getLambda());
        return;
    case ExpressionType::CALL:
        operation.operate(expression.getCall());
        return;
    case ExpressionType::BRANCH:
        operation.operate(expression.getBranch());
        return;
    case ExpressionType::WHILE:
        operation.operate(expression.getWhile());
        return;
    case ExpressionType::BINARY_OPERATION:
        operation.operate(expression.getBinaryOperation());
        return;
    case ExpressionType::ARRAY:
        operation.operate(expression.getArray());
        return;
    case ExpressionType::TUPLE:
        operation.operate(expression.getTuple());
        return;
    case ExpressionType::STRUCT:
        operation.operate(expression.getStructure());
        return;
    }
    throw randomize::exception::StackTracedException{"unhandled case " + to_string(expression.getType())};
}

