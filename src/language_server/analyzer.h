/**
 * @file analyzer.h
 * @date 2022-12-22
 */

#ifndef PIPES_ANALYZER_H
#define PIPES_ANALYZER_H


#include "expression/Expression.h"

inline std::optional<std::reference_wrapper<const Expression>> find_expression_at(
        const Program &program, PositionInCode positionInCode) {
    std::vector<std::reference_wrapper<const Expression>> expressions_in_line;
    recursivelyDo(program.expression, [&](const Expression &expression) {
        if (expression.position().line == positionInCode.line) {
            expressions_in_line.push_back(std::cref(expression));
        }
    });
    std::stable_sort(expressions_in_line.begin(), expressions_in_line.end(),
              [](const auto &left, const auto &right) {
                  return left.get().position().charInLine < right.get().position().charInLine;
              });
    for (auto it = expressions_in_line.rbegin(); it != expressions_in_line.rend(); ++it) {
        if (it->get().position().charInLine < positionInCode.charInLine) {
            return std::make_optional(it.operator*());
        }
    }
    return std::optional<std::reference_wrapper<const Expression>>{};
}

#endif //PIPES_ANALYZER_H
