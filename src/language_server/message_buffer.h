/**
 * @file message_buffer.h
 * @date 2022-12-20
 */

#ifndef PIPES_MESSAGE_BUFFER_H
#define PIPES_MESSAGE_BUFFER_H

#include <sstream>

/**
 * In mac, I'm using clang 13 whose libc++ hasn't implemented stringstream::view() yet, which would
 * allow `buffer_body.view().size()`.
 */
inline long size(const std::stringstream &ss) {
    std::streambuf* buf = ss.rdbuf();

    // Get the current position so we can restore it later
    auto original = buf->pubseekoff(0, ss.cur, std::stringstream::out);

    // Seek to end and get the position
    auto end = buf->pubseekoff(0, ss.end, std::stringstream::out);

    // Restore the position
    buf->pubseekpos(original, std::stringstream::out);

    return end;
}

inline bool did_header_just_complete(const std::stringstream &buffer) {
    auto buffer_size = size(buffer);
    if (buffer_size > 4 and buffer.str().substr(buffer_size - 4) == "\r\n\r\n") {
        return true;
    } else {
        return false;
    }
}

inline long extract_body_size_from_header(const std::string &header) {
    auto size_str = header.substr(std::string{"Content-Length: "}.size(), header.find("\r\n"));
    return std::stol(size_str);
}

class MessageBuffer {
public:
    MessageBuffer() {
        reset();
    }

    void handle_char(char c) {
        if (not header_complete) {
            buffer_header << c;
            if (did_header_just_complete(buffer_header)) {
                header_complete = true;
                body_size = extract_body_size_from_header(buffer_header.str());
            }
        } else if (not body_complete) {
            buffer_body << c;
            if (size(buffer_body) == body_size) {
                body_complete = true;
            }
        } else {
            throw std::runtime_error{
                    "header and body are already complete,"
                    " you should have reset MessageBuffer before passing more data"};
        }
    }

    bool is_message_complete() const {
        return header_complete and body_complete;
    }

    std::string body() const {
        return buffer_body.str();
    }

    void reset() {
        buffer_header = std::stringstream{}; // C++ I hate you. no easy way to reset a stringstream other than recreating.
        buffer_body = std::stringstream{};
        header_complete = false;
        body_complete = false;
        body_size = -1;
    }

private:
    std::stringstream buffer_header;
    std::stringstream buffer_body;
    bool header_complete;
    long body_size;
    bool body_complete;
};
#endif //PIPES_MESSAGE_BUFFER_H
