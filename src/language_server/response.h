/**
 * @file protocol.h
 * @date 2022-12-20
 */

#ifndef PIPES_RESPONSE_H
#define PIPES_RESPONSE_H

#include <string>
#include "request.h"
#include "analyzer.h"

/**
 * The correct way to do this would probably be using stringstreams, but the << operator feels
 * uncomfortable to me nowadays. If you start with "the foo %s is bar", you would have to
 * split that in 2: `ss << "the foo " << value << " is bar"`, and if the string is a raw string
 * with internal indentation it's worse.
 */
inline std::string format(const std::string &format, const std::string &value) {
    std::string buffer;
    buffer.resize(format.size() + value.size(), '\0');
    snprintf(buffer.data(), buffer.size(), format.c_str(), value.c_str());

    // the format specifier like %s (or something longer) makes buffer have too many \0 at the end.
    buffer.resize(buffer.find('\0'));

    return buffer;
}

inline std::string format(const std::string &format, const std::string &value_1, const std::string &value_2) {
    std::string buffer;
    buffer.resize(format.size() + value_1.size() + value_2.size(), '\0');
    snprintf(buffer.data(), buffer.size(), format.c_str(), value_1.c_str(), value_2.c_str());

    // the format specifier like %s (or something longer) makes buffer have too many \0 at the end.
    buffer.resize(buffer.find('\0'));

    return buffer;
}

inline std::string generate_error_unimplemented() {
    return R"(
{
    "jsonrpc": "2.0",
    "id": 1,
    "error": {
        "code": -32601,
        "message": "unimplemented Pipes language server"
    }
}
)";
}

inline std::string generate_notification(std::string notification) {
    return format(R"(
{
    "jsonrpc": "2.0",
    "method": "window/showMessage",
    "params": {
        "type": 2,
        "message": "%s"
    }
}
)", notification);
}

inline std::string generate_message_with_header(const std::string &body) {
    return "Content-Length: " + std::to_string(body.size())
            + "\r\nContent-Type: application/vscode-jsonrpc; charset=utf-8\r\n\r\n"
            + body;
}


/**
 *
 * The server provides go to declaration support.
declarationProvider?: boolean | DeclarationOptions | DeclarationRegistrationOptions;

 * The server provides goto definition support.
definitionProvider?: boolean | DefinitionOptions;

"signatureHelpProvider": {},
"completionProvider": {"resolveProvider": true},

 */
inline std::string generate_response_initialize(std::string id) {
    return format(R"(
{
    "jsonrpc": "2.0",
    "id": "%s",
    "result": {
        "capabilities": {
            "textDocumentSync": {change: 0},
            "hoverProvider": true
        }
    }
}
)", id);
}

inline std::string format_hover_description(const Expression &expression) {
    using enum ExpressionType;
    auto description = to_string(expression.getType()) + " expression";
    if (std::set{IDENTIFIER, PARAMETER}.contains(expression.getType())) {
        description += " " + quote(expression.getIdentifier());
    }
    if (expression.getType() == BINARY_OPERATION) {
        description += " " + quote(to_string(expression.getBinaryOperation().operation));
    }
    auto type_description = "type = " + to_string(expression.pipesType());
    if (std::set{CALL, BRANCH, WHILE, BINARY_OPERATION}.contains(expression.getType())) {
        type_description = "result " + type_description;
    }
    if (expression.getType() == CALL) {
        const auto &call = expression.getCall();
        type_description = "argument type = "
                + to_string(call.argument().pipesType())
                + "\ntype of function being called = "
                + to_string(call.function().pipesType())
                + "\n" + type_description;
    }
    return description + "\n" + type_description;
}

inline std::string generate_response_hover(const Request &request) {
    auto code = getCodeFromPath(request.file());
    auto program = tokenizeAndParseAndCheckTypes(code);
    auto expression_opt = find_expression_at(program, request.location());
    auto expression = expression_opt.value().get();
    auto pipes_type = format_hover_description(expression);

    // parse file in request
    // extract expression at request.location()
    // print type in result.contents.value
    return format(R"(
{
    "jsonrpc": "2.0",
    "id": "%s",
    "result": {
        "contents": {
            "kind": "markdown",
            "value": "%s"
        }
    }
}
)", request.id(), pipes_type);
}

inline std::string generate_response_shutdown(std::string id) {
    return format(R"(
{
    "jsonrpc": "2.0",
    "id": "%s",
    "result": null
}
)", id);
}

struct Response {
    bool should_answer;
    std::string message;

    static Response some(std::string message) {
        return Response {true, std::move(message)};
    }
    static Response none() {
        return Response {false, ""};
    }
};

inline std::ostream &operator<<(std::ostream &out, const Response &response) {
    if (response.should_answer) {
        return out << response.message;
    } else {
        return out << "No response needed";
    }
}

inline Response choose_response(const Request &request) {
    switch (request.method) {
    case UNKNOWN:
        return Response::some(generate_error_unimplemented());
    case INITIALIZE:
        return Response::some(generate_response_initialize(request.id()));
    case INITIALIZED:
        return Response::none();
    case HOVER:
        return Response::some(generate_response_hover(request));
    case SHUTDOWN:
        return Response::some(generate_response_shutdown(request.id()));
    };
    throw std::logic_error{"unhandled case in switch for method: " + quote(to_string(request.method))};
}

inline std::string uglify(std::string s) {
    s.erase(
            std::remove_if(s.begin(), s.end(), [](char c) {
                return c == ' ' or c == '\n';
            }),
            s.end()
    );
    return s;
}

inline Response response(const std::string &request) {
    auto parsed = parse(request);
    auto response_body = choose_response(parsed);
    if (response_body.should_answer) {
        return Response::some(generate_message_with_header(response_body.message));
    } else {
        return response_body;
    }
}

#endif //PIPES_RESPONSE_H
