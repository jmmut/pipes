/**
 * @file request.h
 * @date 2022-12-21
 */

#ifndef PIPES_REQUEST_H
#define PIPES_REQUEST_H

#include <string>
#include <stdexcept>
#include <optional>

enum Method {
    UNKNOWN,
    INITIALIZE,
    INITIALIZED,
    HOVER,
    SHUTDOWN,
};

inline Method parse_method(std::string str) {
    if (str == "initialize") {
        return Method::INITIALIZE;
    } else if (str == "initialized") {
        return Method::INITIALIZED;
    } else if (str == "textDocument/hover") {
        return Method::HOVER;
    } else if (str == "shutdown") {
        return Method::SHUTDOWN;
    } else {
        return Method::UNKNOWN;
    }
}

inline std::string to_string(Method method) {
    switch(method) {
    case UNKNOWN:
        return "UNKNOWN";
    case INITIALIZE:
        return "INITIALIZE";
    case INITIALIZED:
        return "INITIALIZED";
    case HOVER:
        return "HOVER";
    case SHUTDOWN:
        return "SHUTDOWN";
    }
    throw std::logic_error{"unhandled case in switch for method with code: "
                                   + quote(std::to_string(method))};
}

inline bool method_requires_response_with_id(Method method) {
    return std::set{INITIALIZE, HOVER, SHUTDOWN}.contains(method);
}

inline bool method_requires_position(Method method) {
    return std::set{HOVER}.contains(method);
}

inline std::string extract_str(const std::string &request, const std::string &key) {
    auto method_pos = request.find(key);
    if (method_pos == std::string::npos) {
        throw std::runtime_error{
                "Can't parse request. Couldn't find " + key + ": " + request};
    }

    auto method_start = request.find('\"', method_pos + key.size());
    if (method_pos == std::string::npos) {
        throw std::runtime_error{
                "Can't parse request. Couldn't find quote value of " + key + ": " + request};
    }
    method_start += 1;
    auto method_end = request.find('\"', method_start);
    if (method_pos == std::string::npos) {
        throw std::runtime_error{"Can't parse request. Couldn't find end quote of value of "
                                         + key + ": " + request};
    }
    auto method_str = request.substr(method_start, method_end - method_start);
    return method_str;
}

inline std::string extract_method(const std::string &request) {
    return extract_str(request, double_quote("method"));
}

inline long parse_next_number(const std::string &message, size_t pos = 0) {
    while (pos < message.size() and not isNumber(message[pos])) {
        ++pos;
    }
    int n = 0;
    while (pos < message.size() and isNumber(message[pos])) {
        char c = message[pos];
        n *= 10; // assume base-10
        n += c - '0';
        ++pos;
    }
    return n;
}

inline PositionInCode extract_position(const std::string &request) {
    auto pos_str = double_quote("position");
    auto pos_pos = request.find(pos_str);
    if (pos_pos == std::string::npos) {
        throw std::runtime_error{
                "Can't parse request. Couldn't find " + quote(pos_str) + ": " + request};
    }
    auto line_str = double_quote("line");
    auto line_pos = request.find(line_str, pos_pos);
    if (line_pos == std::string::npos) {
        throw std::runtime_error{
                "Can't parse request. Couldn't find " + quote(line_str) + ": " + request};
    }
    auto line = parse_next_number(request, line_pos);

    auto character_str = double_quote("character");
    auto character_pos = request.find(character_str, pos_pos);
    if (character_pos == std::string::npos) {
        throw std::runtime_error{
                "Can't parse request. Couldn't find " + quote(character_str) + ": " + request};
    }
    auto character = parse_next_number(request, character_pos);
    return PositionInCode{-1, line, character};
}

inline std::string extract_file(const std::string &request) {
    auto uri = extract_str(request, double_quote("uri"));
    auto file_path = uri.substr(std::string{"file://"}.size());
    return file_path;
}

struct Request {
    Request(Method method, std::string id, std::optional<PositionInCode> pos,
            std::optional<std::string> file)
        : method{method}, id_{std::move(id)}, location_{pos}, file_{std::move(file)} {}

    Method method;
    std::string id() const {
        if (id_.empty()) {
            throw std::logic_error{"requested id on a request " + quote(to_string(method))
                                           + " that didn't parse an id"};
        }
        return id_;
    }
    PositionInCode location() const {
        if (not location_.has_value()) {
            throw std::logic_error{"requested location on a request " + quote(to_string(method))
                                           + " that didn't parse source position"};
        }
        return location_.value();
    }
    std::string file() const {
        if (not file_.has_value()) {
            throw std::logic_error{"requested file on a request " + quote(to_string(method))
                                           + " that didn't parse uri string"};
        }
        return file_.value();
    }
private:
    std::string id_; ///< might be empty if the request is a notification, which don't need response
    std::optional<PositionInCode> location_;
    std::optional<std::string> file_;
};

inline Request parse(const std::string &request) {
    auto method = parse_method(extract_method(request));
    auto id = std::string{};
    auto pos = std::optional<PositionInCode>{};
    auto file = std::optional<std::string>{};
    if (method_requires_response_with_id(method)) {
        id = extract_str(request, double_quote("id"));
    }
    if (method_requires_position(method)) {
        pos = extract_position(request);
        file = extract_file(request);
    }
    return Request{method, id, pos, file};
}


#endif //PIPES_REQUEST_H
