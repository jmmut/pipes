#include <iostream>
#include "commonlibs/log.h"
#include "commonlibs/SignalHandler.h"
#include "assembler/PipesAssembler.h"


using std::literals::operator ""s;


bool isExitCommand(const std::string &line) {
    return line == "\4" or line == "quit" or line == "exit";
}

int parseInt(const std::string &textInt) {
    int result = 0;
    for (const auto &letter : textInt) {
        if (letter >= '0' and letter <= '9') {
            result = result * 10 + letter - '0';
        } else {
            throw std::invalid_argument{
                    "Can't parse a int from a string that contains non-digit chars: '"
                            + textInt + "'"};
        }
    }
    return result;
}

void extractArgument(std::string &line, int &arg, bool &wasPresent, bool &error) {
    unsigned long found = line.find('$');
    arg = 0;
    wasPresent = false;
    error = false;
    if (found != std::string::npos) {
        wasPresent = true;
        try {
            arg = parseInt(line.substr(0, found));
            line = line.substr(found+1);
        } catch (std::exception &exception) {
            wasPresent = false;
            // TODO: think a better way to pass arguments in the repl
//            std::cout << "only numeric arguments are supported. \"" << line.substr(0, found) << "\" is not a number."
//                      << std::endl;
//            error = true;
        }
    }
}

void printInterpretation(int arg, const CodeFile &code) {
    try {
        auto program = tokenizeAndParseAndCheckTypes(code);
        auto assembler = PipesAssembler{program};
        std::cout << "\nExpression: " << to_string(assembler.getExpression()) << std::endl;
        try {
            auto func = assembler.getEntryPoint();
            auto result = func(arg);
            std::cout << "Result: " << result << std::endl << std::endl;
        } catch (std::exception &exception) {
            std::cout << "failed to execute!: " << exception.what() << std::endl;
        }
    } catch (std::exception &exception) {
        std::cout << "failed to compile!: " << exception.what() << std::endl;
    }
}

void extractArgumentAndPrintInterpretation(std::string line, std::optional<int> argument) {
    auto arg = 0;
    bool wasPresent;
    bool error;
    extractArgument(line, arg, wasPresent, error);
    if (error) {
        return;
    }
    auto code = isPipesCodeFile(line)? getCodeFromPath(line) : CodeFile{line, ".", ""};
    if (argument.has_value()) {
        if (wasPresent) {
            std::cout << "Only one argument can be passed: either as CLI argument or as a 'x$ '"
                         "prefix to the pipes string" << std::endl;
            return;
        } else {
            arg = argument.value();
        }
    }

    printInterpretation(arg, code);
}

void printHelp(std::string programName) {
    std::cout << "Just-In-Time Pipes compiler. Usage:"
              << "\n\n  " << programName << "  --help/-h\n    print this help and exit."
              << "\n\n  " << programName << "  PIPES_CODE_STRING\n    execute PIPES_CODE_STRING and exit."
              << "\n\n  " << programName << "  PIPES_FILE\n    "
                                            "load and execute PIPES_FILE with an argument 0 and exit."
              << "\n\n  " << programName << "  PIPES_CODE_STRING_OR_FILE  ARGUMENT\n    "
                                            "load and execute PIPES_FILE with the number ARGUMENT as argument."
              << "\n\n  " << programName << "  PIPES_CODE_STRING_OR_FILE  ARGUMENT  LOG_LEVEL\n    "
                                            "load and execute PIPES_FILE with the number ARGUMENT as argument and a log_level ("
                      << randomize::utils::to_string(randomize::log::getNames()) << ")"
              << "\n";
}

int main (int argc, char **argv) {
    randomize::exception::SignalHandler::activate();
    randomize::log::setLogLevel(randomize::log::INFO_LEVEL);
    if (argc == 1) {
        printHelp(argv[0]);
    } else if (argc == 2) {
        if (argv[1] == "--help"s or argv[1] == "-h"s) {
            printHelp(argv[0]);
            return 0;
        }
        extractArgumentAndPrintInterpretation(argv[1], {});
    } else if (argc == 3) {
        extractArgumentAndPrintInterpretation(argv[1], parseInt(argv[2]));
    } else if (argc == 4) {
        randomize::log::log_level = randomize::log::parseLogLevel(argv[3]);
        extractArgumentAndPrintInterpretation(argv[1], parseInt(argv[2]));
    } else {
        printHelp(argv[0]);
    }
    return 0;
}
