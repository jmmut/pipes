/**
 * @file loader.cpp
 * @date 2020-12-28
 *
 * Note that this file is purposefully simple to make compilation easy in any (unix?) environment,
 * even if you don't have cmake and conan.
 * "g++ loader.cpp -o loader" should just work.
 *
 * If you have cmake and conan, this file will be compiled along the
 * other programs in this repo.
 */

#if USE_SEGFAULT_DIAGNOSTICS
#include <commonlibs/SignalHandler.h>
#endif

#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
#include "executable/ExecutableMemory.h"
#include "executable/PipesExecutable.h"


void runPipesProgram(std::string path, int parameter) {
    ExecutableMemory executableMemory = loadExecutable(path);
    auto func = executableMemory.getEntryPoint<int(int)>();
    auto result = func(parameter);

    std::cout << "Execution result: " << result << std::endl;
}

int main(int argc, char ** argv) {
#if USE_SEGFAULT_DIAGNOSTICS
    randomize::exception::SignalHandler::activate();
#endif
    if (argc != 3 or strcmp(argv[1], "-h") == 0 or strcmp(argv[1], "--help") == 0) {
        std::cout << "This program takes 2 parameters:\n"
                     "  - the path to the pipes program to run\n"
                     "  - an integer number as parameter to the program.\n"
                     "See pipes/src/assembler/compiler.cpp (or pipes/build/bin/compiler) for instructions compiling."
                  << std::endl;
    } else {
        runPipesProgram(argv[1], std::stoi(argv[2]));
    }
    return 0;
}
