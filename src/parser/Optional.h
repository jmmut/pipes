/**
 * @file Optional.h
 * @date 2021-07-03
 */

#ifndef PIPES_OPTIONAL_H
#define PIPES_OPTIONAL_H

#include "commonlibs/StackTracedException.h"

template<typename T>
struct Optional {
private:
    T* inner;

public:
    Optional(T &value) : inner{&value} {}
    Optional() : inner{nullptr} {}

    Optional(const Optional &other) : inner{other.inner} {}
    Optional(Optional &&other) : inner{std::move(other.inner)} {}
    Optional &operator=(const Optional &other) { inner = other.inner; return *this; }
    Optional &operator=(Optional &&other)  noexcept { inner = std::move(other.inner); return *this; }

    bool isPresent() const { return inner != nullptr; }

    T &get() const {
        return *(operator->());
    }
    T &get() {
        return *(operator->());
    }
    T *operator->() const {
        if (isPresent()) {
            return inner;
        } else {
            throw randomize::exception::StackTracedException{"Trying to use an empty optional. "
                                                             "You should check before accessing."};
        }
    }
    /** see https://stackoverflow.com/a/47369227 */
    T *operator->() {
        return const_cast<T*>(std::as_const(*this).operator->());
    }
    T &get_or_default(T &other) const {
        if (isPresent()) {
            return inner;
        } else {
            return other;
        }
    }
    /** see https://stackoverflow.com/a/47369227 */
    T &get_or_default(T &other) {
        return const_cast<T&>(std::as_const(*this).get_or_default(other));
    }

    /* usages are not as readable as I expected, maybe return to std::optional */
    template<typename F, typename G>
    auto send_or_do(const F& consumer, const G& action_if_empty) const
            -> decltype(consumer(*inner)) {
        if (isPresent()) {
            return consumer(*inner);
        } else {
            return action_if_empty();
        }
    }
    /** see https://stackoverflow.com/a/47369227 */
    template<typename F, typename G>
    auto send_or_do(const F& consumer, const G& action_if_empty) {
        return const_cast<decltype(consumer(*inner))>(
                std::as_const(*this).send_or_do(consumer, action_if_empty));
    }
};

#endif //PIPES_OPTIONAL_H
