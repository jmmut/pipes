#include <list>
#include <stdexcept>
#include <typing/typing.h>
#include <typing/subtyping.h>
#include "commonlibs/StackTracedException.h"
#include "commonlibs/log.h"
#include "commonlibs/Walkers.h"
#include "Parser.h"
#include "parser/TypeParser.h"
#include "parser/parserChecks.h"
#include "parser/import.h"
#include "typing/unify/unify.h"
#include "expression/ExpressionType.h"

#define POS  ((__FILE__ ":") + std::to_string(__LINE__) + " ")

const auto BINARY_OPERATIONS = std::set<Token::Type>{Token::PLUS,
                                                     Token::MINUS,
                                                     Token::MULTIPLY,
                                                     Token::DIVIDE,
                                                     Token::MODULO,
                                                     Token::STATEMENT_SEPARATOR,
                                                     Token::LOOP_CALL,
                                                     Token::MUTABLE_LOOP_CALL,
                                                     Token::FIELD_ACCESS,
                                                     Token::GREATER_THAN,
                                                     Token::DEFINITION

};
Expression parseAtom(const TokenedFile &code, size_t &index,
                     std::vector<ParameterDefinition> &parameterStack,
                     long &nestingLevel, Identifiers &identifiers);
Expression parseOperationAndNextExpression(const TokenedFile &code,
                                           size_t &index,
                                           std::vector<ParameterDefinition> &parameterStack,
                                           long &nestingLevel,
                                           Identifiers &identifiers,
                                           Expression &&previousExpression);


bool isBinaryOperation(const Token &token) {
    return contains(BINARY_OPERATIONS, token.type);
}


Expression getClosuredParameter(const TokenedFile &input,
                                const std::vector<ParameterDefinition> &parameterStack,
                                const std::string &name, long nestingLevel,
                                PositionInCode &positionInCode,
                                const std::string &nameWithoutNesting,
                                const std::string &nestingString) {

    PositionInCode parsedNumberChars{0, 0, 0};
    auto closureLevel = consumeNumber(nestingString, parsedNumberChars);

    if (static_cast<size_t>(parsedNumberChars.absoluteChar) != nestingString.size()) {
        throw syntaxError(input, positionInCode, "Parameters can only have 1 closure level");
    }
    auto candidateClosureLevels = std::vector<long>{};
    for (size_t i = 0; i < parameterStack.size(); ++i) {
        const auto &parameterInStack = parameterStack[parameterStack.size() - 1 - i];
        if (parameterInStack.name == nameWithoutNesting) {
            candidateClosureLevels.push_back(nestingLevel - parameterInStack.nestingLevel);
            if (candidateClosureLevels.back() == closureLevel) {
                int enclosingLambdaId = -1; // parameterStack is not a lambda stack, it's a parsing stack
                auto copied = ofParameter(parameterInStack.name, closureLevel, nestingLevel,
                                          parameterInStack.lambdaId, enclosingLambdaId, positionInCode);
                return copied;
            }
        }
    }
    throw syntaxError(input, positionInCode, "Provided closure level ("
            + std::to_string(closureLevel) + ") for parameter " + quote(name)
            + " doesn't match any of the possible closure levels "
            + randomize::utils::to_string(candidateClosureLevels, "(", ", ", ")"));
}

Expression nestFieldAccess(const std::string &name, const std::string &fieldToken,
                           unsigned long fieldAccessStart, bool nameIncludesField,
                           PositionInCode position, Expression &expression) {
    if (not nameIncludesField) { // TODO: unnecessary? the loop takes care
        return expression;
    } else {
        auto fields = name;
        auto nextFieldStart = fieldAccessStart;
        while(nextFieldStart != std::string::npos) {
            fields = fields.substr(nextFieldStart + 1);
            nextFieldStart = fields.find(fieldToken);
            auto currentField = fields.substr(0, nextFieldStart);
            expression = ofBinaryOperation(
                    Token::FIELD_ACCESS,
                    std::move(expression),
                    ofField(currentField, position),
                    position);
        }
        return expression;
    }
}

/**
 * Note that the iteration is in reverse because we prioritise inner parameterLocations.
 * e.g.: 'function a {function a {a}}' means 'function a_1 { function a_2 {a_2}}'
 */
Expression getParameterOrIdentifier(const TokenedFile &input,
                                    const std::vector<ParameterDefinition> &parameterStack,
                                    const std::string &name,
                                    long nestingLevel, Identifiers &identifiers,
                                    PositionInCode positionInCode) {
    auto fieldToken = to_string(Token::FIELD_ACCESS);
    auto fieldAccessStart = name.find(fieldToken);
    bool nameIncludesField = fieldAccessStart != std::string::npos;

    auto nameWithoutFields = name.substr(0, fieldAccessStart);
    auto nestingStart = nameWithoutFields.find(CLOSURE_LEVEL_TOKEN);
    bool nameIncludesNesting = nestingStart != std::string::npos;
    auto nameWithoutNesting = nameWithoutFields.substr(0, nestingStart);

    bool nameIncludesNamespace =
            nameWithoutNesting.find(NAMESPACE_ACCESS_TOKEN) != std::string::npos;

    if (nameIncludesNamespace) {
        if (nameIncludesField) {
            throw syntaxError(input, positionInCode, "bad identifier: " + quote(name)
                    + ". It doesn't make sense to access a field (with "
                    + quote(fieldToken)
                    + ") of a namespaced identifier (namespaced with "
                    + quote(NAMESPACE_ACCESS_TOKEN) + ")");
        }
        auto maybeImported = maybeImport(input, nameWithoutFields, identifiers, positionInCode);
        return nestFieldAccess(name, fieldToken, fieldAccessStart, nameIncludesField,
                               positionInCode, maybeImported);
    }

    if (nameIncludesNesting) {
        if (nameWithoutNesting.find(fieldToken) != std::string::npos) {
            throw syntaxError(input, positionInCode, "bad identifier: " + quote(name)
                    + ". It doesn't make sense to specify the closure level (with "
                    + quote(CLOSURE_LEVEL_TOKEN)
                    + ") of the field of a struct (accessed with "
                    + quote(fieldToken)
                    + "). E.g.: 'param" + CLOSURE_LEVEL_TOKEN + "1" + fieldToken + "field' is ok,"
                    + " 'param" + fieldToken + "field" + CLOSURE_LEVEL_TOKEN + "1' is wrong.");
        }
        const auto &nestingString = name.substr(nestingStart + 1, fieldAccessStart);
        auto closuredParameter = getClosuredParameter(input, parameterStack, name, nestingLevel,
                                                      positionInCode, nameWithoutNesting,
                                                      nestingString);

        return nestFieldAccess(name, fieldToken, fieldAccessStart, nameIncludesField,
                               positionInCode, closuredParameter);
    }

    // try to find a parameter
    for (size_t i = 0; i < parameterStack.size(); ++i) {
        if (parameterStack[parameterStack.size() - 1 - i].name == nameWithoutNesting) {
            auto &parameterInStack = parameterStack[parameterStack.size() - 1 - i];
            // the nesting level is how deep in the expression tree we are.
            // - nestingLevel is the nesting of the usage of the parameter,
            // - parameterInStack.nestingLevel is the nesting of the definition of the parameter (the lambda that declares it)
            // and the closure level is the number of lambdas you
            // have to skip to match the correct lambda.
            auto closureLevel = nestingLevel - parameterInStack.nestingLevel;
            int enclosingLambdaId = -1; // parameterStack is not a lambda stack, it's a parsing stack

            auto copied = ofParameter(parameterInStack.name, closureLevel, nestingLevel,
                                      parameterInStack.lambdaId, enclosingLambdaId, positionInCode);
            copied.pipesType() = parameterInStack.pipestype->copy();

            return nestFieldAccess(name, fieldToken, fieldAccessStart, nameIncludesField,
                                   positionInCode, copied);
        }
    }

    // try to find a virtual parameter
    {
        Expression virtualParameter;
        for (size_t i = 0; i < parameterStack.size(); ++i) {
            auto &parameterInStack = parameterStack[parameterStack.size() - 1 - i];
            recursivelyDo(parameterInStack.pipestype.operator*(),
                          [&](const PipesType &type) {
                              if (type.instanceName() == nameWithoutNesting) {
                                  // the nesting level is how deep in the expression tree we are.
                                  // - nestingLevel is the nesting of the usage of the parameter,
                                  // - parameterInStack.nestingLevel is the nesting of the definition of the parameter (the lambda that declares it)
                                  // and the closure level is the number of lambdas you
                                  // have to skip to match the correct lambda.
                                  auto closureLevel = nestingLevel - parameterInStack.nestingLevel;
                                  int enclosingLambdaId = -1; // parameterStack is not a lambda stack, it's a parsing stack

                                  auto copied = ofVirtualParameter(type.instanceName(),
                                                                   closureLevel,
                                                                   nestingLevel,
                                                                   parameterInStack.lambdaId,
                                                                   enclosingLambdaId,
                                                                   positionInCode);
                                  copied.pipesType() = type.copy();

                                  virtualParameter = nestFieldAccess(name, fieldToken,
                                                                     fieldAccessStart,
                                                                     nameIncludesField,
                                                                     positionInCode, copied);
                              }

                          },
                          [](const PipesType &type) {});
        }
        if (virtualParameter.getType() != ExpressionType::UNSET) {
            return virtualParameter;
        }
    }

    if (isIntrinsic(name)) {
        return ofIdentifier(name, typeofIntrinsic(name), positionInCode);
    }
//    try {
        auto maybeImported = maybeImport(input, nameWithoutFields, identifiers, positionInCode);
        return nestFieldAccess(name, fieldToken, fieldAccessStart, nameIncludesField,
                               positionInCode, maybeImported);
//    } catch (std::exception &e) {
//        return ofVariable(name, UnknownType::make_ptr(name), positionInCode);
//    }
}

Expression tryGetParameterOrIdentifier(const TokenedFile &input,
                                       const std::vector<ParameterDefinition> &parameterStack,
                                       const std::string &name,
                                       long nestingLevel, Identifiers &identifiers,
                                       PositionInCode positionInCode) {
    try {
        return getParameterOrIdentifier(input, parameterStack, name, nestingLevel, identifiers,
                                 positionInCode);
    } catch (std::exception &e) {
        throw std::runtime_error{"Syntax error: " + quote(name)
                                         + " is not a valid parameter or identifier."
                                         + "\nInner cause: " + e.what()};
    }
}

Expression parseUntilMatching(const TokenedFile &input, size_t &index, std::vector<ParameterDefinition> &parameterStack,
                              long nestingLevel, Identifiers &identifiers, Token::Type open, Token::Type close,
                              std::string humanReadableName) {
    auto &tokens = input.tokens;
    checkFunctionCalledAt(input, index, open);
    size_t matchingParenIndex = index + 1;
    long numberOfNestedParens = 0;
    while (true) {
        if (tokens[matchingParenIndex].type == close) {
            if (numberOfNestedParens == 0) {
                auto code = TokenedFile{{input.text,
                                         input.path,
                                         input.file},
                                        Tokens{tokens.begin() + index + 1,
                                               tokens.begin() + matchingParenIndex}};
                auto subExpression = parse(code, parameterStack, nestingLevel, identifiers);
                index = matchingParenIndex;
                return subExpression;
            } else {
                numberOfNestedParens--;
            }
        } else if (tokens[matchingParenIndex].type == open) {
            numberOfNestedParens++;
        }
        matchingParenIndex++;

        if (tokens.size() <= matchingParenIndex) {
            throw syntaxError(input, tokens[index].positionInCode,
                              "Unmatched " + humanReadableName + " opened");
        }
    }
}

Expression parseUntilMatchingParenthesis(const TokenedFile &code, size_t &index,
                                         std::vector<ParameterDefinition> &parameterStack,
                                         long nestingLevel, Identifiers &identifiers) {
    return parseUntilMatching(code, index, parameterStack, nestingLevel, identifiers, Token::OPEN_PAREN,
                              Token::CLOSE_PAREN,
                              std::string{"parenthesis"});
}

Expression parseUntilMatchingBraces(const TokenedFile &code, size_t &index,
                                    std::vector<ParameterDefinition> &parameterStack,
                                    long nestingLevel, Identifiers &identifiers) {
    return parseUntilMatching(code, index, parameterStack, nestingLevel, identifiers, Token::OPEN_BRACE,
                              Token::CLOSE_BRACE,
                              std::string{"braces"});
}


auto parseLambda(const TokenedFile &code, size_t &index,
                 std::vector<ParameterDefinition> &parameterStack, long nestingLevel, Identifiers &identifiers) {
    static auto generalFormat = std::string{"A function should be in format: \"function\" [\"(\"] "
                                            "[ parameter ] [ \":\" parameterType ] "
                                            "[ \"->\" [ result ] [ \":\" resultType ] ] [\")\"] "
                                            "\"{\" expression \"}\""
                                            "\nExamples:\n"
                                            "function {3}\n"
                                            "function() {3}\n"
                                            "function x {x}\n"
                                            "function(x) {x}\n"
                                            "function -> y {3}\n"
                                            "function x -> y {x}\n"
                                            "function(x -> y) {x}\n"
                                            "function x :int64 {x}\n"
                                            "function x :int64 -> y :int64 {x}\n"
                                            "function(x :int64 -> y :int64) {x}"};
    static auto lambdaId = long{-1};
    ++lambdaId;
    ++nestingLevel;
    auto lambdaLevel = nestingLevel;
    auto innerNestedLevel = nestingLevel;
    auto parameterLevel = nestingLevel;
    auto &tokens = code.tokens;
    checkFunctionCalledAt(code, index, Token::LAMBDA);
    auto lambdaPos = tokens[index].positionInCode;
    ++index;checkEOF(code, tokens, index);
    auto nextToken = [&]() {
        checkEOF(code, tokens, index + 1);
        return tokens[index + 1];
    };

    //TODO change this because parameters are optional now
    PositionInCode parameterPos;
    auto identifierOrTypeOrRetOrBrace = tokens[index];
    if (tokens[index].type == Token::OPEN_PAREN) {
        identifierOrTypeOrRetOrBrace = nextToken();
    }
    if (contains({Token::IDENTIFIER, Token::TYPE_MARKER}, identifierOrTypeOrRetOrBrace.type)) {
        parameterPos = identifierOrTypeOrRetOrBrace.positionInCode;
    } else {
        parameterPos = lambdaPos;
    }

    PtrPipesType functionType;
    try {
        functionType = parseLambdaType(code, tokens, index, parameterStack, nestingLevel,
                                       identifiers);

        //TODO change this because parameters are optional now
        auto parameter = ParameterDefinition{functionType->getParameterType().instanceName(),
                                             parameterLevel, lambdaId,
                                             functionType->getParameterType().copy(),
                                             parameterPos};
        parameterStack.push_back(std::move(parameter));
        ++index;checkEOF(code, tokens, index);
        if (tokens[index].type != Token::OPEN_BRACE) {
            if (functionType->getParameterType() == UnknownType{}
                    or functionType->getReturnedType() == UnknownType{}) {
                throw syntaxError(code, tokens[index].positionInCode,
                                  "Expected a '{' (couldn't interpret it as a type)");
            } else {
                throw syntaxError(code, tokens[index].positionInCode,
                                  "Expected a '{' after function type definition");
            }
        }
    } catch (std::exception &e) {
        throw std::runtime_error{e.what() + std::string{"\n"} + generalFormat};
    }

    // we need this because the parseUntilMatchingBraces will be evaluated first
    auto thisLambdaId = lambdaId;

    auto result = ofLambda(
            thisLambdaId,
            functionType->getParameterType().instanceName(),
            lambdaLevel,
            parseUntilMatchingBraces(code, index, parameterStack, innerNestedLevel, identifiers),
            lambdaPos);
    result.pipesType() = std::move(functionType);
    parameterStack.pop_back();
    return result;
}

auto parseBranch(const TokenedFile &code, size_t &index,
                 std::vector<ParameterDefinition> &parameterStack, long nestingLevel, Identifiers &identifiers)
                 -> Expression {
    static auto generalFormat = std::string{
        "\nA branch should be followed by two expressions inside braces: "
        "\"branch\" \"{\" expressionWhenTrue \"}\" \"{\" expressionWhenFalse \"}\""
        "\nExamples:\nbranch {1} {0}\nbranch { x -1 |factorial |* x } {1}"
    };
    auto &tokens = code.tokens;
    checkFunctionCalledAt(code, index, Token::BRANCH);
    auto branchPos = tokens[index].positionInCode;
    ++index;
    try {
        auto yes = parseUntilMatchingBraces(code, index, parameterStack, nestingLevel, identifiers);
        ++index;
        auto no = parseUntilMatchingBraces(code, index, parameterStack, nestingLevel, identifiers);
        return ofBranch(std::move(yes), std::move(no), branchPos);
    } catch (std::exception &e) {
        throw std::runtime_error{e.what() + generalFormat};
    }
}

auto parseWhile(const TokenedFile &code, size_t &index,
                 std::vector<ParameterDefinition> &parameterStack, long nestingLevel, Identifiers &identifiers)
                 -> Expression {
    static auto generalFormat = std::string{
        "\nA while loop should be followed by an expression inside parenthesis and "
        "another expression inside braces: "
        "\"while\" \"(\" conditionExpression \")\" \"{\" bodyExpression \"}\""
        "\nExamples:\nwhile 0 |some_condition {\"another iteration\" |print}"
    };
    auto &tokens = code.tokens;
    checkFunctionCalledAt(code, index, Token::WHILE);
    auto whilePos = tokens[index].positionInCode;
    ++index;
    try {
        auto condition = parseUntilMatchingParenthesis(code, index, parameterStack, nestingLevel,
                                                       identifiers);
        ++index;
        auto body = parseUntilMatchingBraces(code, index, parameterStack, nestingLevel,
                                             identifiers);
        return ofWhile(std::move(condition), std::move(body), whilePos);
    } catch (std::exception &e) {
        throw std::runtime_error{e.what() + generalFormat};
    }
}

auto parseArray(const TokenedFile &code, size_t &index,
                std::vector<ParameterDefinition> &parameterStack, long nestingLevel, Identifiers &identifiers)
                 -> Expression {
    auto &tokens = code.tokens;
    checkFunctionCalledAt(code, index, Token::OPEN_BRACKET);
    auto iterableStartPos = tokens[index].positionInCode;
    Expressions expressions;
    ++index;
    try {
        while (tokens[index].type != Token::CLOSE_BRACKET) {
            auto atom = parseAtom(code, index, parameterStack, nestingLevel, identifiers);
            ++index;
            while (tokens[index].type != Token::COMMA
                    and tokens[index].type != Token::CLOSE_BRACKET) {
                atom = parseOperationAndNextExpression(code, index, parameterStack,
                                                       nestingLevel,
                                                       identifiers, std::move(atom));
                ++index;
            }
            expressions.push_back(atom);
            if (tokens[index].type == Token::COMMA) {
                // this "if" without "else" is intended to allow an extra comma: [1,]
                ++index;
            }
        }
    } catch (std::exception &e) {
        throw std::runtime_error{e.what() + std::string{
                "\nAn array should be comma-separated expressions inside square brackets: "
                "\"[\" [ expression [ \",\" expression ]* ] \"]\""
                "\nExamples:"
                "\n[]\n[1]\n[1, 2]\n[1 + 2, 3]\n['a' |function c {-32 + c}, 'b']"
        }};
    }
    return ofArray(std::move(expressions), iterableStartPos);
}

auto parseTupleOrParenthesis(const TokenedFile &code, size_t &index,
                             std::vector<ParameterDefinition> &parameterStack, long nestingLevel,
                             Identifiers &identifiers) -> Expression {
    auto &tokens = code.tokens;
    auto initialPosition = tokens[index].positionInCode;
    checkFunctionCalledAt(code, index, Token::OPEN_PAREN);
    auto iterableStartPos = tokens[index].positionInCode;
    Expressions expressions;
    ++index;
    auto commas = 0;
    try {
        while (tokens[index].type != Token::CLOSE_PAREN) {
            auto atom = parseAtom(code, index, parameterStack, nestingLevel, identifiers);
            ++index;
            checkEOF(code, tokens, index);
            while (tokens[index].type != Token::COMMA
                    and tokens[index].type != Token::CLOSE_PAREN) {
                atom = parseOperationAndNextExpression(code, index, parameterStack,
                                                       nestingLevel,
                                                       identifiers, std::move(atom));
                ++index;
                checkEOF(code, tokens, index);
            }
            expressions.push_back(atom);
            if (tokens[index].type == Token::COMMA) {
                // this "if" without "else" is intended to allow an extra comma: [1,]
                ++index;
                checkEOF(code, tokens, index);
                ++commas;
            }
        }
    } catch (std::exception &e) {
        if (commas > 0) {
            throw std::runtime_error{e.what() + std::string{
                    "\nA tuple should be comma-separated expressions inside parenthesis: "
                    "\"(\" [ expression \",\" [ expression \",\" ]* ] \")\""
                    "\nExamples:"
                    "\n(1,)\n(1, 2)\n(1 + 2, 3)\n(function c {c}, 'b')"
            }};
        } else {
            throw std::runtime_error{e.what()};
        }
    }

    if (expressions.size() == 0) {
        throw syntaxError(code, initialPosition, "unsupported: empty tuples");
    }
    bool isSimpleParenthesis = expressions.size() == 1 and commas == 0;
    if (isSimpleParenthesis) {
        return expressions.at(0);
    } else {
        return ofTuple(std::move(expressions), iterableStartPos);
    }
}

Expression parseString(const Token &token) {
    Expressions expressions;
    for (char letter : token.name) {
        expressions.push_back(ofConstant(letter));
    }
    Expression array = ofArray(std::move(expressions), token.positionInCode);
    array.pipesType() = ArrayType::make_ptr(IntType::make_ptr(""), "");
    return array;
}

Expression parseAtom(const TokenedFile &code, size_t &index,
                     std::vector<ParameterDefinition> &parameterStack,
                     long &nestingLevel, Identifiers &identifiers) {
    auto &tokens = code.tokens;
    checkEOF(code, tokens, index);
    auto &token = tokens[index];
    if (token.type == Token::NUMBER) {
        return ofConstant(token.number, token.positionInCode);
    } else if (token.type == Token::MINUS) {
        ++index;checkEOF(code, tokens, index);
        if (tokens[index].type == Token::NUMBER) {
            return ofConstant(-tokens[index].number, token.positionInCode);
        } else {
            throw syntaxError(code, tokens[index].positionInCode,
                              "Too many " + quote(to_string(Token::MINUS)) + ".");
        }
    } else if (token.type == Token::IDENTIFIER) {
        // TODO: allow alias of other types like iterable
        //auto indexCopy = index;
        //try {
        //    parseType(code, tokens, indexCopy, identifiers);
        //    index = indexCopy;
        //} catch (std::exception &e) {
        //    ; // it's not a type. ok. Might be a regular identifier
        //}
        return tryGetParameterOrIdentifier(code, parameterStack, token.name, nestingLevel,
                                           identifiers, token.positionInCode);
    } else if (token.type == Token::FIELD) {
        return ofField(token.name, token.positionInCode);
    } else if (token.type == Token::STRING) {
        return parseString(token);
    } else if (token.type == Token::LAMBDA) {
        return parseLambda(code, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::BRANCH) {
        return parseBranch(code, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::WHILE) {
        return parseWhile(code, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::STRUCT) {
        return parseStructType(code, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::OPEN_PAREN) {
        return parseTupleOrParenthesis(code, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::OPEN_BRACKET) {
        return parseArray(code, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::CLOSE_PAREN or token.type == Token::CLOSE_BRACE) {
        throw syntaxError(code, tokens[index].positionInCode,
                          "unexpected token " + to_string(tokens[index]) + ". Try removing it");
    } else {
        throw syntaxError(code, token.positionInCode,
                          POS + "unimplemented: unexpected token " + to_string(tokens[index]));
    }
    throw syntaxErrorStack(code, token.positionInCode, "should be unreachable");
}

void checkDefinitionSyntax(const BinaryOperation &definition, const TokenedFile &input,
                           const PositionInCode &pos) {
    if (definition.right.getType() != ExpressionType::VARIABLE) {
        throw syntaxError(input, pos, "The operator for defining variables ("
                + to_string(Token::DEFINITION)
                + ") works from left to right, so the right-hand side operator should be a"
                  " variable. Example: '4 =a' puts the value '4' in a variable 'a'. Error");
    }
    recursivelyDo(definition.left, [
            variable = definition.right.getVariable(),
            &input,
            definitionPos = definition.position()](
            const Expression &beforeDefinition) {
        if (beforeDefinition.getType() == ExpressionType::BINARY_OPERATION) {
            auto &op = beforeDefinition.getBinaryOperation();
            if (op.operation == Token::DEFINITION
                    and op.right.getType() == ExpressionType::VARIABLE
                    and op.right.getVariable() == variable) {
                throw syntaxError(input, beforeDefinition.position(),
                                  "variable shadowing (for variable '" + variable
                                          + "') is not supported. Shadowed variable defined at "
                                          + codePosToString(input, definitionPos)
                                          + "\nShadowing variable defined");
            }
        }
        if (beforeDefinition.getType() == ExpressionType::VARIABLE
                && beforeDefinition.getVariable() == variable) {
            throw syntaxError(input, beforeDefinition.position(),
                              "variable '" + variable
                              + "' is used before its definition. Defined at "
                              + codePosToString(input, definitionPos)
                              + "\nUsed");
        }
    });
}

Expression parseOperationAndNextExpression(const TokenedFile &code, size_t &index,
                                           std::vector<ParameterDefinition> &parameterStack,
                                           long &nestingLevel, Identifiers &identifiers,
                                           Expression &&previousExpression) {
    auto &tokens = code.tokens;
    auto &operatorToken = tokens[index];
    if (index + 1 >= tokens.size()) {
        throw syntaxError(code, operatorToken.positionInCode,
                          "Expected more code after binary operator "
                                  + quote(to_string(operatorToken)));
    }
    auto operatorPos = operatorToken.positionInCode;
    ++index;
    auto wrappingExpression = Expression{};
    if (operatorToken.type == Token::TYPE_MARKER) {
        wrappingExpression = std::move(previousExpression);
        auto newType = parseType(code, tokens, index, parameterStack, nestingLevel, identifiers);
        if (not areCompatible(wrappingExpression.pipesType().operator*(), newType.operator*())) {
            throw syntaxError(code, operatorPos, std::string{
                    "Type provided explicitly, which is not compatible with the previous expression:"}
                    + "\nType of previous expression:  " + wrappingExpression.pipesType()->to_string()
                    + "\nExplicit type:                " + newType->to_string() + "\nFound");
        }
        auto inferredType = std::move(wrappingExpression.pipesType());
        wrappingExpression.pipesType() = std::move(newType);
        Unifier::unify(wrappingExpression, inferredType.operator*(), code);
    } else if (operatorToken.type == Token::CALL) {
        auto rightHandAtom = parseAtom(code, index, parameterStack, nestingLevel, identifiers);
        wrappingExpression = ofCall(std::move(previousExpression),
                                    std::move(rightHandAtom),
                                    operatorPos);
    } else if (isBinaryOperation(operatorToken)) {
        auto rightHandAtom = parseAtom(code, index, parameterStack, nestingLevel, identifiers);
        wrappingExpression = ofBinaryOperation(operatorToken.type,
                                               std::move(previousExpression),
                                               std::move(rightHandAtom),
                                               operatorPos);
        if (operatorToken.type == Token::DEFINITION) {
            checkDefinitionSyntax(wrappingExpression.getBinaryOperation(), code, operatorPos);
        }
    } else if (operatorToken.type == Token::OPEN_PAREN
//            and contains({ExpressionType::LAMBDA, ExpressionType::IDENTIFIER, ExpressionType::BINARY_OPERATION}, previousexpression.getType())
            ) {
        --index;
        auto pos = tokens[index].positionInCode;
        auto parens = parseUntilMatchingParenthesis(code, index, parameterStack,
                                                    nestingLevel, identifiers);
//        throw std::runtime_error{
//                "Syntax error: unexpected operatorToken " + to_string(operatorToken)
//                        + " at " + codePosToString(code, operatorToken.positionInCode)
//                        + "\nAre you trying to make a function call? Pipes "
//                          "calls are in the syntax: argument \"|\" function"
//                          "\nMaybe this is what you want:\n"
//                        + to_string(ofCall(std::move(parens), std::move(previousExpression)))};
        wrappingExpression = ofCall(std::move(parens), std::move(previousExpression), pos);
    } else {
        --index; // need to go back to parse the operator
        auto operatorAtom = parseAtom(code, index, parameterStack, nestingLevel, identifiers);
//        if (operatorAtom.type == ExpressionType::BINARY_OPERATION)
        ++index;
        auto rightHandAtom = parseAtom(code, index, parameterStack, nestingLevel, identifiers);
        wrappingExpression = ofCall(std::move(previousExpression),
                                    ofCall(std::move(rightHandAtom),
                                           std::move(operatorAtom),
                                           operatorPos),
                                    operatorPos);
//        throw syntaxError(code, operatorToken.positionInCode, "unexpected operatorToken "
//                + to_string(operatorToken));
    }
    return wrappingExpression;
}

void checkNoVirtualParametersAreUsed(const Expression &expression, const TokenedFile &code) {
    recursivelyDo(expression, [&](const Expression &inner){
        if (inner.getType() == ExpressionType::PARAMETER and inner.getParameter().isVirtual) {
            throw syntaxError(code, inner.positionInCode,
                              "Using a virtual parameter name outside of a typeof is forbidden."
                              " Please use the full name. ");

        }
    });
}

Expression parse(const TokenedFile &code, std::vector<ParameterDefinition> &parameterStack,
                 long &nestingLevel, Identifiers &identifiers) {
    auto &tokens = code.tokens;
    if (tokens.empty()) {
        return ofNothing(PositionInCode{});
    }

    size_t index = 0;
    auto nestingExpression = parseAtom(code, index, parameterStack, nestingLevel, identifiers);

    for (++index; index < tokens.size(); ++index) {
        nestingExpression = parseOperationAndNextExpression(code, index, parameterStack,
                                                            nestingLevel, identifiers,
                                                            std::move(nestingExpression));
    }
    checkNoVirtualParametersAreUsed(nestingExpression, code);
    return nestingExpression;
}



Expression parse(const std::string &code, const Tokens &tokens, std::vector<ParameterDefinition> &parameterStack,
        long &nestingLevel, Identifiers &identifiers) {
    return parse(TokenedFile{{code, ".", ""}, tokens}, parameterStack, nestingLevel, identifiers);
}
Expression tokenizeAndParse(const std::string &code, Identifiers &identifiers) {
    // TODO must unstack lambda parameterLocations on close parens: unstack only the lambdas defined inside the parens
    std::vector<ParameterDefinition> parameterStack;
    long nestingLevel = 0;
    auto tokens = tokenize(code);
    return parse(code, tokens, parameterStack, nestingLevel, identifiers);
}

Expression tokenizeAndParse(const CodeFile &code, Identifiers &identifiers) {
    // TODO must unstack lambda parameterLocations on close parens: unstack only the lambdas defined inside the parens
    std::vector<ParameterDefinition> parameterStack;
    long nestingLevel = 0;
    auto tokens = tokenize(code);
    auto tokenized = TokenedFile{{code.text, code.path, code.file}, tokens};
    return parse(tokenized, parameterStack, nestingLevel, identifiers);
}

Expression tokenizeAndParse(const std::string &code, long &lambdaLevel) {
    // TODO must unstack lambda parameterLocations on close parens: unstack only the lambdas defined inside the parens
    std::vector<ParameterDefinition> parameterStack;
    Identifiers identifiers{};
    return parse(code, tokenize(code), parameterStack, lambdaLevel, identifiers);
}
Expression parse(const std::string &code, const Tokens &tokens) {

    // TODO must unstack lambda parameterLocations on close parens: unstack only the lambdas defined inside the parens
    std::vector<ParameterDefinition> parameterStack;
    long nestingLevel = 0;
    Identifiers identifiers{};
    return parse(code, tokens, parameterStack, nestingLevel, identifiers);
}
Expression tokenizeAndParse(const std::string &code) {
    auto tokens = tokenize(code);
    return parse(code, tokens);
}
