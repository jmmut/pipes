/**
 * @file TypeParser.cpp
 * @date 2021-08-04
 */

#include <typing/PipesType.h>
#include <parser/TypeParser.h>
#include <parser/Identifiers.h>
#include <parser/parserChecks.h>
#include <parser/import.h>


void addVirtualParameter(std::vector<ParameterDefinition> &parameterStack,
                         std::unique_ptr<PipesType> &pipesType, const long &nestingLevel,
                         const PositionInCode &elementPos);

PtrPipesType parseUserType(const TokenedFile &input, const Token &token, Identifiers &identifiers) {
    auto imported = maybeImport(input, token.name, identifiers, token.positionInCode);
    if (imported.pipesType() == nullptr) {
        throw syntaxError(input, token.positionInCode, "Unsupported: recursive types");
    } else {
        return imported.pipesType()->copy();
    }
}

PtrPipesType parseTypeofType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                       std::vector<ParameterDefinition> &parameterStack,
                       long &nestingLevel, Identifiers &identifiers) {
    auto checkEOF = [&code, &tokens, &index](std::string message = "") {
        ::checkEOF(code, tokens, index, message);
    };
    checkEOF();
    auto innerExpression = parseUntilMatchingParenthesis(code, index, parameterStack, nestingLevel,
                                                         identifiers);
    return TypeofType::make_ptr(std::move(innerExpression), "");
}


PtrPipesType parseType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                       std::vector<ParameterDefinition> &parameterStack,
                       long &nestingLevel, Identifiers &identifiers) {
    auto &token = tokens[index];
    if (token.type == Token::IDENTIFIER and token.name == IntType::PARSING_NAME) {
        return IntType::make_ptr("");
    } else if (token.type == Token::LAMBDA) {
        ++index;
        return parseLambdaType(code, tokens, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::IDENTIFIER and token.name == TupleType::PARSING_NAME) {
        ++index;
        return parseIterableType(code, tokens, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::IDENTIFIER and token.name == ArrayType::PARSING_NAME) {
        ++index;
        return parseArrayType(code, tokens, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::IDENTIFIER and token.name == TypeofType::PARSING_NAME) {
        ++index;
        return parseTypeofType(code, tokens, index, parameterStack, nestingLevel, identifiers);
    } else if (token.type == Token::STRUCT) {
        return std::move(parseStructType(code, index, parameterStack, nestingLevel, identifiers)
                                 .pipesType());
    } else if (token.type == Token::IDENTIFIER) {
//        ++index;
        return parseUserType(code, token, identifiers);
    } else {
        throw std::invalid_argument{
                "unrecognized type at " + codePosToString(code, token.positionInCode)};
    }
}

PtrPipesType parseSimpleType(std::string type) {
    auto file = TokenedFile{{type, ".", ""}, tokenize(type)};

    size_t index = 0;
    std::vector<ParameterDefinition> parameters;
    long nestingLevel = 0;
    Identifiers identifiers;
    return parseType(file, file.tokens, index, parameters, nestingLevel, identifiers);
}

PtrPipesType parseOptionallyTypedOptionalIdentifier(const TokenedFile &code, const Tokens &tokens,
                                                    size_t &index,
                                                    std::vector<ParameterDefinition> &parameterStack,
                                                    long &nestingLevel, Identifiers &identifiers) {
    std::string instanceName;
    if (tokens[index].type == Token::IDENTIFIER) {
        instanceName = tokens[index].name;
        ++index;
    }
    if (index < tokens.size() and tokens[index].type == Token::TYPE_MARKER) {
        ++index;
        checkEOF(code, tokens, index, "Expected a type for the previous variable");
        auto parsed = parseType(code, tokens, index, parameterStack, nestingLevel, identifiers);
        parsed->instanceName() = instanceName;
        return parsed;
    } else {
        --index;
        return UnknownType::make_ptr(instanceName);
    }
}

PtrPipesType parseOptionallyTypedIdentifier(const TokenedFile &code, const Tokens &tokens,
                                            size_t &index,
                                            std::vector<ParameterDefinition> &parameterStack,
                                            long &nestingLevel, Identifiers &identifiers) {

    if (tokens[index].type != Token::IDENTIFIER) {
        throw syntaxError(code, tokens[index].positionInCode, "Expected variable name");
    }
    std::string instanceName = tokens[index].name;
    ++index;
    if (index < tokens.size() and tokens[index].type == Token::TYPE_MARKER) {
        ++index;checkEOF(code, tokens, index, "Expected a type for the previous variable");
        auto parsed = parseType(code, tokens, index, parameterStack, nestingLevel, identifiers);
        parsed->instanceName() = instanceName;
        return parsed;
    } else {
        --index;
    }
    return UnknownType::make_ptr(instanceName);
}

Expression parseTypedIdentifier(const TokenedFile &code, const Tokens &tokens, size_t &index,
                                std::vector<ParameterDefinition> &parameterStack,
                                long &nestingLevel, Identifiers &identifiers) {
    if (tokens[index].type != Token::IDENTIFIER) {
        throw syntaxError(code, tokens[index].positionInCode, "Expected variable name");
    }
    auto position = tokens[index].positionInCode;
    auto identifier = tokens[index].name;
    ++index;checkEOF(code, tokens, index, "Expected a type for the previous variable");
    if (tokens[index].type != Token::TYPE_MARKER) {
        throw syntaxError(code, tokens[index].positionInCode,
                          "Expected " + quote(to_string(Token::TYPE_MARKER))
                                  + " and a type after the variable name " + quote(identifier)
                                  + " to define its type");
    }
    ++index;
    checkEOF(code, tokens, index, "Expected a type for the previous variable after "
            + quote(to_string(Token::TYPE_MARKER)));
    auto type = parseType(code, tokens, index, parameterStack, nestingLevel, identifiers);
    type->instanceName() = identifier;
    return ofIdentifier(identifier, std::move(type), position);
}

PtrPipesType parseLambdaType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                       std::vector<ParameterDefinition> &parameterStack,
                       long &nestingLevel, Identifiers &identifiers) {
    auto checkEOF = [&code, &tokens, &index](std::string message = "") {
        ::checkEOF(code, tokens, index, message);
    };
    auto returnPipesType = UnknownType::make_ptr("");
    auto parameterPipesType = UnknownType::make_ptr("");
    if (index >= tokens.size() or tokens[index].type == Token::OPEN_BRACE) {
        --index;
        return FunctionType::make_ptr(std::move(parameterPipesType),
                                      std::move(returnPipesType), "");
    }
    doWithinOptionalParenthesis(code, tokens, index, [&]() {
        auto previousSize = parameterStack.size();
        PositionInCode parameterPos = tokens[index].positionInCode;
        parameterPipesType = parseOptionallyTypedOptionalIdentifier(code, tokens, index,
                                                                    parameterStack,
                                                                    nestingLevel, identifiers);

        addVirtualParameter(parameterStack, parameterPipesType, nestingLevel, parameterPos);

        ++index;
        if (index < tokens.size() and tokens[index].type == Token::LAMBDA_RETURN) {
            ++index;
            checkEOF("If a lambda return " + quote(to_string(Token::LAMBDA_RETURN))
                     + " is provided, a return identifier and/or return type are/is required");
            PositionInCode returnPos = tokens[index].positionInCode;
            returnPipesType = parseOptionallyTypedOptionalIdentifier(code, tokens, index,
                                                                     parameterStack,
                                                                     nestingLevel, identifiers);
            addVirtualParameter(parameterStack, returnPipesType, nestingLevel, returnPos);
        } else {
            --index;
        }
        parameterStack.resize(previousSize);
    });
    return FunctionType::make_ptr(std::move(parameterPipesType), std::move(returnPipesType), "");
}

PtrPipesType parseIterableType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                       std::vector<ParameterDefinition> &parameterStack,
                       long &nestingLevel, Identifiers &identifiers) {
    auto checkEOF = [&code, &tokens, &index](std::string message = "") {
        ::checkEOF(code, tokens, index, message);
    };
    if (index < tokens.size()) {
        if (tokens[index].type == Token::OPEN_PAREN) {
            PipesTypes elements;
            ++index;checkEOF();
            if (tokens[index].type == Token::CLOSE_PAREN) {
                throw syntaxError(code, tokens[index].positionInCode,
                                  "Unsupported: empty tuples");
            }
            auto elementPos = tokens[index].positionInCode;
            elements.push_back(
                    parseOptionallyTypedOptionalIdentifier(code, tokens, index, parameterStack,
                                                           nestingLevel, identifiers));

            auto previousSize = parameterStack.size();
            addVirtualParameter(parameterStack,
                                elements.back(), nestingLevel, elementPos);
            ++index;checkEOF();
            while (tokens[index].type == Token::COMMA) {
                ++index;checkEOF();
                elements.push_back(
                        parseOptionallyTypedOptionalIdentifier(code, tokens, index, parameterStack,
                                                               nestingLevel, identifiers));
                addVirtualParameter(parameterStack, elements.back(), nestingLevel, elementPos);
                ++index;checkEOF();
            }
            if (tokens[index].type != Token::CLOSE_PAREN) {
                throw syntaxError(code, tokens[index].positionInCode,
                                  "Expected closing parenthesis");
            }
            parameterStack.resize(previousSize);
            return TupleType::make_ptr(std::move(elements), "");
        } else {
            --index;
            return TupleType::make_ptr(UnknownType::make_ptr(""), "");
        }
    } else {
        return TupleType::make_ptr(UnknownType::make_ptr(""), "");
    }
}

void addVirtualParameter(std::vector<ParameterDefinition> &parameterStack,
                         std::unique_ptr<PipesType> &pipesType, const long &nestingLevel,
                         const PositionInCode &elementPos) {
    recursivelyDo(pipesType.operator*(), [&](const PipesType &type) {
        auto parameter = ParameterDefinition{type.instanceName(), nestingLevel, -1,
                                             type.copy(),
                                             elementPos};
        parameterStack.push_back(std::move(parameter));
    }, [](const PipesType &type) {});
}

PtrPipesType parseArrayType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                               std::vector<ParameterDefinition> &parameterStack,
                               long &nestingLevel, Identifiers &identifiers) {
    auto parenthesisIndex = index;
    auto tuple = parseIterableType(code, tokens, index, parameterStack, nestingLevel, identifiers);
    if (tuple->getNestedTypes().size() == 1) {
        return ArrayType::make_ptr(tuple->getNestedTypes().at(0)->copy(), tuple->instanceName());
    } else {
        throw syntaxError(code, tokens[parenthesisIndex].positionInCode,
                          "Arrays can only contain 1 type");
    }
}

auto parseStructType(const TokenedFile &code, size_t &index,
                     std::vector<ParameterDefinition> &parameterStack,
                     long &nestingLevel, Identifiers &identifiers)
        -> Expression {
    static auto generalFormat = std::string{
            "\nA struct should be followed by a tuple of typed identifiers: "
            "\"struct\" \"(\" identifier \":\" type [\",\" identifier \":\" type] [\",\"]\")\""
            "\nExamples:\nstruct (x :int64, y :int64)"
    };
    auto &tokens = code.tokens;
    auto checkEOF = [&code, &tokens, &index](std::string message = "") {
        ::checkEOF(code, tokens, index, message);
    };
    checkFunctionCalledAt(code, index, Token::STRUCT);
    auto structPos = tokens[index].positionInCode;
    ++index;checkEOF();
    if (tokens[index].type != Token::OPEN_PAREN) {
        throw syntaxError(code, tokens[index].positionInCode, generalFormat);
    }
    ++index;checkEOF();
    Expressions expressions;
    auto previousSize = parameterStack.size();
    do {
        auto fieldPos = tokens[index].positionInCode;
        auto fieldType = parseOptionallyTypedIdentifier(code, tokens, index, parameterStack,
                                                        nestingLevel, identifiers);
        addVirtualParameter(parameterStack, fieldType, nestingLevel, fieldPos);
        expressions.push_back(ofField(fieldType->instanceName(), std::move(fieldType), fieldPos));

        ++index;checkEOF();
        if (tokens[index].type == Token::CLOSE_PAREN) {
            break;
        }
        if (tokens[index].type == Token::COMMA) {
            // this "if" without "else" is intended to allow an extra comma: [1,]
            ++index;checkEOF();
        }
    } while (tokens[index].type != Token::CLOSE_PAREN);

    parameterStack.resize(previousSize);
    return ofStruct(std::move(expressions), structPos);
}

