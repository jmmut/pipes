/**
 * @file parserChecks.h
 * @date 2021-08-08
 */

#ifndef PIPES_PARSERCHECKS_H
#define PIPES_PARSERCHECKS_H

#include "lexer/Lexer.h"

inline auto syntaxErrorStack(const TokenedFile &input, PositionInCode positionInCode,
                     std::string message = "") {
    return randomize::exception::StackTracedException{
            "Syntax error: " + message + " at " + codePosToString(input, positionInCode)};
}

inline auto syntaxError(const TokenedFile &input, PositionInCode positionInCode,
                      std::string message = "") {
    return std::runtime_error{
            "Syntax error: " + message + " at " + codePosToString(input, positionInCode)};
}

inline void checkBalanced(const TokenedFile &code, const Tokens &tokens,
                     Token::Type open, Token::Type close) {
    std::vector<Token> stack;

    for (const auto &token: tokens) {
        if (token.type == open) {
            stack.push_back(token);
        }
        if (token.type == close) {
            if (stack.size() == 0) {
                throw syntaxError(code, token.positionInCode,
                                  "Unexpected " + quote(to_string(close))
                                          + " without a corresponding " + quote(to_string(close)));
            }
            stack.pop_back();
        }
    }
    if (stack.size() > 0) {
        throw syntaxError(code, stack.back().positionInCode, "Unclosed " + quote(to_string(open)));
    }
}
inline void checkEOF(const TokenedFile &code, const Tokens &tokens, const size_t &index,
              std::string message = "") {
    if (index >= tokens.size()) {
        checkBalanced(code, tokens, Token::OPEN_PAREN, Token::CLOSE_PAREN);
        checkBalanced(code, tokens, Token::OPEN_BRACE, Token::CLOSE_BRACE);
        checkBalanced(code, tokens, Token::OPEN_BRACKET, Token::CLOSE_BRACKET);
        throw syntaxError(code, tokens[index - 1].positionInCode,
                          (message.empty() ? "" : message + ".\n")
                                  + "Unexpected end of expression after "
                                  + to_type_string(tokens[index - 1].type));
    }
}

inline void checkFunctionCalledAt(const TokenedFile &code, const size_t &index, Token::Type token) {
    if (code.tokens[index].type != token) {
        throw randomize::exception::StackTracedException{
                "logic error: this function should be called when the index is on a "
                        + to_type_string(token) + ". State was at a "
                        + to_type_string(code.tokens[index].type) + " at "
                        + codePosToString(code, code.tokens[index].positionInCode)};
    }
}
#endif //PIPES_PARSERCHECKS_H
