/**
 * @file TypeParser.h
 * @date 2021-08-04
 */

#ifndef PIPES_TYPEPARSER_H
#define PIPES_TYPEPARSER_H


#include <lexer/Lexer.h>
#include <typing/typeInterfaces.h>
#include "expression/Expression.h"
#include "Identifiers.h"
#include "Parser.h"
#include "parserChecks.h"

PtrPipesType parseType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                       std::vector<ParameterDefinition> &parameterStack,
                       long &nestingLevel,
                       Identifiers &identifiers);

PtrPipesType parseSimpleType(std::string type);

Expression parseTypedIdentifier(const TokenedFile &code, const Tokens &tokens, size_t &index,
                                std::vector<ParameterDefinition> &parameterStack,
                                long &nestingLevel, Identifiers &identifiers);

PtrPipesType parseLambdaType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                                std::vector<ParameterDefinition> &parameterStack,
                                long &nestingLevel, Identifiers &identifiers);
PtrPipesType parseIterableType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                               std::vector<ParameterDefinition> &parameterStack,
                               long &nestingLevel, Identifiers &identifiers);
PtrPipesType parseArrayType(const TokenedFile &code, const Tokens &tokens, size_t &index,
                                std::vector<ParameterDefinition> &parameterStack,
                                long &nestingLevel, Identifiers &identifiers);

auto parseStructType(const TokenedFile &code, size_t &index,
                     std::vector<ParameterDefinition> &parameterStack,
                     long &nestingLevel, Identifiers &identifiers)
        -> Expression;


template<typename T>
void doWithinOptionalParenthesis(const TokenedFile &code, const Tokens &tokens, size_t &index,
                                 T func) {
    bool parenthesized = false;
    if (tokens[index].type == Token::OPEN_PAREN) {
        parenthesized = true;
        ++index;checkEOF(code, tokens, index);
    }
//    auto result =
            func();
    if (parenthesized) {
        ++index;checkEOF(code, tokens, index);
        if (!(tokens[index].type == Token::CLOSE_PAREN)) {
            throw syntaxError(code, tokens[index].positionInCode, "Expected closing parenthesis");
        }
    }
//    return result;
}

#endif //PIPES_TYPEPARSER_H
