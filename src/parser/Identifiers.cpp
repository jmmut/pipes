/**
 * @file Identifiers.cpp
 * @date 2021-12-19
 */

#include "parser/import.h"
#include "parser/Identifiers.h"
#include "parser/Parser.h"
#include "typing/typing.h"

Identifiers::Identifiers(std::initializer_list<std::pair<std::string, std::string>> init) {
    for (auto elem : init) {
        importWithoutChecks(elem.first, buildCode(elem.second), *this);
    }
}

Identifiers::Identifiers(std::initializer_list<std::tuple<
        std::string /* name */,
        std::string /* code */,
        std::string /* filename */>> init) {
    for (auto elem : init) {
        importWithoutChecks(std::get<0>(elem),
                            CodeFile{std::get<1>(elem), ".", std::get<2>(elem)},
                            *this);
    }
}

Identifiers::Identifiers(std::initializer_list<std::pair<std::string, ParsedFile>> init) {
    for (auto elem : init) {
        map_.emplace(elem.first, ParsedFile::make_ptr(elem.second.code,
                                                      elem.second.expression));
    }
}
