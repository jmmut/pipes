
#include "intrinsics.h"
#include "TypeParser.h"

auto buildIntrinsicTypeMap() {
    std::map<std::string, PtrPipesType> types;
    types.emplace(intrinsics::DECREMENT, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::INCREMENT, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::INCREMENT, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::READ_CHAR, parseSimpleType("function x :int64 -> c :int64"));
    types.emplace(intrinsics::PRINT_CHAR, parseSimpleType("function x :int64 -> c :int64"));
    types.emplace(intrinsics::PRINT_LAST_DIGIT, parseSimpleType("function x :int64 -> c :int64"));
    types.emplace(intrinsics::NEW_ARRAY, parseSimpleType("function n :int64 -> a :iterable"));
    types.emplace(intrinsics::DEREFERENCE, parseSimpleType("function :iterable"));
    types.emplace(intrinsics::FREE, parseSimpleType("function a :iterable -> error_code :int64"));
    types.emplace(intrinsics::COMPILE_TIME_PRINT_TYPE, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::COMPILE_TIME_TRACK_TYPE, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::PRINT_FUNCTION_STACK, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::PRINT_CURRENT_FUNCTION, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::PRINT_CALL_STACK, parseSimpleType("function x -> :typeof(x)"));
    types.emplace(intrinsics::ARGUMENT, parseSimpleType("int64"));
    if (types.size() != (intrinsics::FUNCTIONS.size() + intrinsics::VARIABLES.size())) {
        auto missing = std::vector<std::string>{};
        for (const auto &item: intrinsics::FUNCTIONS) {
            if (not contains(types, item)) {
                missing.push_back(item);
            }
        }
        for (const auto &item: intrinsics::VARIABLES) {
            if (not contains(types, item)) {
                missing.push_back(item);
            }
        }
        throw randomize::exception::StackTracedException(
                "Incomplete definition of types of intrinsic identifiers. Missing types for: "
                + randomize::utils::to_string(missing));
    }
    return types;
}
PtrPipesType typeofIntrinsic(std::string name) {
    static auto types = buildIntrinsicTypeMap();
    return types.at(name)->copy();
}
