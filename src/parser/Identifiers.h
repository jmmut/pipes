/**
 * @file Identifiers.h
 * @date 2021-07-02
 */

#ifndef PIPES_IDENTIFIERS_H
#define PIPES_IDENTIFIERS_H

#include <map>
#include "parser/Optional.h"
#include "parser/intrinsics.h"
#include "lexer/pipesCodeFile.h"
#include "expression/Expression.h"

struct ParsedFile {
    CodeFile code;
    Expression expression;

    ParsedFile(const CodeFile &code, const Expression &expression)
            : code(code), expression(expression) {}

    static std::unique_ptr<ParsedFile> make_ptr(const CodeFile &code,
                                                const Expression &expression) {
        return std::make_unique<ParsedFile>(code, expression);
    }
};
inline ParsedFile buildParsedFile(const std::string &text, const Expression &expression) {
    return ParsedFile{buildCode(text), expression};
}
inline ParsedFile buildParsedFile(Expression expression) {
    return ParsedFile{buildCode(), std::move(expression)};
}


struct Identifiers {
    std::map<std::string, std::unique_ptr<ParsedFile>> map_;

public:
    using key_type = decltype(map_)::key_type;
    using mapped_type = decltype(map_)::mapped_type;
    using value_type = decltype(map_)::value_type;

    Identifiers() {}
    Identifiers(std::initializer_list<std::pair<std::string, std::string>> init);
    Identifiers(std::initializer_list<std::tuple<std::string, std::string, std::string>> init);
    Identifiers(std::initializer_list<std::pair<std::string, ParsedFile>> init);
    Identifiers(std::initializer_list<std::pair<std::string, Expression>> init) {
        for (auto elem : init) {
            map_.emplace(elem.first, ParsedFile::make_ptr(buildCode(), elem.second));
        }
    }
    Identifiers(const Identifiers &other) {
        for (const auto &[name, parsedFile] : other.map_) {
            map_.emplace(name,
                        parsedFile != nullptr
                            ? ParsedFile::make_ptr(parsedFile->code, parsedFile->expression)
                            : nullptr);
        }
    }

    /** create empty identifier with unknown type to prevent infinite recursive loading */
    void reserve(const std::string& identifierName) {
        map_[identifierName] = std::make_unique<ParsedFile>(buildParsedFile(Expression{}));
    }
    bool contains(const std::string &identifierName) const {
        return map_.count(identifierName) != 0;
    }

    const ParsedFile &at(const std::string& identifierName) const {
        if (isIntrinsic(identifierName)) {
            static auto fakeIntrinsicCode = buildParsedFile(Expression{});
            return fakeIntrinsicCode;
        } else {
            auto &ptr = map_.at(identifierName);
            if (ptr == nullptr) {
                throw randomize::exception::StackTracedException{
                        "illegal state, identifier '" + identifierName
                                + "' was not ready when trying to use it"};
            }
            return ptr.operator*();
        }
    }
    /** see https://stackoverflow.com/a/47369227 */
    ParsedFile &mutable_at(const std::string& identifierName) {
        return const_cast<ParsedFile&>(std::as_const(*this).at(identifierName));
    }


    ParsedFile &put(const std::string& identifierName, const ParsedFile &parsedFile) {
        return (map_[identifierName] = std::make_unique<ParsedFile>(parsedFile)).operator*();
    }
    Optional<const ParsedFile> get_optional(const std::string &identifierName) const {
        auto found = map_.find(identifierName);
        if (found == map_.end()) {
            return Optional<const ParsedFile>{};
        } else {
            auto &ptr = found->second;
            if (ptr == nullptr) {
                throw randomize::exception::StackTracedException{
                        "illegal state, identifier '" + identifierName
                                + "' was not ready when trying to use it"};
            }
            return Optional<const ParsedFile>{ptr.operator*()};
        }
    }
    /** see https://stackoverflow.com/a/47369227 */
    Optional<ParsedFile> get_mutable_optional(const std::string &identifierName) {
        const ParsedFile &fakelyConst = std::as_const(*this).get_optional(identifierName).get();
        auto non_const_ptr = const_cast<ParsedFile*>(&fakelyConst);
        return {*non_const_ptr};
    }
};

// TODO use this instead of program?
struct ParsedCode {
    std::string main;
    Identifiers identifiers;

    const Expression &expression() const {
        return identifiers.at(main).expression;
    }
};


#endif //PIPES_IDENTIFIERS_H
