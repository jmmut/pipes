/**
 * @file import.h
 * @date 2021-08-12
 */

#ifndef PIPES_IMPORT_H
#define PIPES_IMPORT_H

#include "typing/typing.h"


inline void importWithoutChecks(const std::string &identifierToBeImported,
                                const CodeFile &codeToBeImported, Identifiers &identifiers) {
    identifiers.reserve(identifierToBeImported);
    auto expression = tokenizeAndParseAndCheckTypesModifyingIdentifiers(codeToBeImported,
                                                                        identifierToBeImported,
                                                                        identifiers);
    identifiers.put(identifierToBeImported, ParsedFile{codeToBeImported, expression});
    // TODO: if the identifier was recursive, check its type again,
    //       because its recursive call was Unknown while itself was being parsed
}

inline std::string getIdentifierPathFromIdentifier(std::string identifier) {
    std::replace(identifier.begin(), identifier.end(), NAMESPACE_ACCESS_TOKEN, '/');
    identifier += PIPES_SUFFIX;
    return identifier;
}

inline Expression import(const TokenedFile &importingCode,
                         const std::string &identifierToBeImported, Identifiers &identifiers,
                         PositionInCode positionInCode) {
    try {
        auto identifierPath = getIdentifierPathFromIdentifier(identifierToBeImported);
        auto codeToBeImported = getCodeFromRelativePath(importingCode.path, identifierPath);
        importWithoutChecks(identifierToBeImported, codeToBeImported, identifiers);
        return ofIdentifier(identifierToBeImported,
                            identifiers.at(identifierToBeImported).expression.pipesType()->copy(),
                            positionInCode);
    } catch (std::exception &e) {
        throw std::runtime_error{
                "Import error of identifier " + quote(identifierToBeImported)
                        + " at " + codePosToString(importingCode, positionInCode)
                        + "\nInner cause: " + e.what()};
    }
}

inline Expression maybeImport(const TokenedFile &input, const std::string &nameWithoutFields,
                              Identifiers &identifiers, const PositionInCode &positionInCode) {
    auto imported = identifiers.get_optional(nameWithoutFields);
    if (imported.isPresent()) {
        return ofIdentifier(
                nameWithoutFields,
                imported.get().expression.pipesType()->copy(),
                positionInCode);
    } else {
        return import(input, nameWithoutFields, identifiers, positionInCode);
    }
}

#endif //PIPES_IMPORT_H
