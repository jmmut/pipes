

#ifndef PIPES_PARSER_H
#define PIPES_PARSER_H

#include <map>
#include <memory>
#include <commonlibs/Walkers.h>
#include "expression/Expression.h"
#include "lexer/Lexer.h"
#include "parser/intrinsics.h"
#include "parser/Identifiers.h"

struct ParameterDefinition {
    std::string name;
    long nestingLevel;
    long lambdaId;
    PtrPipesType pipestype;
    PositionInCode positionInCode;
};


Expression tokenizeAndParse(const std::string &code);
Expression tokenizeAndParse(const std::string &code, Identifiers &identifiers);
Expression parse(const std::string &code, const Tokens &tokens);
Expression tokenizeAndParse(const std::string &code, long &lambdaLevel);
Expression parse(const std::string &code, const Tokens &tokens, std::vector<ParameterDefinition> &parameterStack,
                 long &nestingLevel, Identifiers &identifiers);

Expression parse(const TokenedFile &code, std::vector<ParameterDefinition> &parameterStack,
                 long &nestingLevel, Identifiers &identifiers);
Expression tokenizeAndParse(const CodeFile &code, Identifiers &identifiers);

Expression parseUntilMatchingParenthesis(const TokenedFile &code, size_t &index,
                                         std::vector<ParameterDefinition> &parameterStack,
                                         long nestingLevel, Identifiers &identifiers);
#endif //PIPES_PARSER_H
