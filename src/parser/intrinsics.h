/**
 * @file intrinsics.h
 * @date 2021-06-14
 */

#ifndef PIPES_INTRINSICS_H
#define PIPES_INTRINSICS_H

#include <vector>
#include <string>
#include "commonlibs/Walkers.h"
#include "typing/PipesType.h"

namespace intrinsics {
    static const char *const ARGUMENT = "argument";

    static const char *const DECREMENT = "decrement";
    static const char *const INCREMENT = "increment";
    static const char *const NEW_ARRAY = "new_array";
    static const char *const DEREFERENCE = "deref";
    static const char *const FREE = "free";
    static const char *const READ_CHAR = "read_char";
    static const char *const PRINT_CHAR = "print_char";
    static const char *const PRINT_LAST_DIGIT = "print_last_digit";
    static const char *const COMPILE_TIME_PRINT_TYPE = "compile_time_print_type";
    static const char *const COMPILE_TIME_TRACK_TYPE = "compile_time_track_type";
    static const char *const PRINT_CALL_STACK = "print_call_stack";
    static const char *const PRINT_FUNCTION_STACK = "print_function_stack";
    static const char *const PRINT_CURRENT_FUNCTION = "print_current_function";

    inline const auto VARIABLES = std::vector<std::string>{ARGUMENT};
    inline const auto FUNCTIONS = std::vector<std::string>{
            DECREMENT, INCREMENT, READ_CHAR, PRINT_CHAR, PRINT_LAST_DIGIT,
            NEW_ARRAY, DEREFERENCE, FREE,
            COMPILE_TIME_PRINT_TYPE, COMPILE_TIME_TRACK_TYPE,
            PRINT_CALL_STACK, PRINT_FUNCTION_STACK, PRINT_CURRENT_FUNCTION};
}
inline bool isIntrinsic(const std::string &name) {
    return contains(intrinsics::FUNCTIONS, name)
            or contains(intrinsics::VARIABLES, name);
}

PtrPipesType typeofIntrinsic(std::string name);

#endif //PIPES_INTRINSICS_H
