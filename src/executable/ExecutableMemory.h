/**
 * @file ExecutableMemory.h
 * @date 2020-12-30
 */

#ifndef PIPES_EXECUTABLEMEMORY_H
#define PIPES_EXECUTABLEMEMORY_H

#include <stdexcept>
#include <memory>
#include <sys/mman.h>

using Byte = unsigned char;

/**
 * Note that this function is extremely unsafe because it requests memory that is both
 * writable and executable.
 */
inline Byte *allocate_executable_memory(size_t byte_count)
{
    void *ptr = mmap(nullptr, byte_count, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANON, -1, 0);

    if (ptr == MAP_FAILED) {
        perror("mmap");
        throw std::runtime_error{"mmap failed to allocate " + std::to_string(byte_count) + " bytes"};
    }

    return static_cast<Byte*>(ptr);
}

inline void deallocate_executable_memory(Byte *ptr, size_t size) {
    if (ptr != nullptr) {
        munmap(ptr, size);
    }
}

struct Deleter {
    explicit Deleter(size_t size) : size{size} {}
    size_t size;

    void operator()(Byte ptr[]) const {
//        std::cout << "Deallocating ExecutableMemory of size " << size << std::endl;
        deallocate_executable_memory(ptr, size);
    }
};

struct ExecutableMemory {
    size_t capacity;
    size_t size;
    std::unique_ptr<Byte[], Deleter> data;

    explicit ExecutableMemory(size_t capacity)
            :capacity{capacity},
            size{0},
            data{allocate_executable_memory(capacity), Deleter{capacity}} {
    }

    ExecutableMemory(const ExecutableMemory &other) = delete;
    ExecutableMemory(ExecutableMemory &&other) = default;
    ExecutableMemory &operator=(const ExecutableMemory &other) = delete;
    ExecutableMemory &operator=(ExecutableMemory &&other) = default;
    virtual ~ExecutableMemory() = default;

    /**
     * Returns a function pointer you can call. Calling the function pointer will execute the JIT compiled machine code
     * (compiled with the various Assembler::emit_* functions).
     *
     * @param PROTOTYPE is a function type (not a function pointer type!) just as std::function asks for.
     * @code
     * int example_func(float); // regular declaration of a (float) -> int function
     * std::function<int(float)> example_func; // declaration of the same prototype with std::function
     * auto compiled_func = assembler.getEntryPoint<int(float)>(); // compiled_func is a pointer to a function like example_func
     * auto result = (*compiled_func)(argument); // just as with example_func, argument is float and result is int
     * auto result = compiled_func(argument);    // equivalent syntax
     * @endcode
     * @returns a function pointer with signature PROTOTYPE
     */
    template <typename PROTOTYPE> PROTOTYPE * getEntryPoint() const {
        if (size == 0) {
            throw std::runtime_error{"Can't give an entry point to an empty program!"};
        }
        return reinterpret_cast<PROTOTYPE*>(data.get());
    }
};

#endif //PIPES_EXECUTABLEMEMORY_H
