/**
 * @file Linker.h
 * @date 2021-04-23
 */

#ifndef PIPES_LINKER_H
#define PIPES_LINKER_H

//#include <elf.h>
//#include <cstdio>
//#include <cstring>

#include "ExecutableMemory.h"

struct RuntimeLibraries {
    void *(*mallocPtr)(size_t) = malloc;
    void (*freePtr)(void *ptr) = free;
};


inline void patchRelocations(ExecutableMemory &executableMemory, uint64_t relocationsStart) {
    auto target = RuntimeLibraries{};
    reinterpret_cast<RuntimeLibraries &>(executableMemory.data[relocationsStart]) = target;
}



/*
void read_elf_header(const char* elfFile) {
  // Either Elf64_Ehdr or Elf32_Ehdr depending on architecture.
  Elf64_Ehdr header;

  FILE* file = fopen(elfFile, "rb");
  if(file) {
    // read the header
    fread(&header, sizeof(header), 1, file);

    // check so its really an elf file
    if (memcmp(header.e_ident, ELFMAG, SELFMAG) == 0) {
       // this is a valid elf file
    }

    // finally close the file
    fclose(file);
  }
}
*/
#endif //PIPES_LINKER_H
