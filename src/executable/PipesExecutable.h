/**
 * @file PipesExecutable.h
 * @date 2021-04-24
 */

#ifndef PIPES_PIPESEXECUTABLE_H
#define PIPES_PIPESEXECUTABLE_H

#include <iostream>
#include <sstream>
#include "Linker.h"

static const char *const MAGIC_NUMBER = "PIPE";
static const int MAGIC_NUMBER_LENGTH = 4;

inline void generateExecutable(
        std::string path,
        const ExecutableMemory &executableMemory,
        int64_t relocationStart) {

    std::ofstream file{path, std::ios::binary};
    if (not file) {
        throw std::runtime_error{"Could not open file " + path + " for writing"};
    }

    file << MAGIC_NUMBER;
    if (not file) {
        throw std::runtime_error{"Could not write magic number to file " + path};
    }

    file.write(reinterpret_cast<const char*>(&relocationStart), sizeof(int64_t));
    if (not file) {
        throw std::runtime_error{"Could not write relocationStart to file " + path};
    }

    // C++ I hate you https://stackoverflow.com/questions/1559254/are-there-binary-memory-streams-in-c
    file.write(reinterpret_cast<const char*>(&executableMemory.size), sizeof(executableMemory.size));
    if (not file) {
        throw std::runtime_error{"Could not write size to file " + path};
    }

    file.write(reinterpret_cast<const char*>(executableMemory.data.get()),
               static_cast<int64_t>(executableMemory.size));
    if (not file) {
        throw std::runtime_error{"Could not write machine code to file " + path};
    }
}



inline std::string stringToHexString(std::string s) {
    std::stringstream ss;
    for(const auto &c : s) {
        ss << std::hex << (int) c;
    }
    return ss.str();
}

template<typename T>
std::string intToHexString(T number) {
    std::stringstream ss;
    ss << std::hex << number;
    return ss.str();
}


inline void readMagicNumber(const std::string &path, std::ifstream &file) {
    auto before = std::to_string(file.tellg());
    char actualMagicNumber[MAGIC_NUMBER_LENGTH];
    file.read(actualMagicNumber, MAGIC_NUMBER_LENGTH);
    if (not file) {
        throw std::runtime_error{
                "Could not read magic number from file " + path + " between pos " + before + " and "
                        + std::to_string(file.tellg())};
    }
    auto actualMagicNumberString = std::string(actualMagicNumber, MAGIC_NUMBER_LENGTH);
    if (actualMagicNumberString != MAGIC_NUMBER) {
        auto actualMagicNumberHex = stringToHexString(actualMagicNumberString);
        auto expectedMagicNumberHex = stringToHexString(MAGIC_NUMBER);
        throw std::runtime_error{
                "File " + path + " is probably not a pipes program. Magic number should be (0x"
                        + expectedMagicNumberHex + ") \"" + MAGIC_NUMBER
                        + "\", but it is (0x" + actualMagicNumberHex + ") \"" + actualMagicNumberString + "\""};
    }
}

inline uint64_t readRelocation(const std::string &path, std::ifstream &file) {
    auto before = std::to_string(file.tellg());

    auto relocation = uint64_t{};
    file.read(reinterpret_cast<char*>(&(relocation)), sizeof(relocation));

    if (not file) {
        throw std::runtime_error{
                "Could not correctly read relocation (got 0x" + intToHexString(relocation)
                + ") from file " + path + " between pos " + before + " and " + std::to_string(file.tellg())};
    }
    return relocation;
}

inline decltype(ExecutableMemory::capacity) readCodeSize(const std::string &path, std::ifstream &file) {
    auto size = decltype(ExecutableMemory::capacity){};
    auto before = std::to_string(file.tellg());

    // C++ I hate you https://stackoverflow.com/questions/1559254/are-there-binary-memory-streams-in-c
    file.read(reinterpret_cast<char*>(&(size)), sizeof(size));

    if (not file) {
        throw std::runtime_error{
                "Could not correctly read size (got 0x" + intToHexString(size) + ") from file "
                + path + " between pos " + before + " and " + std::to_string(file.tellg())};
    }
    if (size < 1 or size > 1'000'000) {
        throw std::runtime_error{
                "File " + path + " states that the code it contains is " + std::to_string(size)
                + " bytes, which is too " + (size < 1 ? "small" : "big")};
    }
//    std::cout << "The machine code of the program at " << path << " is " << size << " bytes long." << std::endl;
    return size;
}

inline ExecutableMemory loadExecutable(const std::string &path) {
    std::ifstream file{path, std::ios::binary};
    if (not file) {
        throw std::runtime_error{"Could not open file " + path};
    }

    readMagicNumber(path, file);
    auto relocation = readRelocation(path, file);
    auto size = readCodeSize(path, file);

    auto buffer = ExecutableMemory{size};
    file.read(reinterpret_cast<char*>(buffer.data.get()), static_cast<std::streamsize>(size));
    buffer.size = size;

    patchRelocations(buffer, relocation);

    return buffer;
}

#endif //PIPES_PIPESEXECUTABLE_H
