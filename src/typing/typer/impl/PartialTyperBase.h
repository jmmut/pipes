
#ifndef PIPES_PARTIALTYPERBASE_H
#define PIPES_PARTIALTYPERBASE_H

#include <utility>

#include "typing/unify/unify.h"
#include "typing/typer/PartialTyper.h"
#include "typing/typer/impl/TyperState.h"


class PartialTyperBase : public PartialTyper {
public:
    explicit PartialTyperBase(const CodeFile &code)
            : code{code}, wasChanged_{false} {}


//    [[nodiscard]] PtrPipesType unify(const PipesType &first, const PipesType &second,
//                                     const Expression &expr,
//                                     PartialExceptionMessage message = {}) {
//        return Unifier{expr, state.code}.unify(first, second, std::move(message));
//    }

    void unify(Expression &expression, const PipesType &withThis,
               PartialExceptionMessage message = {}) {
        auto unifier = Unifier{expression, code};
        auto result = unifier.unify(expression.pipesType().operator*(),
                                    withThis, std::move(message));
        if (unifier.wasChanged()) {
            wasChanged_ = true;
        }
        expression.pipesType() = std::move(result);
    }

    void unify(Expression &expression, const Expression &withThis,
               PartialExceptionMessage message = {}) {
        auto unifier = Unifier{expression, withThis, code};
        auto result = unifier.unify(expression.pipesType().operator*(),
                                    withThis.pipesType().operator*(),
                                    std::move(message));
        if (unifier.wasChanged()) {
            wasChanged_ = true;
        }
        expression.pipesType() = std::move(result);
    }

    bool wasChanged() override {
        return wasChanged_;
    }

protected:
    const CodeFile &code;

private:
    bool wasChanged_;
};

#endif //PIPES_PARTIALTYPERBASE_H
