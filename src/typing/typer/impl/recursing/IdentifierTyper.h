
#ifndef PIPES_IDENTIFIERTYPER_H
#define PIPES_IDENTIFIERTYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "RecursiveTyper.h"
#include "typing/subtyping.h"
#include "expression/ExpressionType.h"

class IdentifierTyper : public PartialTyperBase {
public:
    IdentifierTyper(const CodeFile &code,
                    const std::map<std::string, PipesTypes> &identifiersBeingProcessed,
                    const Identifiers &identifiers)
            : PartialTyperBase{code},
              identifiersBeingProcessed{identifiersBeingProcessed},
              identifiers{identifiers} {
    }

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::IDENTIFIER) {
            if (not isIntrinsic(expression.getIdentifier())) {
                auto identifierFile = identifiers.get_optional(expression.getIdentifier());
                if (identifierFile.isPresent()) {
                    addTypeToUserIdentifier(expression, identifierFile->expression);
                } else {
                    throw semanticError(code, expression.position(),
                                        "unknown identifier " + quote(expression.getIdentifier()));
                }
            }
        }
    }
    
    void addTypeToUserIdentifier(Expression &expression, const Expression &body) {
        const auto &identifierType = body.pipesType();
        unify(expression, identifierType.operator*());
        bool usageIsStricter = isMoreRestrictiveTypeAndCompatible(
                expression.pipesType().operator*(),
                identifierType.operator*());
        auto [_, newSpecialization] = identifiers_and_type_already_specialized.insert(to_string_with_types(expression));
//        std::cout << identifiers_and_type_already_specialized.size() << std::endl;
        if (usageIsStricter and newSpecialization) {
            auto found = identifiersBeingProcessed.find(expression.getIdentifier());
            if (found != identifiersBeingProcessed.end()) {
                for (const auto &type: found->second) {
                    if (type.operator*() == expression.pipesType().operator*()) {
                        ; // this is a recursive identifier. don't go into an infinite loop of specialization
                        // TODO: might be interesting to track this for recursive types
                        return;
                    }
                }
            }
            specialize_body_and_unify_expression_again(expression, body);
        }
    }

    void specialize_body_and_unify_expression_again(Expression &expression,
                                                    const Expression &body) {
//        static long specializations = 0;
//        ++specializations;
//        std::cout << specializations << std::endl;
        auto specializationCopy = body;
        specializationCopy.pipesType() = expression.pipesType()->copy();

        auto identifiersStack = copy(identifiersBeingProcessed);
        identifiersStack[expression.getIdentifier()].push_back(specializationCopy.pipesType()->copy());
        auto typer = std::make_unique<RecursiveTyper>(
                specializationCopy,
                identifiers.at(expression.getIdentifier()).code,
                identifiers,
                identifiersStack);
        typer->addTypes();
        unify(expression, specializationCopy.pipesType().operator*());
    }

private:
    static std::set<std::string> identifiers_and_type_already_specialized;
    const std::map<std::string, PipesTypes> &identifiersBeingProcessed;
    const Identifiers &identifiers;
};

#endif //PIPES_IDENTIFIERTYPER_H
