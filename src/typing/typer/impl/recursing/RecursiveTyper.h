
#ifndef PIPES_RECURSIVETYPER_H
#define PIPES_RECURSIVETYPER_H


#include "typing/typer/factory/TyperFactory.h"
#include "typing/typer/impl/unit/TypeofTyper.h"
#include "typing/typer/PartialTyper.h"
#include "expression/ExpressionType.h"

static const int MAX_RECURSIVE_ITERATIONS = 100;


class RecursiveTyper {
public:
    explicit RecursiveTyper(Expression &expression,
                            const CodeFile &code,
                            const Identifiers &identifiers,
                            const std::map<std::string, PipesTypes> &identifiersBeingProcessed)
            : iterations{0}, needAnotherRound{false}, expression{expression}, code{code},
            identifiersBeingProcessed{identifiersBeingProcessed}, identifiers{identifiers} {
    }

    void addTypes() {
        iterations = 0;
        do {
            needAnotherRound = false;
            partialTypers = buildConstTypers(code, identifiersBeingProcessed, identifiers);
            recurseDown();
            recurseDownToCheckTypeofs();
            ++iterations;
        } while (needAnotherRound and iterations < MAX_RECURSIVE_ITERATIONS);

        checkMaxIterations();
        recurseDownToPrintType();
    }

private:
    void recurseDown() {
        recursivelyRewrite(expression, [&](Expression &innerExpression) {
            for (const auto &typer: partialTypers) {
                typer->preAddTypes(innerExpression);
            }
        }, [&](Expression &innerExpression) {
            for (const auto &typer: partialTypers) {
                typer->addTypes(innerExpression);
            }
        });
        for (const auto &typer: partialTypers) {
            if (typer->wasChanged()) {
                needAnotherRound = true;
            }
        }
    }

    void recurseDownToCheckTypeofs() {
        auto typer = TypeofTyper{code};
        recursivelyRewrite(expression, [&](Expression &innerExpression) {
            typer.preAddTypes(innerExpression);
        }, [&](Expression &innerExpression) {
            typer.addTypes(innerExpression);
        });
    }

    void recurseDownToPrintType() {
        recursivelyRewrite(expression, [](Expression &expr){}, [](Expression &expr) {
            if (expr.getType() == ExpressionType::CALL
                    and expr.getCall().function().getType() == ExpressionType::IDENTIFIER) {
                if (expr.getCall().function().getIdentifier() == intrinsics::COMPILE_TIME_PRINT_TYPE) {
                    auto call_argument = expr.getCall().argument();
                    expr = call_argument; // put a breakpoint here to compare the whole call and the argument
                    std::cout << "Inferred types:\n" << to_pretty_string_with_types(expr, 0)
                              << std::endl << std::endl;
                } else if (expr.getCall().function().getIdentifier()
                        == intrinsics::COMPILE_TIME_TRACK_TYPE) {
                    auto call_argument = std::move(expr.getCall().argument());
                    expr = std::move(call_argument); // put a breakpoint here to compare the whole call and the argument
                }
            }
        });
    }

    void checkMaxIterations() {
        if (iterations >= MAX_RECURSIVE_ITERATIONS) {
            throw compilerBug(code, expression.position(),
                              "The type checker has done "
                              + std::to_string(iterations) +
                              " iterations and is still making updates to the types of the program."
                              " Maybe there's an undetected recursive type."
                              " Compilation won't be done");
        }
    }

private:
    int iterations;
    bool needAnotherRound;
    PartialTypers partialTypers;
    Expression &expression;
    const CodeFile &code;
    const std::map<std::string, PipesTypes> &identifiersBeingProcessed;
    const Identifiers &identifiers;
};

#endif //PIPES_RECURSIVETYPER_H
