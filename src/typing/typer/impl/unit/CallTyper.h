
#ifndef PIPES_CALLTYPER_H
#define PIPES_CALLTYPER_H


#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class TypeTracker {
public:
    explicit TypeTracker(Expression &expression)
            : argument{expression.getCall().argument()},
              function{expression.getCall().function()} {}

    void trackTypeStart() {
        if (function.getType() == ExpressionType::IDENTIFIER
                and function.getIdentifier() == intrinsics::COMPILE_TIME_TRACK_TYPE) {
            type_tracking << "Tracking type:\n\tfrom: " << to_pretty_string_with_types(argument, 0);
        }
    }

    void trackTypeEnd() {
        if (function.getType() == ExpressionType::IDENTIFIER
                and function.getIdentifier() == intrinsics::COMPILE_TIME_TRACK_TYPE) {
            type_tracking << "\n\tto:   " << to_pretty_string_with_types(argument, 0);
            std::cout << type_tracking.str() << std::endl;
        }
    }

private:
    std::stringstream type_tracking;
    Expression &argument;
    Expression &function;
};

class CallTyper : public PartialTyperBase {
public:
    using PartialTyperBase::PartialTyperBase;

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::CALL) {
            auto tracker = TypeTracker{expression};
            auto &argument = expression.getCall().argument();
            auto &function = expression.getCall().function();
            tracker.trackTypeStart();

            unify(function, FunctionType(argument.pipesType()->copy(),
                                         expression.pipesType()->copy(), ""),
                  {"",
                   "Actual type (declared or inferred from its body): ",
                   "Function type required in this context: "});
            unify(expression, function.pipesType()->getReturnedType());
            unify(argument, function.pipesType()->getParameterType());

            tracker.trackTypeEnd();
        }
    }
};


#endif //PIPES_CALLTYPER_H
