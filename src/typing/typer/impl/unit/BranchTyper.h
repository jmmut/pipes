
#ifndef PIPES_BRANCHTYPER_H
#define PIPES_BRANCHTYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class BranchTyper : public PartialTyperBase {

public:
    using PartialTyperBase::PartialTyperBase;

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::BRANCH) {
            auto &yes = expression.getBranch().yes();
            auto &no = expression.getBranch().no();

            unify(expression, FunctionType(UnknownType::make_ptr(""),
                                           yes.pipesType()->copy(), ""));
            unify(yes, expression.pipesType()->getReturnedType());

            unify(expression, FunctionType(UnknownType::make_ptr(""),
                                           no.pipesType()->copy(), ""));
            unify(no, expression.pipesType()->getReturnedType());
        }
    }
};

#endif //PIPES_BRANCHTYPER_H
