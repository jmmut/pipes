
#ifndef PIPES_WHILETYPER_H
#define PIPES_WHILETYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class WhileTyper : public PartialTyperBase {

public:
    using PartialTyperBase::PartialTyperBase;

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::WHILE) {
            auto &condition = expression.getWhile().condition();

            unify(expression, IntType{});
            unify(condition, IntType{});
        }
    }
};

#endif //PIPES_WHILETYPER_H
