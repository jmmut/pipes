
#ifndef PIPES_VARIABLETYPER_H
#define PIPES_VARIABLETYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class VariableTyper : public PartialTyperBase {

public:
    using PartialTyperBase::PartialTyperBase;

    explicit VariableTyper(const CodeFile &code)
            : PartialTyperBase{code}, currentNesting{0} {}

    void preAddTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::BINARY_OPERATION
                and expression.getBinaryOperation().operation == Token::DEFINITION) {
            auto &left = expression.getBinaryOperation().left;
            auto &right = expression.getBinaryOperation().right;
            if (right.getType() == ExpressionType::VARIABLE) {
                unify(left, expression);
                unify(right, left);
                auto &updatedRight = insertVariableType(right);
                unify(left, updatedRight);
                unify(expression, updatedRight);
            } else {
                throw compilerBug(
                        code, expression.positionInCode, "The right-hand operator of "
                                + to_string(Token::DEFINITION)
                                + " must be a variable name."
                                  " This should have been detected in earlier stages.");
            }
        } else if (definesScope(expression.getType())) {
            ++currentNesting;
        }
    }


    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::VARIABLE) {
            auto &variableType = getVariableType(expression);
            unify(expression, variableType);
            insertVariableType(expression);
        } else if (definesScope(expression.getType())) {
            --currentNesting;
            std::erase_if(variablesInScope,
                      [&](const VariablesInScope::value_type &entry) -> bool {
                          return entry.second.scopeNesting > currentNesting;
                      });
        }
    }
private:
    VariablesInScope variablesInScope;
    int currentNesting;

    const PipesType &getVariableType(Expression &expression) const {
        const auto &variableName = expression.getVariable();
        auto found = variablesInScope.find(variableName);
        if (found != variablesInScope.end()) {
            return found->second.strictestType.operator*();
        } else {
            throw std::runtime_error{
                    "Semantic error: undefined variable " + variableName + " used at "
                            + codePosToString(code, expression.position())};
        }
    }

    PipesType &insertVariableType(Expression &right) {
        auto variableName = right.getVariable();
        auto &previousType = insertOrRetrieve(variableName);
        unify(right, previousType.strictestType.operator*());
        previousType.strictestType = right.pipesType()->copy();
        return previousType.strictestType.operator*();
    }

    ScopedVariableType &insertOrRetrieve(const std::string &variableName) {
        auto found = variablesInScope.find(variableName);
        if (found != variablesInScope.end()) {
            return found->second;
        } else {
            auto variableType = ScopedVariableType{isGlobal(variableName),
                                                   UnknownType::make_ptr(""),
                                                   currentNesting};
            return variablesInScope.insert({variableName, std::move(variableType)}).first->second;
        }
    }

    bool isGlobal(const std::string &variableName) {
        return false;   // TODO: fix after typer refactor
    }
};

#endif //PIPES_VARIABLETYPER_H
