
#ifndef PIPES_BINARYOPERATIONTYPER_H
#define PIPES_BINARYOPERATIONTYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "typing/subtyping.h"
#include "parser/TypeParser.h"
#include "expression/ExpressionType.h"

struct BinaryOperationHelper {
    Expression &expression;
    Expression &left;
    Expression &right;
    Token::Type operation;
    std::string opName;
};

class BinaryOperationTyper : public PartialTyperBase {

public:
    explicit BinaryOperationTyper(const CodeFile &code);

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::BINARY_OPERATION) {
            auto h = BinaryOperationHelper{
                    .expression = expression,
                    .left = expression.getBinaryOperation().left,
                    .right = expression.getBinaryOperation().right,
                    .operation = expression.getBinaryOperation().operation,
                    .opName = quote(to_string(expression.getBinaryOperation().operation)),
            };
            auto found = types.find(h.operation);
            if (found != types.end()) {
                auto message = getMessage(h.opName);
                unify(expression, found->second.operator*(), message);
                unify(h.left, found->second.operator*(), message);
                unify(h.right, found->second.operator*(), message);
            } else if (h.operation == Token::PLUS
                       or h.operation == Token::MINUS) {
                unifyPlusOrMinus(h);
            } else if (h.operation == Token::STATEMENT_SEPARATOR) {
                unify(expression, h.right,
                      {"", "Expected type for the result of the operation " + h.opName + ": ",
                       "Expression type: "});
                unify(h.right, expression);
            } else if (h.operation == Token::LOOP_CALL) {
                unifyLoop(h);
            } else if (h.operation == Token::MUTABLE_LOOP_CALL) {
                unifyMutableLoop(h);
            } else if (h.operation == Token::FIELD_ACCESS) {
                addTypesToFieldAccess(h);
            } else if (h.operation == Token::DEFINITION) {
                ; // implemented in VariableTyper
            } else {
                throw randomize::exception::StackTracedException{
                        "unimplemented typer for binary operation " + h.opName};
            }
        }
    }

    PartialExceptionMessage getMessage(const std::string& operationName) {
        std::string firstName  = "Type of the expression (inferred or declared): ";
        auto secondName = "Type required for operation " + operationName + ": ";
        return PartialExceptionMessage{"", firstName, secondName};
    }

    void unifyPlusOrMinus(BinaryOperationHelper h) {
        auto op = quote(h.operation);
        if (areAny<UnknownType, IntType>(h.left.pipesType(), h.right.pipesType())) {
            unify(h.expression, IntType{});
        } else if (h.left.pipesType()->isIterable()) {
            unify(h.expression, h.left);
            unify(h.right, IntType{});
        } else if (h.right.pipesType()->isIterable()) {
            unify(h.expression, h.right);
            unify(h.left, IntType{});
        } else {
            throw compilerBug(code, h.expression.position(),
                              "unhandled possibility with operation " + h.opName +
                              " and expression " + quote(to_string_with_types(h.expression)));
        }
    }

    void unifyLoop(BinaryOperationHelper h) {
        unify(h.left, ArrayType(UnknownType::make_ptr(""), ""));
        unify(h.expression, ArrayType(UnknownType::make_ptr(""), ""));

        unify(h.right,
              FunctionType(
                      getStrictestTypeOrUnknownIfIncompatible(
                              h.left.pipesType()->getNestedTypes()),
                      getStrictestTypeOrUnknownIfIncompatible(
                              h.expression.pipesType()->getNestedTypes()),
                      ""),
              {"",
               "Actual type (declared or inferred from its body): ",
               "Function type required in this context: "});

        unify(h.expression, ArrayType(h.right.pipesType()->getReturnedType().copy(), ""));
        unify(h.left, ArrayType(h.right.pipesType()->getParameterType().copy(), ""));
    }

    void unifyMutableLoop(BinaryOperationHelper h) {
        unify(h.left, ArrayType(UnknownType::make_ptr(""), ""));
        unify(h.expression, h.left);

        unify(h.right,
              FunctionType(
                      getStrictestTypeOrUnknownIfIncompatible(
                              h.left.pipesType()->getNestedTypes()),
                      getStrictestTypeOrUnknownIfIncompatible(
                              h.left.pipesType()->getNestedTypes()),
                      ""),
              {"",
               "Actual type (declared or inferred from its body): ",
               "Function type required in this context: "});

        unify(h.expression, ArrayType(h.right.pipesType()->getReturnedType().copy(), ""));
        unify(h.left, ArrayType(h.right.pipesType()->getParameterType().copy(), ""));
    }

    void addTypesToFieldAccess(BinaryOperationHelper h) {
        if (is<UnknownType>(h.left.pipesType())) {
            unify(h.left, UserType(getTypes(UnknownType::make_ptr("")), ""));
            return;
        }
        auto fieldDescription = h.right.getType() == ExpressionType::FIELD
                                ? quote(h.right.getIdentifier())
                                : (quote(to_string(h.right))
                                   + " (incorrectly parsed as a "
                                   + quote(to_string(h.right.getType()))
                                   + " expression)");
        auto &shouldBeUserType = maybeUnwrapTypeof(h.left.pipesType().operator*());
        if (is<UserType>(shouldBeUserType)) {
            if (shouldBeUserType.getNestedTypes().size() == 1
                    and is<UnknownType>(shouldBeUserType.getNestedType())) {
                return;
            }
            auto &userType = dynamic_cast<const UserType&>(shouldBeUserType);
            auto optionalField = userType.getFieldIndex(h.right.getIdentifier());
            if (optionalField.has_value()) {
                auto &field = userType.getNestedTypes().at(optionalField.value());
                unify(h.expression, field.operator*());
            } else {
                //incomplete struct
                throw TypeCheckException{
                        "Compilation error: Tried to access a field " + fieldDescription
                        + " that doesn't exist in struct " + quote(to_string(h.left.pipesType()))
                        + " at "
                        + codePosToString(code, h.right.positionInCode)};
            }
        } else {
            throw TypeCheckException{
                    "Compilation error: Tried to access a field " + fieldDescription
                    + " on something that is not a struct. Expression at "
                    + codePosToString(code, h.left.positionInCode)
                    + "\nshould be a struct but is: " + h.left.pipesType()->to_string()};
        }
    }

    void addTypesToDefinition(BinaryOperationHelper h);
    bool isGlobal(const std::string &variableName);

private:
    std::map<Token::Type, PtrPipesType> types;
    VariablesInScope variablesInScope;

    auto buildTypes() {
        decltype(types) t;
        t[Token::GREATER_THAN] = IntType::make_ptr("");
        t[Token::MULTIPLY] = IntType::make_ptr("");
        t[Token::DIVIDE] = IntType::make_ptr("");
        t[Token::MODULO] = IntType::make_ptr("");
//        t[Token::PLUS] = IntType::make_ptr("");
//        t[Token::MINUS] = IntType::make_ptr("");
        return t;
    }
};

inline BinaryOperationTyper::BinaryOperationTyper(const CodeFile &code)
        : PartialTyperBase{code} {
    types = buildTypes();
}


#endif //PIPES_BINARYOPERATIONTYPER_H
