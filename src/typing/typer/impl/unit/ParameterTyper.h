
#ifndef PIPES_PARAMETERTYPER_H
#define PIPES_PARAMETERTYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class ParameterTyper : public PartialTyperBase {
public:
    using PartialTyperBase::PartialTyperBase;

    void preAddTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::LAMBDA) {
            strictestParameterType[expression.getLambda().id] = expression;
        }
    }

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::PARAMETER) {
            addTypesParameter(expression);
        } else if (expression.getType() == ExpressionType::LAMBDA) {
            addTypesFunction(expression);
        }
    }

    void addTypesParameter(Expression &expression) {
        auto &strictestExpr = strictestParameterType.at(expression.getParameter().lambdaId);

        const auto &strictest = getStrictest(strictestExpr, expression);

        unify(expression, strictest);

        strictestParameterType[expression.getParameter().lambdaId] = expression;
//            lambdasWithOutdatedParameterType[expression.getParameter().lambdaId]++;
    }

    Expression getStrictest(Expression &strictestExpr, Expression &wrappingParameter) const {
        if (strictestExpr.getType() == ExpressionType::LAMBDA) {
            auto param = ofParameter(strictestExpr.getLambda().argumentName, -1, -1,
                                     strictestExpr.getLambda().id, -1, strictestExpr.positionInCode);
            param.pipesType() = strictestExpr.pipesType()->getParameterType().copy();
            return param;
        } else if (strictestExpr.getType() == ExpressionType::PARAMETER) {
            return strictestExpr;
        } else {
            throw compilerBug(code, wrappingParameter.position(),
                                "strictestParameterType should contain a "
                                "parameter or a lambda, contains "
                                + quote(to_string(strictestExpr)));
        }
    }

    void addTypesFunction(Expression &expression) {
        unify(expression, FunctionType(chooseStrictestParameter(expression.getLambda()),
                                       expression.getLambda().body().pipesType()->copy(), ""),
              {"",
               "Function type (declared or inferred from its context): ",
               "Function type (inferred from its body): "});
        unify(expression.getLambda().body(), expression.pipesType()->getReturnedType());

    }

    PtrPipesType chooseStrictestParameter(const Lambda &expression) {
        auto &strictest = strictestParameterType[expression.id];
        return strictest.getType() == ExpressionType::PARAMETER ? strictest.pipesType()->copy()
                                                                : UnknownType::make_ptr("");
    }

private:
    std::map<LambdaId, Expression> strictestParameterType;
};

#endif //PIPES_PARAMETERTYPER_H
