
#ifndef PIPES_TUPLETYPER_H
#define PIPES_TUPLETYPER_H



#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class TupleTyper : public PartialTyperBase {
public:
    using PartialTyperBase::PartialTyperBase;

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::TUPLE) {
            PipesTypes innerTypes;
            for (const auto &element : expression.getTuple().getElements()) {
                innerTypes.push_back(element.pipesType()->copy());
            }

            auto inferredTuple = TupleType::make_ptr(std::move(innerTypes), "");
            unify(expression, *inferredTuple);
            int i = 0;
            for (auto &element: expression.getTuple().getElements()) {
                unify(element, chooseNestedType(expression, i).operator*());
                ++i;
            }
        }
    }

    /**
     * This takes into account that a tuple of >1 elements might be marked
     * as :iterable(:singleType) if it can be homogeneous
     */
    static const PtrPipesType &chooseNestedType(Expression &expression, int i) {
        const auto &types = expression.pipesType()->getNestedTypes();
        return types.size() == 1 ? types.at(0) : types.at(i);
    }
};


#endif //PIPES_TUPLETYPER_H
