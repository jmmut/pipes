
#ifndef PIPES_ARRAYTYPER_H
#define PIPES_ARRAYTYPER_H


#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"

class ArrayTyper : public PartialTyperBase {
public:
    using PartialTyperBase::PartialTyperBase;

    void addTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::ARRAY) {
            unify(expression, ArrayType(getStrictest(expression.getArray().getElements()), ""));
            for (auto &element: expression.getArray().getElements()) {
                unify(element, expression.pipesType()->getNestedType());
            }
        }
    }

    PtrPipesType getStrictest(const std::vector<Expression> &elements) {
        if (elements.empty()) {
            return UnknownType::make_ptr("");
        } else {
            Expression strictest = ofArray({}, elements.at(0).positionInCode);
            strictest.pipesType() = elements.at(0).pipesType()->copy();
            for (size_t i = 1; i < elements.size(); ++i) {
                unify(strictest, elements[i]);
                strictest.positionInCode = elements[i].positionInCode;
            }
            return std::move(strictest.pipesType());
        }
    }
};

#endif //PIPES_ARRAYTYPER_H
