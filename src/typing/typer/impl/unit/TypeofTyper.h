
#ifndef PIPES_TYPEOFTYPER_H
#define PIPES_TYPEOFTYPER_H

#include "typing/typer/impl/PartialTyperBase.h"
#include "expression/ExpressionType.h"


/*
 enlist_and_map =
 function f :function(l :iterable(elem))  {
    function e :typeof(elem) { [e] |f } }

 do_something_with_list =
 function l :iterable -> l2 :iterable { ... }

 4
 enlist_and_map do_something_with_list

 4
 |(
    do_something_with_list :function l :iterable -> l2 :iterable
    |(enlist_and_map :function f :function(l :iterable(elem) -> l2 :iterable) -> function e :typeof(elem) )
    :function e :typeof(elem)
 ) :?

 4
 |(
    do_something_with_list :function l :iterable -> l2 :iterable
    |(enlist_and_map :function f :function(l :iterable(elem) -> l2 :iterable) -> function(e :typeof(elem) -> l2 :iterable ))
    :function e :typeof(elem) -> l2 :iterable
 ) :?

 4
 |(
    do_something_with_list :function l :iterable -> l2 :iterable
    |(enlist_and_map :function f :function(l :iterable(elem) -> l2 :iterable) -> function(e :typeof(elem :int64) -> l2 :iterable ))
    :function e :typeof(elem :int64) -> l2 :iterable
 ) :?

 */
class TypeofTyper : public PartialTyperBase {
public:
    using PartialTyperBase::PartialTyperBase;

    explicit TypeofTyper(const CodeFile &code) : PartialTyperBase{code}, wasChanged_{false} {}

    void preAddTypes(Expression &expression) override {
        if (expression.getType() == ExpressionType::LAMBDA) {
            strictestParameterType[expression.getLambda().id] = expression;
        }
    }

    void addTypes(Expression &expression) override {
        addVirtualParameterTypes(expression);
        addInnerExpressionTypes();
    }

    void addVirtualParameterTypes(Expression &expression) {
        std::map<std::string, std::vector<std::reference_wrapper<PtrPipesType>>> virtualTypes;

        recursivelyRewrite(expression.pipesType(), [&](PtrPipesType &type) {
            if (is<TypeofType>(type)) {
                auto &typeofType = dynamic_cast<TypeofType&>(type.operator*());
                if (typeofType.getInnerExpression().getType() == ExpressionType::PARAMETER) {
                    auto foundVirtual = virtualTypes.find(typeofType.getInnerExpression().getParameter().name);
                    auto found = strictestParameterType.find(
                            typeofType.getInnerExpression().getParameter().lambdaId);
                    bool virtualFromAnotherFunction = typeofType.getInnerExpression().getParameter().isVirtual;
                    auto ptrStrictest = UnknownType::make_ptr("");
                    if (foundVirtual == virtualTypes.end()
                            and found == strictestParameterType.end()) {
                        throw semanticError(code, expression.position(),
                                            "Compilation error: can't find what typeof("
                                            + typeofType.getInnerExpression().getParameter().name
                                            + ") refers to");
                    } else if (virtualFromAnotherFunction) {
                        throw semanticError(code, expression.position(),
                                            "Compilation error: virtual parameter "
                                            + quote(typeofType.getInnerExpression().getParameter().name)
                                            + " is not allowed inside a typeof in this function. "
                                              "Only virtual parameters defined "
                                              "in the same function are allowed inside a typeof");
                    } else if (foundVirtual != virtualTypes.end()) {
                        auto helperUnifier = Unifier{expression, code};
                        ptrStrictest = foundVirtual->second.at(0).get()->copy();
                        for (size_t i = 1; i < foundVirtual->second.size(); ++i) {
                            ptrStrictest = helperUnifier.unify(ptrStrictest.operator*(),
                                                         foundVirtual->second.at(i).get().operator*());
                        }
                    } else {
                        if (found->second.getType() == ExpressionType::LAMBDA) {
                            ptrStrictest = found->second.pipesType()->getParameterType().copy();
                        } else {
                            ptrStrictest = found->second.pipesType()->copy();
                        }
//                        if (false) {
//                            std::cout << "call stack (inner first): "
//                                      << randomize::utils::to_string(getCallStack(this->state))
//                                      << std::endl;
//                        }
                    }
                    auto relevantUnifier = Unifier{expression, code};
                    auto cached = relevantUnifier.unify(type.operator*(), ptrStrictest.operator*());
                    if (relevantUnifier.wasChanged()) {
                        wasChanged_ = true;
                    }
                    type = std::move(cached);
                } else {
                    throw std::runtime_error{"unsupported: typeof"
                        + parenthesize(to_string(typeofType.getInnerExpression().getType()))};
                }
            }
            bool typeofItself = false;
            if (is<TypeofType>(type)) {
                auto &typeofType = dynamic_cast<TypeofType &>(type.operator*());
                if (typeofType.getInnerExpression().getType() == ExpressionType::PARAMETER
                        and typeofType.getInnerExpression().getParameter().name == type->instanceName()){
                    typeofItself = true;
                }
            }
            if (not typeofItself and not isAnonymous(type.operator*())) {
                virtualTypes[type->instanceName()].push_back(type);
            }
        }, [](PtrPipesType &type) {});
    }

    void addInnerExpressionTypes() {

    }

    bool wasChanged() override {
        return wasChanged_;
    }

private:
    std::map<LambdaId, Expression> strictestParameterType;
    bool wasChanged_;
};


#endif //PIPES_TYPEOFTYPER_H
