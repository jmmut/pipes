
#ifndef PIPES_TYPERSTATE_H
#define PIPES_TYPERSTATE_H

#include <map>
#include <vector>
#include "expression/Expression.h"
#include "parser/Identifiers.h"

using ParameterStack = std::vector<Expression>;

struct ScopedVariableType {
    bool isGlobal;
    PtrPipesType strictestType;
    int scopeNesting;

    ScopedVariableType()
            : isGlobal{false}, strictestType{UnknownType::make_ptr("")}, scopeNesting{-1} {}
    ScopedVariableType(bool isGlobal, const PtrPipesType &strictestType, int scopeNesting)
            : isGlobal(isGlobal), strictestType{copy(strictestType)}, scopeNesting{scopeNesting} {}

    ScopedVariableType(const ScopedVariableType &other) {
        this->isGlobal = other.isGlobal;
        this->strictestType = copy(other.strictestType);
        this->scopeNesting = other.scopeNesting;
    }
};

using VariablesInScope = std::map<std::string, ScopedVariableType>;

struct TyperState {
    const CodeFile &code;
    const Identifiers &identifiers;
    const std::map<std::string, PipesTypes> &identifiersBeingProcessed;
    const TyperState * const upper = nullptr;
};



template <typename T>
auto copy(const std::map<std::string, T> &identifiersBeingProcessed) -> std::map<std::string, T> {
    std::map<std::string, T> copied;
    for (const auto &[identifier, types] : identifiersBeingProcessed) {
        T copiedTypes;
        for (const auto &type: types) {
            copiedTypes.push_back(type->copy());
        }
        copied[identifier] = std::move(copiedTypes);
    }
    return copied;
}

inline auto getCallStack(const TyperState &state) {
    const TyperState *current = &state;
    std::vector<std::string> identifiers;
    do {
        identifiers.push_back(current->code.file);
        current = current->upper;
    } while (current != nullptr);
    return identifiers;
}


#endif //PIPES_TYPERSTATE_H
