
#ifndef PIPES_TYPERFACTORY_H
#define PIPES_TYPERFACTORY_H

#include "typing/typer/PartialTyper.h"
#include "parser/Identifiers.h"

PartialTypers buildConstTypers(const CodeFile &code,
                               const std::map<std::string, std::vector<PtrPipesType>> &identifiersBeingProcessed,
                               const Identifiers &identifiers);

#endif //PIPES_TYPERFACTORY_H
