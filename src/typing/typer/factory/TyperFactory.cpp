
#include "TyperFactory.h"
#include "typing/typer/impl/unit/CallTyper.h"
#include "typing/typer/impl/unit/ParameterTyper.h"
#include "typing/typer/impl/unit/ArrayTyper.h"
#include "typing/typer/impl/unit/TupleTyper.h"
#include "typing/typer/impl/unit/binaryOperations/BinaryOperationTyper.h"
#include "typing/typer/impl/recursing/IdentifierTyper.h"
#include "typing/typer/impl/unit/BranchTyper.h"
#include "typing/typer/impl/unit/WhileTyper.h"
#include "typing/typer/impl/unit/VariableTyper.h"


PartialTypers buildConstTypers(
        const CodeFile &code,
        const std::map<std::string, std::vector<PtrPipesType>> &identifiersBeingProcessed,
        const Identifiers &identifiers) {
    PartialTypers typers;
    typers.push_back(std::make_unique<TupleTyper>(code));
    typers.push_back(std::make_unique<WhileTyper>(code));
    typers.push_back(std::make_unique<ParameterTyper>(code));
    typers.push_back(std::make_unique<CallTyper>(code));
    typers.push_back(std::make_unique<BranchTyper>(code));
    typers.push_back(std::make_unique<ArrayTyper>(code));
    typers.push_back(std::make_unique<BinaryOperationTyper>(code));
    typers.push_back(std::make_unique<IdentifierTyper>(code,
                                                       identifiersBeingProcessed,
                                                       identifiers));
    typers.push_back(std::make_unique<VariableTyper>(code));
    return typers;
}
