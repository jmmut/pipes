
#ifndef PIPES_PARTIALTYPER_H
#define PIPES_PARTIALTYPER_H

#include <memory>
#include <vector>
#include "expression/Expression.h"


class PartialTyper {
public:
    virtual ~PartialTyper() = default;
    virtual void preAddTypes(Expression &expression) {}
    virtual void addTypes(Expression &expression) = 0;
    virtual bool wasChanged() = 0;
};

using PtrPartialTyper = std::unique_ptr<PartialTyper>;
using PartialTypers = std::vector<PtrPartialTyper>;

#endif //PIPES_PARTIALTYPER_H
