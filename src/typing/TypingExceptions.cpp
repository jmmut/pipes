/**
 * @file Exceptions.cpp
 * @date 2021-08-03
 */

#include "TypingExceptions.h"

#include <utility>

IncompatibleTypesException::IncompatibleTypesException(const PipesType &first,
                                                       const std::string &firstPosition,
                                                       const std::string &firstName,
                                                       const PipesType &second,
                                                       const std::string &secondPosition,
                                                       const std::string &secondName,
                                                       std::string message)
        : runtime_error{message
                                + "Expression at "
                                + firstPosition
                                + "\nis incompatible with expression at "
                                + secondPosition
                                + "\n" + firstName
                                + first.to_string()
                                + "\n" + secondName
                                + second.to_string()} {
}

IncompatibleTypesException::IncompatibleTypesException(const PipesType &first,
                                                       const PipesType &second,
                                                       const std::string &firstPosition,
                                                       const std::string &secondPosition,
                                                       std::string message)
        : IncompatibleTypesException{first,
                                     firstPosition,
                                     "First type:  ",
                                     second,
                                     secondPosition,
                                     "Second type: ",
                                     std::move(message)} {
}

IncompatibleTypesException::IncompatibleTypesException(const PipesType &first,
                                                       const PipesType &second,
                                                       std::string message)
        : IncompatibleTypesException{first,
                                     "First type:  ",
                                     second,
                                     "Second type: ",
                                     std::move(message)} {
}

IncompatibleTypesException::IncompatibleTypesException(const PipesType &first,
                                                       const std::string &firstName,
                                                       const PipesType &second,
                                                       const std::string &secondName,
                                                       std::string message)
        : runtime_error{message
                                + "\n" + addSpacesToMatch(firstName, secondName)
                                + first.to_string()
                                + "\n" + addSpacesToMatch(secondName, firstName)
                                + second.to_string()} {
}
