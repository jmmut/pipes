/**
 * @file typing.h
 * @date 2021-05-08
 */

#ifndef PIPES_TYPING_H
#define PIPES_TYPING_H

#include <expression/Expression.h>
#include <parser/Parser.h>
#include "typing/Program.h"

Program addInferredTypes(const Program &program);

Program tokenizeAndParseAndCheckTypes(const std::string &code);
Program tokenizeAndParseAndCheckTypes(const CodeFile &code);
Program tokenizeAndParseAndCheckTypes(const std::string &code, const Identifiers &identifiers);
Program tokenizeAndParseAndCheckTypes(const CodeFile &code, const Identifiers &identifiers);
Expression tokenizeAndParseAndCheckTypesModifyingIdentifiers(const CodeFile &code,
                                                             const std::string &identifier,
                                                             Identifiers &identifiers);


#endif //PIPES_TYPING_H
