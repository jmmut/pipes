/**
 * @file interfaces.h
 * @date 2021-06-03
 */

#ifndef PIPES_TYPEINTERFACES_H
#define PIPES_TYPEINTERFACES_H

#include <memory>

struct PipesType;

template<typename T>
class CopyableUniquePtr : public std::unique_ptr<T> {
public:
    CopyableUniquePtr() = default;
    CopyableUniquePtr(const CopyableUniquePtr &other) : inner{copy(other)} {}
    CopyableUniquePtr(CopyableUniquePtr &&other) noexcept = default;
    CopyableUniquePtr(nullptr_t other) {};

    CopyableUniquePtr &operator=(const CopyableUniquePtr &other) {
        if (&other != this) {
            inner = copy(other);
        }
        return *this;
    }
    CopyableUniquePtr &operator=(CopyableUniquePtr &&other) noexcept {
        if (&other != this) {
            inner = std::move(other);
        }
        return *this;
    }

private:
    std::unique_ptr<T> inner;
};

//using PtrPipesType = CopyableUniquePtr<PipesType>;
using PtrPipesType = std::unique_ptr<PipesType>;
using PipesTypes = std::vector<PtrPipesType>;
using PipesTypesRefs = std::vector<std::reference_wrapper<PtrPipesType>>;

inline PipesTypes getTypes(PtrPipesType type) {
    PipesTypes types;
    types.push_back(std::move(type));
    return types;
}

struct CallableInterface {
    virtual bool isCallable() const = 0;
    virtual const PipesType &getParameterType() const = 0;
    virtual const PipesType &getReturnedType() const = 0;
    virtual PtrPipesType &getParameterTypeNonConst() = 0;
    virtual PtrPipesType &getReturnedTypeNonConst() = 0;
};
struct Callable : virtual CallableInterface {
    bool isCallable() const override { return true; }
};
struct NonCallable : virtual CallableInterface {
    bool isCallable() const override { return false; }
    const PipesType &getParameterType() const override {
        throw randomize::exception::StackTracedException{
            "This function should not be called if the type is not callable"};
    }
    const PipesType &getReturnedType() const override {
        throw randomize::exception::StackTracedException{
            "This function should not be called if the type is not callable"};
    }
    PtrPipesType &getParameterTypeNonConst() override {
        throw randomize::exception::StackTracedException{
            "This function should not be called if the type is not callable"};
    }
    PtrPipesType &getReturnedTypeNonConst() override {
        throw randomize::exception::StackTracedException{
            "This function should not be called if the type is not callable"};
    }
};


struct HomogeneousIterableInterface {
    virtual bool isHomogeneous() const = 0;
    virtual const PipesType &getNestedType() const = 0;
    virtual PtrPipesType &getNestedTypeNonConst() = 0;
};

struct HeterogeneousIterableInterface {
    virtual bool isHeterogeneous() const = 0;
};


struct IterableInterface
        : virtual HomogeneousIterableInterface, virtual HeterogeneousIterableInterface {
    virtual bool isIterable() const = 0;
    virtual bool canBeHomogeneous() const = 0;
    virtual const PipesTypes &getNestedTypes() const = 0;
    virtual PipesTypes &getNestedTypesNonConst() = 0;
};
struct Iterable : virtual IterableInterface {
    bool isIterable() const override { return true; }
};


struct HomogeneousIterable : virtual HomogeneousIterableInterface, virtual Iterable {
    explicit HomogeneousIterable(PtrPipesType innerType)
        : innerType{getTypes(std::move(innerType))} {}
    bool isHomogeneous() const override { return true; }
    bool canBeHomogeneous() const override { return true; }

    const PipesTypes &getNestedTypes() const override { return innerType; }
    PipesTypes &getNestedTypesNonConst() override { return innerType; }
    const PipesType &getNestedType() const override { return innerType.at(0).operator*(); }
    PtrPipesType &getNestedTypeNonConst() override { return innerType.at(0); }

protected:
    PipesTypes innerType;
};
struct NonHomogeneousIterable : virtual HomogeneousIterableInterface, virtual Iterable {
    bool isHomogeneous() const override { return false; }
    const PipesType &getNestedType() const override {
        throw randomize::exception::StackTracedException{
                "This function should not be called if the type is not an homogeneous iterable"};
    }
    PtrPipesType &getNestedTypeNonConst() override {
        throw randomize::exception::StackTracedException{
                "This function should not be called if the type is not an homogeneous iterable"};
    }
};
struct HeterogeneousIterable : virtual HeterogeneousIterableInterface, virtual Iterable {
    explicit HeterogeneousIterable(PipesTypes types) : types(std::move(types)) {}
    bool isHeterogeneous() const override { return true; }
    const PipesTypes &getNestedTypes() const override { return types; }
    PipesTypes &getNestedTypesNonConst() override { return types; }

protected:
    PipesTypes types;
};
struct NonHeterogeneousIterable : virtual HeterogeneousIterableInterface, virtual Iterable {
    bool isHeterogeneous() const override { return false; }
};

struct NonIterable
        : virtual IterableInterface,
          virtual NonHomogeneousIterable, virtual NonHeterogeneousIterable{
    bool isIterable() const override { return false; }
    bool canBeHomogeneous() const override { return false; }
    const PipesTypes &getNestedTypes() const override {
        throw randomize::exception::StackTracedException{
                "This function should not be called if the type is not iterable"};
    }
    PipesTypes &getNestedTypesNonConst() override {
        throw randomize::exception::StackTracedException{
                "This function should not be called if the type is not iterable"};
    }
};



struct InferrableInterface {
    virtual bool isConcrete() const = 0;
    virtual bool isUnknown() const = 0;
};

struct Unknown : virtual InferrableInterface {
    bool isUnknown() const override { return true; }
    bool isConcrete() const override { return false; }
};

struct Generic : virtual InferrableInterface {
    bool isUnknown() const override { return false; }
    bool isConcrete() const override { return false; }
};

struct Known : virtual InferrableInterface {
    bool isUnknown() const override { return false; }
    bool isConcrete() const override { return true; }
};
struct Unresolvable : virtual InferrableInterface {
    bool isUnknown() const override { return true; }
    bool isConcrete() const override { return true; }
};

#endif //PIPES_TYPEINTERFACES_H
