/**
 * @file Exceptions.h
 * @date 2021-08-03
 */

#ifndef PIPES_TYPING_EXCEPTIONS_H
#define PIPES_TYPING_EXCEPTIONS_H

#include <exception>
#include <stdexcept>
#include "PipesType.h"

class TypeCheckException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
class IncompatibleTypesException : public std::runtime_error {
public:
    IncompatibleTypesException(const PipesType &first,
                               const std::string &firstPosition,
                               const std::string &firstName,
                               const PipesType &second,
                               const std::string &secondPosition,
                               const std::string &secondName,
                               std::string message = "");
    IncompatibleTypesException(const PipesType &first,
                               const PipesType &second,
                               const std::string &firstPosition,
                               const std::string &secondPosition,
                               std::string message = "");
    IncompatibleTypesException(const PipesType &first, const PipesType &second,
                               std::string message = "");
    IncompatibleTypesException(const PipesType &first, const std::string &firstName,
                               const PipesType &second, const std::string &secondName,
                               std::string message = "");
};

inline std::string addSpacesToMatch(const std::string &willGrowIfShorter,
                                    const std::string &thanThis) {

    auto sizeDifference = static_cast<long>(thanThis.size())
            - static_cast<long>(willGrowIfShorter.size());
    if (sizeDifference > 0) {
        return willGrowIfShorter + std::string(sizeDifference, ' ');
    } else {
        return willGrowIfShorter;
    }
}

inline auto semanticError(const CodeFile &input, PositionInCode positionInCode, std::string message = "") {
    return std::runtime_error{
            message + ".\nSemantic error at " + codePosToString(input, positionInCode)};
}

#endif //PIPES_TYPING_EXCEPTIONS_H
