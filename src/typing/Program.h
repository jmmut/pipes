
#ifndef PIPES_PROGRAM_H
#define PIPES_PROGRAM_H

#include <expression/Expression.h>
#include <parser/Parser.h>

struct Program : public CodeFile {
    const Expression expression;
    const Identifiers identifiers;
};

inline ParsedFile toParsedFile(const Program &program) {
    return ParsedFile{program, program.expression};
}

#endif //PIPES_PROGRAM_H
