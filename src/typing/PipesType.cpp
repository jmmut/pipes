/**
 * @file PipesType.cpp
 * @date 2021-05-08
 */

#include <stdexcept>
#include <utility>
#include <lexer/Lexer.h>
#include "PipesType.h"
#include "commonlibs/Walkers.h"
#include "expression/Expression.h"
#include "subtyping.h"

const std::string UnknownType::NAME = "UnknownType";
const std::string NothingType::NAME = "NothingType";
const std::string IntType::NAME = "IntType";
const std::string IntType::PARSING_NAME = "int64";
const std::string TupleType::NAME = "TupleType";
const std::string TupleType::PARSING_NAME = "iterable";
const std::string FunctionType::NAME = "FunctionType";
const std::string ArrayType::NAME = "ArrayType";
const std::string ArrayType::PARSING_NAME = "array";
const std::string UserType::NAME = "UserType";
const std::string UnresolvedType::NAME = "UnresolvedType";
const std::string TypeofType::NAME = "TypeofType";
const std::string TypeofType::PARSING_NAME = "typeof";
//static const PipesTypes NO_TYPES = {};
//static auto NOTHING_TYPE = NothingType{};
const PipesTypes EMPTY_TYPES;
PipesTypes NON_CONST_EMPTY_TYPES;


std::string addSpaceIfNotAnonymous(const std::string &name) {
    return name.empty()? std::string{} : name + " ";
}

std::string &PipesType::instanceName() {
    return instanceName_;
}

const std::string &PipesType::instanceName() const {
    return instanceName_;
}

std::string PipesType::to_string_with_names() const {
    return addSpaceIfNotAnonymous(this->instanceName()) + ::to_string(Token::TYPE_MARKER) +
            this->to_string_without_toplevel_name();
}

const PipesType &maybeUnwrapTypeof(const PipesType &maybeTypeof) {
    if (is<TypeofType>(maybeTypeof)) {
        return maybeTypeof.getNestedType();
    } else {
        return maybeTypeof;
    }
}
bool operator==(const PipesType &lhs, const PipesType &rhs) {
    if (lhs.getGenericName() == UnknownType::NAME
            and rhs.getGenericName() == UnknownType::NAME) {
        return true;
    } else {
        const auto &innerLhs = maybeUnwrapTypeof(lhs); // TODO remove this. Typeof(int) should not be == int
        const auto &innerRhs = maybeUnwrapTypeof(rhs);
        if (innerLhs.isCallable() and innerRhs.isCallable()) {
            return innerLhs.getGenericName() == innerRhs.getGenericName()
                    and innerLhs.getParameterType() == innerRhs.getParameterType()
                    and innerLhs.getReturnedType() == innerRhs.getReturnedType();
        } else if (innerLhs.isIterable() and innerRhs.isIterable()) {
            return innerLhs.getGenericName() == innerRhs.getGenericName()
                and innerLhs.getNestedTypes() == innerRhs.getNestedTypes();
        } else {
            return innerLhs.getGenericName() == innerRhs.getGenericName();
        }
    }
}

/////////////// UnknownType

PtrPipesType UnknownType::make_ptr(std::string name) {
    auto p = std::make_unique<UnknownType>();
    p->instanceName() = name;
    return p;
}

std::string UnknownType::to_string_without_toplevel_name() const {
    return "?";
}


/////////////// NothingType
PtrPipesType NothingType::make_ptr(std::string name) {
    auto p = std::make_unique<NothingType>();
    p->instanceName() = name;
    return p;
}

std::string NothingType::to_string_without_toplevel_name() const {
    return "Nothing";
}

/////////////// IntType
PtrPipesType IntType::make_ptr(std::string name) {
    auto p = std::make_unique<IntType>();
    p->instanceName() = name;
    return p;
}

std::string IntType::to_string_without_toplevel_name() const {
    return IntType::PARSING_NAME;
}

/////////////// TupleType

TupleType::TupleType(PipesTypes types, std::string name) : HeterogeneousIterable{std::move(types)} {
    instanceName() = std::move(name);
}

TupleType::TupleType(PtrPipesType type, std::string name)
        : HeterogeneousIterable{getTypes(std::move(type))} {
    // I found no easy way to do "PipesTypes{UnknownType::make_ptr()}"
//    types.push_back(std::move(type));
    instanceName() = std::move(name);
}

PtrPipesType TupleType::make_ptr(PipesTypes types, std::string name) {
    return std::make_unique<TupleType>(std::move(types), std::move(name));
}

PtrPipesType TupleType::make_ptr(PtrPipesType type, std::string name) {
    return std::make_unique<TupleType>(std::move(type), std::move(name));
}

PtrPipesType TupleType::copy() const {
    auto copied = PipesTypes{};
    for (const auto &type : types) {
        copied.push_back(type->copy());
    }
    return make_ptr(std::move(copied), instanceName());
}

std::string TupleType::to_string_without_toplevel_name() const {
    std::stringstream ss;
    ss << TupleType::PARSING_NAME << "(";
    for (const auto &type: types) {
        ss << type->to_string_with_names() << ", ";
    }
    if (not types.empty()) {
        ss.seekp(-2, ss.cur);
    }
    ss << ")";
    auto str = ss.str();
    str.pop_back();
    return str;
}

const PipesType &TupleType::getNestedType() const {
    if (types.size() == 1) {
        return types.at(0).operator*();
    } else {
        try {
            strictestType = getStrictestType(types);
            return strictestType.operator*();
        } catch (std::exception &e) {
            throw randomize::exception::StackTracedException{
                    "This function should not be called if the type is not an homogeneous iterable "
                    + parenthesize(this->to_string())
            };
        }
    }
}

PtrPipesType &TupleType::getNestedTypeNonConst() {
    if (types.size() == 1) {
        return types.at(0);
    } else {
        throw randomize::exception::StackTracedException{
                "It doesn't make sense to change the homogeneous iterable type of a tuple of more than 1 element "
                + parenthesize(std::to_string(types.size()))
        };
    }
}

const PipesTypes &TupleType::getNestedTypes() const {
    return types;
}

PipesTypes &TupleType::getNestedTypesNonConst() {
    return types;
}

bool TupleType::isConcrete() const {
    for (const auto &type: this->types) {
        if (not type->isConcrete()) {
            return false;
        }
    }
    return true;
}

bool TupleType::canBeHomogeneous() const {
    return areCompatible(types);
}


/////////////// FunctionType
FunctionType::FunctionType(PtrPipesType parameters_, PtrPipesType returns_, std::string name)
        : parameters{std::move(parameters_)}, returns{std::move(returns_)} {
    instanceName() = std::move(name);
}

PtrPipesType FunctionType::make_ptr(PtrPipesType parameters, PtrPipesType returns,
                                    std::string name) {
    return std::make_unique<FunctionType>(std::move(parameters), std::move(returns),
                                          std::move(name));
}

PtrPipesType FunctionType::copy() const {
    return make_ptr(parameters->copy(), returns->copy(), instanceName());
}

std::string FunctionType::to_string_without_toplevel_name() const {
    return ::to_string(Token::LAMBDA)
           + "(" + parameters->to_string_with_names()
           + " -> " + returns->to_string_with_names() + ")";
}

const PipesType &FunctionType::getParameterType() const {
    return *parameters;
}

const PipesType &FunctionType::getReturnedType() const {
    return *returns;
}

PtrPipesType &FunctionType::getParameterTypeNonConst() {
    return parameters;
}

PtrPipesType &FunctionType::getReturnedTypeNonConst() {
    return returns;
}

bool FunctionType::isConcrete() const {
    return parameters->isConcrete() and returns->isConcrete();
}

/////////////// ArrayType

ArrayType::ArrayType(PtrPipesType innerType, std::string name)
        : HomogeneousIterable{std::move(innerType)} {
    instanceName() = std::move(name);
}

std::string ArrayType::to_string_without_toplevel_name() const {
    std::stringstream ss;
    ss << ArrayType::PARSING_NAME << "(";
    ss << getNestedType();
    ss << ")";
    return ss.str();
}

PtrPipesType ArrayType::make_ptr(PtrPipesType innerType, std::string name) {
    return std::make_unique<ArrayType>(std::move(innerType), std::move(name));
}

PtrPipesType ArrayType::copy() const {
    return make_ptr(getNestedType().copy(), instanceName());
}

bool ArrayType::isConcrete() const {
    return getNestedType().isConcrete();
}


/////////////// UserType

UserType::UserType(PipesTypes types, std::string name)
        : HeterogeneousIterable{std::move(types)} {
    instanceName() = std::move(name);
}

PtrPipesType UserType::make_ptr(PipesTypes types, std::string name) {
    return std::make_unique<UserType>(std::move(types), std::move(name));
}

PtrPipesType UserType::copy() const {
    PipesTypes copies;
    for (const auto &type : types) {
        copies.push_back(type->copy());
    }
    return make_ptr(std::move(copies), instanceName());
}

std::string UserType::to_string_without_toplevel_name() const {
    return ::to_string(Token::STRUCT) + randomize::utils::to_string(types, "(", ", ", ")");
}

const PipesType &UserType::getNestedType() const {
    if (types.size() == 1) {
        return types.at(0).operator*();
    } else {
        try {
            strictestType = getStrictestType(types);
            return strictestType.operator*();
        } catch (std::exception &e) {
            throw randomize::exception::StackTracedException{
                    "This function should not be called if the type is not an homogeneous iterable "
                    + parenthesize(this->to_string())
            };
        }
    }
}

PtrPipesType &UserType::getNestedTypeNonConst() {
    if (types.size() == 1) {
        return types.at(0);
    } else {
        throw randomize::exception::StackTracedException{
                "It doesn't make sense to change the homogeneous iterable type of a tuple of more than 1 element "
                + parenthesize(std::to_string(types.size()))
        };
    }
}

std::optional<int> UserType::getFieldIndex(const std::string &fieldName) const {
    int i = 0;
    for (const auto &nestedType : types) {
        if (nestedType->instanceName() == fieldName) {
            return {i};
        }
        ++i;
    }
    return {};
}


bool UserType::isConcrete() const {
    for (const auto &type: this->types) {
        if (not type->isConcrete()) {
            return false;
        }
    }
    return true;
}

bool UserType::canBeHomogeneous() const {
    return areCompatible(types);
}


/////////////// UnresolvedType

UnresolvedType::UnresolvedType(std::string name, std::string instanceName_)
        : name{std::move(name)} {
    instanceName() = std::move(instanceName_);
}

UnresolvedType::UnresolvedType(std::string name) : UnresolvedType{name, name} {
}

PtrPipesType UnresolvedType::make_ptr(std::string name, std::string instanceName) {
    return std::make_unique<UnresolvedType>(std::move(name), instanceName);
}
PtrPipesType UnresolvedType::make_ptr(std::string name) {
    return make_ptr(name, name);
}


PtrPipesType UnresolvedType::copy() const {
    return make_ptr(name, instanceName());
}

std::string UnresolvedType::to_string_without_toplevel_name() const {
    return "unresolved(" + name + ")";
}

/////////////// TypeofType

TypeofType::TypeofType(Expression innerExpression, std::string name)
        : HomogeneousIterable{innerExpression.pipesType()->copy()},
        innerExpression{std::make_unique<Expression>(innerExpression)} {
    instanceName() = name;
}

PtrPipesType TypeofType::make_ptr(Expression innerExpression, std::string name) {
    return std::make_unique<TypeofType>(std::move(innerExpression), std::move(name));
}

PtrPipesType TypeofType::copy() const {
    updateExpression();
    return make_ptr(Expression{innerExpression.operator*()}, instanceName());
}

std::string TypeofType::to_string_without_toplevel_name() const {
    updateExpression();
    return PARSING_NAME + parenthesize(::to_string_with_types(innerExpression.operator*()));
}

const Expression &TypeofType::getInnerExpression() const {
    updateExpression();
    return innerExpression.operator*();
}

bool TypeofType::isConcrete() const {
    return getNestedType().isConcrete();
}

/**
 * as we need to hold the type in the PipesTypes list to make getNestedTypes work,
 * the type in the expression is a non-authoritative copy, as the master copy in the PipesTypes
 * might have been updated through getNestedtypesNonConst and getNestedTypeNonConst.
 * So everytime the the expression is needed, its type has to be updated.
 */
void TypeofType::updateExpression() const {
    innerExpression->pipesType() = getNestedType().copy();
}

