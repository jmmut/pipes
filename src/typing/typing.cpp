/**
 * @file typing.cpp
 * @date 2021-05-08
 */

#include <typing/typing.h>
#include <typing/subtyping.h>
#include <set>
#include <utility>
#include <commonlibs/Walkers.h>
#include <commonlibs/log.h>
#include "TypingExceptions.h"
#include "typing/unify/unify.h"
#include "typing/Program.h"
#include "typing/typer/factory/TyperFactory.h"
#include "typing/typer/impl/recursing/RecursiveTyper.h"


Program addInferredTypes(Expression &typed, const CodeFile &code, const Identifiers &identifiers,
                         const std::map<std::string, PipesTypes> &identifiersBeingProcessed) {
    auto typer = RecursiveTyper{typed, code, identifiers, identifiersBeingProcessed};
    typer.addTypes();
    return Program{code, typed, identifiers};
}


Program addInferredTypes(const Program &program) {
    auto typed = program.expression;
    auto &identifiers = program.identifiers;
    const CodeFile &code = program;
    return addInferredTypes(typed, code, identifiers, {});
}

Program tokenizeAndParseAndCheckTypes(const CodeFile &code) {
    Identifiers identifiers{};
    return tokenizeAndParseAndCheckTypes(code, identifiers);
}

void checkNoUnkownsInMain(const CodeFile &code, const Program &typed) {
    recursivelyDo(typed.expression, [](const Expression &expr){}, [&code](const Expression &expr) {
        recursivelyDo(expr.pipesType().operator*(), [](const PipesType &type) {},
                      [&code, &expr](const PipesType &type) {
                          if (is<UnknownType>(type)) {
                              if (randomize::log::log_level <= randomize::log::WARN_LEVEL) {
                                  std::cout << "Warning: could not infer type for expression at "
                                            << codePosToString(code, expr.positionInCode).c_str()
                                            << "\nIncomplete type: "
                                            << to_pretty_string_with_types(expr, 0)
                                            << std::endl;
                              }
                          }
                      });
    });
}

Program tokenizeAndParseAndCheckTypes(const std::string &text, const Identifiers &identifiers) {
    return tokenizeAndParseAndCheckTypes(buildCode(text), identifiers);
}

Program tokenizeAndParseAndCheckTypes(const CodeFile &code, const Identifiers &identifiers) {
    auto identifiersCopy = identifiers;
    auto expression = tokenizeAndParse(code, identifiersCopy);
    auto program = Program{code, expression, identifiersCopy};
    auto typed = addInferredTypes(program);

    checkNoUnkownsInMain(code, typed);
//    checkNoUnknownStructsInIdentifiers
    return typed;
}

Program tokenizeAndParseAndCheckTypes(const std::string &text) {
    return tokenizeAndParseAndCheckTypes(buildCode(text));
}

Expression tokenizeAndParseAndCheckTypesModifyingIdentifiers(const CodeFile &code,
                                                             const std::string &identifier,
                                                             Identifiers &identifiers) {
    auto expression = tokenizeAndParse(code, identifiers);
    auto typed = addInferredTypes(expression, code, identifiers, {});
    return typed.expression;
}

