/**
 * @file PipesType.h
 * @date 2021-05-08
 */

#ifndef PIPES_PIPESTYPE_H
#define PIPES_PIPESTYPE_H

#include <utility>
#include <vector>
#include <memory>
#include <optional>
#include <functional>
#include <commonlibs/StackTracedException.h>
#include <typing/typeInterfaces.h>
#include <lexer/pipesCodeFile.h>
#include <commonlibs/RAII.h>

struct PipesType
        : virtual CallableInterface, virtual IterableInterface, virtual InferrableInterface {
    virtual ~PipesType() = default;
    virtual PtrPipesType copy() const = 0;
    virtual PtrPipesType copy(std::string withInstanceName) const {
        auto copied = copy();
        copied->instanceName() = std::move(withInstanceName);
        return copied;
    };
    virtual std::string to_string() const { return this->to_string_with_names(); };
    virtual std::string to_string_with_names() const;
    virtual std::string to_string_without_toplevel_name() const = 0;
    virtual const std::string &getGenericName() const = 0;
    virtual std::string &instanceName();
    virtual const std::string &instanceName() const;
private:
    std::string instanceName_;
};

inline bool isAnonymous(const std::string &name) {
    return name.empty();
}
inline bool isAnonymous(const PipesType &type) {
    return isAnonymous(type.instanceName());
}

template<typename T>
bool is(const PipesType &type) {
    return type.getGenericName() == T::NAME;
}

template<typename T>
bool is(const PtrPipesType &type) {
    return is<T>(type.operator*());
}

template<typename T>
bool are(const PipesType &firstType, const PipesType &secondType) {
    return is<T>(firstType) and is<T>(secondType);
}

template<typename T>
bool are(const PtrPipesType &firstType, const PtrPipesType &secondType) {
    return are<T>(firstType.operator*(), secondType.operator*());
}

template<typename T, typename U>
bool are(const PipesType &firstType, const PipesType &secondType) {
    return (is<T>(firstType) and is<U>(secondType))
            or (is<U>(firstType) and is<T>(secondType));
}

template<typename T, typename U>
bool are(const PtrPipesType &firstType, const PtrPipesType &secondType) {
    return are<T, U>(firstType.operator*(), secondType.operator*());
}
template<typename T, typename U>
bool areAny(const PipesType &firstType, const PipesType &secondType) {
    return (is<T>(firstType) or is<U>(firstType))
            and (is<T>(secondType) or is<U>(secondType));
}

template<typename T, typename U>
bool areAny(const PtrPipesType &firstType, const PtrPipesType &secondType) {
    return areAny<T, U>(firstType.operator*(), secondType.operator*());
}

class UnknownType : public PipesType, public NonCallable, public NonIterable, public Unknown {
public:
    static const std::string NAME;
    static PtrPipesType make_ptr(std::string name);
    PtrPipesType copy() const override { return make_ptr(instanceName()); }

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;
};

class NothingType : public PipesType, public NonCallable, public NonIterable, public Known {
public:
    static const std::string NAME;
    static PtrPipesType make_ptr(std::string name);
    PtrPipesType copy() const override { return make_ptr(instanceName()); }

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;
};

class IntType : public PipesType, public NonCallable, public NonIterable, public Known {
public:
    static const std::string NAME;
    static const std::string PARSING_NAME;
    static PtrPipesType make_ptr(std::string name);
    PtrPipesType copy() const override { return make_ptr(instanceName()); }

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;
};

class TupleType
        : public PipesType, public NonCallable, public HeterogeneousIterable,
        public NonHomogeneousIterable, public Generic {
public:
    static const std::string NAME;
    static const std::string PARSING_NAME;

    explicit TupleType(PipesTypes types, std::string name);
    explicit TupleType(PtrPipesType type, std::string name);
    ~TupleType() override = default;
    static PtrPipesType make_ptr(PipesTypes types, std::string name);
    static PtrPipesType make_ptr(PtrPipesType type, std::string name);
    PtrPipesType copy() const override;

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;

    const PipesType &getNestedType() const override;
    PtrPipesType &getNestedTypeNonConst() override;

    const PipesTypes &getNestedTypes() const override;
    PipesTypes &getNestedTypesNonConst() override;

    bool canBeHomogeneous() const override;

    bool isConcrete() const override;

private:
    mutable PtrPipesType strictestType;
};

//    static PipesTypes make_types(PtrPipesType type) {
//        auto types = PipesTypes{};
//        types.push_back(std::move(type));
//        return types;
//    }
class FunctionType : public PipesType, public Callable, public NonIterable, public Generic {
public:
    static const std::string NAME;
    FunctionType(PtrPipesType parameters, PtrPipesType returns, std::string name);
    ~FunctionType() override = default;
    static PtrPipesType make_ptr(PtrPipesType parameters, PtrPipesType returns, std::string name);
    PtrPipesType copy() const override;
    const std::string &getGenericName() const override { return NAME; }

    std::string to_string_without_toplevel_name() const override;

    const PipesType &getParameterType() const override;
    const PipesType &getReturnedType() const override;
    PtrPipesType &getParameterTypeNonConst() override;
    PtrPipesType &getReturnedTypeNonConst() override;

    bool isConcrete() const override;
private:
    PtrPipesType parameters;
    PtrPipesType returns;
};

class ArrayType : public PipesType, public NonCallable, public HomogeneousIterable,
        public NonHeterogeneousIterable, public Generic {
public:
    static const std::string NAME;
    static const std::string PARSING_NAME;
    explicit ArrayType(PtrPipesType elements, std::string name);
    ~ArrayType() override = default;
    static PtrPipesType make_ptr(PtrPipesType element, std::string name);
    PtrPipesType copy() const override;

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;

    bool isConcrete() const override;
};

class UserType : public PipesType, public NonCallable, public NonHomogeneousIterable,
        public HeterogeneousIterable, public Generic {
public:
    static const std::string NAME;
    explicit UserType(PipesTypes types, std::string name);
    UserType(const UserType &other) = delete;
    ~UserType() override = default;
    static PtrPipesType make_ptr(PipesTypes types, std::string name);
    PtrPipesType copy() const override;

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;

    bool isConcrete() const override;
    bool canBeHomogeneous() const override;

    const PipesType &getNestedType() const override;
    PtrPipesType &getNestedTypeNonConst() override;

    std::optional<int> getFieldIndex(const std::string &fieldName) const;
private:
    mutable PtrPipesType strictestType;
};

class UnresolvedType : public PipesType, public NonCallable, public NonIterable, public Unresolvable {
public:
    static const std::string NAME;
    explicit UnresolvedType(std::string name, std::string instanceName);
    explicit UnresolvedType(std::string name);
    ~UnresolvedType() override = default;
    static PtrPipesType make_ptr(std::string name, std::string instanceName);
    static PtrPipesType make_ptr(std::string name);
    PtrPipesType copy() const override;

    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;
private:
    std::string name;
};


struct Expression;
using PtrExpression = std::unique_ptr<Expression>;

class TypeofType : public PipesType, public NonCallable, public HomogeneousIterable,
        public NonHeterogeneousIterable, public Generic {
public:
    static const std::string NAME;
    static const std::string PARSING_NAME;
    explicit TypeofType(Expression innerExpression, std::string name);
    ~TypeofType() override = default;
    static PtrPipesType make_ptr(Expression innerExpression, std::string name);
    PtrPipesType copy() const override;
    const std::string &getGenericName() const override { return NAME; }
    std::string to_string_without_toplevel_name() const override;

    bool isConcrete() const override;

    const Expression &getInnerExpression() const;
private:
    void updateExpression() const;
    PtrExpression innerExpression;
};

inline PtrPipesType copy(const PtrPipesType &other) {
    if (other == nullptr) {
        return nullptr;
    } else {
        return other->copy();
    }
}

inline PipesTypes copy(const PipesTypes &other) {
    PipesTypes types;
    for (const auto &type: other) {
        types.emplace_back(type->copy());
    }
    return types;
}

inline std::string to_string(const PipesType &type) {
    return type.to_string();
}
inline std::string to_string(const PtrPipesType &type) {
    return type->to_string();
}

bool operator==(const PipesType &lhs, const PipesType &rhs);

inline bool operator!=(const PipesType &lhs, const PipesType &rhs) {
    return !(rhs == lhs);
}

inline std::ostream &operator<<(std::ostream &out, const PtrPipesType &type) {
    return out << type->to_string();
}
inline std::ostream &operator<<(std::ostream &out, const PipesType &type) {
    return out << type.to_string();
}


inline bool operator==(const PipesTypes &lhs, const PipesTypes &rhs) {
    if ((lhs.size() == 1 and is<UnknownType>(lhs[0]))
            and (rhs.size() == 1 and is<UnknownType>(rhs[0]))) {
        return true;
    } else {
        if (lhs.size() != rhs.size()) {
            return false;
        } else {
            for (size_t i = 0; i < lhs.size(); ++i) {
                if (lhs[i].operator*() != rhs[i].operator*()) {
                    return false;
                }
            }
            return true;
        }
    }
}

const PipesType &maybeUnwrapTypeof(const PipesType &maybeTypeof);

inline void recursivelyRewrite(PtrPipesType &type,
                               const std::function<void(PtrPipesType &)> &preAction,
                               const std::function<void(PtrPipesType &)> &postAction) {
    RAII actioner([&]() {preAction(type);},
                  [&]() {postAction(type);});
    if (type->isIterable()) {
        if (type->isHeterogeneous()) {
            for (auto &item: type->getNestedTypesNonConst()) {
                recursivelyRewrite(item, preAction, postAction);
            }
        } else if (type->isHomogeneous()) {
            recursivelyRewrite(type->getNestedTypeNonConst(), preAction, postAction);
        }
    } else if (type->isCallable()) {
        recursivelyRewrite(type->getParameterTypeNonConst(), preAction, postAction);
        recursivelyRewrite(type->getReturnedTypeNonConst(), preAction, postAction);
    }
}
inline void recursivelyRewriteWithParent(
        PtrPipesType &type,
        const PtrPipesType &yourParent,
        const std::function<void(PtrPipesType &you, const PtrPipesType &yourParent)> &preAction,
        const std::function<void(PtrPipesType &you, const PtrPipesType &yourParent)> &postAction) {
    RAII actioner([&]() {preAction(type, yourParent);},
                  [&]() {postAction(type, yourParent);});
    if (type->isIterable()) {
        if (type->isHeterogeneous()) {
            for (auto &item: type->getNestedTypesNonConst()) {
                recursivelyRewriteWithParent(item, type, preAction, postAction);
            }
        } else if (type->isHomogeneous()) {
            recursivelyRewriteWithParent(type->getNestedTypeNonConst(), type, preAction, postAction);
        }
    } else if (type->isCallable()) {
        recursivelyRewriteWithParent(type->getParameterTypeNonConst(), type, preAction, postAction);
        recursivelyRewriteWithParent(type->getReturnedTypeNonConst(), type, preAction, postAction);
    }
}

inline void recursivelyDo(const PipesType &type,
                          const std::function<void(const PipesType &)> &preAction,
                          const std::function<void(const PipesType &)> &postAction) {
    RAII actioner([&]() {preAction(type);},
                  [&]() {postAction(type);});
    if (type.isIterable()) {
        if (type.isHeterogeneous()) {
            for (auto &item: type.getNestedTypes()) {
                recursivelyDo(item.operator*(), preAction, postAction);
            }
        } else if (type.isHomogeneous()) {
            recursivelyDo(type.getNestedType(), preAction, postAction);
        }
    } else if (type.isCallable()) {
        recursivelyDo(type.getParameterType(), preAction, postAction);
        recursivelyDo(type.getReturnedType(), preAction, postAction);
    }
}

#endif //PIPES_PIPESTYPE_H
