
#ifndef PIPES_UNIFY_H
#define PIPES_UNIFY_H


#include <utility>

#include "expression/Expression.h"
#include "typing/TypingExceptions.h"
#include "expression/ExpressionString.h"
#include "expression/ExpressionType.h"

struct PartialExceptionMessage {
    std::string message;
    std::string firstName = "First type:  ";
    std::string secondName= "Second type: ";
    std::string getMessage(const Expression &expression) {
        auto exprType = expression.getType() == ExpressionType::UNSET ? std::string{}
                                                             : to_string(expression.getType()) + " ";
        auto name = isAnonymous(expression.pipesType().operator*())
                    ? std::string{}
                    : quote(expression.pipesType()->instanceName()) + " ";
        auto extraMessage = message.empty() ? std::string{} : ". " + message;
        return exprType + "expression " + name + "has incompatible types" + extraMessage;
    }
};

bool isCompletelyDefined(const PtrPipesType &ptrType);

class Unifier {
public:
    Unifier(const Expression &expression, const CodeFile &code)
            : expression(expression), secondExpression{noSecondExpression},
            code(code), changed{false} {
    }

    Unifier(const Expression &expression, const Expression &secondExpression, const CodeFile &code)
            : expression(expression), secondExpression{secondExpression},
            code(code), changed{false} {
    }

    /** assumes you want to use the expression.pipesType() as first PipesType, and replace it. */
    static void unify(Expression &expression, const PipesType &second, const CodeFile &code,
               PartialExceptionMessage message = {}) {
        auto unifier = Unifier{expression, code};
        expression.pipesType() = unifier.unify(expression.pipesType().operator*(), second);
    }

    bool wasChanged() {
        return changed;
    }

    PtrPipesType unify(const PipesType &first, const PipesType &second,
                       PartialExceptionMessage message = {}) {
        try {
            return unifyWithoutCatch(first, second, message, 0);
        } catch (randomize::exception::StackTracedException &e) {
            throw std::move(e);
        } catch (std::exception &e) {
            throw IncompatibleTypesException{
                    first, message.firstName, second, message.secondName,
                    "Compilation error: " + message.getMessage(expression)
                    + (strlen(e.what()) > 0? " " + parenthesize(e.what()) : std::string{}) + " at "
                    + codePosToString(code, expression.position())
                    + (secondExpression.getType() == ExpressionType::UNSET? std::string{}
                    : "\nand at " + codePosToString(code, secondExpression.position()))};
        }
    }

private:
    PtrPipesType unifyWithoutCatch(const PipesType &first, const PipesType &second,
                                   PartialExceptionMessage message = {}, long nesting = 0) {
        int max_levels = 20;
        if (++nesting > max_levels) {
            throw std::runtime_error{"This type is more than " + std::to_string(max_levels)
                                     + " nested levels deep. The compiler assumed it's an "
                                       "anonymous recursive type and won't compile it. You can use "
                                       "a named recursive type or mark it as :? to keep it untyped"};
        }
//        static int typeofsUnwrapped = 0;
        auto name = unifyNames(first, second);
        if (is<UnknownType>(second)) {
            return first.copy(name);
        } else if (is<UnknownType>(first)) {
            auto anonSecond = anonymize(second.copy(name));
            if (not is<UnknownType>(anonSecond)) {
                setChanged();
            }
            return anonSecond;
        } else if (are<TypeofType>(first, second)) {
            return unifyTypeof(dynamic_cast<const TypeofType &>(first),
                               dynamic_cast<const TypeofType &>(second).getNestedType(),
                               name,
                               nesting);
        } else if (is<TypeofType>(first)) {
            return unifyTypeof(dynamic_cast<const TypeofType &>(first),
                               second, name, nesting);
        } else if (is<TypeofType>(second)) {
            return unifyWithoutCatch(first,
                                     (dynamic_cast<const TypeofType &>(second)).getNestedType(),
                                     message, nesting);
        } else if (first.isCallable() and second.isCallable()) {
            return FunctionType::make_ptr(
                    unifyWithoutCatch(first.getParameterType(), second.getParameterType(),
                                      message, nesting),
                    unifyWithoutCatch(first.getReturnedType(), second.getReturnedType(),
                                      message, nesting),
                    name);
        } else if (first.isIterable() and second.isIterable()) {
            return unifyIterable(first, second, nesting);
        } else if (not first.isCallable() and not first.isIterable()
                and not second.isCallable() and not second.isIterable()
                and first.getGenericName() == second.getGenericName()) {
            return first.copy(name);
        } else {
            throw std::runtime_error{""};
        }
    }

    PtrPipesType anonymize(PtrPipesType type) {
        auto topLevelName = type->instanceName();
        recursivelyRewriteWithParent(type, nullptr, [](PtrPipesType &innerType, const PtrPipesType &parent) {
            auto name = innerType->instanceName();
            while (is<TypeofType>(innerType)) {
                innerType = dynamic_cast<TypeofType &>(innerType.operator*())
                        .getInnerExpression().pipesType()->copy();

                // if this typeof is a struct field, keep the outer name,
                // not the name of the innerExpression inside the typeof
                innerType->instanceName() = name;
            }
            if (parent == nullptr or not is<UserType>(parent)) {
                // don't anonymize struct fields
                innerType->instanceName() = "";
            }
        }, [](PtrPipesType &innerType, const PtrPipesType &parent) {});
        type->instanceName() = topLevelName;
        return type;
    }

    PtrPipesType unifyTypeof(const TypeofType &typeofFirst, const PipesType &second,
                              std::string &name, long nesting) {
        auto unifiedType = unifyWithoutCatch(typeofFirst.getNestedType(), second, {}, nesting);
//        if (isCompletelyDefined(unifiedType)) {
//            setChanged();
//            unifiedType->instanceName() = name;
//            return unifiedType;
//        } else {
            auto expressionCopy = typeofFirst.getInnerExpression();
            expressionCopy.pipesType() = std::move(unifiedType);
            return TypeofType::make_ptr(expressionCopy, name);
//        }
    }

    void setChanged() {
        changed = true;
    }

    auto compilerBug(std::string message) {
        return randomize::exception::StackTracedException{
                "Compiler bug" + (message.empty()? "." : ": " + message)
                + ".\nDetected at " + codePosToString(code, expression.position())};
    }

    PtrPipesType unifyIterable(const PipesType &first, const PipesType &second, long nesting) {
        auto name = unifyNames(first, second);
        PipesTypes types;
        if (bool sameSize =
                    first.getNestedTypes().size() == second.getNestedTypes().size();
                sameSize) {
            for (size_t i = 0; i < first.getNestedTypes().size(); ++i) {
                types.push_back(unifyWithoutCatch(first.getNestedTypes().at(i).operator*(),
                                                  second.getNestedTypes().at(i).operator*(),
                                                  {}, nesting));
            }
            if (first.isHomogeneous()) {
                return ArrayType::make_ptr(types.at(0)->copy(), name);
            } else if (is<UserType>(first)) {
                if (is<UserType>(second)) {
                    for (size_t i = 0; i < first.getNestedTypes().size(); ++i) {
                        const auto &firstField = first.getNestedTypes().at(i)->instanceName();
                        const auto &secondField = second.getNestedTypes().at(i)->instanceName();
                        if (not isAnonymous(secondField)) {
                            if (not isAnonymous(firstField)) {
                                if (firstField != secondField) {
                                    throw std::runtime_error{
                                        "structs have different field names or different ordering"};
                                }
                            } else {
                                // field names need to be unified in structs at least
                                types.at(i)->instanceName() = secondField;
                            }
                        }
                    }
                }
                return UserType::make_ptr(std::move(types), name);
            } else if (is<TupleType>(first)) {
                return TupleType::make_ptr(std::move(types), name);
            } else {
                throw compilerBug("unhandled type " + quote(to_string(first)));
            }
        } else if (first.isHomogeneous()) {
            const auto &firstElem = first.getNestedType();
            for (const auto &secondElem: second.getNestedTypes()) {
                types.push_back(unifyWithoutCatch(firstElem, secondElem.operator*(),
                                                  {}, nesting));
            }
            //TODO forbid if second is a struct?
            return ArrayType::make_ptr(getStrictestType(types, nesting), name);
        } else if (first.isHeterogeneous()) {
            auto firstCount = first.getNestedTypes().size();
            auto secondCount = second.getNestedTypes().size();
            auto ctor = is<UserType>(first) ?
                        [](PipesTypes types, std::string name) {
                            return UserType::make_ptr(std::move(types), std::move(name));
                        }
                        : [](PipesTypes types, std::string name) {
                            return TupleType::make_ptr(std::move(types), std::move(name));
                        };
            if (firstCount == 1) {
                if (is<UnknownType>(first.getNestedTypes().at(0))) {
                    setChanged();
                    auto typesToOverwriteSingleUnknown = copy(second.getNestedTypes());
                    for (auto &item: typesToOverwriteSingleUnknown) {
                        // note: we could prioritize inner names from second tuple because first is a tuple of a single unknown
                        // but right now we have disabled name propagation (for all except structs) due to issues with typeof
                        if (is<UserType>(second) and is<UserType>(first)) {
                            ; // keep names from second
                        } else {
                            item->instanceName() = first.getNestedType().instanceName();
                        }
                    }
                    return ctor(std::move(typesToOverwriteSingleUnknown), name);
                } else {
                    auto firstElem = first.getNestedTypes().at(0)->copy();
                    for (const auto &secondElem: second.getNestedTypes()) {
                        firstElem = unifyWithoutCatch(firstElem.operator*(), secondElem.operator*(),
                                                      {}, nesting);
                        firstElem->instanceName() = unifyNames(firstElem.operator*(),
                                                               secondElem.operator*());
                    }
                    return ctor(getTypes(std::move(firstElem)), name);
                }
            } else if (secondCount == 1) {
                if (is<UnknownType>(second.getNestedTypes().at(0))) {
                    auto copied = copy(first.getNestedTypes());
                    for (auto &item: copied) {
                        // TODO: can this make a struct with all fields named the same?
                        item->instanceName() = unifyNames(item.operator*(),
                                                          second.getNestedTypes().at(
                                                                  0).operator*());
                    }
                    return ctor(std::move(copied), name);
                } else {
                    auto secondElem = second.getNestedTypes().at(0)->copy();
                    for (const auto &firstElem: first.getNestedTypes()) {
                        types.push_back(unifyWithoutCatch(firstElem.operator*(),
                                                          secondElem.operator*(),
                                                          {}, nesting));
                    }
                    return ctor(std::move(types), name);
                }
            }
        }
        throw std::runtime_error{""};
    }
    
    PtrPipesType getStrictestType(const PipesTypes &types, long nesting) {
        if (types.empty()) {
            return UnknownType::make_ptr("");
        } else {
            auto strictest = types.at(0)->copy();
            for (const auto &type: types) {
                strictest = unifyWithoutCatch(strictest.operator*(), type.operator*(), {}, nesting);
            }
            return strictest;
        }
    }

    std::string unifyNames(const PipesType &first, const PipesType &second) {
//        if (isAnonymous(first)) {
//            return second.instanceName();
//        } else {
            return first.instanceName();
//        }
    }


private:
    Expression noSecondExpression;
    const Expression &expression;
    const Expression &secondExpression;
    const CodeFile &code;
    bool changed;
};

// TODO/optimization: cache this in PipesType
inline bool isCompletelyDefined(const PtrPipesType &ptrType) {
    bool everything_is_defined = true;
    recursivelyDo(*ptrType, [&](const PipesType &type) {
        if (is<UnknownType>(type)) {
            everything_is_defined = false; //TODO/optimization: allow early stop in recursive walk
        }
    }, [](const PipesType &type) {

    });
    return everything_is_defined;
}

inline bool containsTypeof(const PtrPipesType &ptrType) {
    bool containsTypeof = false;
    recursivelyDo(*ptrType, [&](const PipesType &type) {
        if (is<TypeofType>(type)) {
            containsTypeof = true; //TODO/optimization: allow early stop in recursive walk
        }
    }, [](const PipesType &type) {

    });
    return containsTypeof;
}


/**
 * You might want to unify(stricter, comparedToThis) before calling this function, as none of
 * 'function :? -> :int64' and 'function :int64 -> :?' is more restrictive than the other. However,
 * after unifying, the first will be stricter.
 * //TODO/optimize: then we are calling twice to unify(stricter, comparedToThis), outside and in here
 */
inline bool isMoreRestrictiveTypeAndCompatible(const PipesType &stricter, const PipesType &comparedToThis) {
    Unifier unifier = Unifier{Expression{}, CodeFile{}};
    auto unified = unifier.unify(stricter, comparedToThis);
    bool stricterChanged = unifier.wasChanged();
    unified = unifier.unify(comparedToThis, stricter);
    bool comparedChanged = unifier.wasChanged();
    return not stricterChanged and comparedChanged;

}

inline bool isEqualOrMoreRestrictiveTypeAndCompatible(const PipesType &equalOrStricter,
                                               const PipesType &comparedToThis) {
    if (equalOrStricter == comparedToThis) {
        return true;
    } else {
        return isMoreRestrictiveTypeAndCompatible(equalOrStricter, comparedToThis);
    }
}

#endif //PIPES_UNIFY_H
