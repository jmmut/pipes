/**
 * @file subtyping.h
 * @date 2021-05-23
 */

#ifndef PIPES_SUBTYPING_H
#define PIPES_SUBTYPING_H


#include "TypingExceptions.h"
#include "typing/unify/unify.h"

inline bool isSubType(const PipesType &child, const PipesType &parent);

inline bool isMoreRestrictiveTypeAndCompatible(const PipesType &stricter,
                                               const PipesType &comparedToThis);

bool areSubtype(PipesTypes &childs, PipesTypes &parents);

inline bool areCompatible(const PipesType &type, const PipesType &otherType) {
    return isSubType(type, otherType) or isSubType(otherType, type);
}
inline bool areCompatible(const PtrPipesType &type, const PtrPipesType &otherType) {
    return areCompatible(type.operator*(), otherType.operator*());
}

inline PtrPipesType getStrictestType(const PipesTypes &types) {
    auto strictest = UnknownType::make_ptr("");
    for (const auto &type : types) {
        if (not areCompatible(type, strictest)) {
            throw IncompatibleTypesException{type.operator*(), strictest.operator*()};
        }
        if (isMoreRestrictiveTypeAndCompatible(type.operator*(), strictest.operator*())) {
            strictest = type->copy();
        }
    }
    return strictest;
}

inline bool areCompatible(const PipesTypes &types) {
    auto strictest = UnknownType::make_ptr("");
    for (const auto &type : types) {
        if (not areCompatible(type, strictest)) {
            return false;
        }
        if (isMoreRestrictiveTypeAndCompatible(type.operator*(), strictest.operator*())) {
            strictest = type->copy();
        }
    }
    return true;
}

inline PtrPipesType getStrictestTypeOrUnknownIfIncompatible(const PipesTypes &types) {
    auto strictest = UnknownType::make_ptr("");
    for (const auto &type : types) {
        if (not areCompatible(type, strictest)) {
            return UnknownType::make_ptr("");
        }
        if (isMoreRestrictiveTypeAndCompatible(type.operator*(), strictest.operator*())) {
            strictest = type->copy();
        }
    }
    return strictest;
}

inline PtrPipesType getStrictestTypeInIterable(const IterableInterface &iterable) {
    return getStrictestType(iterable.getNestedTypes());
}

inline bool areSubtype(const PipesTypes &childs, const PipesTypes &parents) {
    bool sameSize = childs.size() == parents.size();
    if (sameSize) {
        for (size_t i = 0; i < childs.size(); ++i) {
            if (not isSubType(
                    childs.at(i).operator*(),
                    parents.at(i).operator*())) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

/**
 * I'm wondering, maybe
 * List<Integer> is a subtype of List<Number>
 * as long as we don't add Numbers to the List<Integer> and keep it as List<Integer>.
 * I.e. we can add Numbers and rebrand it as List<Number>
 */
inline bool isArraySubtype(const PipesType &child, const PipesType &parent) {
    if (not (child.getGenericName() == ArrayType::NAME
            and parent.getGenericName() == ArrayType::NAME)) {
        throw randomize::exception::StackTracedException{
                "logic error: should be called only with two arrays. Called with "
                        + quote(to_string(child) + " and " + quote(to_string(parent)))};
    }
    return isSubType(child.getNestedType(), parent.getNestedType());
}

inline bool isTupleSubtype(const PipesType &child, const PipesType &parent) {
    if (not (child.getGenericName() == TupleType::NAME
            and parent.getGenericName() == TupleType::NAME)) {
        throw randomize::exception::StackTracedException{
                "logic error: should be called only with two tuples. Called with "
                        + quote(to_string(child) + " and " + quote(to_string(parent)))};
    }
    if (child.getNestedTypes().size() == 1) {
        const auto &childElem = child.getNestedType();
        for (const auto &compareElem : parent.getNestedTypes()) {
            if (not isSubType(childElem, compareElem.operator*())) {
                return false;
            }
        }
        return true;
    } else if (parent.getNestedTypes().size() == 1) {
        const auto &compareElem = parent.getNestedType();
        for (const auto &childElem: child.getNestedTypes()) {
            if (not isSubType(childElem.operator*(), compareElem)) {
                return false;
            }
        }
        return true;
    } else if (bool sameSize = child.getNestedTypes().size() == parent.getNestedTypes().size();
            sameSize) {
        for (size_t i = 0; i < child.getNestedTypes().size(); ++i) {
            if (not isSubType(child.getNestedTypes().at(i).operator*(),
                              parent.getNestedTypes().at(i).operator*())) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

inline bool isSameTypeIterableSubtype(const PipesType &child, const PipesType &parent) {
    if (child.getGenericName() != parent.getGenericName()) {
        throw randomize::exception::StackTracedException{
                "logic error: should be called only with same types. Called with "
                        + quote(to_string(child) + " and " + quote(to_string(parent)))};
    }
    if (are<UserType>(child, parent)) {
        // TODO: no inheritance yet
        return areSubtype(child.getNestedTypes(), parent.getNestedTypes());
    } else if (are<TupleType>(child, parent)) {
        return isTupleSubtype(child, parent);
    } else if (are<ArrayType>(child, parent)) {
        return isArraySubtype(child, parent);
    } else if (are<TypeofType>(child, parent)) {
        return isSubType(child.getNestedType(),
                         parent.getNestedType());
    } else {
        throw randomize::exception::StackTracedException{
                "logic error: missing iterable subtype analysis. Called with "
                        + quote(to_string(child) + " and " + quote(to_string(parent)))};
    }
}

inline bool isDifferentTypeSubType(const PipesType &child, const PipesType &parent) {
    if (child.getGenericName() == parent.getGenericName()) {
        throw randomize::exception::StackTracedException{
                "logic error: should be called only with different types. Called with "
                        + quote(to_string(child) + " and " + quote(to_string(parent)))};
    }
    bool childIsTuple = is<TupleType>(child);
    bool parentIsTuple = is<TupleType>(parent);
    bool childIsTypeof = is<TypeofType>(child);
    bool parentIsTypeof = is<TypeofType>(parent);
    // TODO: passing a non-array tuple to a function(iterable(:?)) assumes parent (func param) is
    //  an array
    if (childIsTuple and is<ArrayType>(parent)) {
        try {
            auto strictestInTuple = getStrictestTypeInIterable(child);
            return isArraySubtype(ArrayType{std::move(strictestInTuple), child.instanceName()},
                                  parent);
        } catch (IncompatibleTypesException &e) {
            return false;
        }
    } else if (is<ArrayType>(child) and parentIsTuple) {
        try {
            auto strictestInTuple = getStrictestTypeInIterable(parent);
            return isSubType(child, ArrayType{std::move(strictestInTuple), parent.instanceName()});
        } catch (IncompatibleTypesException &e) {
            return false;
        }
    } else if ((childIsTuple and is<UserType>(parent)) or (is<UserType>(child) and parentIsTuple)) {
        if (childIsTuple and child.getNestedTypes().size() == 1) {
            try {
                auto strictestInStruct = getStrictestTypeInIterable(parent);
                return isSubType(child, TupleType{std::move(strictestInStruct), parent.instanceName()});
            } catch (IncompatibleTypesException &e) {
                return false;
            }
        } else if (parentIsTuple and parent.getNestedTypes().size() == 1) {
            if (is<UnknownType>(parent.getNestedTypes().at(0))) {
                return true;
            } else {
                try {
                    auto strictestInStruct = getStrictestTypeInIterable(child);
                    return isSubType(TupleType{std::move(strictestInStruct), child.instanceName()},
                                     parent);
                } catch (IncompatibleTypesException &e) {
                    return false;
                }
            }
        } else {
            return areSubtype(child.getNestedTypes(), parent.getNestedTypes());
        }
    } else if (childIsTypeof) {
        return isSubType(child.getNestedType(), parent);
    } else if (parentIsTypeof) {
        return isSubType(child, parent.getNestedType());
    } else {
        return false;
    }
}
inline bool isSubType(const PipesType &child, const PipesType &parent) {
    if (parent == UnknownType{}
//            ) {
            or child == UnknownType{}) {    // TODO: define better UnknownType.
                                            // Is it a supertype? is it compatible with everything?
                                            // shouldn't it be compatible only if the function can
                                            // be specialized?
        return true;
    } else if (child.getGenericName() == parent.getGenericName()) {
        if (child.isCallable() and parent.isCallable()) {
            // the parent's param should be a subtype of the child's param: contravariant
            return isSubType(parent.getParameterType(), child.getParameterType())
                    // the child's return should be a subtype of the parent's return: covariant
                    and isSubType(child.getReturnedType(), parent.getReturnedType());
        } else if (child.isIterable() and parent.isIterable()) {
            return isSameTypeIterableSubtype(child, parent);
        } else {
            return true;
        }
    } else {
        return isDifferentTypeSubType(child, parent);
    }
}


#endif //PIPES_SUBTYPING_H
