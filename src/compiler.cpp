/**
 * @file compiler.cpp
 * @date 2020-12-28
 */

#include <string>
#include <fstream>
#include <chrono>
#include "lexer/pipesCodeFile.h"
#include "assembler/PipesAssembler.h"
#include "commonlibs/StackTracedException.h"
#include "commonlibs/SignalHandler.h"

void compilePipesProgram(std::string path) {
    auto code = getCodeFromPath(path);
    auto program = tokenizeAndParseAndCheckTypes(code);
    auto assembler = PipesAssembler{program};
    assembler.generateExecutable(getExecutablePath(path));
    std::cout << "The compiled instructions are "
        << assembler.getAssembler().getBuffer().size << " bytes" << std::endl;
}

template<typename T>
void measureTimeAndPrint(T function) {
    auto t0 = std::chrono::steady_clock::now();
    function();
    auto dt = std::chrono::steady_clock::now() - t0;
    auto mus = std::chrono::duration_cast<std::chrono::microseconds>(dt);
    std::cout << "Took " << mus.count() / 1'000'000 << ".";
    std::cout.fill('0');
    std::cout.width(6);
    std::cout << mus.count() << " s" << std::endl;
}

int main(int argc, char ** argv) {
    randomize::exception::SignalHandler::activate();
    if (argc != 2 or strcmp(argv[1], "-h") == 0 or strcmp(argv[1], "--help") == 0) {
        std::cout << "Usage: " << argv[0] << " FILE\n"
                  << "Takes a file with pipes code and compiles it into a binary file (with the same name, without the '.pipes' extension).\n"
                  << "Example: " << argv[0] << " my_code.pipes" << std::endl;
    } else {
        try {
            measureTimeAndPrint([&]() {
                compilePipesProgram(argv[1]);
            });
        } catch (std::exception &e) {
            std::cout << e.what() << std::endl;
            return 1;
        }
    }
    return 0;
}
