/**
 * @file StackTracedException.h
 * @author jmmut 
 * @date 2016-03-26.
 */

#ifndef COMMONLIBS_STACKTRACEDEXCEPTION_H
#define COMMONLIBS_STACKTRACEDEXCEPTION_H


// in case the user doesn't have execinfo.h, or unix's signals: disable backtracing on segfaults:
//#define DO_NOT_BACKTRACE


#if !defined DO_NOT_BACKTRACE && !defined WIN32

#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include <sys/types.h>
#include <unistd.h>
#include <cxxabi.h>
#endif // DO_NOT_BACKTRACE

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <platform/source_location.h>

namespace randomize::exception {

/**
 * exception that carries the call stack in the message. Use as a regular exception:

    if (badcondition) {
        throw StackTracedException("something went wrong");
    }

 * or use as a stream (like std::cout) for complex messages:

    if (badcondition) {
        throw StackTracedException() << 9 << " out of " << 10 << " dentists disapprove your code!";
    }
 */
class StackTracedException : public std::exception {
private:
    std::stringstream message;
    mutable std::string copy;


public:
    explicit StackTracedException();
    explicit StackTracedException(const std::string &message);

    const char *what() const noexcept override;

    template<typename T>
    void append(T addition) {
        using ::operator<<; // WTF, why do I need to do this?
                            // the operator<< is defined in the global namespace,
                            // it has location as one of its arguments.
                            // Why does it matter that this code is inside a namespace??
        message << addition;
    }
};

template <typename T>
StackTracedException &&operator<<(StackTracedException &&e, T addition) {
    e.append(addition);
    return std::move(e);
}
template <typename T>
StackTracedException &operator<<(StackTracedException &e, T addition) {
    e.append(addition);
    return e;
}

void print_stacktrace(std::ostream &out, unsigned int max_frames = 63);


class AssertionException : public std::runtime_error {
public:
    AssertionException(source_location location = source_location::current())
        :std::runtime_error{""} {
    using ::operator<<; // WTF, why do I need to do this?
                        // the operator<< is defined in the global namespace,
                        // it has location as one of its arguments.
                        // Why does it matter that this code is inside a namespace??
    message << "Assertion failed at " << location << ":\n";
}

    const char *what() const noexcept override {
        copy = message.str();
        return copy.c_str();
    }

    friend AssertionException &&operator<<(AssertionException &&e, const auto &addition) {
        e.message << addition;
        return std::move(e);
    }
    friend AssertionException &operator<<(AssertionException &e, const auto &addition) {
        e.message << addition;
        return e;
    }

private:
    std::stringstream message;
    mutable std::string copy;
};

};


#endif //COMMONLIBS_STACKTRACEDEXCEPTION_H
