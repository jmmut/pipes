/**
 * @file TestSuite.cpp
 * @author jmmut
 * @date 2015-10-10.
 */

#include "TestSuite.h"

namespace randomize::test {
    using std::string;


    TestSuite::TestSuite(string name, std::initializer_list<void (*)()> mains) : name(name) {

        for (auto &&func : mains) {
            tests.push_back(Test{func});
        }
        run();
    }
    TestSuite::TestSuite(string name, std::initializer_list<Test> tests)
            : name(name), tests(tests) {
        run();
    }

    void TestSuite::run() {
        int i = 0;
        for (auto &test : tests) {
            bool failed = false;
            try {
                test.func();
            } catch (randomize::exception::AssertionException & except) {
                std::cout << "failed test '" << name
                          << "': "
                          << except.what()
                          << std::endl;
                failed = true;
            } catch (std::exception &except) {
                std::cout << "failed test '" << name
                          << "' (at " << test << "): threw exception: "
                          << except.what()
                          << std::endl;
                failed = true;
            }
            if (failed) {
                failedTests.insert(i);
            }
            i++;
        }

        std::cout << "TestSuite: " << failedTests.size() << "/" << tests.size() << " "
                  << name << " tests failed " << std::endl;

//        if (failedTests.size() != 0) {
//            string tests;
//            for (auto index : failedTests) {
//                tests += std::to_string(index);
//                tests += ", ";
//            }
//            LOG_DEBUG("failed tests indices are: %s", tests.c_str());
//        }
    }

    unsigned long TestSuite::countFailed() {
        return failedTests.size();
    }

};
