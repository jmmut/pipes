/**
 * @file TestSuite.h
 * @author jmmut
 * @date 2015-10-10.
 */

#ifndef COMMONLIBS_TESTSUITE_H
#define COMMONLIBS_TESTSUITE_H

#include <utility>
#include <vector>
#include <set>
#include <functional>
#include <iostream>
#include <string>
#include "platform/source_location.h"
#include "StackTracedException.h"
#include "log.h"

namespace randomize::test {


    struct Test {
        Test(void (*func)(), source_location location = source_location::current())
                :func{func}, location{location} {
        }
        void (*func)();
        source_location location;
        friend std::ostream &operator<<(std::ostream &out, const Test &test) {
            return out << test.location;
        }
    };
    /**
     * to use like this:
    ```
    int main (int argc, char **argv) {
        int failed = 0;
        failed += TestSuite{"my test suite or class test", {Test{[]() {
            // assertions here
        }}, Test{[]() {
            // different assertions
        }}}}.countFailed();
        return failed != 0;
    }
    ```
     * You can use the assert_* functions inside the tests. If you throw and exception inside the
     * test, that will count as a failed test, and reported as such.
     */
    class TestSuite {
    private:
        std::string name;
        std::vector<Test> tests;
        std::set<int> failedTests;

    public:
        TestSuite(std::string name, std::initializer_list<void(*)()>);
        TestSuite(std::string name, std::initializer_list<Test>);
        unsigned long countFailed();

        void run();
    };

    inline void assert_equals(const auto &actual, const auto &expected, std::string message = "",
                       source_location location = source_location::current()) {
        using ::operator<<;
        if (not (actual == expected)) {
            throw randomize::exception::AssertionException{location}
                    << "should be equal:"
                    << "\n  actual:   " << actual
                    << "\n  expected: " << expected
                    << (message.empty() ? std::string{""} : "\n  explanation: " + message)
                    << "\n";
        }
    }
    inline void assert_throws(auto functionThatShouldThrow, std::string message = "",
                              source_location location = source_location::current()) {
        bool threw = false;
        try {
            functionThatShouldThrow();
        } catch (std::exception &e) {
            LOG_DEBUG("expected exception's message: %s", e.what());
            threw = true;
        }
        if (not threw) {
            throw randomize::exception::AssertionException{location}
                << "should have thrown an exception."
                << (message.empty()? std::string{""} : " " + message)
                << "\n";
        }
    }

#define ASSERT_OR_THROW_STACK(expression) ASSERT_OR_THROW_STACK_MSG((expression), "")

#define ASSERT_OR_THROW_STACK_MSG(expression, message) \
    if (not (expression)) \
        throw randomize::exception::StackTracedException(\
                __FILE__ ":" + std::to_string(__LINE__) + (": assertion ("  #expression  ") failed: ")\
                        + std::to_string(expression) + ". " + (message))

#define ASSERT_OR_THROW(expression) ASSERT_OR_THROW_MSG((expression), "")

#define ASSERT_OR_THROW_MSG(expression, message) \
    if (not (expression)) \
        throw std::runtime_error(\
                __FILE__ ":" + std::to_string(__LINE__) + (": assertion ("  #expression  ") failed: ")\
                        + std::to_string(expression) + ". " + (message))




#define ASSERT_EQUALS_OR_THROW_STACK(actual, expected) ASSERT_EQUALS_OR_THROW_STACK_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_STACK_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw randomize::exception::StackTracedException( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed: ") \
                + std::to_string(actual) + " == " + std::to_string(expected) \
                + ". " + (message))


#define ASSERT_EQUALS_OR_THROW(actual, expected) ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (const auto &_actualResult(actual); true)              \
        if (decltype(_actualResult) &_expectedResult(expected); \
                not ((_actualResult) == (_expectedResult)))   \
            if (std::stringstream comparisonMessage; true)    \
                throw std::runtime_error(                     \
                    ((comparisonMessage << __FILE__ << ":" << __LINE__\
                        << (": assertion (" #actual " == " #expected ") failed:\n  actual:   ") \
                        << _actualResult << "\n  expected: " << _expectedResult                 \
                        << (std::string{message}.empty()?     \
                        std::string{""} : std::string{"\n  explanation: "} + (message))         \
                        << "\n"), comparisonMessage.str()))




#define ASSERT_STRING_EQUALS_OR_THROW_STACK(actual, expected) ASSERT_STRING_EQUALS_OR_THROW_STACK_MSG((actual), (expected), "")

#define ASSERT_STRING_EQUALS_OR_THROW_STACK_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw randomize::exception::StackTracedException( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed: ") \
                + std::string(actual) + " == " + std::string(expected) \
                + ". " + (message))


#define ASSERT_STRING_EQUALS_OR_THROW(actual, expected) ASSERT_STRING_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_STRING_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw std::runtime_error( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed:\n") \
                + std::string(actual) + " ==\n" + std::string(expected) \
                + (std::string{message}.empty()? std::string{""} : std::string{". "} + (message)) \
                + "\n")





#define ASSERT_EQUALS_OR_THROW_SS_STACK(actual, expected) ASSERT_EQUALS_OR_THROW_SS_STACK_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_SS_STACK_MSG(actual, expected, message) \
    if (const auto &_actualResult{actual}; true) \
        if (const auto &_expectedResult{expected}; not ((_actualResult) == (_expectedResult))) \
            if (std::stringstream comparisonMessage; true) \
                throw randomize::exception::StackTracedException( \
                    ((comparisonMessage << __FILE__ << ":" << __LINE__ \
                        << (": assertion (" #actual " == " #expected ") failed: ") \
                        << _actualResult << " == " << _expectedResult \
                        << ". " << (message)), comparisonMessage.str()))


#define ASSERT_EQUALS_OR_THROW_SS(actual, expected) ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_SS_MSG(actual, expected, message) \
ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), message)

#define ASSERT_THROWS(codeThatShouldThrow) ASSERT_THROWS_MSG(codeThatShouldThrow, "")

#define ASSERT_THROWS_MSG(codeThatShouldThrow, message) { \
    bool threw = false;                                   \
    try {                                                 \
        codeThatShouldThrow                               \
    } catch (std::runtime_error &e) {                     \
        LOG_DEBUG("expected exception's message: %s", e.what()); \
        threw = true;                                     \
    }                                                     \
    if (not threw) {                                      \
        auto messageStr = std::string{message};           \
        if (not messageStr.empty()) {                     \
            messageStr = " " + messageStr;                \
        }                                                 \
        throw std::runtime_error(__FILE__  ":"            \
            + std::to_string(__LINE__)                    \
            + " assertion failed: should have thrown an exception." \
            + messageStr);                                \
    }                                                     \
}                                                         \

};

#endif //COMMONLIBS_TESTSUITE_H
