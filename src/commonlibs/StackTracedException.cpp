/**
 * @file StackTracedException.cpp
 * @author jmmut 
 * @date 2016-03-26.
 */

#include "StackTracedException.h"


namespace randomize::exception {

StackTracedException::StackTracedException() {
    print_stacktrace(this->message);
}

StackTracedException::StackTracedException(const std::string &message) {
    print_stacktrace(this->message);
    this->message << message;
}

const char *StackTracedException::what() const noexcept {
    copy = message.str();
    return copy.c_str();
}


#if !defined DO_NOT_BACKTRACE && !defined WIN32
/** Print a demangled stack backtrace of the caller function to ostream out.
 * Courtesy of (c) 2008, Timo Bingmann from http://idlebox.net/
 * published under the WTFPL v2.0
 */
void print_stacktrace(std::ostream &out, unsigned int max_frames)
{
    out << "stack trace:\n";
    size_t max_line_length = 2000;

    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0) {
        out << "  <empty, possibly corrupt>\n";
        return;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
        char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

        // find parentheses and +address offset surrounding the mangled name:
        // ./module(function+0x15c) [0x8048a6d]
        for (char *p = symbollist[i]; *p; ++p)
        {
            if (*p == '(')
                begin_name = p;
            else if (*p == '+')
                begin_offset = p;
            else if (*p == ')' && begin_offset) {
                end_offset = p;
                break;
            }
        }

        if (begin_name && begin_offset && end_offset
            && begin_name < begin_offset)
        {
            *begin_name++ = '\0';
            *begin_offset++ = '\0';
            *end_offset = '\0';

            // mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():

            int status;
            char* ret = abi::__cxa_demangle(begin_name,
                    funcname, &funcnamesize, &status);
            char str[max_line_length];
            if (status == 0) {
                funcname = ret; // use possibly realloc()-ed string
                snprintf(str, max_line_length, "  %s : %s+%s\n",
                        symbollist[i], funcname, begin_offset);
            }
            else {
                // demangling failed. Output function name as a C function with
                // no arguments.
                snprintf(str, max_line_length, "  %s : %s()+%s\n",
                        symbollist[i], begin_name, begin_offset);
            }
            out << str;
        }
        else
        {
            // couldn't parse the line? print the whole line.
            out << "  " << symbollist[i] << std::endl;
        }
    }

    free(funcname);
    free(symbollist);
}

#else // DO_NOT_BACKTRACE

void print_stacktrace(std::ostream &out, unsigned int max_frames) {}

#endif // DO_NOT_BACKTRACE


};
