
#ifndef __SIGNALHANDLER_H_
#define __SIGNALHANDLER_H_


#include <iostream>
#include <stdexcept>
#include "StackTracedException.h"

namespace randomize::exception {
/**
 If a program receives a segmentation fault signal, this class raises
 an exception with the call stack in the message with some debugging info.

 To use put this somewhere in your program:
 
 SigsegvPrinter::activate();
 */
    class SignalHandler {
    public:
        /**
         * use this if you want to copy directly to any stream, like cerr or cout. If you are
         * not catching the StackTracedException and you let it close your program the stack will be
         * printed twice. use activate() to print only in the exception message.
         */
        static void activate(std::ostream &output_stream);

        static void activate();

        /**
         * use this if you want to catch other signal than the default SIGSEGV. Be careful of
         * infinite recursion by catching too much, e.g. doing a double free,
         */
        static void activate(int signal);

    private:
        static std::ostream *out;

        static void (*defaultAbortHandler)(int);

        static bool didWeGetThePreviousHandler;

        /**
         * In case of SIGABRT, this will only handle it the first time.
         * If a previous abort was raised in the same execution, it may be a different one,
         * but if it's the same and we handle it again we make infinite recursion.
         */
        static void handleSignal(int ignored);
    };

    using SigsegvPrinter = SignalHandler;

//void ActivateSigsegvPrinter(std::ostream & os);
//void handleSignal();
};
#endif
