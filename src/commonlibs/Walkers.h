/**
 * @file Walkers.h
 * @author jmmut 
 * @date 2016-02-15.
 */

#ifndef COMMONLIBS_WALKERS_H
#define COMMONLIBS_WALKERS_H

#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include <numeric>
#include <set>
#include <map>
#include <utility>

template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::pair<T1, T2> &p);

namespace randomize::utils {

    template<typename C>
    std::string container_to_string(C v) {
//    string joined = join<vector<T>, string, function<string(T, string)>>(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
//    string joined = join(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
        std::string joined = std::accumulate(
                std::begin(v),
                std::end(v),
                std::string(),
                [] (std::string s, decltype(*std::begin(v)) elem) {
                    return s + " " + std::to_string(elem) + ",";
                }
        );
        joined[0] = '[';
        joined[joined.size()-1] = ']';
        return joined;
    }


    /**
     * the other one can not print vector<string>
     */
    template<typename C>
    std::string container_to_string_ss(C v) {
//    string joined = join<vector<T>, string, function<string(T, string)>>(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
//    string joined = join(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
        std::string joined = std::accumulate(
                std::begin(v),
                std::end(v),
                std::string(),
                [] (std::string s, decltype(*std::begin(v)) elem) {
                    std::stringstream ss;
                    ss << s << " " << elem << ",";
                    return ss.str();
                }
        );
        joined[0] = '[';
        joined[joined.size()-1] = ']';
        return joined;
    }

    /**
     * A container is anything that has defined `.size()`, `.begin()` and `.end()` and can be iterated with them.
     * To print the inner types, the operator<< has to be defined for them.
     */
    template<typename C>
    std::string to_string(const C &container, std::string open, std::string separator, std::string close) {
        if (container.size() == 0) {
            return open + close;
        } else {
            std::stringstream ss;
            ss << open;
            auto it = container.begin();
            ss << *it;
            ++it;
            for (; it != container.end(); ++it) {
                ss << separator << *it;
            }
            ss << close;
            return ss.str();
        }
    }

    template<typename C>
    std::string to_string(const C &container) {
        return to_string(container, "[", ", ", "]");
    }
};

template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::pair<T1, T2> &p) {
    return out << "{" << p.first << ", " << p.second << "}";
}

template <typename VALUE, template <typename, typename> typename CONTAINER, typename ALLOCATOR>
bool contains(const CONTAINER<VALUE, ALLOCATOR> &container, const VALUE &value) {
    return std::find(container.begin(), container.end(), value) != container.end();
}

// note this is overloading! no template specialization
// (c++ forbids function template specialization, don't know why)
template <typename VALUE>
bool contains(const std::set<VALUE> &container, const VALUE &value) {
    return container.find(value) != container.end();
}

// note this is overloading! no template specialization
// (c++ forbids function template specialization, don't know why)
template <typename KEY, typename VALUE>
bool contains(const std::map<KEY, VALUE> &container, const KEY &key) {
    return container.find(key) != container.end();
}



template <typename C, typename K>
bool containsKey(const C &container, K value) {
    return container.find(value) != container.end();
}

inline std::string tolower(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(),
                // static_cast<int(*)(int)>(std::tolower)         // wrong
                // [](int c){ return std::tolower(c); }           // wrong
                // [](char c){ return std::tolower(c); }          // wrong
                   [](unsigned char c){ return std::tolower(c); } // correct
                  );
    return s;
}

inline bool contains_ignorecase(std::string str, const std::string& substr) {
    return tolower(std::move(str)).find(substr) != std::string::npos;
}

#endif //COMMONLIBS_WALKERS_H
