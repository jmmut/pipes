/*
 * log.h
 * 
 * Copyright 2014 Jacobo <jacollmo@alumni.uv.es>
 * 
 * Last modification: 2014/01/11 17:32:42
 */


#include "Walkers.h"
#include "log.h"

namespace randomize::log {

    int log_level = LOG_DEFAULT_LEVEL;
    int log_stderr_ok = LOG_DEFAULT_VERBOSE;
    char log_tag[LOG_TAG_LENGTH] = LOG_DEFAULT_TAG;
    FILE* log_file = stderr;

    std::map<enum Levels, std::string> levelNames = {
            {VERBOSE_LEVEL, "verbose"},
            {DEBUG_LEVEL, "debug"},
            {INFO_LEVEL,  "info"},
            {WARN_LEVEL,  "warn"},
            {ERROR_LEVEL, "error"},
            {FATAL_LEVEL, "fatal"},
    };



    int parseLogLevel(const char* name) {

        for (const auto &entry : levelNames) {
            if (strncasecmp(entry.second.c_str(), name, entry.second.size()) == 0) {
                return entry.first;
            }
        }

        // no entry matched
        std::string names = utils::container_to_string_ss(getNames());
        throw std::invalid_argument(std::string("there's no loglevel called \"") + name + "\", try " + names);
    }

    void setLogLevel(Levels level) {
        log_level = level;
    }

    void setLogLevel(const char * level_name) {
        log_level = parseLogLevel(level_name);
    }

    bool trySetLogLevel(const char * level_name) {
        for (const auto &[candidate_level, candidate_name] : levelNames) {
            if (strncasecmp(candidate_name.c_str(), level_name, candidate_name.size()) == 0) {
                log_level = candidate_level;
                return true;
            }
        }
        return false;
    }

    std::vector<enum Levels> getLevels() {
        std::vector<enum Levels> values;
        for (const auto &entry : levelNames) {
            values.push_back(entry.first);
        }
        return values;
    }


    std::vector<std::string> getNames() {
        std::vector<std::string> values;
        for (const auto &entry : levelNames) {
            values.push_back(entry.second);
        }
        return values;
    }

};
