/**
 * @file RAII.h
 * @date 2021-07-28
 */

#ifndef PIPES_RAII_H
#define PIPES_RAII_H

#include <exception>

/** Resource Acquisition Is Initialization, which means
 * using constructors and destructors to do something at the beginning and end of a scope.
 * @example
```
RAII scopeCounter([&]() {++i;},
                  [&]() {--i;});
```
 * @tparam F1,F2 anything callable (that has operator()): a function pointer, a lambda, a functor...
 */
template<typename F1, typename F2>
struct RAII {
    F2 deferredAction;

    RAII(F1 immediateAction, F2 deferredAction) : deferredAction{deferredAction} {
        immediateAction();
    }

    virtual ~RAII() noexcept(false) {
        if (std::uncaught_exceptions() == 0) {
            deferredAction();
        }
    }
};

#endif //PIPES_RAII_H
