#include <iostream>
#include "source_location/source_location.hpp"

source_location src_clone(source_location a = source_location::current())
{
    return a;
}

int main()
{
    auto s2 = src_clone();          // location should point here 
    std::cout
        << s2.line() << ' ' << s2.function_name() << '\n';
}
