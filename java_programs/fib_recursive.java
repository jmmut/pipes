
class fib_recursive {
    public static int fib(int x) {
        if (x == 0 || x == 1) {
            return 1;
        } else {
            return fib(x - 2) + fib(x - 1);
        }
    }
    public static void main(String[] args) {
        System.out.println(fib(Integer.parseInt(args[0])));
    }
}
