#!/bin/bash

set -uo pipefail

if [ $# -eq 1 ]
then
  build_folder=$1
else
  build_folder=build
fi

if ! cd $build_folder
then
  echo -e "build folder \"$build_folder\" doesn't exist! please create it first:\nmkdir $build_folder"
  exit 1
fi

if ! (cmake -DCMAKE_BUILD_TYPE=Release ../src && make -j)
then
  echo "compilation failed!"
  exit 1
fi

cd -

function execute_tests () {
  local FAILS=0
  while read file
  do
    echo ""
    echo "    ---     executing $file"
    $file 2>&1 | grep "^TestSuite" |
        sed -E "s,TestSuite: ([1-9][0-9]*)(/[0-9]+ )(.*) tests failed,TestSuite: $(tput setaf 1)\1$(tput sgr0)\2$(tput setaf 1)\3$(tput sgr0) tests failed,"
    EXIT_STATUS=$?
    #echo "exit code: $EXIT_STATUS"
    FAILS=$(($FAILS + $EXIT_STATUS))
    #echo "fails: $FAILS"
  done

  echo ""
  #echo $FAILS
  if [ $FAILS -ne 0 ]
  then
    echo "Some unit tests $(tput setaf 1)failed$(tput sgr0)!"
  else
    echo "All unit tests $(tput setaf 2)passed$(tput sgr0)"
  fi
  return $(($FAILS != 0))
}

ls ${build_folder}/bin/test* | execute_tests
UNIT_RESULT=$?

echo ""

function execute_integration_tests () {
  local FAILS=0
#  set -x
  while read file
  do
    EXECUTABLE=$(echo $file | sed 's/\.pipes$//')
    PARAM=3
    #set -x
    RESULT=$(${build_folder}/bin/pipes-compiler $file 2>&1 > /dev/null \
      && ${build_folder}/bin/pipes-loader $EXECUTABLE $PARAM \
      | grep result | grep -o -e '-\?[0-9]\+$')
    EXIT_STATUS=$?
    EXPECTED_RESULT=0
    if [ $EXIT_STATUS -ne 0 ] || [ "$RESULT" != "$EXPECTED_RESULT" ];
    then
      echo -n "failed test: $(echo $file | sed -E "s,/([^/]+\.pipes),/$(tput setaf 1)\1$(tput sgr0), ") ("
      if [ $EXIT_STATUS -ne 0 ]
      then
        echo "returned exit code $EXIT_STATUS), try \"${build_folder}/bin/pipes-jit-repl $file $PARAM\""
      else
        echo "returned $RESULT instead of $EXPECTED_RESULT), try \"${build_folder}/bin/pipes-jit-repl $file $PARAM\""
      fi
      FAILS=$(($FAILS + 1))
    fi
    set +x
#    echo "fails: $FAILS"
  done

  #set +x
  #echo $FAILS

  if [ $FAILS -ne 0 ]
  then
    echo "Some ($FAILS) integration tests $(tput setaf 1)failed$(tput sgr0)!"
  else
    echo "All integration tests $(tput setaf 2)passed$(tput sgr0)"
  fi
  return $(($FAILS != 0))
}

ls pipes_programs/tests/test*.pipes | execute_integration_tests
INTEGRATION_RESULT=$?

exit $(($UNIT_RESULT + $INTEGRATION_RESULT))
