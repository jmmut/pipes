
set -euo pipefail

if [ $# != 2 ]
then
	echo "need 2 arguments: old and new versions"
	exit 1
fi

old_version=$1
new_version=$2
echo "bumping version from ${old_version} to ${new_version}"


echo "  - updating conanfile.py version"
if [[ $(git status --porcelain 2> /dev/null | grep -v "??" | wc -l)  != "0" ]]
then
	echo "git workspace is dirty. Please commit your changes before tagging a version"
	exit 1
fi

sed -i "s/version = \"${old_version}\"/version = \"${new_version}\"/" conanfile.py
echo "  - committing conanfile.py version change"
git add conanfile.py
git commit -m "bumping version in conanfile.py to ${new_version}"



echo "  - tagging"
echo -e "$new_version\n\nCurrent roadmap:\n" | cat - docs/roadmap.md |git tag $new_version -a --file=-

echo -e "version bumping done\n"
echo "do the next command to edit the tag with the highlights of this version:"
echo "git tag -a -f ${new_version}"





