
// use this function to free a pointer (like an array) after doing some action
// equivalent java if java could free pointers:
//  ```
//  Function<T, R> do_and_free(Function<List<T>, R> action) {
//      return (List<T> ptr_to_free) -> {
//          R result = action(ptr_to_free);
//          free(ptr_to_free);
//          return result;
//      }
//  }
//  ```
//
// equivalent c++ (haven't tested if this is entirely syntax-correct):
//  ```
//  template<T, R>
//  auto do_and_free(std::function<R(T*)> action) {
//      return [](T *ptr_to_free) -> R {
//          R result = action(ptr_to_free);
//          free(ptr_to_free);
//          return result;
//      }
//  }
//  ```
function action :function {
    function ptr_to_free {
        ptr_to_free
        |action
        |function result {
            ptr_to_free
            |free;
            result
        }
    }
}
//function action :function {
//    function ptr_to_free {
//        ptr_to_free
//        |action
//        lambda_calc/false (ptr_to_free |free)
//    }
//}
