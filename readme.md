# Pipes programming language

This toy project aims to study and experiment with designing a programming language called Pipes, and developing an interpreter and a compiler for it.

Head to [docs/](docs) for more info about the next topics:

## Programming in Pipes

- [Installing Pipes](docs/installing.md) to get a working Pipes compiler
- [Quick introduction](docs/pipes-quick-intro.md) to learn quickly how to program in pipes.

## Philosophy of Pipes

- [Pipes core ideas](docs/pipes-core-ideas.md) for the motivation to create this language.
- [Pipes is like...](docs/pipes-is-like.md) for some semi-serious aphorisms.
- [Rant](docs/rant.md) for random complaints and other thoughts I have about programming languages.
- [Brainstorm](docs/brainstorm.md) for getting a sense of the goal of pipes programs with not-yet implemented features.
- Look in [pipes_programs/](pipes_programs) (outside of docs/) for already-working pipes code, especially [pipes_programs/tests/](pipes_programs/tests).

## About programming the Pipes compiler

- [Roadmap](docs/roadmap.md) for a list of implemented / not-implemented features.
- [Developing resources](docs/developing-resources.md) for more details and links to learning material about compilers, which I found useful.
- [This blog post of mine](https://jmmut.github.io/2021/01/01/The-stupidest-compiler-ever.html) 
  for the basics on how I did the first versions of the compiler.
