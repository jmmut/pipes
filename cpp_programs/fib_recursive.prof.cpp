#include <iostream>
#include <utility>
// g++ fib_recursive.cpp -o fib_recursive

//auto equals_zero(int x) {
//    if (static_cast<bool>(x)) {
//        return false;
//    } else {
//        return true;
//    }
//}
//auto equals_one(int x) {
//    return equals_zero(x - 1);
//}
//auto my_or(bool a) {
//    if (a) {
//        return [=](bool b) {
//            return true;
//        };
//    }
//}
//auto equals_zero_or_one(int x) {
//    return my_or(equals_zero(x))(equals_one(x));
//}
auto sum_pair_and_add_1_to_second(std::pair<long, long> p1, std::pair<long, long> p2) {
    return std::make_pair(p1.first + p2.first, p1.second + p2.second + 1);
}
auto fib(long x) {
    if (x == 0 or x == 1) {
        return std::make_pair(1l, 1l);
    } else {
        return sum_pair_and_add_1_to_second(fib(x - 2), fib(x - 1));
    }
}
int main (int argc, char **argv) {
    auto result = fib(std::atoi(argv[1]));
    std::cout << "first: " << result.first << ", second: " << result.second << std::endl;
    return 0;
}
