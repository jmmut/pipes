#include<iostream>
// g++ fib_recursive.cpp -o fib_recursive

//auto equals_zero(int x) {
//    if (static_cast<bool>(x)) {
//        return false;
//    } else {
//        return true;
//    }
//}
//auto equals_one(int x) {
//    return equals_zero(x - 1);
//}
//auto my_or(bool a) {
//    if (a) {
//        return [=](bool b) {
//            return true;
//        };
//    }
//}
//auto equals_zero_or_one(int x) {
//    return my_or(equals_zero(x))(equals_one(x));
//}
auto fib(int x) {
    if (x == 0 or x == 1) {
        return 1;
    } else {
        return fib(x - 2) + fib(x - 1);
    }
}
int main (int argc, char **argv)
{
    std::cout << fib(std::atoi(argv[1])) << std::endl;
    return 0;
}
