#include <stdio.h>
#include <stdlib.h>
// g++ fib_recursive.cpp -o fib_recursive

int fib(int x) {
    if (x == 0 || x == 1) {
        return 1;
    } else {
        return fib(x - 2) + fib(x - 1);
    }
}
int main (int argc, char **argv)
{
    printf("%d\n", fib(atoi(argv[1])));
    return 0;
}
