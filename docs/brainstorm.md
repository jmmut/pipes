# Brainstorm

This file shows non-working Pipes code, exploring in which direction the design should go.

The first section of this file compares java code with different proposals for pipes code, 
none of them definitive.

```java
// original:
public class VariantRowFilterFactory {

    private <T> Predicate<T> mergePredicates(List<Predicate<T>> predicates,
                                             VariantQueryUtils.QueryOperation operation) {
        if (predicates.size() == 1) {
            return predicates.get(0);
        } else {
            Predicate<T> p = predicates.get(0);
            for (int i = 1; i < predicates.size(); i++) {
                if (operation.equals(VariantQueryUtils.QueryOperation.OR)) {
                    p = p.or(predicates.get(i));
                } else {
                    p = p.and(predicates.get(i));
                }
            }
            return p;
        }
    }
}

// variable declaration:
name :type;

// declare a variable that is a generic function:
my_func function arg -> ret;    // "-> ret" would be optional in most cases?

// declare a variable that is a fully typed function:
my_func function arg :ArgType -> ret :RetType;

// define a named function
my_func = function arg -> ret { //something }

// I want to do this but it looks weird
function arg -> ret { //something }
>> my_func

// define a anonymous function (lambda)
function arg -> ret { //something }

// many args and returns. I want to support many returns unless the implementation is too slow
function arg1 :ArgType1, arg2 :ArgType2 -> ret1 :RetType1, ret2 :RetType2 { //something }
function (arg1 :ArgType1, arg2 :ArgType2) -> (ret1 :RetType1, ret2 :RetType2) { //something }   // 
Maybe optional parenthesis?


// haven't thought about how to represent generics, using T? for now

/** Note:
 * - small changes in this alternative
 * - the syntax to put the break point anywhere in the loop
 * - how the condition is piped to the |if
 */
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {
        i = 0;
        p = predicates[i];
        loop {
            ++i;
            stay if i < predicates.size();
            operation.equals(VariantQueryUtils.QueryOperation.OR)
            |if {
                p = p.or(predicates[i]);
            } else {
                p = p.and(predicates[i]);
            }
        }
        p
    }
}

/** Note:
 * - this reduce helper function allows easily iterating n elements, in n-1 pairs, returning n-1 elems
 * - I think I will put simple whiles too as shown here
 * - the result of a while is the last expression of the last iteration? a list with the last expression of each iteration
 */
reduce = function
        list :List(T?),
        join :function(elem :T?, nextElem :T? -> joined T?)
        -> reduced T? {
    iter = list.iterator();
    iter.hasNext()
    |if {
        result = iter.next();
        while iter.hasNext() {
            result, iter.next()
            |join
            >> result
        }
        // return result; // for explicitness?
    } else {
        // TODO: error, list was empty, what is the expected result? return optional<T?>
    }
}

/** Note:
 * - piped |if and generalized loop again, just to showcase. these examples look better with a "while cond {...}"
 * - piping to f as "a, b |f" is the same as "(a, b) |f" 
 */
reduce = function
        list :List(T?),
        join :function(elem :T?, nextElem :T? -> joined T?)
        -> reduced T? {
    iter = list.iterator();
    iter.hasNext()
    |branch {
        result = iter.next();
        loop {
            stay if iter.hasNext();
            result, iter.next()
            |join
            >> result
        }
    } else {
        // TODO: error, list was empty, what is the expected result? return optional<T?>
    }
}
    
    
    
/** Note:
 * - The most coherent with the piping idea, but "predicates, function [...] |reduce" doesn't read well... it seems you
 *   need to read "reduce" first, to contextualize the function.
 */
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {
        predicates,
        function pred, nextPred {
            operation.equals(VariantQueryUtils.QueryOperation.OR)
            |if {
                pred.or(nextPred)
            } else {
                pred.and(nextPred)
            }
        }
        |reduce
    }
}

/** Note:
 * - Like the previous but the args are a tuple with parenthesis "(a,b)"
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {
        (predicates
        ,function pred, nextPred {
                operation.equals(VariantQueryUtils.QueryOperation.OR)
                |if {
                    pred.or(nextPred)
                } else {
                    pred.and(nextPred)
                }
            }
        )|reduce
    }
}

/** Note:
 * - In an attempt to solve the readability issue of the previous example, the "a|compose f b" pattern 
 *   would be equivalent to "a, b |f".
 */
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {

        predicates
        |compose reduce function pred, nextPred {
            operation.equals(VariantQueryUtils.QueryOperation.OR)
            |if {
                pred.or(nextPred)
            } else {
                pred.and(nextPred)
            }
        }
    }
}

/** Note:
 * - this is the only example where the piped "|if" gives some insight on chained usages. Still could be better
 * - see below for this version in java
 */
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {

        operation.equals(VariantQueryUtils.QueryOperation.OR)
        |if {
            function pred, nextPred { pred.or(nextPred) }
        } else {
            function pred, nextPred { pred.and(nextPred) }
        }
        >> join
            
        predicates
        |compose reduce join

        // I'm not sure the compose thingy fits with the rest of the syntax. in any case this would work:
        // predicates, join
        // |reduce
    }
}

/** Note:
 * - introducing "|branch" which is an attempt to do something like ?: but clearer and closer to if-else
 * - see below for this version in java
 */
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {

        operation.equals(VariantQueryUtils.QueryOperation.OR)
        |branch function pred, nextPred { pred.or(nextPred) },
                function pred, nextPred { pred.and(nextPred) }
        >> join
            
        predicates |compose reduce join

        // I'm not sure the compose thingy fits with the rest of the syntax. in any case this would work:
        // predicates, join |reduce
    }
}

/** Note:
 * - Previous pipes version but in java.
 * - ok being able to do Predicate::or is quite cool
 * - also, in the pipes version I'm not checking for empty lists, but this one does.
 */
public class VariantRowFilterFactory {
    private <T> Predicate<T> mergePredicates(List<Predicate<T>> predicates,
                                             VariantQueryUtils.QueryOperation operation) {
        BinaryOperator<Predicate<T>> join;
        if (operation.equals(VariantQueryUtils.QueryOperation.OR)) {
            join = Predicate::or;
        } else {
            join = Predicate::and;
        }
        return predicates.stream().reduce(join).orElseThrow(IllegalStateException::new);
    }
}

/** Note:
 * - maybe it's a good idea to allow arguments by pipe and by parenthesized call. I think scala does something similar 
 *   so maybe that's a bad idea. Edit: scala even allows defining infix operators. You define a method m and can call
 *   either "a.m(b)" or "a m b". I could do this like "a|m(b)", without needing m to be a method,
 *   just a function a, b {}. Edit2: it's now possible for a "m = function b {function a {}}" to be
 *   used like "a m b", which is equivalent to "a|(b |m)".
 * - also, maybe we can use method references as in java? that would be cool. 
 * - see below for this version in java
 */
class VariantRowFilterFactory {
    mergePredicates = function
            predicates :List(Predicate(T?)),
            operation :VariantQueryUtils.QueryOperation 
            -> :Predicate(T?) {

        predicates
        reduce (
            operation =? VariantQueryUtils.QueryOperation.OR
            |branch {Predicate.or} {Predicate.and}
        )
        orElseThrow IllegalStateException.new
    }
}


/** Note:
 * - Another java version with the inline function. less readable but cooler. I wish the ?: operator was more readable.
 */
public class VariantRowFilterFactory {
    private <T> Predicate<T> mergePredicates(List<Predicate<T>> predicates,
                                             VariantQueryUtils.QueryOperation operation) {
        return predicates.stream()
                .reduce(operation.equals(VariantQueryUtils.QueryOperation.OR) ? Predicate::or 
                                                                              : Predicate::and)
                .orElseThrow(IllegalStateException::new);
    }
}
```


### Possible shapes of 'branch'
```
cond
|branch
    a
    b

cond
|branch
    a + some_long_thing
        |funcA
    b + some_long_thing
        |funcB
|func_with_result_from_branch


cond
|branch {
    a + some_long_thing
    |funcA
    ,
    b + some_long_thing
    |funcB
}
|func_with_result_from_branch

cond
|branch {
    a + some_long_thing
    |funcA
} {
    b + some_long_thing
    |funcB
}
|func_with_result_from_branch


cond
|branch
    {a + some_long_thing
        |funcA}
    {b + some_long_thing
        |funcB}
|func_with_result_from_branch
```


### Proposal for "transformations" (unimplemented)

this:
```
[1, 2, 3]
||function x { x + 1 }
|array/print_ints
|function list {
    list
    |array/size
    |int64/print
}    
```

could be simplified to:

```
[1, 2, 3]
||{+1}
|array/print_ints
|{
    |array/size
    |int64/print
}
```

