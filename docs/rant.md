# Rant

## Expert-friendly

I hate when someone says "that way of doing things is unnecessary and a beginner antipattern". A tool that 
needs lots of learning is a bad tool, and you should not be proud about it!

Even if a tool is different to most existing tools, its behaviour should be as obvious as possible, once the basic
premises are explained.

# Other random thoughts

Should polymorphism be detached from using pointers?

Like, in C++ to use polymorphism you need to use pointers, but maybe that is an
implementation detail that should be abstracted for the user?



