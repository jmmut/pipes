# Pipes feature tour

Pipes is a language based on making pipelines transforming data, where code structure resembles execution flow.

## Functions and calls

The fundamental constructs are calls ('|') and anonymous functions ('function ARG { BODY }'). Anonymous functions are sometimes called lambdas.

Call ordering is reversed compared to mainstream languages: `A |f |g` means passing A to f, and then pass the result to g. The equivalent in mainstream languages would be `g(f(A))`. See [Pipes core ideas](pipes-core-ideas.md) for some more discussion about this.

Pipes supports lambdas with closures.

Examples:
- `5 |function x {x}` is a call, passing 5 to the identity function.
- `function f {5 |f}` is an anonymous function, that takes another function f and calls it with the value 5.
- `5 |f |g` is the same as `(5 |f) |g`; calls are left associative.
- `function x {function a {x}}` are two nested lambdas, where the inner has a closure on x.

### Tricks

These constructs allow interesting patterns, although some of them are somewhat ugly:

#### Stacking values

At the moment, functions can only take 1 parameter, but this doesn't reduce the number of possible programs. For instance, to make a function `sum` that takes 2 parameters, you define 2 nested functions, and do partial evaluation:
```
function a {function b {
    a + b
}}
```

if you put the above code in a file `sum.pipes` you can "import" it transparently. You could use it like this: 
```
5 |( 4 |sum)
```
Note the (ugly) pattern of `|(`. It basically means "push previous value into a stack", and it will be passed to the result of evaluating the parenthesis.

#### Partial evaluation

Following on the previous example, note that you don't need to pass a second value to `4 |sum` immediately. `4 |sum` is a partial evaluation (meaning "plus 4", or `function b {4 + b}`) and can be called several times with different values of b.

The only problem is that you need variables to call the partial evaluation several times, and variables are not implemented yet, but you can put that in a file `plus_4.pipes` or use closures instead. See next section.

#### Variable declaration through nested lambdas

At the moment Pipes doesn't support variables yet, but it's possible to mimic them with closures. Take this code that uses variables:

```
f = function x {5 |x}
g = function x {3 |x}
h |f |g     // imagine h is some function
```
That won't compile at the moment, but the same code can be expressed like this:

```
function x {5 |x}
|function f {

  function x {3 |x}
  |function g {

    h |f |g

  }
}
```

because the `function x {5 |x}` is passed to a function that puts its argument in f, and f is closured in the inner functions.

#### Recursion

The compiler supports recursive functions, even without having variables nor _named_ functions (you can name functions by extracting them to a file).

The classic example of fibonacci looks like this (if put into a file `fib_recursive.pipes`):
```
function x {
    x
    |branch {
        x -1
        |branch {
            x -2 |fib_recursive + (x -1 |fib_recursive)
        } {
            1
        }
    } {
        1
    }
}
```

However, recursion can work even without putting that into a file (which gives the identifier to 
the function).

How can a function call itself if the function is anonymous? With the magic of fixed-point combinators.

Take this silly C function that takes a positive number and negates it:
```
int negate(int n) {
    if (n != 0) {
        return negate(n-1) -1;
    } else {
        return 0;
    }
}
```

Now, let's define the Z combinator in pipes. At the moment of writing the type checker doesn't 
allow this, but the compiler would work otherwise:
```
function pseudoRec {
    function iteration {function value {value |(iteration |iteration)} |pseudoRec}
    |function iteration {function value {value |(iteration |iteration)} |pseudoRec}
}
```

and now the recursive negate function in pipes:
```
function n {
    n |(
        function recurse {
            function x {
                x |branch
                    (x |decrement |recurse |decrement),
                    0
             }
        }
        |lambda_calc.Z
    )
}
```

This is obviously horrible to read, but hopefully is not the final coding style in pipes, and just a cool trick. In defense of Pipes, the comparison with the C function is not fair. This would be a pseudo-C++ version (with the "negate" function being anonymous), but it wouldn't compile for several reasons I'm not going to bother to analyse:
```
auto Z (auto pseudoRec) {
    return [](auto iteration){ pseudoRec([](auto value) {return iteration(iteration)(value);})}(
            [](auto iteration){ pseudoRec([](auto value) {return iteration(iteration)(value);})}
    );
}

[](auto n) {
    return Z([](auto recurse){
        [](auto x) {
            if (x != 0) {
                return recurse(x-1) -1;
            } else {
                return 0;
            }
        }
    })(n);
}
```
