
# Pipes is like...

## Pipes is like Bash, but with types

The inspiration for Pipes comes from Unix shells, where you can quickly process data with the classic Unix
tools like `head`, `tail`, `grep`, `sed`, `wc` and many others. Still, I usually struggle with text
parsing and regex, and I think those pipelines would benefit from using types other than strings.

For instance, in Pipes you can define a function that takes a number and returns a pair of numbers, and another
function that takes a pair and returns the sum:

```
// duplicate
function n :int64 -> pair :struct(first :int64, second :int64) { (n, n+1) }
```
and
```
// sum
function pair :struct(first :int64, second :int64) -> n :int64 { pair.first + pair.second }
```

and you can pipe both: `3 |duplicate |sum` which will return 7. And if you mismatch the types it will
clearly complain (for some value of "clearly"):
```
$ pipes-jit-repl "3 |duplicate |duplicate"
Compilation error: Type mismatch when calling a function at line 1:14: 
3 |duplicate |duplicate
             ^
Expected parameter type (inferred from function):       n :int64
Actual parameter type passed when calling the function: pair :struct(first :int64, second :int64)
```

Also, Pipes is a compiled language (can be used as interpreted via Just-In-Time compilation), so
it will complain at compile time, which is better than complaining at runtime.

Another interesting feature that Pipes doesn't have yet, but might have in the future, is the ability
of Bash pipelines to be stopped from either side (provider or consumer).


## Pipes is like Lisp, but without parenthesis

I think most software is about having a single (or just a few) value or data structure and apply
several consecutive transformations. So in Lisp, most code would look like `(g (f a))`. If you
wanted to call a third function `h`, you would need to add parenthesis at both ends of the
expression to write `(h (g (f a)))`. In pipes that would be written `a |f |g |h`, where you only
have to add things at the end when you want to do more things.

Admittedly, Pipes doesn't have half of the interesting properties that Lisp has, but does have
a sense of functional programming because it has first-class functions (functions can be passed and
returned from functions, and there are anonymous functions, or lambdas. Basically, functions are
expressions). Also, because of simplicity of implementation, simple values are immutable.


## Pipes is like haskell, but reversed right-to-left

In haskell, for simple non-higher order functions, you could write something
like `h $ g (f a)`, or `(h . g . f) a`. There are several ways
to write that, but the right-most functions will be applied earlier, given this kind of parenthesis
usage. The same in pipes is 
`a |f |g |h` or `a |(f compose g compose h)` (reversed compared to Haskell).
The user would just need to define `compose` as:

```
function g {
    function f {
        function x {
            x |f |g
        }
    }
}
```

If, by contrast, we are dealing with higher order functions like `partial_sum 3 5`, where 
`partial_sum 3` would return a function `Int -> Int` that will sum 3 to the next parameter, in pipes that
would be `5 |(3 |partial_sum)`, again reversed right-to-left.

It was later brought to my attention that Haskell also has the operator `&`, which is pretty much 
the `|` operator in Pipes. So actually you can also write haskell itself reversed! `a &f &g 
&h`. 

### Pipes aims to be as readable as Java, but with proper lambda type syntax

In Java, the types of lambdas are written like these:
```
BiFunction<Integer, Integer, Integer> f = (t, u) -> t + u;
BiConsumer<Integer, Integer> g = (t, u) -> System.out.println(t + u);
```

...

Java, go home; you are drunk.

In all seriousness, the more I program in other languages, the more readable I think Java is in
general, except for lambda types and functional interface definition.
These lambda types are needed when you extract inline lambdas into variables
to modularize the code.

Below you can see how the first of the above functions would be like in Pipes. Admittedly, the part 
about taking 2 parameters is worse, but the function type in general I think is much better:
```
function p :struct(t :int64, u :int64) -> :int64 { p.t + p.u }
```
I haven't discarded yet to allow the next, either with syntax sugar (for a struct or for nested 
functions) or actually pass two real parameters with two registers:
```
function t :int64, u :int64 -> :int64 { t + u }
```
alternatively, split the function definition and its type:
```
function t, u { t + u }
:function t :int64, u :int64 -> r :int64
```
Note how you can also add a name (`r`) to the return value.

### Pipes is like C++, but not crazy and with fast compilation times

One of the reasons I started this project is that C++ is my less-hated language, but is still horrible.
When you program C++ you have to keep in mind millions of little and obscure rules. Some of them you
need to know if you want to write performant code, but others are just consequence of bad syntax.

Although inconsequential, the silliest example to me is the dereference operator `*`. How many times
you do something like accessing a field
`thing.elements`, which is a vector, and you want the first one, `thing.elements[0]`, which is a 
pointer, so you *have to go back to the beginning to add the `*`* in `*thing.elements[0]` 
and then go again to the end to continue the line. Also, are you sure the operator precedence is
what you think? Go google [the damned table](https://en.cppreference.com/w/cpp/language/operator_precedence)
and check if you need to `*(thing.elements[0])` or even `(*(thing.elements[0]))...` depending on what
goes next. The `->` operator, you say? sure, but you'll have to revert to `*` if you just want the
pointer content and not the inner fields. Some time ago I resorted to always do `thing.elements[0].operator*()...`
which is horrible too but to me looks more consistent.

I was also inspired by `auto`. My goal with Pipes is to make all the types `auto` implicitly and by
default, and the compiler would infer all the types, and complain when a type can not be inferred,
allowing the user to insert type annotations in any place when needed.
By contrast, in C++ you either have types or `auto`s all over the place, and they are mostly redundant because
the compiler complains when you write the wrong type.


With Pipes, I aimed quite low-level, to the point that the compiler outputs machine code (not even assembly),
but I have tried to keep syntax and compilation as simple as possible, to avoid needing to have
complex parsing or complex analysis. I can imagine that C++ is so slow compiling partly because the grammar is ambiguous,
and the compilation model (e.g. copy-pasting code with `#include`s) is outdated.

I also focused on generating the best assembly I could for lambdas. C++ takes a hit on performance if
you use many lambdas, especially by wrapping them in `std::function<>`s. Also, in C++ you can't pass
a lambda to a function that takes a function pointer. I didn't like this, but I think I now understand
that this separation is required to avoid making slower the use cases where only function pointers are needed.

However, C++ does have some good reasons to have slow compilation, like all the optimization
calculations. Of course, Pipes machine code can't compete with that of C++, as I can't mirror all the effort that
compiler programmers have put in making optimizing C++ compilers.
