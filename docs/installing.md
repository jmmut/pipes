
# Installing the Pipes compiler

At the moment the only way to get a working Pipes compiler is to compile it yourself.

You will need cmake and a c++ compiler:

```
git clone https://bitbucket.org/jmmut/pipes.git
cd pipes
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ../src
make -j
cd ..
```

and you should have the `pipes-compiler`, `pipes-loader` and `pipes-jit` under `build/bin/`.

## Installing Pipes Core library

Basically you just need to copy (or link) the folder `corelib/` into `~/.local/share/pipes/`.
You can use the trivial script `install_corelib.sh` in the root folder of this repo.

### Tests

You can run the tests compiled above by doing:

```
./runTests.sh build/
```
which will rerun cmake and recompile if needed. Some unit tests may fail, as they check features
that are partially implemented (TDD), but the integration tests should all pass.
