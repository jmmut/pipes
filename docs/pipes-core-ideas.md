
## Pipes core ideas

These are some of the reasons I wanted to create a language. Some of these ideas are not endorsed by barely any language I know.

### Data flow

The core idea is to make the language easy to understand and follow. In this way, the *things that get executed first, usually appear sooner*. This is of course a minor problem, and I expect this won't yield any real clarity improvement. It's a toy language.

The tipical counter-example is function call syntax:
```javascript
f(g(A))
```

This is used by most languages, and it means that first `g` is executed, and then `f` is executed on its result.

The Pipes way to do the same, is more similar to bash pipes:
```
A |g |f;
```

#### Tests

One place when this ordering issue shows, is in tests. A simple test for a function `increment`
would look like this:
```javascript
equals(increment(5), 6)
```
or, making BDD more explicit:
```javascript
input = 5                   // given
actual = increment(input)   // when
equals(actual, 6)           // then
```

In pipes the order is more natural:
```
5                       // given
|increment              // when
assert/int_equals 6     // then
```

### Mostly auto

I agree with C++ that most variables should be marked as `auto` type, and let the compiler infer and track the type.
Then, why require typing `auto` for the case that should be most common?

In Pipes you can mark the type of any value (the parameter `a` in the next example): `function(a :int64) {...}`, but most times it's not interesting and can be omitted: `function(a) {...}`.
However, this is not dynamic typing, where `a` can be anything and will fail at runtime if it can't be used. If there's no type that can make the program valid, the compilation will fail.

### Mostly const

Again, I agree with C++ that most variables should be `const`, so make that the implicit default.
It is not supported yet in Pipes, but the default (no annotations) will be const, and `mutable` will be needed for true "variables".

### Simple or no project structure definition

I agree with Java that it's very beneficial to have a convention for the structure of Java projects.
I disagree that languages to build a project should be Turing-complete.
I disagree with Python that the relative imports should depend on *the path where the interpreter is launched*.

In Pipes, the relative imports depend on *the path where the main program is*.

Right now, Pipes programs can be split in folders and there's no need for project structure definition, no flag for folders with headers, nothing.
A project definition might be needed in the future for dependencies, but right now you can download a dependency to a folder next to the main Pipes file, and it would work.

A fair point is that Pipes only links statically with non-incremental builds (always build everything), but there shouldn't be so many ways to build a project that you need turing completeness *on the user side*.
