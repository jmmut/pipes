
- [x] calls, functions, values
    - [x] use 2-argument function as operators (`arg1 operator_func arg2`)
    - [ ] prefix call (`|<`)
    - [ ] overload functions
    - [ ] concise notation?
        - `{$ +1}` same as `function x {x +1}`, like bash/perl, so maybe bad idea
        - or maybe `{+1}` for just a list of transformations where the input can only be at the beginning
- [x] closures and name shadowing
- [x] recursion
- [ ] variables
    - [ ] assignment overwriting (not creating) (`3 =>a`)
    - [ ] commutable unification (`3 =a` would be the same as `a =3`)
- [x] import
- [ ] tail recursion
- [/] branch
    - [ ] extra condition? (
            `value |branch $ cond/equals 4 {$} {$ +2}` or
            `value |branch v (v cond/equals 4) {v} {v + 2}`) or
            `value => v cond/equals 4 |branch {v} {v + 2}` just with variables
- [x] arithmetic +, -, |*, |/ (by design, multiplication `a |*b` has same precedence as addition.)
- [/] intrinsics
    - [x] current_stacktrace
    - [ ] caller code location
- [x] pipes std lib
- [/] use libc
    - [x] be able to call malloc from pipes runtime
    - [ ] be able to call libc functions that the compiler is not aware of
- [/] correct heap management
    - [x] free
    - [ ] deallocate stack variables
    - [ ] count news/frees
    - [ ] don't limit number of closures
- [/] syscalls
    - [x] print
    - [x] read
    - [ ] files
        - [ ] read
        - [ ] write
    - [ ] generic syscall intrinsic (so that pipes code can add support to new syscalls)
- [ ] docs about ModR/M, SIB and REX
- [/] optimize calls when not passed as arguments
    - [x] closureless identifiers
    - [ ] inline closureless lambdas
- [/] type checking
    - [x] explicit types
    - [x] inferred types
    - [x] types on intrinsic functions
    - [/] user defined types
        - [x] structs
        - [ ] traits
        - [/] type aliases
        - [x] tuples
            - [ ] empty tuples as parameter for 0-argument functions (prefix and postfix) and for
                the this pointer in tuples/classes
            - [ ] parenthesis-less tuples: `4 +5 ,(4 + 6) ,(4 +7) |func`
    - [ ] matching types 
        - [ ] 'function<T> x :T -> y :T'
        - [/] 'function x -> y :typeof(x)'
            - [x] typeof parameters (and virtual parameters defined in the same function) 
            - [ ] subtypeof 
            - [ ] supertypeof
    - [ ] Unresolved type (required for combinator Z)
- [/] lists
    - [x] constant size arrays
    - [ ] growing array
- [/] strings
    - [x] hack: strings as arrays of int64
    - [ ] proper strings
- [/] loops
    - [x] basic while loops
    - [ ] weird loops (condition inside the body)
    - [/] stream-like loops
        - [x] type keeping only for rewriting, can do new array if a different type is needed
    - [/] loops with counter (these are the only ones I miss when having only array iteration)
        - [x] emulate with functions (see loop/range.pipes)


Other pending issues

- Sometimes, with syntax errors, the most important code location is not
  mentioned.
- In some type checking errors, it's not clear what part of the type is wrong.
- why do I need the s.current type here?

```
pipes-jit-repl "[[4],[5],[6]]
array/mapreduce (1, function s :struct(current :array(:int64), accumulated) {
    s.current |array/front + s.accumulated
})"
```
