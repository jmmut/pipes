# Pipes quick introduction

See [Installing Pipes](installing.md) to get a working compiler and be able to try stuff in this page as you read this.

## Invoking the compiler

You should be able to run this (Just-In-Time compiler): `pipes-jit "3 |function x {x+1}"`,
which should give a result `4`.

Alternatively, to compile a file, put the code in a file with an extension ".pipes". Then call
`pipes-compiler your_file.pipes`, and then run it with `pipes-loader your_file 0`. The 0 is the program 
argument (integer) and is put into the identifier `argument`.

For example: put `argument + 1` in `arg_plus_1.pipes`, and then do 
`pipes-compiler arg_plus_1.pipes` and `pipes-loader arg_plus_1 3`, which should give 4.

## Basic syntax

Currently you can only use these atoms:

- 64-bit integer numbers (e.g. `4`, `-5`, `'a'` (syntax sugar for ASCII code 97))
- Functions (e.g. `function x { x }` (which is the anonymous identity function))
- Arrays (e.g. `[1, 2, 3]`, allocated in the heap)
- Identifiers (e.g. `my_parameter2`)
- Strings (e.g. "hello world" (syntax sugar for an array of ASCII ints))

With these operations:

- Binary operators `+`, `-`, `|*`, `|/`, `>`, (e.g. `2 + 3`)
- Parenthesize (e.g. `(2 + 3)`)
- Statement separator (e.g. `2; 3` (which evaluates to 3))
- Function call (e.g. `arg |func` (the usual syntax `func(arg)` is allowed but discouraged, see below))
- Looped function call (e.g. `[1, 2, 3] ||function x { x + 1 }` modifies the array by 
  incrementing all elements)
- Ifs/Branching (e.g. `condition |branch {5} {6}` (where 5 will be returned if `condition` is not 0, 6
  will be returned if `condition` is 0))
- Type annotation (e.g. `2 :int64`)
- Struct field access (e.g. given `a :struct(x :int64)`, you can do `a.x` or `a .x`. The space is 
  significant, those are different things, see below)
- Comments, ignoring the rest of the line (e.g. `42 // the answer`)

And some extra operations defined in the corelib library (see [how to install
the corelib](installing.md#installing-pipes-core-library), like:


- other comparison operators: `=?` (equals), `>=`, `<=`, `<`.
- array operations: `#` (get element 1-based), `array/size`, `++` (concatenate),
`array/print_ints`, and others.
- string operations: `print`, `string/read_line`, `string/print_lines`,
`string/read_lines`.
- int64 operations: `int64/print`, `int64/parse`, and others.
- generators: `loop/lazy`, `loop/range`.

The general structure of a Pipes program is:
```
value
operator value
operator value
...
```
where functions can also be values, and everything is left-associative so it's
the same as `((value) operator value) operator value`.
For example: `a |f |g` is the same as `(a |f) |g`, or `g(f(a))`

As classic example, this is a recursive fibonacci function (if written into a fibonacci.pipes file):
```
// fibonacci.pipes
function x {
    x =? 1
    |branch {
        1
    } {
        x =? 0
        |branch {
            1
        } {
            x -1 |fibonacci + (x -2 |fibonacci)
        }
    }
}
```

## Function definition

The next anonymous function takes a parameter `x` and returns its value incremented by 1:
```
function x { x + 1 }
```
Optionally, the parameter part can have parenthesis for disambiguating and/or readability:
```
function(x) { x + 1 }
```

You can restrict the types to do static type checking:
```
function(x :int64) { x + 1 }
```
You can add a return type and name (both optional on their own):
```
function(x :int64 -> x_plus_1 :int64) {
    x + 1
}
```

And take or return functions:
```
function(func :function(a :int64) -> decorated_func :function(b :int64)) {
    function(b) {
        b +1 |func
    }
}
```

## Named functions (files and namespaces)

To be able to reuse a function by name you have to put it in a file. For example, a file 
`plus_1.pipes` with the content of `function a {a +1}` could be used as `3 |plus_1`.

The function importing happens automatically.

Starting from the folder where the main program is located, subfolders define namespaces. For 
example, a file `main.pipes` with code `2 |my_namespace/plus_1` works if there's a file 
`my_namespace/plus_1.pipes`. Look at the folder `pipes_programs` for some examples. In 
particular, `pipes_programs/demos/` might be interesting.

## Calling several functions (associativity)

Pipes is designed to apply consecutive transformations. Everything is left-associative and has 
the same precedence. For example:
```
-1
|plus_1
|branch {20} {50}
+5
// result is 55. Same as `(((-1 |plus_1) |branch {20} {50}) + 5)`
```

The struct field access also follows this convention. While `3 |a.f` means `3 |(a.f)` because `a.f`
is handled like a single identifier, you can 
do left-associativity on a field access like `3 |a .f` which means `(3 |a) .f`, but you don't need
parenthesis for either usage.

The alternative way to call functions is the notation `f(a)` but it is discouraged because the
left-associativity is always maintained, so `3 |f(a)` means, perhaps surprisingly, `(3 |f)(a)`, or
`a |(3 |f)`, (or `a f 3`, see below on curried function).

## Loops

At the moment, only a simple looping method is implemented: operator `||` (loop cal).
This operator will apply the function to each element of the array/tuple/struct
*and create a new array*, potentially with a different type. It's equivalent to a functional
`map` operation.
```
[5, 6, 7]
||function x { x + 10 }
```
The previous code will return a new array `[15, 16, 17]`.

To loop an array overwriting its elements, use `=||`. The next code will overwrite the elements of
an array and will print the first (updated) element of the original array, which is 15:
```
[5, 6, 7]
|function (list) {
    list =|| function x { x + 10 }
    ; list #1
}
```


## Passing more (or fewer) than 1 argument

Pipes is focused on making transformations of a single value, so functions always take and return
1 value only.

When more arguments or returned values are needed, you can do 2 things:

### Structs, arrays, tuples

Internally, structs, arrays and tuples have the same memory layout, so their internal behaviour is
almost identical. Syntactically, arrays require all elements to have the same type and allow to 
have an arbitrary number of elements, and structs
can have elements with different types and allow access to fields by name.
Tuples are like heterogeneous arrays, or nameless structs (don't allow access by name).

So you can pass two values to a function like `(3, 4) |f`, which will create a tuple with 2 values,
and pass the tuple to `f`. This has the extra
cost of a dynamic allocation for the tuple. Tuples, arrays and structs need to be manually `free`d.

You can define a function that takes a struct and returns another one like:
```
function(pair :struct(x :int64, y :int64)) {
  (pair.x -1, pair.y +10)
}
```
which, if passed `(3, 4)`, will return a tuple `(2, 14)`.


### Curried functions

Some functional languages workaround the issue of several parameters by returning functions that will
take the next parameters. This also incurs in an extra memory allocation for the closure, and invocation
overhead, but has the benefit of allowing partial evaluation. A function for emulating addition could be:

```
// plus.pipes
function b {
    function a {
        a + b
    }
}
```
Note that the order of parameters is reversed from the call site: `a |(b |plus)`. This is because
in the call site, `b` is passed first, and the resulting function will take `a`.

An alternative notation for `a |(b |plus)` is `a plus b`, following the idea that `plus` is 
really meant to be used as a binary operator. This simplified notation is just syntax sugar for
the nested notation and can be used with any function.


## Intrinsics

There are some functions that needed to be implemented in the compiler, as otherwise there would be
no way to do that from Pipes code. The most important ones currently are:

### `new_array :function(size :int64 -> array :iterable)`
Allows creating an array even if the number of elements is only known at runtime. The resulting array
can not change size and needs to be manually deallocated with `free`. At the moment it's good
practice to explicitly mark the type of the resulting array, like `3 |new_array :array(:int64)` to
create an array of 3 integers.

### `free :function(a :iterable -> error_code :int64)`
Takes an array or struct or tuple and deallocates it. Further accesses are undefined behaviour.

### `read_char :function(unused :int64 -> char :int64)`
Reads a character from standard input and puts its ASCII code into an `int64`. The parameter is
unused for now.

### `print_char :function(char :int64 -> same_char :int64)`
Prints a character into standard output. Returns the same character, so that the function can be used
like an identity function with the side effect of printing the value that passes through it.

### `compile_time_print_type :function x -> typeof(x)`
Useful for debugging, prints (at compile time) the type of the previous expression that was inferred
by the compiler. Just like adding an explicit `:int64` in the middle of a program is how the user
tells the compiler the type of the expression at that point, putting `|compile_time_print_type` is
like the user asking the compiler which type the compiler thinks the previous expression is.

### `compile_time_track_type :function x -> :typeof(x)`
Similar to the previous one, prints the type of an expression at every iteration of the type
inference.

### `print_current_function :function x -> :typeof(x)`
Print the name of the function where print_current_function is called.

### `print_function_stack :function x -> :typeof(x)`
Print the function names where the current function is defined (at compile time).

### `print_call_stack :function x -> :typeof(x)`
Print the function names from where the current function was called (at run time).


### `deref :function`
Dereferences a pointer. Takes an array, tuple or struct (optionally increased by some offset) and
returns the contained element.

This is an unsafe function that is needed to access individual elements of an array without making
a loop (a loop with `||` will go over all elements). Using this function is discouraged; you
can use `array/get` and `array/size`. This function also loses information about types, so it makes
it harder for the compiler to infer properly the types involved after usages of `deref`.


## Corelib

Some basic utilities are available under the [corelib/](../corelib/) folder. For instance,
see [array/sort](../corelib/array/sort.pipes) for a basic version of quicksort.
