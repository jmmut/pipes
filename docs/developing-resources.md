# Development process and external resources

I plan to log here most interesting things I learn while developing this project. This will only be useful for people that wants to learn the same things I wanted to learn, hopefully helping them learn faster.

# Intel 64

To narrow the scope, I plan to output machine code only for the intel 64-bits architecture, which anyway looks like the most used instruction set in computer nowadays (citation needed, not including phones). AMD64 is similar to intel x86-64 but not identical and I'm not going to do any effort (for now) to support AMD64.

[This page](https://www.intel.com/content/dam/develop/external/us/en/documents/introduction-to-x64-assembly-181178.pdf) is a good starting point. It describes very nicely basic concepts about the processor architecture, the instructions, OS memory layout, calling conventions and has examples. Taken from there, this is the most explanatory image I have seen so far about the register architecture. Every time I look at it, I learn new things:  

![register structure](https://software.intel.com/content/dam/develop/external/us/en/images/29529-figure-1-181178.jpg)

Interesting sections in the [Intel 64 manuals](https://software.intel.com/content/www/us/en/develop/articles/intel-sdm.html), sorted by most introductory first, most advanced last:

##### Volume 1, chapter 1.3 "NOTATIONAL CONVENTIONS"
Very useful introductory definitions. Especially 1.3.1 "Bit and byte order" (explaining little-endianness with a nice explanatory figure),
and 1.3.2.1 "Instruction Operands" (saying that in this manual, in instructions with two operands, the right one is source and the left is the destination).

##### Volume 1, chapter 3.4 "Basic program execution registers"
Nice explanation of the registers and how they are actually extensions of smaller registers. Example: AL is the first byte of one of the registers, AH is the second byte, AX is those 2 bytes (a word, 16 bits), EAX is 4 bytes (a double word, 32 bits), and RAX is 8 bytes (a quadword, 64 bits).

In general, the 64 bit registers like RAX are only available in 64 bit mode, while the others can be used both in 64 and 32 bit modes, with some exceptions.

##### Volume 2, chapter 2.1 "Instruction format"
First image about the bit structure of the instruction set.

##### Volume 2, chapter 3.1.1.9  "Operation Section"
Explains the notation of the pseudo-code that illustrates the instructions explained in section 3.2

##### Volume 2, Appendix B "Instruction formats and encodings"
Some more information about how to encode in bits a given instruction, like bits "000" means register EAX (B.1.4.2
Reg Field (reg) for 64-Bit Mode), and so on.

##### Volume 1, chapter 6 "PROCEDURE CALLS, INTERRUPTS, AND EXCEPTIONS
Introduction to procedure call conventions. Covers the very basics, good as introduction. The whole chapter is quite short.
Introduction to the instructions call+ret / enter+leave, and how the stack works.

##### Volume 1, chapter 7 "PROGRAMMING WITH GENERAL-PURPOSE INSTRUCTIONS"
Specially 7.1 and 7.2 are a very concise description of available registers. 7.3 is a nice list of instructions grouped by topic.

##### Volume 1, chapter 2.1.5 "Addressing-Mode Encoding of ModR/M and SIB Bytes"
How the ModR/M byte works. This is used as operand in some instructions.

# C call convention

The C call convention is the state of the memory and registers that functions in C expect when being called by other functions or when called functions return. For instance, where the stack registers (rbp and rsp) are pointing relative to the call stack, or which general-purpose registers can be used without pushing in the stack (calle/caller saved).

- https://aaronbloomfield.github.io/pdr/book/x86-64bit-ccc-chapter.pdf
- http://unixwiz.net/techtips/win32-callconv-asm.html

# Syscalls

Syscalls are basically functions that only the OS can implement if you are running inside an OS.
Things like opening a file, writing to stdout, allocating memory and so on.

- http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
- https://opensource.apple.com/source/xnu/xnu-3789.41.3/bsd/kern/syscalls.master.auto.html

# Language design

## Functional

At the beginning I wasn't planning making the language very functional-ish, but I learnt some things about Haskell and lambda-calculus that made me focus a little bit on making proper lambdas, unlike the lambdas in C++ and Java, the languages I have the most experience with.

[Here](https://www.youtube.com/watch?v=3VQ382QG-y4) is a fun talk about combinators in lambda-calculus, with live examples in javascript. (Found [here](https://risingentropy.com/for-loops-and-bounded-quantifiers-in-lambda-calculus/)).
