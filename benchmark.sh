#!/bin/bash

# ./benchmark.sh "g++ cpp_programs/fib_recursive.cpp -o cpp_programs/fib_recursive" "./cpp_programs/fib_recursive"
# ./benchmark.sh "g++ -O3 cpp_programs/fib_recursive.cpp -o cpp_programs/fib_recursive" "./cpp_programs/fib_recursive"
# ./benchmark.sh "g++ -O2 cpp_programs/fib_recursive.cpp -o cpp_programs/fib_recursive" "./cpp_programs/fib_recursive"
# ./benchmark.sh "g++ -O1 cpp_programs/fib_recursive.cpp -o cpp_programs/fib_recursive" "./cpp_programs/fib_recursive"
# ./benchmark.sh "g++ -O0 cpp_programs/fib_recursive.cpp -o cpp_programs/fib_recursive" "./cpp_programs/fib_recursive"
# ./benchmark.sh "g++ -g cpp_programs/fib_recursive.cpp -o cpp_programs/fib_recursive" "./cpp_programs/fib_recursive"
# ./benchmark.sh "gcc -O3 cpp_programs/fib_recursive.c -o cpp_programs/fib_recursive_c" "./cpp_programs/fib_recursive_c"
# ../benchmark.sh "/home/jmmut/.jdks/openjdk-16/bin/javac fib_recursive.java" "java fib_recursive"
# ./benchmark.sh "./build-make/bin/pipes-compiler pipes_programs/benchmarks/fib_recursive_main.pipes" "./build-make/bin/pipes-loader pipes_programs/benchmarks/fib_recursive_main"
# ./benchmark.sh "sleep 1" "python3 python_programs/fib_recursive.py"


set -euo pipefail
#set -x

compile=$1
run=$2


echo -n "compiling: "
(time $compile) 2>&1 | grep real | sed -e 's/.*m//' | sed -e 's/s//'

#inputs="0 5 10 20 25 30 35 38 40"
inputs="0 5 10 20 25 30 35 38 40 42 43 44 45"
echo -e "\nrunning: "
for i in $inputs
do
	echo -e "$((time $run $i) 2>&1 | grep real | sed -e 's/.*m//' | sed -e 's/s//')"
done

